install(FILES
boundarytypes.hh
connectivitymap.hh
elementvolumevariables.hh
facevariables.hh
fickslaw.hh
fourierslaw.hh
fvgridgeometrytraits.hh
gridvolumevariables.hh
maxwellstefanslaw.hh
properties.hh
staggeredgeometryhelper.hh
subcontrolvolumeface.hh
velocityoutput.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/discretization/staggered/freeflow)
