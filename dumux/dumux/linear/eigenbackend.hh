/*!
 * \file
 * \ingroup Linear
 * \brief Provides a parallel linear solver based on the ISTL AMG preconditioner
 *        and the ISTL BiCGSTAB solver.
 */
#ifndef DUMUX_EIGENBACKEND_HH
#define DUMUX_EIGENBACKEND_HH

#include "../../../eigen/Eigen/Sparse"


namespace Dumux {

//typedef Eigen::SparseMatrix<double, Eigen::ColMajor> SparseMatrix;
//typedef Eigen::Triplet<double> Triplet;


} // namespace Dumux

#endif // DUMUX_EIGENBACKEND_HH