// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 * \brief Implementation of a regularized version of the Brooks-Corey
 *        capillary pressure / relative permeability  <-> saturation relation.
 */
#ifndef REGULARIZED_BROOKS_COREY_HH
#define REGULARIZED_BROOKS_COREY_HH

#include "brookscorey.hh"
#include "regularizedbrookscoreyparams.hh"



#include <dumux/common/spline.hh>

namespace Dumux
{
/*!
 * \ingroup Fluidmatrixinteractions
 * \brief Implementation of the regularized  Brooks-Corey
 *        capillary pressure / relative permeability  <-> saturation relation.
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values are "regularized".
 *        This means that in stead of following the curve of the material law in these regions, some linear approximation is used.
 *        Doing this is not worse than following the material law. E.g. for very low wetting phase values the material
 *        laws predict infinite values for \f$\mathrm{p_c}\f$ which is completely unphysical. In case of very high wetting phase
 *        saturations the difference between regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *         For an example figure of the regularization: RegularizedVanGenuchten
 *
 * \see BrooksCorey
 */
template <class ScalarT, class ParamsT = RegularizedBrooksCoreyParams<ScalarT> >
class MyBrooksCorey
{
    using BrooksCorey = Dumux::BrooksCorey<ScalarT, ParamsT>;

public:
    using Params = ParamsT;
    using Scalar = typename Params::Scalar;
    
    static constexpr Scalar p[5] = {24.2050, -65.8584, 61.5392, -24.3534, 3.9684};   // in bars

    /*!
     * \brief Polynomially fitted capillary pressure from Graue SPE56672 
     * The fit is based on the following data:
     * Sw = [0.15	0.2	0.25	0.35	0.4	0.5	0.6	0.7	0.7297];
     * Swe = (Sw-0.15)/(-0.15+0.7297); 
     * (for ROCK.Swres = 0.15, 1-ROCK.Sores = 0.7297)
     * Pc= [4.136856	1.9305328	1.378952	0.689476	0.5515808	0.2068428  0.0689476	0	-0.7584236]; % in bars
     * p = polyfit(Swe, Pc, 4);
     * f = polyval(p, Sw);
     * 
     */
    
    static Scalar pc(const Params &params, Scalar swe)
    {
               
        Scalar pc_ =  0.;
        if (params.pe() > 0) pc_ = (p[0]*pow(swe, 4) + p[1]*pow(swe, 3) + p[2]*pow(swe, 2) + p[3]*swe + p[4]) * 1e5;
        
        return pc_;

    }

   
    /*!
     * \brief Derivative of the polynomially fitted capillary pressure from Graue SPE56672 
     *  w.r.t. effective saturation
     */
    static Scalar dpc_dswe(const Params &params, Scalar swe)
    {
        
        Scalar dpcdswe = 0.;
        if (params.pe() > 0) dpcdswe = (4.*p[0]*pow(swe, 3) + 3.*p[1]*pow(swe, 2) + 2.*p[2]*pow(swe, 1) + p[3]) * 1e5;
        
        return dpcdswe;
        
    }

   

    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the wetting phase of
     *          the medium implied by the Brooks-Corey
     *          parameterization.
     *
     *  regularized part:
     *    - below \f$\mathrm{\overline{S}_w =0}\f$:                  set relative permeability to zero
     *    - above \f$\mathrm{\overline{S}_w =1}\f$:                  set relative permeability to one
     *    - between \f$\mathrm{ 0.95 \leq \overline{S}_w \leq 1}\f$:  use a spline as interpolation
     *
     *  For not-regularized part:
        \copydetails BrooksCorey::krw()
     */
    static Scalar krw(const Params &params, Scalar swe)
    {
        if (swe <= 0.0)
            return 0.0;
        else if (swe >= 1.0)
            return 1.0;

        return BrooksCorey::krw(params, swe);
    }

    /*!
     * \brief   Regularized version of the  relative permeability
     *          for the non-wetting phase of
     *          the medium implied by the Brooks-Corey
     *          parameterization.
     *
     * regularized part:
     *    - below \f$\mathrm{\overline{S}_w =0}\f$:                  set relative permeability to zero
     *    - above \f$\mathrm{\overline{S}_w =1}\f$:                  set relative permeability to one
     *    - for \f$\mathrm{0 \leq \overline{S}_w \leq 0.05}\f$:     use a spline as interpolation
     *
     * \copydetails BrooksCorey::krn()
     *
     */
    static Scalar krn(const Params &params, Scalar swe)
    {
        if (swe >= 1.0)
            return 0.0;
        else if (swe <= 0.0)
            return 1.0;

        return BrooksCorey::krn(params, swe);
    }
};
}

#endif
