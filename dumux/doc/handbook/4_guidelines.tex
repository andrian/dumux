\section{Coding Guidelines}
\label{sc_guidelines}
Writing code in a readable manner is very important, especially
for future code developers (e.g. for adding features, debugging, etc.).
This section is inspired by the DUNE coding guidelines
\url{https://www.dune-project.org/dev/codingstyle/}, which is strongly recommended.

\subsection{Documentation:}
Please document freely what each part of your code \emph{should} do. All comments/documentation
is in English.
We proclaim the Doc-Me Dogma, which means whatever you do, please document
it at least with:
\begin{lstlisting}[style=DumuxCode]
  \todo Please doc me!
\end{lstlisting}
That way at least the absence of documentation is documented, and it is easier
to get rid of it systematically.
In the following we will describe several ways for
commenting code in \Dumux.\\
There are four ways to comment a \textbf{single line} of code:
\begin{lstlisting}[style=DumuxCode]
Line of code 1 // Short comment on line of code 1 that will not show in doxygen
Line of code 2 //!< Short comment on line of code 2 that will show in doxygen
//! Short comment on the following line (line of code 3) that will show in doxygen
Line of code 3
/*!
 * Longer comment on line of code 4
 * with several lines of length
 * that will show in doxygen
 */
Line of code 4
\end{lstlisting}
The third and fourth of the above shown options can also be used to document functions with
neither method parameters nor return value.\\
For doxygen to be able to group the \textbf{files} correctly, please add before the headerguard:
\begin{lstlisting}[style=DumuxCode]
/*!
 * \file
 * \ingroup GroupName
 * \brief A short description of the file.
 */

#ifndef HEADERGUARD
#define HEADERGUARD
\end{lstlisting}
For doxygen to be able to group the \textbf{classes} correctly, please add before each class:
\begin{lstlisting}[style=DumuxCode]
/*!
 * \ingroup GroupName
 * \brief A short description of the class.
 *
 * Optional detailed description of the class
 * over several lines.
 */
template <class TypeTag>
\end{lstlisting}
For an overview over all available groups and to add new groups please
see the file\\ \texttt{doc/doxygen/modules.txt} from the \texttt{dumux} module.\\
Please add before each \textbf{function} with method parameters or return value:
\begin{lstlisting}[style=DumuxCode]
/*!
 * \brief A short description of the function.
 *
 * Optional detailed description of the function
 * over several lines.
 *
 * \param paramName Very short description of paramName
 * \return returnName Very short description of returnName
 */
paramType functionName(paramType paramName)
{
   ...
   return returnName
}
\end{lstlisting}
Furthermore, please comment \textit{all}:
\begin{itemize}
  \item Template Parameters
  \item Exceptions thrown by a method
  \item Method parameters which are not self-explanatory should always
        have a meaningful comment at calling sites. Example:
  \begin{lstlisting}[style=DumuxCode]
    localResidual.eval(/*includeBoundaries=*/true);
  \end{lstlisting}
\end{itemize}

\subsection{Naming:}
To avoid duplicated types or variables and for a better understanding and usability
of the code we have the following naming conventions:
\begin{itemize}
\item \textbf{Variables/Functions \ldots}
  \begin{itemize}
  \item start in \emph{lower} case and contain letters.
  \item \emph{CamelCase}: if the name consists of  several words, then
        the first letter of a new word is capital.
  \item \emph{Self-Explaining}: in general abbreviations should be avoided (write saturation in stead of S)
  \item \emph{Abbreviations}: If and only if a single letter that represents an
         abbreviation or index is followed by a single letter (abbreviation or index),
         CamelCase is \textbf{not} used. An inner-word underscore is only permitted if
         it symbolizes a fraction line. Afterwards, we continue with lower case, i.e.
         the common rules apply to both enumerator and denominator. Examples:
  \begin{itemize}
      \item \texttt{pw} but \texttt{pressureW} $\rightarrow$ because ``pressure'' is a word.
      \item \texttt{srnw} but \texttt{sReg} $\rightarrow$ because ``reg'' is not an
            abbreviation of a single letter. ``n'' abbreviates ``non'',
             ``w'' is ``wetting'', so not CamelCase.
      \item \texttt{pcgw} but \texttt{dTauDPi} $\rightarrow$ Both ``Tau'' and ``Pi''
            are words, plus longer than a letter.
      \item \textbf{But:} \texttt{CaCO3} The only exception: chemical formulas are
            written in their chemically correct way $\rightarrow$ \texttt{CaCO3}
  \end{itemize}
  \end{itemize}
\item \textbf{Private Data Variables:} Names of private data variables end with an
      underscore and are the only variables that contain underscores.
\item \textbf{Type names:} For type names, the same rules as for variables apply. The
      only difference is that the \emph{first letter is capital}.
\item \textbf{Files:} File names should consist of \emph{lower case} letters
      exclusively. Header files get the suffix \texttt{.hh}, implementation files the
      suffix \texttt{.cc}
\item \textbf{The Exclusive-Access Macro:} Every header file traditionally begins
      with the definition of a preprocessor constant that is used to make sure that
      each  header file is only included once. If your header file is called
      'myheaderfile.hh', this constant should be DUMUX\_MYHEADERFILE\_HH.
\item \textbf{Macros:} The use of preprocessor macros is strongly discouraged. If you
      have to use them for whatever reason, please use capital letters only.
\end{itemize}
