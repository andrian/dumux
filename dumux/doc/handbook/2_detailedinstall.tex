\section{Detailed Installation Instructions}
\label{install}

Installing \Dumux means that you first unpack \Dune and \Dumux in a root directory,
(section \ref{sc:ObtainingSourceCode}).
In a second step of the installation, all modules are configured with CMake
(section \ref{buildIt}).
After successfull installation of \Dumux we guide you to start a test application,
described in section \ref{quick-start-guide}.
In section \ref{sec:build-doxy-doc} we explain how to build the \Dumux documentation.
Lastly, section \ref{sec:external-modules-libraries} provides details on optional libraries and modules.

In a technical sense \Dumux is a module of \Dune.
Thus, the installation procedure of \Dumux is the same as that of \Dune.
Details regarding the installation of \Dune are provided on the \Dune website \cite{DUNE-HP}.


\subsection{Obtaining Source Code for \Dune and \Dumux}
\label{sc:ObtainingSourceCode}
The \Dumux release and trunk (developer tree) are based on the most recent
\Dune release 2.5, comprising the core modules dune-common, dune-geometry, dune-grid,
dune-istl and dune-localfunctions. For working with \Dumux, these modules are required.
All \Dune modules, including the \Dumux module, get extracted into a common root directory, as it
is done in an ordinary \Dune installation.
We usually name our root directory \texttt{DUMUX} but an arbitrary name can be chosen.
Source code files for each \Dune module are contained in their own subdirectory within the root directory.
The subdirectories for the modules are named after the module names (depending on how
the modules were obtained a version number is added to the module name).
The name of each \Dune module is defined in the file \texttt{dune.module}, which is
in the root directory of the respective module. This should not be changed by the user.

Two possibilities exist to get the source code of \Dune and \Dumux.
Firstly, \Dune and \Dumux can be downloaded as tar files from the respective \Dune and \Dumux website.
They have to be extracted as described in the next paragraph.
% TODO: alpha version was not released with a tarball. For the next releases the following lines need to be deleted again
There is no tar file for the current \DumuxVersion~release.
Secondly, a method to obtain the most recent source code (or, more generally, any of its previous revisions) by direct access
to the software repositories of the revision control system is described in the subsequent part.
Be aware that you cannot get \texttt{dumux-devel} or the external libraries from \texttt{dumux-external} unless
you have an GitLab account with the right privileges.

In section \ref{sec:prerequisites} we list some prerequisites for running \Dune and \Dumux.
Please check in said paragraph whether you can fulfill them before continuing.

% TODO: alpha version was not released with a tarball. For the next releases the following lines need to be uncommented again
% \paragraph{Obtaining the software by installing tar files}
% The slightly old-fashionedly named tape-archive-file, shortly named tar file or
% tarball, is a common file format for distributing collections of files contained
% within these archives.
% The extraction from the tar files is done as follows:
% Download the tarballs from the respective \Dune (version 2.5) and \Dumux websites
% to a certain folder in your file system.
% Create the common root directory, named \texttt{DUMUX} in the example below.
% Then extract the content of the tar files, e.\,g. with the command-line program
% \texttt{tar}.
% This can be achieved by the following shell commands. Replace \texttt{path\_to\_tarball}
% with the directory name where the downloaded files are actually located.
% After extraction, the actual name of the dumux subdirectory is \texttt{dumux-\DumuxVersion}
% (or whatever version you downloaded).
%
% \begin{lstlisting}[style=Bash]
% $ mkdir DUMUX
% $ cd DUMUX
% $ tar xzvf path_to_tarball_of/dune-common-2.5.0.tar.gz
% $ tar xzvf path_to_tarball_of/dune-geometry-2.5.0.tar.gz
% $ tar xzvf path_to_tarball_of/dune-grid-2.5.0.tar.gz
% $ tar xzvf path_to_tarball_of/dune-istl-2.5.0.tar.gz
% $ tar xzvf path_to_tarball_of/dune-localfunctions-2.5.0.tar.gz
% $ tar xzvf path_to_tarball_of/dumux-3.0-alpha.tar.gz
% \end{lstlisting}
%
% Furthermore, if you wish to install the optional \Dune Grid-Howto which provides a tutorial
% on the Dune grid interface, act similar.

\paragraph{Obtaining \Dune and \Dumux from software repositories}
Direct access to a software revision control system for downloading code can be of advantage later on.
It is easier to keep up with code changes and to receive important bug fixes.
\Dune and \Dumux use Git for their software repositories. To access them a Git client is needed.

In the technical language of Git, \emph{cloning a certain software version} means nothing more then fetching
a local copy from the software repository and laying it out in the file system.
In addition to the software, some more files for the use of the software revision
control system itself are created. If you have developer access to \Dumux, it is
also possible to do the opposite, i.\,e. to load up a modified revision of software
into the software repository. This is usually termed as \emph{commit} and \emph{push}.

The installation procedure is done as follows:
Create a common root directory, named e.g. \texttt{DUMUX} in the lines below.
Then, enter the previously created directory and check out the desired modules.
As you see below, the check-out uses two different servers for getting the sources,
one for \Dune and one for \Dumux.

\begin{lstlisting}[style=Bash]
$ mkdir DUMUX
$ cd DUMUX
$ git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-common.git
$ git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-geometry.git
$ git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-grid.git
$ git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-istl.git
$ git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-localfunctions.git
$ git clone -b 3.0.0-alpha https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
\end{lstlisting}

The newest and maybe unstable developments of \Dune and \Dumux are also provided in these repositories and can be found in the \emph{master} branch.
Please check the \Dune website \cite{DUNE-HP} for further information on the \Dune development. We always try to keep up with the latest developments of \Dune.
However, the current \Dumux release is based on the stable 2.5 release and it might not compile without further adaptations using the newest versions of \Dune.

Furthermore, if you wish to install the optional \Dune Grid-Howto which provides a tutorial
on the Dune grid interface, act similar.

\paragraph{Patching \Dune or external libraries}
\label{sc:patchingDUNE}
Patching of \Dune modules in order to work together with \Dumux can be necessary for several reasons.
Software like a compiler or even a standard library
changes at times. But, for example, a certain release of a software component that we depend on,
may not reflect that change and thus it has to be modified.
In the dynamic developing process of software which depends on other modules it is not always feasible
to adapt everything to the most recent version of each module. They may fix problems with a certain module
of a certain release without introducing too much structural change.

\Dumux contains patches and documentation about their usage and application within the
directory \texttt{dumux/patches}.
Please check the README file in that directory for recent information.
In general, a patch can be applied as follows
(the exact command or the used parameters may be slightly different).
We include here an example of a patching dune-grid.

\begin{lstlisting}[style=Bash]
$ # make sure you are in the common root directory
$ cd dune-grid
$ patch -p0 < ../dumux/patches/grid-2.3.1.patch
\end{lstlisting}

It can be removed by
\begin{lstlisting}[style=Bash]
$ path -p0 -R < ../dumux/patches/grid-2.3.1.patch
\end{lstlisting}

\paragraph{Hints for \Dumux-Developers}
If you also want to actively participate in the development of \Dumux, you can allways send patches
to the Mailing list.
To get more involved, you can apply either for full developer
access or for developer access on certain parts of \Dumux. Granted developer access means that
you are allowed to commit own code and that you can access the \texttt{dumux-devel} module.
This enhances \texttt{dumux} by providing maybe unstable code from the developer group.

\subsection{Build of \Dune and \Dumux}
\label{buildIt}
Configuring \Dune and \Dumux is done by the shell-command \texttt{dunecontrol} which is part of the \Dune build system.
If you are interested in more details about the build system that is used,
they can be found in the \Dune buildsystem documentation\footnote{\url{https://www.dune-project.org/buildsystem/}} and
CMake's documentation\footnote{\url{https://cmake.org/documentation/}}.
If something fails during the execution of \texttt{dunecontrol} feel free to report it to the \Dune or \Dumux developer mailing list,
but also try to include error details.

It is possible to compile \Dumux with nearly no explicit options to the build system.
However, for the successful compilation of \Dune and \Dumux, it is currently necessary to pass
the option \texttt{-fno-strict-aliasing} to the \Cplusplus compiler,
which is done here via a command-line argument to \texttt{dunecontrol}:
\begin{lstlisting}[style=Bash]
$ # make sure you are in the common root directory
$ ./dune-common/bin/dunecontrol --configure-opts="CXXFLAGS=-fno-strict-aliasing" --use-cmake all
\end{lstlisting}

Too many options can make life hard. That's why usually option files are being used together with \texttt{dunecontrol} and its sub-tools.
Larger sets of options are kept in them. If you are going to compile with options suited for debugging the code, the following
can be a starting point:
\begin{lstlisting}[style=Bash]
$ # make sure you are in the common root directory
$ cp dumux/debug.opts my-debug.opts      # create a personal version
$ gedit my-debug.opts                    # optional editing the options file
$ ./dune-common/bin/dunecontrol --opts=my-debug.opts --use-cmake all
\end{lstlisting}

More optimized code, which is typically not usable for standard debugging tasks, can be produced by
\begin{lstlisting}[style=Bash]
$ cp dumux/optim.opts my-optim.opts
$ ./dune-common/bin/dunecontrol --opts=my-optim.opts --use-cmake all
\end{lstlisting}

Sometimes it is necessary to have additional options which
are specific to a package set of an operating system or
sometimes you have your own preferences.
Feel free to work with your own set of options, which may evolve over time.
The option files above are to be understood more as a starting point
for setting up an own customization than as something which is fixed.
The use of external libraries can make it necessary to add quite many options in an option file.
It can be helpful to give your customized option file its own name, as done above,
to avoid confusing it with the option files which came out of the distribution.

\subsection{The First Run of a Test Application}
\label{quick-start-guide}
The previous section showed how to install and compile \Dumux. This chapter
shall give a very brief introduction how to run a first test application and how
to visualize the first output files. A more detailed explanations can be found in
the tutorials in the following chapter.\\
All executables are compiled in the \texttt{build} subdirectories of \Dumux.
If not given differently in the input files, this is \texttt{build-cmake} as default.

\begin{enumerate}
\item Go to the directory \texttt{build-cmake/test}. There, various test application
      folders can be found. Let us consider as example\\
      \texttt{porousmediumflow/2p/implicit/incompressible/test{\_}2p{\_}incompressible{\_}tpfa}.
\item Enter the folder \texttt{porousmediumflow/2p/implicit/incompressible}.\\ Type \texttt{make test{\_}2p{\_}incompressible{\_}tpfa} 
      in order to compile the application\\ \texttt{test{\_}2p{\_}incompressible{\_}tpfa}. To run the simulation, 
      type \texttt{./test{\_}2p{\_}incompressible{\_}tpfa}
      into the console. If you explicitly want to state a parameter file, type\\ 
      \texttt{./test{\_}2p{\_}incompressible{\_}tpfa test\_2p.input}.
      Adding \texttt{test\_2p.input} specifies that all
      important parameters (like first timestep size, end of simulation and location
      of the grid file) can be found in a text file in the same directory  with the
      name \texttt{test\_2p.input}.
\item The simulation starts and produces some .vtu output files and also a .pvd
      file. The .pvd file can be used to examine time series and summarizes the .vtu
      files. It is possible to stop a running application by pressing $<$Ctrl$><$c$>$.
\item You can display the results using the visualization tool ParaView (or
      alternatively VisIt). Just type \texttt{paraview} in the console and open the
      .pvd file. On the left hand side, you can choose the desired parameter to be displayed.
\end{enumerate}

\subsection{Building Documentation}
\subsubsection{Doxygen}
\label{sec:build-doxy-doc}
Doxygen documentation is done by especially formatted comments integrated in the source code,
which can get extracted by the program \texttt{doxygen}. Beside extracting these comments,
\texttt{doxygen} builds up a web-browsable code structure documentation
like class hierarchy of code displayed as graphs, see \url{http://www.stack.nl/~dimitri/doxygen/}.

The Doxygen documentation of a module can be built, if \texttt{doxygen} is installed,
by running \texttt{dunecontrol}, entering the \texttt{build-*}directory, and execute
\texttt{make doc}. Then point your web browser to the file
\texttt{MODULE\_BUILD\_DIRECTORY/doc/doxygen/html/index.html} to read the generated documentation.
This should also work for other \Dune modules.

\subsubsection{Handbook}
To build the \Dumux handbook go into the \texttt{build-}directory and
run \texttt{make doc} or \texttt{make 0\_dumux-handbook\_pdf}. The pdf can then be found
in \texttt{MODULE\_BUILD\_DIRECTORY/doc/handbook/0\_dumux-handbook.pdf}.

\subsection{External Libraries and Modules} \label{sec:external-modules-libraries}
The libraries described below provide additional functionality but are not generally required to run \Dumux.
If you are going to use an external library check the information provided on the \Dune website%
\footnote{DUNE: External libraries, \url{https://www.dune-project.org/doc/external-libraries/}}.
If you are going to use an external \Dune module the website on external modules%
\footnote{DUNE: External modules, \url{https://www.dune-project.org/groups/external/}}
can be helpful.

Installing an external library can require additional libraries which are also used by \Dune.
For some libraries, such as BLAS or MPI, multiple versions can be installed on the system.
Make sure that it uses the same library as \Dune when configuring the external library.

Some of the libraries are then compiled within that directory and are not installed in
a different place, but \Dune may need to know their location. Thus, one may have to refer to
them as options for \texttt{dunecontrol}, for example via the options file \texttt{my-debug.opts}.
Make sure you compile the required external libraries before you run \texttt{dunecontrol}.

An easy way to install some of the libraries and modules given below is the
\texttt{installexternal.sh} script located in \texttt{bin}. The script
has to be called from your common root directory.


\subsubsection{List of External Libraries and Modules}
In the following list, you can find some external modules and external libraries,
and some more libraries and tools which are prerequisites for their use.

\begin{itemize}
\item \textbf{dune-ALUGrid}: Grid library, comes as a \Dune module.
  The parallel version needs also a graph partitioner, such as {ParMETIS}.
  Download: \url{https://gitlab.dune-project.org/extensions/dune-alugrid}

\item \textbf{dune-foamgrid}: External grid module. One- and two-dimensional grids
  in a physical space of arbitrary dimension; non-manifold grids, growth, element
  paramterizations, and movable vertices. This makes FoamGrid the grid data structure
  of choice for simulating structures such as foams, discrete fracture networks,
  or network flow problems.
  Download: \url{https://gitlab.dune-project.org/extensions/dune-foamgrid}

\item \textbf{SuperLU}: External library for solving linear equations. SuperLU is a general purpose
  library for the direct solution of large, sparse, non-symmetric systems of linear equations.
  Download: \url{http://crd.lbl.gov/~xiaoye/SuperLU}

\item \textbf{UMFPack}: External library for solving linear equeations. It is part of SuiteSparse.

\item \textbf{dune-UG}: External library for use as grid. UG is a toolbox for unstructured grids, released under GPL.
  To build UG the tools \texttt{lex}/\texttt{yacc} or the GNU variants of \texttt{flex}/\texttt{bison} must be provided.
  Download: \url{https://gitlab.dune-project.org/staging/dune-uggrid}
\end{itemize}

The following are dependencies of some of the used libraries. You will need them
depending on which modules of \Dune and which external libraries you use.

\begin{itemize}
\item \textbf{MPI}: The parallel version of \Dune and also some of the external dependencies need MPI
  when they are going to be built for parallel computing. \texttt{OpenMPI} and \texttt{MPICH} in a recent
  version have been reported to work.

\item \textbf{BLAS}: SuperLU makes use of BLAS. Thus install GotoBLAS2, ATLAS, non-optimized BLAS
  or BLAS provided by a chip manufacturer. Take care that the installation scripts select the intended
  version of BLAS.

\item \textbf{METIS} and \textbf{ParMETIS}: This are dependencies of ALUGrid and can be used with UG, if run in parallel.

\item \textbf{Compilers}: Beside \texttt{g++}, \Dune can be built with Clang from the LLVM project and
  Intel \Cplusplus compiler. C and Fortran compilers are needed for some external libraries. As code of
  different compilers is linked together they have to be be compatible with each other.
\end{itemize}
