\section{Developing \Dumux}
\label{sc_developingdumux}

\subsection{Communicate with \Dumux Developers}

\paragraph{Issues and Bug Tracking}
The bug-tracking system \emph{GitLab Issues} offers the possibility to report bugs or discuss new development requests.
Feel free to register (if you don't have a \emph{Git} account already) and to constribute
at \url{https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/issues}.

\paragraph{Commits, Merges, etc.}
To be up-to-date with the latest changes made to any git-repository you can use RSS Feeds.
Simply click on \emph{Issues} or \emph{Activity} and then select a tab you are interested in
and use your favorite RSS-application for receiving the news.

\paragraph{Automatic Testing Dashboard}
The automatic testing using \emph{BuildBot} helps to constantly check the
\Dumux problems for compiling and running correctly. It is available at
\url{https://git.iws.uni-stuttgart.de/buildbot/#/builders}.

\paragraph{The General Mailing List:}
If you have questions, specific problems (which you really struggle to solve on your own),
or hints for the \Dumux-developers, please contact the mailing list \url{dumux@iws.uni-stuttgart.de}.
You can subscribe to the mailing list via
\url{https://listserv.uni-stuttgart.de/mailman/listinfo/dumux}, then you
will be informed about upcoming releases or events.

\subsection{Tips and Tricks}
\Dumux users and developers at the LH2 are also referred to the internal Wiki for
more information.

\paragraph{Option Files optim.opts and debug.opts}
\Dune and \Dumux are built with the help of \texttt{dunecontrol}, as explained on page \pageref{buildIt}.
The options needed to be specified for that are provided using option files like
\texttt{debug.opts} and \texttt{optim.opts}. These two compile \Dune and \Dumux
either for debugging or for fast simulation. Programs compiled with optimization options
can lead to a speedup of factor up to ten!\\
In contrast programs that are compiled with optimization can hardly be debugged.
You can modify the files and change the compiler, the name of the build director,
add third-party dependencies, add additional compiler flags, ... .
\begin{lstlisting}[style=Shell]
BUILDDIR=build-clang
CMAKE_FLAGS="\
  -DCMAKE_C_COMPILER=/usr/bin/clang \
  -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
  -DUG_DIR=./externals/ug-3.12.1"
\end{lstlisting}

\paragraph{Dunecontrol for selected modules}
A complete build using \texttt{dunecontrol} takes some time. In many cases not all modules need to be re-built.
Pass the flag \texttt{--only=dumux} to \texttt{dunecontrol} for configuring or building only \Dumux. A more
complex example would be the use of an additional module. Then you have to configure and build only \Dune{}-grid
and \Dumux by adding \texttt{--only=MODULE,dumux}.

\paragraph{Patching Files or Modules}
If you want to send changes to an other developer of \Dumux providing patches
can be quite smart. To create a patch simply type:
\begin{lstlisting}[style=Bash]
$ git diff > PATCHFILE
\end{lstlisting}
\noindent which creates a text file containing all your changes to the files
in the current folder or its subdirectories.
To apply a patch in the same directory type:
\begin{lstlisting}[style=Bash]
$ patch -p1 < PATCHFILE
\end{lstlisting}
See \ref{sc:patchingDUNE} if you need to apply patches to \Dumux or \Dune.

\paragraph{File Name and Line Number by Predefined Macro}
If you want to  know where some output or debug information came from, use the predefined
macros \texttt{\_\_FILE\_\_} and \texttt{\_\_LINE\_\_}:
\begin{lstlisting}[style=DumuxCode]
std::cout << "# This was written from "<< __FILE__ << ", line " << __LINE__ << std::endl;
\end{lstlisting}

\paragraph{Using \Dune Debug Streams}
\Dune provides a helpful feature, for keeping your debug-output organized.
It uses simple streams like \texttt{std::cout}, but they can be switched on and off
for the whole project. You can chose five different levels of severity:
\begin{verbatim}
5 - grave (dgrave)
4 - warning (dwarn)
3 - info (dinfo)
2 - verbose (dverb)
1 - very verbose (dvverb)
\end{verbatim}
\noindent They are used as follows:
\begin{lstlisting}[style=DumuxCode]
// define the minimal debug level somewhere in your code
#define DUNE_MINIMAL_DEBUG_LEVEL 4
Dune::dgrave << "message"; // will be printed
Dune::dwarn << "message"; // will be printed
Dune::dinfo << "message"; // will NOT be printed
\end{lstlisting}

\paragraph{Make headercheck:}
To check one header file for all necessary includes to compile the contained code, use \texttt{make headercheck}.
Include the option \texttt{-DENABLE\_HEADERCHECK=1} in your opts file and run \texttt{dunecontrol}.
Then go to the top level in your build-directory and type \texttt{make headercheck} to check all headers
or press 'tab' to use the auto-completion to search for a specific header.

\paragraph{Naming conventions}
General guidelines for naming conventions are specified in Section \ref{sc_guidelines}.
However, in order to avoid ambiguity a list of proposed names for variables, types,
functions etc is provided where users and mainly \Dumux developers can refer for
standards (check \texttt{dumux-devel/\allowbreak doc/\allowbreak naminglist/\allowbreak naming-conventions.odt}).
