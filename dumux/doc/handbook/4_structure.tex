\section{Directory Structure}
\label{sc_structure}

\Dumux has the following folder structure, which is similar to other \Dune modules.
\begin{itemize}
\item \texttt{bin}: binaries, e.g. used for the automatic testing, postprocessing, installation
\item \texttt{cmake}: the configuration options for building \Dumux
\item \texttt{doc}: files necessary for the Doxygen documentation and this handbook, and various logos
\item \texttt{dumux}: the main folder, containing the source files. see \ref{fig:dumux-structure}
      for a visualized structure. For more information on the models have a look at the
      Doxygen documentation.
\item \texttt{test}: tests for each numerical model and some functionality.
      The structure is equivalent to the dumux folder, the \texttt{references} folder
      contains solutions for the automatic testing. Each test program consist of source
      \texttt{*.cc}, the problem definition \texttt{*problem.hh}, and an input file \texttt{*.input}.
      If necessary, spatially dependent parameters are defined in \texttt{*spatialparameters.hh}.
      For more detailed descriptions of the tests, please have a look at the Doxygen documentation.
\item \texttt{tutorial}: contains the tutorials.
\end{itemize}

\begin{figure}
% \begin{sidewaysfigure}
\begin{tikzpicture}[scale=0.8,grow'=right,level distance=1.5in,sibling distance=.05in]
\tikzset{edge from parent/.style={thick, draw=dumuxBlue, edge from parent fork right}}
\tikzset{every tree node/.style={draw, thick, align=center}}
\tikzset{frontier/.style={distance from root=6.0in}}

\tikzset{FirstLevel/.style={
  draw=dumuxYellow,
  rectangle,
  align=center,
  minimum width=1.1in,
  minimum height=0.2in,
  text width=1.2in,
}}
\tikzset{SecondLevel/.style={
  draw=dumuxBlue,
  rectangle,
  align=center,
  minimum width=1.1in,
  minimum height=0.2in,
  text width=1.2in,
}}

\tikzset{ThirdLevel/.style={
  draw=none,
  align=left,
  minimum width=4.2in,
  text width=4.1in,
  font=\footnotesize
}}


\Tree
[.\node[draw=dumuxYellow, ultra thick] {dumux};
  [.\node[FirstLevel] {adaptive};
    \node[ThirdLevel] {
          Data transfer on new grid, adaptation indicators.};
  ]
  [.\node[FirstLevel] {assembly};
    \node[ThirdLevel] {
          Matrix assembler and residual for all discretization methods.};
  ]
  [.\node[FirstLevel] {common};
    \node[ThirdLevel] {
          Common files of all models:
          definition of boundary conditions, time stepping, splines, dimensionless numbers ...};
    [.\node[SecondLevel] {geometry};
      \node[ThirdLevel] {Geometrical algorithms};
    ]
    [.\node[SecondLevel] {properties};
      \node[ThirdLevel] {Base properties for all models.};
    ]
  ]
  [.\node[FirstLevel] {discretization};
%     [.\node[SecondLevel] {\emph{models}};
      \node[ThirdLevel] {Common methods for all discretizations: variable caching, advective and diffusive fluxes, upwinding...};
%     ]
    [.\node[SecondLevel] {box};
      \node[ThirdLevel] {Specific files for the box finite volume method:
                         specifications for advective and diffusive fluxes...};
    ]
    [.\node[SecondLevel] {cellcentered};
      \node[ThirdLevel] {Specific files for cell centered finite volume methods.};
    ]
    [.\node[SecondLevel] {staggered};
      \node[ThirdLevel] {Specific files for staggered finite volume method.};
    ]
  ]
  [.\node[FirstLevel] {freeflow};
    [.\node[SecondLevel] {\emph{models}};
      \node[ThirdLevel] {Single-phase free flow models using Navier-Stokes
                         and eddy-viscosity based Reynolds-averaged Navier-Stokes turbulence models.};
    ]
  ]
  [.\node[FirstLevel] {io};
    \node[ThirdLevel] {Additional in-/output possibilities like restart files, gnuplot-interface,
                       VTKWriter extensions and files for grid generation.};
  ]
  [.\node[FirstLevel] {linear};
    \node[ThirdLevel] {Linear solver backend.};
  ]
  [.\node[FirstLevel] {material};
    [.\node[SecondLevel] {binarycoefficients};
      \node[ThirdLevel] {Binary coefficients (like binary diffusion coefficients) and those
                         needed for the constitutive relationships (e.g. Henry coefficient)};
    ]
    [.\node[SecondLevel] {chemistry};
      \node[ThirdLevel] {Files needed to account for, e.g. electrochemical processes as in a fuel cell.};
    ]
    [.\node[SecondLevel] {components};
      \node[ThirdLevel] {Properties of a pure chemical substance (e.g. water)
                         or pseudo substance (e.g. air).};
    ]
    [.\node[SecondLevel] {constraintsolvers};
      \node[ThirdLevel] {Constraint solvers to make sure that the resulting fluid state is consistent with a
                         given set of thermodynamic equations.};
    ]
    [.\node[SecondLevel] {eos};
      \node[ThirdLevel] {Equations of state (eos) are auxiliary classes which provide
                         relations between a fluid phase's temperature, pressure, composition
                         and density.};
    ]
    [.\node[SecondLevel] {fluidmatrixint.};
      \node[ThirdLevel] {Constitutive relationships (e.g. capillary pressures, relative permeabilities)};
    ]
    [.\node[SecondLevel] {fluidstates};
      \node[ThirdLevel] {Fluid states are responsible for caching the thermodynamic
                         configuration of a system at a given spatial and temporal position.};
    ]
    [.\node[SecondLevel] {fluidsystems};
      \node[ThirdLevel] {Fluid systems express the thermodynamic relations between quantities.};
    ]
    [.\node[SecondLevel] {spatialparams};
      \node[ThirdLevel] {Base class for all spatially dependent variables, like permeability and
                         porosity. Includes spatial averaging routines. All other properties are
                         specified in the specific files of the respective models.};
    ]
  ]
  [.\node[FirstLevel] {mixeddimension};
    \node[ThirdLevel] {
          Coupled model with different dimensions.};
    [.\node[SecondLevel] {embedded};
      \node[ThirdLevel] {Embedded mixed dimension method.};
    ]
    [.\node[SecondLevel] {facet};
      \node[ThirdLevel] {Facet mixed dimension method.};
    ]
    [.\node[SecondLevel] {glue};
      \node[ThirdLevel] {Grid glue backend.};
    ]
  ]
  [.\node[FirstLevel] {nonlinear};
    \node[ThirdLevel] {Newton's method.};
  ]
  [.\node[FirstLevel] {parallel};
    \node[ThirdLevel] {Helper files for parallel simulations.};
  ]
  [.\node[FirstLevel] {porousmediumflow};
    [.\node[SecondLevel] {\emph{models}};
    \node[ThirdLevel] {Specific definition for porous medium flow simulations for all models:
                       implementation of equations,
                       model specific properties and indices.};
    ]
  ]
]
\end{tikzpicture}
\caption{Structure of the directory \texttt{dumux} containing the \Dumux source files.}
\label{fig:dumux-structure}
% \end{sidewaysfigure}
\end{figure}
