\section{Grid Handling}
\label{sec:gridhandling}

This section summarizes some ideas about grid generation and grid formats that can be used by \Dumux. In general,
\Dumux can read grids from file, or, construct grids inside the code. All grids are constructed inside a so called \texttt{GridCreator} which is a \Dumux property.
Note that some \texttt{GridCreator}s are already available in \Dumux, so e.g.
construction of a structured grid is fairly easy. We will subsequently introduce the supported file formats, the standard grid creator and its capabilities,
and briefly mention how to customize and deal with common other grid formats.

\subsection{Supported file formats}
\Dumux can read grids from file using the Dune Grid Format (DGF) or the Gmsh mesh format.

\subsubsection{Dune Grid Format}
Most of our \Dumux tests and tutorials use the Dune Grid Format (DGF) to read in grids. A detailed description
of the DGF format and some examples can be found in the \Dune doxygen documentation
\textbf{(Modules $\rightarrow$ I/O $\rightarrow$ Dune Grid Format (DGF)}). To generate larger or more
complex DGF files, we recommend to write your own scripts, e.g in \Cplusplus, Matlab or Python.

The DGF format can also used to read in spatial parameters defined on the grid. These parameters can
be defined on nodes as well as on the elements. An example for predefined parameters on a grid
can be found in the  \texttt{dumux/test/porousmediumflow/co2/implicit/} folder.

\subsubsection{Gmsh Mesh Format}
Gmsh is an open-source flexible grid generator for unstructured finite-element meshes (\cite{GEUZAINE2009}, \url{http://geuz.org/gmsh/}).
\Dumux supports the default Gmsh mesh format (MSH). For the format specifics and how to create grids with Gmsh, e.g. using
the provided GUI, we refer to the Gmsh documentation (\url{http://geuz.org/gmsh/doc/texinfo/gmsh.html}).

The MSH format can contain element and boundary markers defined in the grid. Thus, boundaries can be easily marked as e.g. inflow boundaries
using Gmsh. Further, the format supports higher order elements. They can be used to create boundary parameterization supported by e.g. the grid
manager \texttt{UGGrid}.
An example can be found in \texttt{dumux/test\allowbreak/io/gridcreator}.



\subsection{The default \texttt{GridCreator}}
The default \texttt{GridCreator} is called \texttt{GridCreator} and is automatically avaible in all problems.
It can construct grids from a DGF file (*.dgf) by simply providing the filename to the grid in the \texttt{Grid} group~\footnote{Note
that group name \texttt{Grid} is the default group name and can be customized in your problem changing the string property \texttt{GridParameterGroup}.
This way it is possible, e.g. for problems with more than one grid, to set different group names for each grid, thus configuring them separately.}
of the input file:
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
File = mydgfgrid.dgf
\end{lstlisting}
If you are using an unstructured grid manager like \texttt{UGGrid} or \texttt{ALUGrid}, constructing a grid from a Gmsh mesh file (*.msh) is just changing a line:
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
File = mygmshgrid.msh
\end{lstlisting}
\Dumux will tell you in case your selected grid manager does not support reading Gmsh files. You want to intially refine your grid? It's just adding a line:
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
File = mydgfgrid.dgf
Refinement = 4
\end{lstlisting}
When reading a Gmsh file, further parameters are recognized. \texttt{Verbose} enables verbose output on grid construction when set to $1$.
\texttt{BoundarySegments} enables reading parametrized boundaries. \texttt{PhysicalEntities} enables reading boundary and element flags.

\subsubsection{Grid manager specific parameters}
The default \texttt{GridCreator} supports also a selection of grid specific parameters.
To give an example we look at the commonly used unstructured grid manager \texttt{UGGrid}.
\texttt{UGGrid}s support red-green refinement per default. One can turn off the green closure by setting the grid's closure type
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
File = mydgfgrid.dgf
ClosureType = None # or Green
\end{lstlisting}
For all available parameters see the Doxygen documentation.

\subsubsection{Structured grids}
If you want to construct a structured grid with the default grid creator instead of the \texttt{File} key supply
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
LowerLeft = 0 0 0
UpperRight = 1 1 1
Cells = 10 10 20
\end{lstlisting}
where \texttt{LowerLeft} is a vector to the lower left corner of the grid and \texttt{UpperRight} a vector to the upper right corner.
\texttt{Cells} is a vector with the number of cells in each coordinate direction. Note that for a grid in a two-dimensional world, the
vectors only have two entries.

Depending on the grid manager further parameters are recognized.
\texttt{UGGrid}s, for example, supports simplex elements as well as hexahedral elements
(called simplified ``cube'' in \Dune). When creating a structured grid, we can select the cell type as follows
\begin{lstlisting}[style=DumuxParameterFile]
[Grid]
LowerLeft = 0 0 0
UpperRight = 1 1 1
Cells = 10 10 20
CellType = Cube # or Simplex
\end{lstlisting}
For all available parameters see the Doxygen documentation.

\subsection{Other grid formats and customized grid creators}
Other grid formats than DGF and MSH have to be converted to DGF or MSH to be read into \Dumux. A second possiblity (advanced \Cplusplus) is to write your own
\texttt{GridCreator}.
A (non-comprehensive) list of hints for some other common grid formats follows.

\subsubsection{Petrel}
Grids from Petrel (in ASCII format with the extension *.GRDECL) can be imported into \Dumux in two ways:
  \begin{enumerate}
  \item Using the GRDECL format directly with the help of the grid-manager \texttt{dune-cornerpoint}.
  \item Converting the GRDECL file into the DGF format.
  \end{enumerate}
The fist options requires the installation of \texttt{dune-cornerpoint} along with its dependencies. Set the property \texttt{Grid} to \texttt{Dune::CpGrid} in your problem file.

The second option has the advantage that you end up with a DGF which can then be used with any grid-manager (\texttt{dune-alugrid}, \texttt{UG} etc.) You also have to install \texttt{dune-cornerpoint}. Additionally you have to modify the converter \texttt{grdecl2vtu} found in \texttt{dune-cornerpoint/examples} to also write a DGF. To do so you have to:
\begin{itemize}
 \item Include the \texttt{dgfwriter.hh} found in \texttt{dune-grid/dune/grid/io/file/dgfparser}
 \item Create an object of the \texttt{Dune::DGFWriter} and call the its function \texttt{write()} within the \texttt{main} function for example after the \texttt{vtkwriter()} is called:
\begin{lstlisting}[style=DumuxCode]
Dune::DGFWriterParam<CpGrid::LeafGridView> dgfWriter(grid.leafView()))
dgfWriter.write(fnamebase + ".dgf")
\end{lstlisting}
\end{itemize}
Material parameters for elements with Petrel specific keywords like \texttt{PORO} are parsed by the converter \texttt{grdecl2vtu} (see the \texttt{main} function). They are available as vectors within the \texttt{main} function. The main GRDECL file with the coordinates must include the GRDECL files of the parameters, if for example the parameters are not already included, include the file bearing your parameter in your main GRDECL file:
\begin{lstlisting}
INCLUDE
'PARAMETER_X.GRDECL'
/
\end{lstlisting}
To add the parameters to your DGF you have to make changes to the header \texttt{dgfwriter.hh} such that they are passed as arguments of the \texttt{write()} function and written after each element (modify \texttt{writeElement()} and internal \texttt{write()} functions accordingly). Take caution that you stick to the correct DGF syntax (see \textbf{Modules $\rightarrow$ I/O $\rightarrow$ Dune Grid Format (DGF)} for reference).

\subsubsection{ArtMesh}
\href{http://www.topologica.org/toplog/wp/}{ArtMesh} is a 3D mesh generation software. It has its own mesh file format
which can be read by \Dumux via the \texttt{ArtGridCreator}. Traditionally it was used within \Dumux for fracture simulations with
the discrete fracture matrix model (\texttt{2pdfm}). A detailed description of the fracture network creation and gridding
can be found for example in \cite{Tatomir2012a}, pp. 68.

\subsubsection{ICEM}
For complex geometries a graphical tool to create grids might be appropriate. One possibility to mesh for example CAD
geometry data is the commercial software \href{http://www.ansys.com/Products/Other+Products/ANSYS+ICEM+CFD/}{ANSYS ICEM
CFD}. A very detailed, but outdated description can be found at the LH2 internal wiki. A more recent best practice guide is available
in dumux-devel at dumux-devel/util/gridconverters/Documentation\_ICEM\_CFD\_create\_mesh.odt. At LH2 exists a script which converts the ICEM mesh into the DGF.

% \subsubsection{Cornerpoint grids}
% TODO
