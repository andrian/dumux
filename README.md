<img src="dumux/doc/logo/dumux_logo_hires_whitebg.png" alt="dumux logo" width="400"/>

# DuMuX

This project contains a self-contained version of [DuMuX][0] with the following additional modules:

* [Discrete Fracture-Matrix (DFM) oil-water model](./dfm-ow/README.md)
* [DFM oil-water model where water is represented by two components](./dfm-ow3c/README.md)
* [DFM oil-water model where water is represented by $`\displaystyle N`$ components](./dfm-ownc/README.md)
* [DFM single-phase model](./dfm-1p/README.md)
* [DFM single-phase model for permeability upscaling](./dfm-1p-upscaling/README.md)

# Installation

1. Prerequisites

Recent versions of the following software packages are required:

  * `gcc`
  * `cmake`
  * `pkg-config`
  * `gfortran` 
  * `libsuitesparse-dev` and `libsuitesparse-doc`
  
All of the above packages can be installed with `sudo apt install <package name>`.

The [dfn-ownc](./dfm-ownc/README.md) model uses a modified version of the geochemical reaction module [PhreeqcRM](https://www.usgs.gov/software/phreeqc-version-3), which can be downloaded [here](./PHREEQCRM.zip). The modified PhreeqcRM version contains several functions to access the the data, which is not available through the standard PhreeqcRM interface. The installation instructions are the same as for the standard version.


2. Clone the project  

```bash
git clone https://git.iws.uni-stuttgart.de/andrian/dumux.git
```

3. Configure the release version of the code

    ```bash  
    cd dumux/
    ./dune-common/bin/dunecontrol --opts=cmake_O3.opts --builddir=build-cmake-O3 all  
    ```  
For a debug version, replace `cmake_O3.opts` with `debug.opts` and change the build directory accordingly.

If PhreeqcRM is not installed on your machine,  remove the linker option `-lphreeqcrm` from the corresponding `.opts` file.


4. Compile the DFM oil-water model

    ```bash
    cd dfm-ow/build-cmake-O3/src/
    make dfm-ow
    ```   
(for other models, replace `dfm-ow` with either `dfm-ow3c` or `dfm-ownc`).

[0]: https://dumux.org
