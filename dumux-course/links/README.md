# DuMu<sup>x</sup> course useful links

* DuMu<sup>x</sup> website: http://www.dumux.org/
* DuMu<sup>x</sup> repository: https://git.iws.uni-stuttgart.de/dumux-repositories/dumux
