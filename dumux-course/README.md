# DuMu<sup>x</sup> course material (DuMu<sup>x</sup> course July 2018)

The material is organized as follows

* exercises, click [here](./exercises/README.md) to go to the exercise description
* scripts, click [here](./scripts/README.md) for an overview of useful scripts (e.g. install script)
* slides, click [here](./slides/README.md) to download the slides
* links, click [here](./links/README.md) to go to a list of useful links
