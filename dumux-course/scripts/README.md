# DuMu<sup>x</sup> course useful scripts

The install script is a shell script that will setup DuMu<sup>x</sup> on your computer
in the version that is required to complete the current course's exercises.

Installation guide dumux
-------------------------

You can set up dumux using the attached script install.sh.
You need to have the following REQUIREMENTS installed:
  * gcc >= 4.9
  * cmake >= 3.1
  * git
  * pkg-config
  * paraview (to visualize the results)
  * gnuplot (to plot some curves)
  * wget (to download some config files during the installation)

On debian-based system you can use this:
  apt-get install build-essential pkg-config cmake git paraview wget gnuplot

Then, you can the execute the script and it will download the dune repositories and dumux
and configure all modules with CMake
  ./install.sh
This will clone the necessary repositories (in a subfolder 'dumux'),
build all libaries.

Run the script test_dumux.sh in the newly created dumux folder
to test your installation of dumux.
  ./test_dumux.sh
It will compile and run a simple one-phase ground water flow example
and visualizes the result using paraview.

* Click [here](./install.sh) to view and download the install script
