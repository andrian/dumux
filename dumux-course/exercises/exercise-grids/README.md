# Exercise Grids (DuMuX course)
<br>
## Problem set-up

Here we will expand on what we've covered in the basics exercise, and the problem set up will remain the same.

## Preparing the exercise

* Navigate to the directory `dumux-course/exercises/exercise-grids/`

We'll be editing the following files during this exercise:
* The __input file__: `exercise_grids.input` ,
* and the __problem file__: `injection2pproblem.hh`

Currently our grid is defined using the following input parameters in the input file:

```bash
[Grid]
UpperRight = 60 40
Cells = 24 16
```

The upper right point of our domain is defined using the first parameter, and all directions are split into a number of equal sized cells defined by the second parameter.

As you may have noticed, the resolution of our grid isn't quite optimal, and a finer grid could help to show a more realistic picture of the physics.

<br><br>
### Task 1: Applying a Global Refinement
<hr>
A simple strategy for addressing this resolution issue would be a global refinement.
In the Grid section of your input file, you can add a parameter "Refinement" which will apply a global refinement to the grid you provided. A refinement of value 2 will split each cell into two cells per dimension.

* > __Task 1__: Apply a global refinement of value 2. Make note of the increase in simulation run time.

The simulation should take much more time. Luckily there are other ways to resolve your grid that will not be as computationally expensive.

<br><br>
### Task 2: Changing the Grid Type
<hr>

Up until now, we have used a basic uniform structured grid. This grid is provided in the Dune environment and is called the YASP grid (Yet Another Structured Parallel Grid).

In the Problem file, the grid properties are defined using the following command.

```c++
// Set the grid type
SET_TYPE_PROP(Injection2pTypeTag, Grid, Dune::YaspGrid<2>);
```

This sets the Grid, which belongs to the Injection2pTypeTag type tag, and calls the manager with a basic YaspGrid in the second dimension.

If we look in the grid manager header file,  `dumux/dumux/io/grid/gridmanager.hh`,
we will see that there are a few grid development options. Until now, we have used the most basic definition.

Let's change our grid manager to a slightly more complicated version. We will still call a YaspGrid, but can use the Dune coordinates system named TensorProductCoordinates rather than the basic version. The following class can be found in `dumux/dumux/io/grid/gridmanager.hh`.

```c++
GridManager<Dune::YaspGrid<dim, Dune::TensorProductCoordinates<ctype, dim> >>
```

A YaspGrid, of dimension `dim` is used, and the coordinate system `Dune::TensorProductCoordinates`, with arguments coordinate type `ctype` and dimension `dim`, is provided as a secondary argument. This allows us to switch to a grid format that allows for more cells in certain zones, and a cell grading.

To use this format, we need to set the grid property in our problem file accordingly. Our problem dimension is 2 (`dim`), and we will provide the coordinates as doubles (`ctype`).
To switch the property we should replace the `Dune::YaspGrid<2>` argument with `Dune::YaspGrid<dim, Dune::TensorProductCoordinates<ctype, dim> >`, and the `dim` and `ctype` with `2` and `double`

In our input file, we can now specify more specific Grid sizing directions: Positions, Cells, and Grading, each of which can be defined per direction.

In order to redevelop the exact same grid that was build beforehand, we can enter the following parameters to the Grid section of the input file.

```bash
[Grid]
Positions0 = 0 60
Positions1 = 0 40
Cells0 = 24
Cells1 = 16
Grading0 = 1.0
Grading1 = 1.0
```

* > __Task 2__: Following the format shown above, change the grid manager. Make the required changes to the input file and run the simulation.

<br><br>
### Task 3: Grid Zoning and Grading
<hr>
The Positions0 and Positions1 parameters will segment the grid into zones split at the values provided. In this case, the X direction
will be made of one zone between 0 and 60, and the Y direction will be split into one zone between 0 and 40.

```bash
[Grid]
Positions0 = 0 60
Positions1 = 0 40
```

The Cells0 and Cells1 parameters will define a number of cells to each of the zones. The first number provided will be applied to the first zone.
In the first zone in the X direction, the domain is split into 24 cells. In the first zone in the Y direction, the domain is split into 16 cells.

```bash
[Grid]
Cells0 = 24
Cells1 = 16
```

The Grading0 and Grading1 will apply a cell size grading throughout the zone in which it is applied.
The value 1.0 will split the domain evenly such that each cell has the same size in one direction.
Any other values will distribute the cell size unevenly throughout the domain.
These sizes are defined such that the next cell in the domain will be sized as the grading value multiplied by the previous cell size.

```bash
[Grid]
Grading0 = 1.0
Grading1 = 1.0
```
Positive values will make the cells closer to the start of the zone small, and those further from the start of the zone larger.
Negative values will make the cells closer to the start of the zone large, and those further from the start of the zone smaller.

Considering that the majority of the action in this simulation will occur along the interface between the aquifer and the aquitard, and along the injection axis, we should provide more resolution in these areas, and less in the other areas.

* > __Task 3__: Using the Zone defintions described above, edit your grid to focus your simulation on the areas in greatest concern.

One example of this would be the following definition.

```bash
[Grid]
Positions0 = 0 45 60
Positions1 = 0 25 30 35 40
Cells0 = 9 10
Cells1 = 5 5 5 2
Grading0 = 1.0 -1.3
Grading1 = 1.0 -1.3 1.3 1.0
```

<br><br>
### Task 4: Reading in a Structured Grid (*.dgf or *.msh grid files)
<hr>

DuMuX can also read in grids from external files. There are two supported file types: `.dgf` grids, and `.msh` grids.
__`.dgf`__ grids (DUNE Grid Format grids) are developed within the dune environment and are often used in dumux tests.
(see: `https://www.dune-project.org/doxygen/2.4.1/group__DuneGridFormatParser.html`)
__`.msh`__ grids are made from the open-source Gmsh software, and is also supported by dumux.
(see: `http://gmsh.info//` and `http://gmsh.info//doc/texinfo/gmsh.html`)

We can read in simple .dgf files using basic YaspGrids, but if we want to use more complicated external grids, we would need to use other grid formats, like UGGrid or ALUGrid.
For this example, we can rewrite our Grid Properties to develop an ALUGrid. The grid manager class takes the following form:
```c++
GridManager<Dune::ALUGrid<dim, dimworld, elType, refinementType>>
```
Our `dim` and `dimworld` will both be 2, we can use `Dune::cube` as our `elType` (element type), and for the `refinementType` we can use `Dune::nonconforming`.

There are two external structured grid files located in the `grid/` folder (`grid_structured.msh` and `grid_structured.dgf`). A path to one of these grids should be included in the input file.


* > __Task 4__: Edit the grid properties to set up an ALUGrid made of cubes. Call one of the strucutred meshes via the input file.


<br><br>
### Task 5: Reading in a Unstructured Grid (*.dgf or *.msh grid files)
<hr>

An example of an Unstructured grid is located in the `grid/` folder (`grid_unstructured.msh`)

This grid is made up of triangles (simplices of dimension 2). This element type can be set in the grid properties section.

* > __Task 5__: Change the grid property element type to include `Dune::simplex` instead of `Dune::cube`. Read in the unstructured grid via the input file and run the simulation.







