add_input_file_links()

# executables for ex_interface_coupling_ff-pm
dune_add_test(NAME ex_turbulence_coupling_ff-pm
              SOURCES ex_turbulence_coupling_ff-pm.cc
              CMD_ARGS ex_turbulence_coupling_ff-pm.input)

# add tutorial to the common target
add_dependencies(test_exercises ex_turbulence_coupling_ff-pm)
