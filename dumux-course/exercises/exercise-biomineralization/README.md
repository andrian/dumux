# Exercise Biomineralization

The aim of this exercise is to get a first glimpse at the _DuMuX_ way of implementing mineralization and reaction processes. In the scope of this exercise, the setting of boundary conditions is revisited and a new reaction term is implemented.

## Problem set-up

The domain has a size of 20 x 15 m and contains a sealing aquitard in the middle. The aquitard is interrupted by a "fault zone" and thereby connects the upper drinking water aquifer and the lower CO2-storage aquifer. Initially, the domain is fully water saturated and biofilm is present in the lower CO2-storage aquifer. Calcium and urea are injected in the upper drinking water aquifer by means of a Neumann boundary condition. The remaining parts of the upper and the entire lower boundary are Neumann no-flow while on the right side a Dirichlet boundary conditions is applied according to the initial values.


Disclaimer: Please note, that this is not a realistic scenario. One does not want to store gaseous CO2 in such a subcritical setting.

![](../extradoc/exercisebiomin_setup.png)


## Preparing the exercise

* Navigate to the directory `dumux-course/exercises/exercise-biomineralization`

### 1. Getting familiar with the code

Locate all the files you will need for this exercise
* The __main file__ : `exercisebiomin.cc`
* The __input file__: `exercisebiomin.input`
* The __problem file__ : `biominproblem.hh`
* The __spatial parameters file__: `biominspatialparams.hh`

Furthermore you will find the following folders:
* `chemistry`: Provides a way to formulate reactions in therms of source/sink terms.
* `components`: Provides some additional components e.g. biofilm and urea.
* `fluidsystems`: Stores headers containing data/methods on fluid mixtures of pure components.
* `solidsystems`:  Stores headers containing data/methods on solid mixtures of the components.

To see more chemistry, components, fluidsystems and solidsystems implementations, have a look at the folder `dumux/material`.

__Special note on solidsystems:__
There are two types of solid components. Reactive and inert. For each reactive component one mass balance is solved. The inert components compose the "unchanging" (inert) rock matrix.

### 2. Implement a chemical equation

In the following the basic steps required to set the new chemical equation are outlined. Here, this is done in the __chemistry__ folder in the prepared file: `simplebiominreactions.hh` starting in line 96.
Please be aware, that the chemistry file already provides some convenience functions (e.g. ``moleFracToMolality()``).

__Task__

Add a kinetic reaction by first calculating the current mass of available biofilm. Note that the density and volume fraction of biofilm are already defined for you.

$`\displaystyle mass_{biofilm} = \rho_{biofilm} * \phi_{biofilm}`$

Next, we want to implement the rate of ureolysis. This can be done with the following simplified equation:

$`\displaystyle  r_{urea} = k_{urease} * Z_{urease,biofilm} * m_{urea} / (K_{urea} + m_{urea})`$,

where $`\displaystyle  r_{urea}`$ is the rate of ureolysis, $`\displaystyle  k_{urease}`$ the urease enzyme activity, $`\displaystyle m_{urea}`$ the molality of urea and $`\displaystyle K_{urea}`$ the half-saturation constant for urea for the ureolysis rate.

Note, that the urease concentration $`\displaystyle Z_{urease,biofilm}`$ has a kinetic term of urease production per biofilm :

$`\displaystyle Z_{urease,biofilm} = k_{urease,biofilm} * mass_{biofilm}`$

The last step is defining the source term for each component according to the chemical equation:

$`\displaystyle Ca^{2+} + CO(NH)_{2} + 2 H_{2}O -> 2 NH_{4}^{+} + CaCO_{3}`$

which is:

Calcium ion + Urea + 2 Water &rarr; 2 Ammonium ions + Calcite

### 3. Make use of your newly created chemical equation

To enable your newly created chemical equation, the chemistry file has to be included in your problem file. This has to be done in line 35:

```c++
#include "chemistry/simplebiominreactions.hh"   // chemical reactions
```

Additionally the TypeTag of your chemistry file needs to be set in the problem file (line 125):

```c++
using Chemistry = typename Dumux::SimpleBiominReactions<TypeTag>;
```

__Task__

Now the source/sink term can be updated in the problem file. You find it in line 343. You can access the newly created chemistry file and call the reactionSource()-function from it. Make sure to call the reactionSource()-function with the correct arguments. Return the updated source terms in the end.
The volume variables can be set using the element volume variables and the sub control volume:

```c++
const auto& volVars = elemVolVars[scv];
```

In order to compile and execute the program, change to the build-directory

```bash
cd build-cmake/exercises/exercise-biomineralization
```

and type

```bash
make exercisebiomin
./exercisebiomin exercisebiomin.input
```

### 4. Seal leakage pathway in the aquitard

 In the input file, you will find some parameters concerning the mineralization process. We want to seal the leakage pathway in the aquitard. The  leakage pathway is assumed to be sealed when the porosity is reduced to `0.07` or less.

 __Task:__

 Vary input parameters in the input file to seal the leakage pathway. The overall injection duration in days (`InjBioTime`), the initial biomass (`InitBiofilm`), the overall injection rate (`InjVolumeflux`) and the injected concentrations of urea and calcium (`ConcUrea` and `ConcCa`) are available for variation. When changing the concentrations, keep in mind that both urea and calcium are needed for the reaction and their mass ratio should be 2 calcium to 3 urea for a good molar ratio of 1 mol urea per 1 mol of calcium, see also the molar masses in the component files.

 The result for the porosity should like like this:

 ![](../extradoc/exercisebiomin_porosityFinal.png)


### 5. CO2 injection to test aquitard integrity

Now the sealed aquitard is tested with a CO2-Injection into the lower CO2-storage aquifer.

__Task:__

Implement a new boundary condition on the left boundary, injecting CO2 from 2 m to 3 m from the bottom. Make sure, that the injection time for the calcium and urea is finished. You can use the predefined value `gasFlux` directly and divide it by the molar mass of CO2.
Run two simulations and compare them side by side by creating two input files, or overwriting the input file in the command line:
```bash
./exercisebiomin -Problem.Name biominNoUrea -Injection.ConcUrea 0
```
The result for the biomineralization process after the CO2 injection should look like this:

![](../extradoc/exercisebiomin_injectionFinal.png)

### Bonus: Paraview Magic: Compare different results using Programmable Filter

In the last step, the manual comparison of the results can be quite difficult. Paraview offers the option to use programmable python filters. To use them, make sure two result files with __different names__ are loaded. Mark both of them and click on `Filters --> Alphabetical --> Programmable Filter`. Now a new field opens on the left side. Copy the following lines there:

```python
Sn_0 = inputs[0].CellData['S_n'];
Sn_1 = inputs[1].CellData['S_n'];
output.CellData.append(abs(Sn_0-Sn_1),'diffSn');
```

Click `Apply` and select `diffSn` as new output. You should now see the difference between the two result files. You can also change the output to a not absolute value by changing the last line to:

```python
output.CellData.append((Sn_0-Sn_1),'diffSn');
```
