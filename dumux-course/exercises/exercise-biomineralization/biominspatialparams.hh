// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial parameters for the bio mineralization problem where urea is used to
 * precipitate calcite.
 */
#ifndef DUMUX_BIOMIN_SPATIAL_PARAMETERS_HH
#define DUMUX_BIOMIN_SPATIAL_PARAMETERS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>

#include <dumux/discretization/methods.hh>

namespace Dumux
{
//forward declaration
template<class TypeTag>
class BioMinSpatialparams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(BioMinSpatialparams);

// Set the spatial parameters
SET_TYPE_PROP(BioMinSpatialparams, SpatialParams, BioMinSpatialparams<TypeTag>);
} // end namespace Properties

/*!
 * \brief Definition of the spatial parameters for the biomineralisation problem
 * with geostatistically distributed initial permeability.
 */
template<class TypeTag>
class BioMinSpatialparams
: public FVSpatialParams<typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                         typename GET_PROP_TYPE(TypeTag, Scalar),
                         BioMinSpatialparams<TypeTag>>
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, BioMinSpatialparams<TypeTag>>;
    using EffectiveLaw = RegularizedBrooksCorey<Scalar>;
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);

    using GridView = typename FVGridGeometry::GridView;
    using CoordScalar = typename GridView::ctype;
    enum { dimWorld=GridView::dimensionworld };
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;

public:
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using PermeabilityType = Tensor;
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    /*!
    * \brief The constructor
    *
    * \param gridView The grid view
    */
    BioMinSpatialparams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        //! set initial aquifer params
        initialPorosity_ = getParam<Scalar>("SpatialParams.InitialPorosity");
        initialPermeability_ = getParam<Scalar>("SpatialParams.InitialPermeability");

        // set main diagonal entries of the permeability tensor to a value
        // setting to one value means: isotropic, homogeneous

        // residual saturations
        materialParams_.setSwr(0.2);
        materialParams_.setSnr(0.05);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(1e4);
        materialParams_.setLambda(2.0);

        //! hard code specific params for aquitard layer
        aquitardPorosity_ = 0.1;
        aquitardPermeability_ = 1e-15;

        // residual saturations
        aquitardMaterialParams_.setSwr(0.0);
        aquitardMaterialParams_.setSnr(0.0);

        // parameters for the Brooks-Corey law
        aquitardMaterialParams_.setPe(1e7);
        aquitardMaterialParams_.setLambda(2.0);
    }

    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        return 1-referencePorosity(element, scv);
    }

    /*!
     *  \brief Define the initial porosity \f$[-]\f$ distribution
     *  For this special case, the initialPorosity is precalculated value
     *  as a referencePorosity due to an initial volume fraction of biofilm.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element,
                             const SubControlVolume &scv) const
    {
        const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
        return referencePorosity_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
     *  This value calculates the correct porosity that needs to be set
     *  in for the initialPorosity when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePorosity from the porosityprecipitation law.
     *  The reference porosity is calculated as:
     *  \f[referencePorosity = initialPorosity + \sum \phi \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
     *
     * \param fvGridGeometry The fvGridGeometry
     * \param sol The (initial) solution vector
     */
    void computeReferencePorosity(const FVGridGeometry& fvGridGeometry,
                                  const SolutionVector& sol)
    {
        referencePorosity_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->fvGridGeometry().elementMapper().index(element);

            auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePorosity_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& dofPosition = scv.dofPosition();
                const bool isInAquitardNotFaultZone = isInAquitard_(dofPosition) && !isFaultZone_(dofPosition);
                auto phi = isInAquitardNotFaultZone ? aquitardPorosity_ : initialPorosity_;
                auto phiEvaluated = poroLaw_.evaluatePorosity(element, scv, elemSol, phi);
                referencePorosity_[eIdx][scv.indexInElement()] = calculatephiRef(phi, phiEvaluated);
            }
        }
    }

    /*!
     *  \brief Return the actual recent porosity \f$[-]\f$ accounting for
     *  clogging caused by mineralization.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        const auto refPoro = referencePorosity(element, scv);
        return poroLaw_.evaluatePorosity(element, scv, elemSol, refPoro);
    }

    /*!
     *  \brief Define the reference permeability \f$[m^2]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    PermeabilityType referencePermeability(const Element& element,
                                           const SubControlVolume &scv) const
    {
        const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
        return referencePermeability_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference permeability which depends on the porosity.
     *  This value calculates the correct permeability that needs to be set
     *  for the initialPermeability when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePermeability function
     *  from the Kozeny-Carman permeability law.
     *  The reference permeability is calculated as:
     *  \f[referencePorosity = initialPermeability
     *  \cdot f(refPoro / initPoro) - evaluatePermeability \cdot f(refPoro/initPoro) \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePermeability = initialPermeability^2 / evaluatePermeability() \f]
     *
     * \param fvGridGeometry The fvGridGeometry
     * \param sol The (initial) solution vector
     */
    void computeReferencePermeability(const FVGridGeometry& fvGridGeometry,
                                      const SolutionVector& sol)
    {
        referencePermeability_.resize(fvGridGeometry.gridView().size(0));
        for (const auto& element : elements(fvGridGeometry.gridView()))
        {
            auto fvGeometry = localView(fvGridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->fvGridGeometry().elementMapper().index(element);

            auto elemSol = elementSolution(element, sol, fvGridGeometry);
            referencePermeability_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& dofPosition = scv.dofPosition();
                const bool isInAquitardNotFaultZone = isInAquitard_(dofPosition) && !isFaultZone_(dofPosition);
                auto kInit = isInAquitardNotFaultZone ? aquitardPermeability_ : initialPermeability_;
                const auto refPoro = referencePorosity(element, scv);
                const auto poro = porosity(element, scv, elemSol);
                auto kEvaluated = permLaw_.evaluatePermeability(kInit, refPoro, poro);
                referencePermeability_[eIdx][scv.indexInElement()] = calculateKRef(kInit, kEvaluated);
            }
        }
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function.
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto refPerm = referencePermeability(element, scv);
        const auto refPoro = referencePorosity(element, scv);
        const auto poro = porosity(element, scv, elemSol);
        return permLaw_.evaluatePermeability(refPerm, refPoro, poro);
    }

    /*!
     * \brief return the parameter object for the Brooks-Corey material law which depends on the position
     *
     * \param globalPos The global position
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        if (isInAquitard_(globalPos) && !isFaultZone_(globalPos))
            return aquitardMaterialParams_;
        else
            return materialParams_;
    }

    // define which phase is to be considered as the wetting phase
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::liquidPhaseIdx; }

private:

    static constexpr Scalar eps_ = 1e-6;

    //! calculateKRef for tensorial permeabilities
    PermeabilityType calculateKRef(PermeabilityType K, PermeabilityType kEvaluated)
    {
        //TODO this assumes the change in permeability is homogeneous as currently also assumed in PermeabilityKozenyCarman.
        // there also might be nicer/safer ways to calculate this.
        Scalar factor = 0.0;
        int counter = 0;
        for (int i = 0; i < dimWorld; ++i)
            for (int j = 0; i < dimWorld; ++i)
                if(kEvaluated[i][j]!=0)
                {
                factor += K[i][j] / kEvaluated[i][j];
                    ++counter;
                }
        factor /= counter;
        K *= factor;

        return K;
    }

    //! calculateKRef for scalar permeabilities
    template<class PT = PermeabilityType,
             std::enable_if_t<Dune::IsNumber< PT >::value, int> = 0>
    PermeabilityType calculateKRef(PermeabilityType K, PermeabilityType kEvaluated)
    {
        Scalar factor = K / kEvaluated;
        K *= factor;
        return K;
    }

    Scalar calculatephiRef(Scalar phiInit, Scalar phiEvaluated)
    { return 2*phiInit - phiEvaluated;}

    bool isInAquitard_(const GlobalPosition &globalPos) const
    { return globalPos[dimWorld-1] > 8 - eps_ && globalPos[dimWorld-1] < 10 + eps_;}

    bool isFaultZone_(const GlobalPosition &globalPos) const
    { return globalPos[dimWorld-2] > 2 - eps_ && globalPos[dimWorld-2] < 3 + eps_;}

    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    PorosityPrecipitation<Scalar, ModelTraits::numComponents(), ModelTraits::numSolidComps()> poroLaw_;
    PermeabilityKozenyCarman<PermeabilityType> permLaw_;


    Scalar initialPorosity_;
    std::vector< std::vector<Scalar> > referencePorosity_;
    Scalar initialPermeability_;
    std::vector< std::vector<PermeabilityType> > referencePermeability_;

    MaterialLawParams materialParams_;

    Scalar aquitardPorosity_;
    Scalar aquitardPermeability_;
    MaterialLawParams aquitardMaterialParams_;
};

} // end namespace Dumux

#endif
