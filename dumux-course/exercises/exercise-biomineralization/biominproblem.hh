// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Tutorial problem for a fully coupled TwoPNCMineralization CC Tpfa model.
 */
#ifndef DUMUX_EXERCISE_FOUR_PROBLEM_HH
#define DUMUX_EXERCISE_FOUR_PROBLEM_HH

#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "solidsystems/biominsolidphase.hh" // The biomineralization solid system

#include <dumux/material/components/co2tablereader.hh>

#include "fluidsystems/biomin.hh" // The biomineralization fluid system
// TODO: dumux-course-task
// include chemistry file here
#include "biominspatialparams.hh" // Spatially dependent parameters

namespace Dumux {

/*!
 * \brief Tutorial problem for a fully coupled TwoPNCMineralization CC Tpfa model.
 */

//! Provides the precalculated tabulated values of CO2 density and enthalpy.
#include <dumux/material/components/co2tables.inc>

template <class TypeTag>
class ExerciseFourBioMinProblem;

namespace Properties
{
//! Create new type tag for the problem
NEW_TYPE_TAG(ExerciseFourBioMinTypeTag, INHERITS_FROM(TwoPNCMin, BioMinSpatialparams));
NEW_TYPE_TAG(ExerciseFourBioMinCCTpfaTypeTag, INHERITS_FROM(CCTpfaModel, ExerciseFourBioMinTypeTag));

//! Set the problem property
SET_TYPE_PROP(ExerciseFourBioMinTypeTag, Problem, ExerciseFourBioMinProblem<TypeTag>);

//! Set grid and the grid creator to be used
#if HAVE_DUNE_ALUGRID
SET_TYPE_PROP(ExerciseFourBioMinTypeTag, Grid, Dune::ALUGrid</*dim=*/2, 2, Dune::cube, Dune::nonconforming>);
#elif HAVE_UG
SET_TYPE_PROP(ExerciseFourBioMinTypeTag, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(ExerciseFourBioMinTypeTag, Grid, Dune::YaspGrid<2>);
#endif // HAVE_DUNE_ALUGRID

//! Set the fluid system type
SET_PROP(ExerciseFourBioMinTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using CO2Tables = Dumux::CO2Tables;
    using H2OType = Components::TabulatedComponent<Components::H2O<Scalar>>;
public:
    using type = FluidSystems::BioMin<Scalar, CO2Tables, H2OType>;
};

SET_PROP(ExerciseFourBioMinTypeTag, SolidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = SolidSystems::BiominSolidPhase<Scalar>;
};


SET_BOOL_PROP(ExerciseFourBioMinTypeTag, EnableFVGridGeometryCache, false);
SET_BOOL_PROP(ExerciseFourBioMinTypeTag, EnableGridVolumeVariablesCache, false);
SET_BOOL_PROP(ExerciseFourBioMinTypeTag, EnableGridFluxVariablesCache, false);

} // end namespace properties
/*!
 *
 * \brief Problem biomineralization (MICP) in an experimental setup.
 */
template <class TypeTag>
class ExerciseFourBioMinProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;

    // Grid dimension
    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimension>;
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using ElementVolumeVariables = typename GET_PROP_TYPE(TypeTag, GridVolumeVariables)::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    // TODO: dumux-course-task
    // set the chemistry TypeTag

    enum
    {
        numComponents = FluidSystem::numComponents,

        pressureIdx = Indices::pressureIdx,
        switchIdx   = Indices::switchIdx, //Saturation

        //Indices of the components
        H2OIdx  = FluidSystem::H2OIdx,
        CO2Idx  = FluidSystem::CO2Idx,
        CaIdx   = FluidSystem::CaIdx,
        UreaIdx = FluidSystem::UreaIdx,

        BiofilmIdx = SolidSystem::BiofilmIdx+numComponents,
        CalciteIdx = SolidSystem::CalciteIdx+numComponents,

        //Index of the primary component of wetting and nonwetting phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        liquidPhaseOnly = Indices::firstPhaseOnly,
    };

    /*!
     * \brief The constructor
     *
     * \param fvGridGeometry The finite volume grid geometry
     */
public:
    ExerciseFourBioMinProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
#if !DUNE_VERSION_NEWER(DUNE_COMMON,2,7)
        Dune::FMatrixPrecision<Scalar>::set_singular_limit(1e-35);
#endif
        name_           = getParam<std::string>("Problem.Name");
        //initial values
        densityW_       = getParam<Scalar>("Initial.InitDensityW");
        initPressure_   = getParam<Scalar>("Initial.InitPressure");
        initBiofilm_    = getParam<Scalar>("Initial.InitBiofilm");

        //injection values
        injVolumeflux_  = getParam<Scalar>("Injection.InjVolumeflux");
        injBioTime_     = getParam<Scalar>("Injection.InjBioTime")*86400;
        injCO2_         = getParam<Scalar>("Injection.InjCO2");
        concCa_         = getParam<Scalar>("Injection.ConcCa");
        concUrea_       = getParam<Scalar>("Injection.ConcUrea");

        unsigned int codim = GET_PROP_TYPE(TypeTag, FVGridGeometry)::discMethod == DiscretizationMethod::box ? dim : 0;
        Kxx_.resize(fvGridGeometry->gridView().size(codim));
        Kyy_.resize(fvGridGeometry->gridView().size(codim));

        //initialize the fluidsystem
        FluidSystem::init(/*startTemp=*/288,
                          /*endTemp=*/ 303,
                          /*tempSteps=*/5,
                          /*startPressure=*/1e4,
                          /*endPressure=*/1e6,
                          /*pressureSteps=*/500);
    }

    void setTime(Scalar time)
    {
        time_ = time;
    }

    /*!
     * \name Problem parameters
     */

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 300 degrees Kelvin.
     */
    Scalar temperature() const
    {
        return 300; // [K]
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        // we don't set any BCs for the solid phases
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
        // set all Neumann
        bcTypes.setAllNeumann();

        // set right boundary to Dirichlet
        if(globalPos[0] > xmax - eps_)
            bcTypes.setAllDirichlet();

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        // recycle initialAtPos() for Dirichlet values
        return initialAtPos(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        Scalar waterFlux = injVolumeflux_; // divide by area if area not 1! [m/s]
        Scalar gasFlux = injCO2_; // divide by area if area not 1! [m/s]

        // Set values for Ca + urea injection above aquitard.
        // Use negative values for injection.
        if(globalPos[0] < eps_
           && globalPos[1] > 11.0 + eps_
           && globalPos[1] < 12.0 + eps_
           && time_ < injBioTime_)
        {
            values[conti0EqIdx + H2OIdx]  = - waterFlux * densityW_ / FluidSystem::molarMass(H2OIdx);
            values[conti0EqIdx + CaIdx]   = - waterFlux * concCa_ / FluidSystem::molarMass(CaIdx);
            values[conti0EqIdx + UreaIdx] = - waterFlux * concUrea_ / FluidSystem::molarMass(UreaIdx);
        }
        // TODO: dumux-course-task
        // Set CO2 injection below aquitard after the injBioTime
        else
        {
            values = 0.0; //mol/m²/s
        }

        return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(liquidPhaseOnly);

        Scalar hmax = this->fvGridGeometry().bBoxMax()[1];
        priVars[pressureIdx] = initPressure_ - (globalPos[1] - hmax)*densityW_*9.81;
        priVars[switchIdx]   = 0.0;
        priVars[CaIdx]       = 0.0;
        priVars[UreaIdx]     = 0.0;
        priVars[CalciteIdx]  = 0.0;
        priVars[BiofilmIdx]  = 0.0;

        // set Biofilm presence only in aquitard zone and below
        if (globalPos[1] < 10 + eps_)
        {
            priVars[switchIdx]  = 0.0;
            priVars[BiofilmIdx] = initBiofilm_; // [m^3/m^3]
        }

        return priVars;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        // TODO: dumux-course-task
        // set Chemistry
        // set VolumeVariables
        // call reactionSource() in chemistry

        return source;
    }


    const std::vector<Scalar>& getKxx()
    {
        return Kxx_;
    }

    const std::vector<Scalar>& getKyy()
    {
        return Kyy_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     * Function is called by the output module on every write.
     */
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
            }
        }
    }

private:
    static constexpr Scalar eps_ = 1e-6;

    Scalar densityW_;
    Scalar initPressure_;
    Scalar initBiofilm_;

    Scalar injVolumeflux_;
    Scalar injBioTime_;
    Scalar injCO2_;
    Scalar concCa_;
    Scalar concUrea_;

    std::vector<double> Kxx_;
    std::vector<double> Kyy_;
    std::string name_;
    Scalar time_;
};

} //end namespace

#endif
