# the grid exercise simulation program
dune_add_test(NAME exercise_grids_solution
              SOURCES exercise_grids_solution.cc
              CMD_ARGS exercise_grids_solution.input)

# add tutorial to the common target
add_dependencies(test_exercises exercise_grids_solution)

# add a symlink for the input file
dune_symlink_to_source_files(FILES "exercise_grids_solution.input")
dune_symlink_to_source_files(FILES grids)
