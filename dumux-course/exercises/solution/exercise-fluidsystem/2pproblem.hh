// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Tutorial problem for a fully coupled twophase box model.
 */
#ifndef DUMUX_EXERCISE_FLUIDSYSTEM_A_PROBLEM_HH
#define DUMUX_EXERCISE_FLUIDSYSTEM_A_PROBLEM_HH

// The numerical model
#include <dumux/porousmediumflow/2p/model.hh>

// The box discretization
#include <dumux/discretization/box/properties.hh>

// The grid managers
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif

// The porous media base problem
#include <dumux/porousmediumflow/problem.hh>

// Spatially dependent parameters
#include "spatialparams.hh"

// The water component
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>

// The components that will be created in this exercise
#include "components/myincompressiblecomponent.hh"
#include "components/mycompressiblecomponent.hh"

// We will only have liquid phases here
#include <dumux/material/fluidsystems/1pliquid.hh>

// The two-phase immiscible fluid system
#include <dumux/material/fluidsystems/2pimmiscible.hh>

// The interface to create plots during simulation
#include <dumux/io/gnuplotinterface.hh>

namespace Dumux{
// Forward declaration of the problem class
template <class TypeTag> class ExerciseFluidsystemProblemTwoP;

namespace Properties {
// Create a new type tag for the problem
NEW_TYPE_TAG(ExerciseFluidsystemTwoPTypeTag, INHERITS_FROM(TwoP, BoxModel));

// Set the "Problem" property
SET_TYPE_PROP(ExerciseFluidsystemTwoPTypeTag, Problem, ExerciseFluidsystemProblemTwoP<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(ExerciseFluidsystemTwoPTypeTag, SpatialParams,
              ExerciseFluidsystemSpatialParams<typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                         typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Set grid to be used
#if HAVE_DUNE_ALUGRID
SET_TYPE_PROP(ExerciseFluidsystemTwoPTypeTag, Grid, Dune::ALUGrid</*dim=*/2, 2, Dune::cube, Dune::nonconforming>);
#elif HAVE_UG
SET_TYPE_PROP(ExerciseFluidsystemTwoPTypeTag, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(ExerciseFluidsystemTwoPTypeTag, Grid, Dune::YaspGrid<2>);
#endif // HAVE_DUNE_ALUGRID

// we use the immiscible fluid system here
SET_PROP(ExerciseFluidsystemTwoPTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TabulatedH2O = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using LiquidWater = typename FluidSystems::OnePLiquid<Scalar, TabulatedH2O>;
    /*!
     * Uncomment first line and comment second line for using the incompressible component
     * Uncomment second line and comment first line for using the compressible component
     */
    using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyIncompressibleComponent<Scalar> >;
    // using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyCompressibleComponent<Scalar> >;

public:
    using type = typename FluidSystems::TwoPImmiscible<Scalar, LiquidWater, LiquidMyComponentPhase>;
};

}

/*!
 * \ingroup TwoPBoxModel
 * \brief  Tutorial problem for a fully coupled twophase box model.
 */
template <class TypeTag>
class ExerciseFluidsystemProblemTwoP : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    // Grid dimension
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld
    };
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // Dumux specific types
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);

    enum {
        waterPressureIdx = Indices::pressureIdx,
        naplSaturationIdx = Indices::saturationIdx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx, // water transport equation index
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx // napl transport equation index
    };

public:
    ExerciseFluidsystemProblemTwoP(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    , eps_(3e-6)
    {
#if !(HAVE_DUNE_ALUGRID || HAVE_UG)
        std::cout << "If you want to use simplices instead of cubes, install and use dune-ALUGrid or UGGrid." << std::endl;
#endif // !(HAVE_DUNE_ALUGRID || HAVE_UG)

        // initialize the tables for the water properties
        std::cout << "Initializing the tables for the water properties" << std::endl;
        Components::TabulatedComponent<Components::H2O<Scalar>>::init(/*tempMin=*/273.15,
                                                                      /*tempMax=*/623.15,
                                                                      /*numTemp=*/100,
                                                                      /*pMin=*/0.0,
                                                                      /*pMax=*/20e6,
                                                                      /*numP=*/200);

        // set the depth of the bottom of the reservoir
        depthBOR_ = this->fvGridGeometry().bBoxMax()[dimWorld-1];

        // plot density over pressure of the phase consisting of your component
        if(getParam<bool>("Output.PlotDensity"))
            plotDensity_();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the temperature \f$ K \f$
     */
    Scalar temperature() const
    { return 283.15; }

     /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param bcTypes The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
         BoundaryTypes bcTypes;

        if (globalPos[0] < eps_ || globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_)
           bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        // use the initial values as Dirichlet values
        return initialAtPos(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    PrimaryVariables neumannAtPos(const GlobalPosition& globalPos) const
    {
        // initialize values to zero, i.e. no-flow Neumann boundary conditions
        PrimaryVariables values(0.0);

        Scalar up = this->fvGridGeometry().bBoxMax()[dimWorld-1];

        // influx of oil (30 g/m/s) over a segment of the top boundary
        if (globalPos[dimWorld-1] > up - eps_ && globalPos[0] > 20 && globalPos[0] < 40)
        {
            values[contiWEqIdx] = 0;
            values[contiNEqIdx] = -3e-2;
        }
        else
        {
            // no-flow on the remaining Neumann-boundaries.
            values[contiWEqIdx] = 0;
            values[contiNEqIdx] = 0;
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const

    {
        PrimaryVariables values(0.0);

        // use hydrostatic pressure distribution with 2 bar at the top and zero saturation
        values[waterPressureIdx] = 200.0e3 + 9.81*1000*(depthBOR_ - globalPos[dimWorld-1]);
        values[naplSaturationIdx] = 0.0;

        return values;
    }
    // \}

    /*!
     * \brief Returns the source term
     *
     * \param values Stores the source values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} / (m^\textrm{dim} \cdot s )] \f$
     * \param globalPos The global position
     */
    PrimaryVariables sourceAtPos(const GlobalPosition& globalPos) const
    {
        // we define no source term here
        PrimaryVariables values(0.0);
        return values;
    }

private:
    void plotDensity_()
    {
        FluidState fluidState;
        fluidState.setTemperature(temperature());
        int numberOfPoints = 100;
        Scalar xMin = 1e4;
        Scalar xMax = 1e7;
        Scalar spacing = std::pow((xMax/xMin), 1.0/(numberOfPoints-1));
        std::vector<double> x(numberOfPoints);
        std::vector<double> y(numberOfPoints);
        for (int i=0; i<numberOfPoints; ++i)
            x[i] = xMin*std::pow(spacing,i);
        for (int i=0; i<numberOfPoints; ++i)
        {
            fluidState.setPressure(FluidSystem::phase1Idx, x[i]);
            y[i] = FluidSystem::density(fluidState, FluidSystem::phase1Idx);
        }
        gnuplot_.resetPlot();
        gnuplot_.setXRange(xMin, xMax);
        gnuplot_.setOption("set logscale x 10");
        gnuplot_.setOption("set key left top");
        gnuplot_.setYRange(1440, 1480);
        gnuplot_.setXlabel("pressure [Pa]");
        gnuplot_.setYlabel("density [kg/m^3]");
        gnuplot_.addDataSetToPlot(x, y, "YourComponentPhase_density.dat", "w l t 'Phase consisting of your component'");
        gnuplot_.plot("YourComponentPhase_density");
    }

    Scalar eps_; //! small epsilon value
    Scalar depthBOR_; //! depth at the bottom of the reservoir
    Dumux::GnuplotInterface<double> gnuplot_; //! collects data for plotting
};
}

#endif
