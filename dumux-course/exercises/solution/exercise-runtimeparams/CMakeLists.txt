# the runtime parameter exercise simulation program
dune_add_test(NAME exercise_runtimeparams_solution
              SOURCES exercise_runtimeparams_solution.cc
              CMD_ARGS exercise_runtimeparams_solution.input)

# add tutorial to the common target
add_dependencies(test_exercises exercise_runtimeparams_solution)

# add a symlink for the input file
dune_symlink_to_source_files(FILES "exercise_runtimeparams_solution.input")

