// grid size specifications
lc = 5;  // grid size at domain corners
lcf = 3; // grid size at fracture points

// domain corners
Point(1) = {0, 0, 0, lc};
Point(2) = {100, 0, 0, lc};
Point(3) = {100, 100, 0, lc};
Point(4) = {0, 100, 0, lc};

// fracture corners
Point(5) = {5.0000,   41.6000, 0, lcf};
Point(6) = {22.0000,  06.2400, 0, lcf};
Point(7) = {5.0000,   27.5000, 0, lcf};
Point(8) = {25.0000,  13.5000, 0, lcf};
Point(9) = {15.0000,  63.0000, 0, lcf};
Point(10) = {45.0000, 09.0000, 0, lcf};
Point(11) = {15.0000, 91.6700, 0, lcf};
Point(12) = {40.0000, 50.0000, 0, lcf};
Point(13) = {65.0000, 83.3300, 0, lcf};
Point(15) = {70.0000, 23.5000, 0, lcf};
Point(17) = {60.0000, 38.0000, 0, lcf};
Point(18) = {85.0000, 26.7500, 0, lcf};
Point(19) = {35.0000, 97.1400, 0, lcf};
Point(20) = {80.0000, 71.4300, 0, lcf};
Point(21) = {75.0000, 95.7400, 0, lcf};
Point(22) = {95.0000, 81.5500, 0, lcf};
Point(23) = {15.0000, 83.6300, 0, lcf};
Point(24) = {40.0000, 97.2700, 0, lcf};
Point(25) = {15.2174, 20.3478, 0, lcf};
Point(26) = {18.6341, 85.6127, 0, lcf};
Point(27) = {37.3260, 95.8111, 0, lcf};
Point(28) = {66.2058, 79.3111, 0, lcf};
Point(29) = {81.5036, 28.3234, 0, lcf};
Point(30) = {84.9723, 16.7625, 0, lcf};
Point(31) = {60.625, 100.0, 0, lcf};

// domain outline
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 31};
Line(4) = {31, 4};
Line(5) = {4, 1};

// fracture lines
Line(6) = {5, 25};
Line(7) = {7, 25};
Line(8) = {9, 10};
Line(9) = {11, 26};
Line(10) = {13, 28};
Line(11) = {15, 30};
Line(12) = {17, 29};
Line(13) = {19, 27};
Line(14) = {21, 22};
Line(15) = {23, 26};
Line(16) = {6, 25};
Line(17) = {8, 25};
Line(18) = {12, 26};
Line(19) = {26, 27};
Line(20) = {28, 27};
Line(21) = {24, 27};
Line(22) = {29, 28};
Line(23) = {20, 28};
Line(24) = {29, 30};
Line(25) = {18, 29};
Line(26) = {31, 13};

// domain surface
// Note that all entities that have physical indices
// appear in the grid file. Thus, we give the surface
// and all fracture lines physical indices!
Line Loop(1) = {1, 2, 3, 4, 5};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

// make elements conforming to fractures
Line {6:26} In Surface{1};

// conductive fractures get physical index 1
Physical Line(1) = {6,7,9,11,12,13,14,15,16,17,18,19,20,21,23,25};

// blocking fractures get physical index 2
Physical Line(2) = {8,10,22,24,26};
