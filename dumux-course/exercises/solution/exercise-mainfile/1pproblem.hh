// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The one-phase porousmediumflow problem for exercise mainfile
 */
#ifndef DUMUX_EX_MAINFILE_ONEP_TEST_PROBLEM_HH
#define DUMUX_EX_MAINFILE_ONEP_TEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/cellcentered/tpfa/properties.hh>
#include <dumux/discretization/cellcentered/mpfa/properties.hh>
#include <dumux/discretization/box/properties.hh>


// TODO: dumux-course-task
// uncomment the incompressiblelocalresidual which is a specialization of the standard immisible localresidual for one phase incompressible cases and provides an analytic jacobian.
#include <dumux/porousmediumflow/1p/incompressiblelocalresidual.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "1pspatialparams.hh"

namespace Dumux
{
// forward declarations
template<class TypeTag> class OnePTestProblem;

namespace Properties
{
// create the type tag nodes. Here we define the incompressible type tag as well as the compressible type tag. The incompressible uses a different fluidsystem than the compressible
NEW_TYPE_TAG(OnePBase, INHERITS_FROM(OneP));
NEW_TYPE_TAG(OnePIncompressible, INHERITS_FROM(CCTpfaModel, OnePBase));
NEW_TYPE_TAG(OnePCompressible, INHERITS_FROM(CCTpfaModel, OnePBase));

// Set the grid type
SET_TYPE_PROP(OnePBase, Grid, Dune::YaspGrid<2>);

// Set the problem type
SET_TYPE_PROP(OnePBase, Problem, OnePTestProblem<TypeTag>);

// set the spatial params
SET_TYPE_PROP(OnePBase, SpatialParams, OnePTestSpatialParams<TypeTag>);

// the fluid system for incompressible tests
SET_PROP(OnePIncompressible, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::SimpleH2O<Scalar> >;
};

// TODO: dumux-course-task
// set the OneP Incompressible local residual for the OnePIncompressible type tag. This provides an analytic jacobian to be used for the analytic solution. Change that by setting:
SET_TYPE_PROP(OnePIncompressible, LocalResidual, OnePIncompressibleLocalResidual<TypeTag>);


// the fluid system for compressible tests
SET_PROP(OnePCompressible, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::TabulatedComponent<Components::H2O<Scalar>>>;
};

// Disable caching (for testing purposes)
SET_BOOL_PROP(OnePBase, EnableGridVolumeVariablesCache, false);
SET_BOOL_PROP(OnePBase, EnableGridFluxVariablesCache, false);
SET_BOOL_PROP(OnePBase, EnableFVGridGeometryCache, false);

} // end namespace Properties
/*!
 * \ingroup OnePTests
 * \brief  Test problem for the compressible one-phase model:
 * \todo doc me!
 * <tt>./test_box1pfv</tt> or
 * <tt>./test_cc1pfv</tt>
 */
template<class TypeTag>
class OnePTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);

public:
    OnePTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        FluidSystem::Component::init(/*tempMin=*/272.15,
                                     /*tempMax=*/294.15,
                                     /*numTemp=*/10,
                                     /*pMin=*/1.0e4,
                                     /*pMax=*/1.0e6,
                                     /*numP=*/200);
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        Scalar eps = 1.0e-6;
        if (globalPos[dimWorld-1] < eps || globalPos[dimWorld-1] > this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps)
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0);
        values[0] = 1.0e5*(2.0 - globalPos[dimWorld-1]);
        return values;
    }

    /*!
     * \brief Evaluate the initial conditions
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        return PrimaryVariables(1.0e5);
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperature() const
    {
        return 283.15; // 10°C
    }
};

} // end namespace Dumux

#endif
