# DuMu<sup>x</sup> course exercises

Click on the exercise to go the description

* [Exercise on the basics](./exercise-basic/README.md)
* [Exercise on the main file](./exercise-mainfile/README.md)
* [Exercise on runtime parameters](./exercise-runtimeparams/README.md)
* [Exercise on grids](./exercise-grids/README.md)
* [Exercise on properties](./exercise-properties/README.md)
* [Exercise on fluid systems](./exercise-fluidsystem/README.md)
* [Exercise on how to create a new Dune module](./exercise-dunemodule/README.md)
* [Exercise on coupling free and porous medium flow (SFB PA A)](./exercise-coupling-ff-pm/README.md)
* [Exercise on discrete fracture modelling (SFB PA B)](./exercise-fractures/README.md)
* [Exercise on biomineralization (SFB PA C)](./exercise-biomineralization/README.md)
