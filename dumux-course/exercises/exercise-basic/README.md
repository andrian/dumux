# Exercise Basics (DuMuX course)
<br>
## Problem set-up

N$`_2`$ is injected in an aquifer previously saturated with water with an injection rate of 0.0001 kg/(s*m$`^2`$).
The aquifer is situated 2700 m below sea level and the domain size is 60 m x 40 m. It consists of two layers, a moderately permeable one ($`\Omega_1`$) and a lower permeable one ($`\Omega_2`$).

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/exercises/extradoc/exercise1_setup.png" width="1000">

## Preparing the exercise

* Navigate to the directory `dumux-course/exercises/exercise-basic`

This exercise deals with two problems: a two-phase immiscible problem (__2p__) and a two-phase compositional problem (__2p2c__). They both set up the same scenario with the difference that the 2p2c assumes a miscible fluid state for the two fluids (water and gaseous N$`_2`$) and the 2p model assumes an immiscible fluid state.

<br><br>
### Task 1: Getting familiar with the code
<hr>

Locate all the files you will need for this exercise
* The __main file__ for the __2p__ problem : `exercise_basic_2p.cc`
* The __main file__ for the __2p2c__ problem : `exercise_basic_2p2c.cc`
* The __problem file__ for the __2p__ problem: `injection2pproblem.hh`
* The __problem file__ for the __2p2c__ problem: `injection2p2cproblem.hh`
* The shared __spatial parameters file__: `injection2pspatialparams.hh`
* The shared __input file__: `exercise_basic.input`

<hr><br><br>
### Task 2: Compiling and running an executable
<hr>

* Change to the build-directory

```bash
cd ../../build-cmake/exercises/exercise-basic
```

* Compile both executables `exercise_basic_2p` and `exercise_basic_2p2c`

```bash
make exercise_basic_2p exercise_basic_2p2c
```

* Execute the two problems and inspect the result

```bash
./exercise_basic_2p exercise_basic.input
./exercise_basic_2p2c exercise_basic.input
```

* you can look at the results with paraview

```bash
paraview injection-2p2c.pvd
```

<hr><br><br>
### Task 3: Setting up a new executable (for a non-isothermal simulation)
<hr>

* Copy the main file `exercise_basic_2p.cc` and rename it to `exercise_basic_2pni.cc`
* In  `exercise_basic_2pni.cc`, include the header `injection2pniproblem.hh` instead of `injection2pproblem.hh`.
* In  `exercise_basic_2pni.cc`, change `Injection2pCCTypeTag` to `Injection2pNICCTypeTag` in the line `using TypeTag = TTAG(Injection2pCCTypeTag);`
* Add a new executable in `CMakeLists.txt` by adding the lines

```cmake
# the two-phase non-isothermal simulation program
dune_add_test(NAME exercise_basic_2pni
              SOURCES exercise_basic_2pni.cc
              CMD_ARGS exercise_basic.input)
```

* Test that everything compiles without error

```bash
make # should rerun cmake
make exercise_basic_2pni # builds new executable
```

<hr><br><br>
### Task 4: Setting up a non-isothermal __2pni__ test problem
<hr>

* Open the file `injection2pniproblem.hh`. It is a copy of the `injection2pproblem.hh` with some useful comments on how to implement a non-isothermal model. Look for comments containing

```c++
// TODO: dumux-course-task
```

* The following set-up should be realized:

  __Boundary conditions:__ Dirichlet conditions for the temperature with a temperature gradient of 0.03 K/m and a starting temperature of 283 K.

  __Initial conditions:__ The same temperature gradient as in the boundary conditions with an additional lens (with position: 20 < x < 30, 5 < y < 35), which has an initial temperature of 380 K.

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/exercises/extradoc/exercise1_nonisothermal.png" width="800">

The non-isothermal model requires additional parameters like the thermal conductivity of the solid component. They are already implemented and set in `exercise_basic.input`, you just need to _uncomment_ them.
