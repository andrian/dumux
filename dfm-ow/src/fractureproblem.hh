// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \ingroup MultiDomain
  * \ingroup MultiDomainFacet
  * \ingroup TwoPTests
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH

// we use alu grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>

// we want to simulate nitrogen gas transport in a water-saturated medium
//#include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/components/mysimpleoil.hh> 
#include <dumux/material/components/mysimpleh2o.hh>


// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
//#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"

// VTK include files to read the restarts from VTK
/*
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
*/

namespace Dumux {
// forward declarations
template<class TypeTag> class FractureSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag nodes
NEW_TYPE_TAG(FractureProblemTypeTag, INHERITS_FROM(CCTpfaModel, TwoP));

// Set the grid type
SET_TYPE_PROP(FractureProblemTypeTag, Grid, Dune::FoamGrid<1, 2>);

// Set the problem type
SET_TYPE_PROP(FractureProblemTypeTag, Problem, FractureSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(FractureProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(FractureProblemTypeTag,
              SpatialParams,
              FractureSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                     typename GET_PROP_TYPE(TypeTag, Scalar) >);
// the fluid system
//SET_TYPE_PROP(FractureProblemTypeTag,
//              FluidSystem,
//              Dumux::FluidSystems::H2ON2< typename GET_PROP_TYPE(TypeTag, Scalar),
//                                          FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/true> >);

// Set the fluid system
SET_PROP(FractureProblemTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::SimpleH2O<Scalar> >;
    using NonwettingPhase = FluidSystems::OnePLiquid<Scalar, Components::Trichloroethene<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};



} // end namespace Properties

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
	using GridIndexType = typename GridView::IndexSet::IndexType;

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem); 
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    
    enum
    {
    	wPhaseIdx = FluidSystem::phase0Idx,	// wetting phase index
        nPhaseIdx = FluidSystem::phase1Idx
    };
    
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx
    };
    
    // formulations
    static constexpr auto p0s1 = TwoPFormulation::p0s1;
    static constexpr auto p1s0 = TwoPFormulation::p1s0;

    // the formulation that is actually used
    static constexpr auto formulation = ModelTraits::priVarFormulation();    

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                       const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, dir_(getParamFromGroup<Scalar>(paramGroup, "Problem.Direction"))
    , boundaryRightPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryRightPressure"))
    , Swr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"))
    //, aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    {        
		// Infer the type of left boundary conditions from input data
		bool bcSet = false;
		boundaryLeftRate_ = 1e100;
		try {	
			// If BoundaryLeftRate is present then left boundary is Neumann with the corresponding rate
			boundaryLeftRate_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftRate");		
			boundaryLeftDirichlet = false;
			bcSet = true;
			std::cout << "\nUsing Neumann conditions at the left boundary..\n" << std::endl;
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided for the left boundary
			//std::cout << e.what(); 
			try {	
				boundaryLeftPressure_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftPressure");
				boundaryLeftSaturation_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftSaturation");
				boundaryLeftDirichlet = true;		
				bcSet = true;
				std::cout << "\nUsing Dirichlet conditions at the left boundary..\n" << std::endl;
			} catch (const std::exception& e) {	}			
		}
		
		if (!bcSet) 
		{				
			std::cout << "\nNeither Dirichlet nor Neumann conditions are provided for the left boundary..\n" << std::endl;
			throw Dune::NotImplemented();
		}
			
		
		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();
        
		
        // Warn that p1s0 formulation is needed to set up constant pressure BC
        if (formulation != p1s0) {
            std::cout << "\nWARNING: Please set up the p1s0 formulation in porousmediumflow/2p/model.hh\n" << std::endl;
        //    assert(formulation == p1s0);
        }
        
		// Get the Dune indices of elements' scv's (apparently = original gmsh indices - 1) in order to set the variable apertures
		GridIndexType N = 0;
		std::vector<int> indElem;
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            for (auto&& scv : scvs(fvGeometry))
            {				
				// Ensure that the Dune indices are consecutive from 0 
				if (scv.elementIndex() == N)
					indElem.push_back(N);
				else 
				{
					std::cout << "ERROR in numbering: element #" << scv.elementIndex() << " is not consecutive (<>" << N << ")" << std::endl;
					DUNE_THROW(Dune::NotImplemented, "DUNE indices not consecutive");
				}
				N++;
			}
		}
		
		// Read the aperture(s) - either a single constant value, or a file with consecutive values
		aperture.resize(N);
		std::string fileName = getParamFromGroup<std::string>(paramGroup, "SpatialParams.Aperture");
		std::ifstream file( fileName.c_str() );			// Try to read a file with variable apertures
		if (file)
		{
			// Fill the array of apertures with the values from fileName
			std::string str;
			GridIndexType i = 0;
			while (!file.eof())
			{
				std::getline(file, str);
				aperture[i] = atof(str.c_str());
				i++;				
			}
			file.close();  
			std::cout << "\nUsing variable fracture segments' apertures from " << fileName << "\n" << std::endl;
		}
		else
		{
			// Check whether a single aperture value is provided
			try {	
				Scalar constAperture = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture");
				aperture.assign(N, constAperture);
				std::cout << "\nUsing constant fracture apertures..\n" << std::endl;
			} catch (const std::exception& e) {	
				DUNE_THROW(Dune::InvalidStateException, "Unrecognized format for fractures aperture!!");
			}			
		}
		
        
    }
    
    
    // NA: get the OIP and WIP
    // analogously to postTimeStep from richardsnc    
    void getVolumesFluxes(std::vector<Scalar>& Volume, std::vector<Scalar>& InFlux, std::vector<Scalar>& OutFlux,                           
    			  const SolutionVector& curSol,
                          const GridVariables& gridVariables)
    {    	
    	std::fill(Volume.begin(), Volume.end(), 0.);
    	std::fill(InFlux.begin(), InFlux.end(), 0.);
    	std::fill(OutFlux.begin(), OutFlux.end(), 0.);     
        
        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            //fvGeometry.bindElement(element);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            //int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = x[eIdx];
            
            VolumeVariables volVars;

            for (auto&& scv : scvs(fvGeometry))
            {
                //const auto& volVars = elemVolVars[scv];
                volVars = elemVolVars[scv];
                
                // volVars.extrusionFactor() is the fracture aperture
                // scv.volume() is the length of the fracture segment
                // Assuming that extrusionFactor() == 1 for the matrix, get the correct volume of the fracture segment
                Volume[nPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(nPhaseIdx);
                Volume[wPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(wPhaseIdx);

            }

            // Loop through the faces 
            // see porousmediumflow/velocityoutput.hh and porousmediumflow/nonequilibrium/gridvariables.hh
            // bind the element flux variables cache

            // the upwind term to be used for the volume flux evaluation
            auto upwindTermN = [](const auto& volVars) { return volVars.mobility(nPhaseIdx); };
            auto upwindTermW = [](const auto& volVars) { return volVars.mobility(wPhaseIdx); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {

            	if (scvf.boundary()) {

                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();
                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar fn = fluxVars.advectiveFlux(nPhaseIdx, upwindTermN); 
                        Scalar fw = fluxVars.advectiveFlux(wPhaseIdx, upwindTermW); 
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);                        
                    }
                    else if (bcTypes.hasOnlyNeumann()) {
                        NumEqVector flux = this->neumannAtPos(scvf.center());   
                        Scalar fn = flux[nPhaseIdx];
                        Scalar fw = flux[wPhaseIdx];
                        
                        fn *= scvf.area() * extrusionFactor / volVars.density(nPhaseIdx);   // Convert to volumetric flux
                        fw *= scvf.area() * extrusionFactor / volVars.density(wPhaseIdx);   // Convert to volumetric flux
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);                          
                    }
                    else {
                        DUNE_THROW(Dune::NotImplemented, "fractureproblem.hh: boundary condition not defined..");
                    }
                    
                }
                
            }
                        
        }

    }  
    
    
    // Compute the pore volume occupied by fractures
    Scalar poreVolume(const SolutionVector& curSol, const GridVariables& gridVariables)
    {
        Scalar PV = 0.0;

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                
                // volVars.extrusionFactor() is the fracture aperture
                // scv.volume() is the length of the fracture segment
                // Assuming that extrusionFactor() == 1 for the matrix, get the correct volume of the fracture segment
                PV += scv.volume() * volVars.porosity() * volVars.extrusionFactor();
            }
        }
        
        return PV;
    }
    
    /*!
         * \brief Restarting the matrix solution using the VTU file restartFilename
    */

    void readRestartSolution(SolutionVector& sol, const std::string& restartFilename)
    {
    	// Identify the fields to be initialized
		std::string pressure, saturation;
		std::vector<Scalar> P;
		std::vector<Scalar> S;
		if (formulation == p0s1) {
			// In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
			//    values[pressureIdx] is interpreted as wetting phase pressure
			//    values[saturationIdx] is interpreted as non-wetting phase saturation
			pressure = "p_w";
			saturation = "S_n";
		}
		else if (formulation == p1s0) {
			// In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
			//    values[pressureIdx] is interpreted as non-wetting phase pressure
			//    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
			pressure = "p_w";
			saturation = "S_n";
		}
		else {
			std::cout << "readRestartSolution: formulation not implemented.." << std::endl;

		}

		// Read the restart file
		std::ifstream file( restartFilename.c_str() );
		if (!file)
		{
			std::cerr << "ERROR: couldn't open file `" << restartFilename << "'" << std::endl;
		}
		file.close();

		/*
		std::cout << "Restarting fracture solution using " << restartFilename.c_str() << std::endl;
		vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();

		reader->SetFileName(restartFilename.c_str());
		reader->Update();

		vtkUnstructuredGrid* ugrid = reader->GetOutput();
		unsigned int cellNumber = ugrid->GetNumberOfCells();
		//std::cout << "There are " << cellNumber << " input cells." << std::endl;

		vtkCellData *cellData = ugrid->GetCellData();
		for (int i = 0; i < cellData->GetNumberOfArrays(); i++)
		{
			vtkDataArray* data = cellData->GetArray(i);
			//cout << "name: " << data->GetName() << endl;

			// Reading pressures
			if (strcmp(pressure.c_str(), data->GetName()) == 0) {
				for (int j = 0; j < data->GetNumberOfTuples(); j++)
				{
					P.push_back(data->GetTuple1(j));
				}
			}

			// Reading saturations
			if (strcmp(saturation.c_str(), data->GetName()) == 0) {
				for (int j = 0; j < data->GetNumberOfTuples(); j++)
				{
					S.push_back(data->GetTuple1(j));
				}
			}
		}

*/
		// Set the solution with corresponding values
		int i = 0;
		for (const auto& element : elements(this->fvGridGeometry().gridView()))
		{
			const auto dofIdxGlobal = this->fvGridGeometry().elementMapper().index(element);

			PrimaryVariables values;
			values[pressureIdx]   = P[i];
			values[saturationIdx] = S[i];
			i++;

			sol[dofIdxGlobal] = values;
		}
    }
   
    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // We only use no-flow boundary conditions for all immersed fractures
        // in the domain (fracture tips that do not touch the domain boundary)
        // Otherwise, we would lose mass leaving across the fracture tips.
        values.setAllNeumann();

        // However, there is one fracture reaching the top boundary. For this
        // fracture tip we set Dirichlet Bcs as in the matrix domain
        // TODO Task A: Change boundary conditions
        
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];	
		
		if (boundaryLeftDirichlet) 
		{
			// Dirichlet at the left and at the right
			values.setAllNeumann();
			if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}
		else 
		{
			// Neumann at the left, Dirichlet at the right
			values.setAllNeumann();        
			if (globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}

        return values;
                
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

//     //! Set the aperture as extrusion factor.
//     Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
//     {
//         // We treat the fractures as lower-dimensional in the grid,
//         // but we have to give it the aperture as extrusion factor
//         // such that the dimensions are correct in the end.
//         return aperture_;
//     }

	template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {        
        	
		// forward to generic interface		
		//asImp_().extrusionFactorAtPos(scv.center());
		
		// For debugging
		//GlobalPosition center = scv.center();
		//std::cout << "scv #" << scv.elementIndex() << " with center at (" << center[0] << ", " << center[1] << ")" << std::endl;		
		//return aperture_;	
		
		GridIndexType i = scv.elementIndex();
		Scalar a = aperture[i];
		
		return aperture[ scv.elementIndex() ];	
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {  
        PrimaryVariables values;
 
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];	

        if (globalPos[dir_] < xmin + 1e-6) {
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryLeftPressure_;            // assign the oil pressure boundaryLeftPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - boundaryLeftSaturation_;   	// Oil saturation                
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryLeftPressure_ ;
                values[saturationIdx] = boundaryLeftSaturation_; 
            }
        }

        
        // Fixed right pressure 
        if (globalPos[dir_] > xmax - 1e-6) {
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryRightPressure_;   // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - Swr_;            
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryRightPressure_ ;
                values[saturationIdx] = Swr_; 
            }
            else {
                std::cout << "dirichletAtPos: formulation not implemented.." << std::endl;
                
            }
            
        }
        
        return values;
        
    }
    

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
        
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];

        if (globalPos[dir_] < xmin + 1e-6) {
            values[nPhaseIdx] =  -1e-100; // kg/(m*s) - Apparently need to set a non-zero value
            values[wPhaseIdx] = boundaryLeftRate_; // kg/(m*s)
        }

        return values;
    }
    
        

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        
        PrimaryVariables values;
        
        // For the grid used here, the height of the domain is equal
        // to the maximum y-coordinate
        //const auto domainHeight = this->fvGridGeometry().bBoxMax()[1];
        // we assume a constant water density of 1000 for initial conditions!
        //const auto& g = this->gravityAtPos(globalPos);        
        //Scalar densityW = 1000.0;
        //values[pressureIdx] = 1e5; // - (domainHeight - globalPos[1])*densityW*g[1];
        //values[saturationIdx] = 0.5; //1 - Swr_;	// 0.0;
        
        /*
        // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
        //    values[pressureIdx] is interpreted as non-wetting phase pressure
        //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
        values[pressureIdx] = boundaryRightPressure_ ;
        values[saturationIdx] = Swr_; //0.5;    // Swr_; 
        
        return values;
        */

        
        if (formulation == p0s1) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as wetting phase pressure
            //    values[saturationIdx] is interpreted as non-wetting phase saturation  
            values[pressureIdx] = boundaryRightPressure_ ;  // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
            values[saturationIdx] = 1 - Swr_;	
        }
    	else if (formulation == p1s0) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as non-wetting phase pressure
            //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
            values[pressureIdx] = boundaryRightPressure_ ;
            values[saturationIdx] = Swr_; 
        }
    	else {
            std::cout << "initialAtPos: formulation not implemented.." << std::endl;
    	}

        return values;          
        
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    

    void evalRelPerms() const
    {	    
		
		std::string filename( "fracture_relperms.csv");
		std::ofstream file( filename.c_str() );
		if (file) {	
			const char *header = "sw,"
                             "krw,"
                             "krn";
			file << header << std::endl;        			
		}
		else
		{
			std::cerr << "ERROR: couldn't open file `" << filename << "'" << std::endl;
			
		}		
		
		GlobalPosition globalPos({0.0, 0.0});
		const auto& materialParams = this->spatialParams().materialLawParamsAtPos(globalPos);
		using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
		
		int N = 100;
		Scalar ds = 1. / (N+1);
		Scalar sw = 0;
		Scalar krw, krn;
		for (int n = 0; n < N; n++) {
			krw = MaterialLaw::krw(materialParams, sw);
			krn = MaterialLaw::krn(materialParams, sw);		
			sw += ds;
			
			file << sw  << "," <<              
                    krw << "," <<   
                    krn << 
                    std::endl;	
		}
		file.close();		
	}
	    
        
    

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	bool boundaryLeftDirichlet;
	
	int dir_;
	Scalar boundaryLeftPressure_;
    Scalar boundaryLeftSaturation_;    
    Scalar boundaryRightPressure_;
	Scalar boundaryLeftRate_;	
	
    Scalar Swr_;
    //Scalar aperture_;
	std::vector<Scalar> aperture;	

    
};

} // end namespace Dumux

#endif
