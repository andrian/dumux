import os
from shutil import copyfile

destdir = '.'
srcdir = '/home/nikolai/dumux/rate-sensitivity/build-cmake-O3/src/Laegerdorf'

destfname = 'dfm-ow'
srcfname = 'rate_sensitivity'

sectors = ['anisotropic', 'highfrac', 'isolated']
apertures = ['apert_1e-3', 'apert_1e-4', 'apert_1e-5']
rates = ['Q1-6', 'Q2-6', 'Q4-6']

# Create the folder structure
for sector in sectors:
    path = destdir + '/' + sector
    if not(os.path.isdir(path)):
        os.mkdir(path)

    for apert in apertures:
        path = destdir + '/' + sector + '/' + apert
        if not(os.path.isdir(path)):
            os.mkdir(path)

        for rate in rates:
            #print(apert + ' ' + rate)
            copypath = path + '/' + rate
            if not(os.path.isdir(copypath)):
                os.mkdir(copypath)

                # Copy the .input ...
                src = srcdir + '/' + copypath + '/' + srcfname + '.input'
                if os.path.isfile(src):
                    dest = destdir + '/' + copypath + '/' + destfname  + '.input'
                    copyfile(src, dest)
                    print('Copied ' + src + '  --->  ' + dest)
                else:
                    print('WARNING: ' + src + ' does not exist! Skip copying..')

                # ... and mesh files from corresponding subfolders of srcdir
                src = srcdir + '/' + copypath + '/' + sector + '.msh'
                if os.path.isfile(src):
                    dest = destdir + '/' + copypath + '/' + sector + '.msh'
                    copyfile(src, dest)
                    print('Copied ' + src + '  --->  ' + dest)
                else:
                    print('WARNING: ' + src + ' does not exist! Skip copying..')


# for subdir, dirs, files in os.walk(destdir):
#     for dir in dirs:
#         path = subdir + '/' + dir
#         print(subdir + '/' + dir)

