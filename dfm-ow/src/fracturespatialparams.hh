// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial parameters for the fracture sub-domain in the exercise
 *        on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH

#include <dumux/io/grid/griddata.hh>

#include <dumux/material/spatialparams/fv.hh>

#include "fluidmatrixinteractions/regularizedbrookscorey.hh"
#include "fluidmatrixinteractions/efftoabslaw.hh"

namespace Dumux {

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class FractureSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, FractureSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = FractureSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;

    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
	using GridIndexType = typename GridView::IndexSet::IndexType;

    // use a regularized van-genuchten material law
    //using EffectiveLaw = RegularizedVanGenuchten<Scalar>;
    // use a Brooks-Corey rel perms and polynomially fitted capillary pressure curve
    //using EffectiveLaw = MyBrooksCorey<Scalar>;

    // we identify those fractures as barriers, that have a domain marker
    // of 2. This is what is set in the grid file (see grids/complex.geo)
    static constexpr int barriersDomainMarker = 2;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
	using MaterialLaw = EffToAbsLaw<RegularizedBrooksCorey<Scalar>>;
    using MaterialLawParams = typename MaterialLaw::Params;

    //! the constructor
    FractureSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          std::shared_ptr<const Dumux::GridData<Grid>> gridData,
                          const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        //permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");

		materialLawParams_.setKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.krw"));
		materialLawParams_.setKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.krn"));
		materialLawParams_.setNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.nw"));		
		materialLawParams_.setNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.nn"));
		materialLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));
		materialLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
		
//        porosityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Porosity");
//        permeabilityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Permeability");
//
//		materialLawParamsBarrier_.setKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_krw"));
//		materialLawParamsBarrier_.setKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_krn"));
//		materialLawParamsBarrier_.setNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_nw"));
//		materialLawParamsBarrier_.setNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_nn"));
//		materialLawParamsBarrier_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Swr"));
//		materialLawParamsBarrier_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));


		// Get the Dune indices of elements' scv's (apparently = original gmsh indices - 1) in order to set the variable apertures
		GridIndexType N = 0;
		std::vector<int> indElem;
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            for (auto&& scv : scvs(fvGeometry))
            {
				// Ensure that the Dune indices are consecutive from 0
				if (scv.elementIndex() == N)
					indElem.push_back(N);
				else
				{
					std::cout << "ERROR in numbering: element #" << scv.elementIndex() << " is not consecutive (<>" << N << ")" << std::endl;
					DUNE_THROW(Dune::NotImplemented, "DUNE indices not consecutive");
				}
				N++;
			}
		}

		// Read the permeabilities - either a single constant value, or a file with consecutive values
        permeability_.resize(N);
		std::string fileName = getParamFromGroup<std::string>(paramGroup, "SpatialParams.Permeability");
		std::ifstream file( fileName.c_str() );			// Try to read a file with variable permeabilities
		if (file)
		{
			// Fill the array of apertures with the values from fileName
			std::string str;
			GridIndexType i = 0;
			while (!file.eof())
			{
				std::getline(file, str);
				permeability_[i] = atof(str.c_str());
				i++;
			}
			file.close();
			std::cout << "\nUsing variable fracture segments' permeabilities from " << fileName << "\n" << std::endl;
		}
		else
		{
			// Check whether a single permeability value is provided
			try {
				Scalar constPerm = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
				permeability_.assign(N, constPerm);
				std::cout << "\nUsing constant fracture permeabilities..\n" << std::endl;
			} catch (const std::exception& e) {
				DUNE_THROW(Dune::InvalidStateException, "Unrecognized format for fracture permeabilities!!");
			}
		}
		
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template< class ElementSolution >
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
		//GridIndexType i = scv.elementIndex();
		//Scalar a = permeability_[i];
		return permeability_[ scv.elementIndex() ];
    }

    //! Return the porosity
    template< class ElementSolution >
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        // TODO Task B: change fracture properties
        return porosity_;
    }

    //! Return the material law parameters
    template< class ElementSolution >
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        // TODO Task B: change fracture properties
        return materialLawParams_;
    }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the H2oN2 fluid system
        return FluidSystem::phase0Idx;
    }
    
    // NA
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        return materialLawParams_;
    }
    

    //! returns the domain marker for an element
    int getElementDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    Scalar porosity_;
    std::vector<Scalar> permeability_;
    MaterialLawParams materialLawParams_;

//    Scalar porosityBarrier_;
//    PermeabilityType permeabilityBarrier_;
//    MaterialLawParams materialLawParamsBarrier_;
};

} // end namespace Dumux

#endif
