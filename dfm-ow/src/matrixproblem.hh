// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// we want to simulate nitrogen gas transport in a water-saturated medium
//#include <dumux/material/fluidsystems/h2on2.hh>
// NA: we want to simulate 2 phase immiscible probles
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/components/mysimpleoil.hh> 
#include <dumux/material/components/mysimpleh2o.hh>

// Use Darcy's law which does not include the contributions from capillary forces for boundary fluxes.
// The implementation explicitly calls for pressure component depending on the current formulation.
#include "discretization/properties.hh"
// The original include file which does include contributions from capillary forces for boundary fluxes.
//#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
//#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

// VTK include files to read the restarts from VTK
/* 
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellData.h>
*/

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the matrix sub-problem
// We need to put the facet-coupling type tag after the physics-related type tag
// because it overwrites some of the properties inherited from "TwoP". This is
// necessary because we need e.g. a modified implementation of darcys law that
// evaluates the coupling conditions on faces that coincide with the fractures.
NEW_TYPE_TAG(MatrixProblemTypeTag, INHERITS_FROM(TwoP, CCTpfaFacetCouplingModel));

// Set the grid type
SET_TYPE_PROP(MatrixProblemTypeTag, Grid, Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>);

// Set the problem type
SET_TYPE_PROP(MatrixProblemTypeTag, Problem, MatrixSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(MatrixProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(MatrixProblemTypeTag,
              SpatialParams,
              MatrixSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);
// the fluid system
//SET_TYPE_PROP(MatrixProblemTypeTag,
//              FluidSystem,
//              Dumux::FluidSystems::H2ON2< typename GET_PROP_TYPE(TypeTag, Scalar),
//                                          FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/true> >);


// Set the fluid system
SET_PROP(MatrixProblemTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::SimpleH2O<Scalar> >;
    using NonwettingPhase = FluidSystems::OnePLiquid<Scalar, Components::Trichloroethene<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};

} // end namespace Properties

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;	
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

    enum
    {
    	wPhaseIdx = FluidSystem::phase0Idx,	// wetting phase index
        nPhaseIdx = FluidSystem::phase1Idx
    };    
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx
    };
    
    // formulations
    static constexpr auto p0s1 = TwoPFormulation::p0s1;
    static constexpr auto p1s0 = TwoPFormulation::p1s0;

    // the formulation that is actually used
    static constexpr auto formulation = ModelTraits::priVarFormulation();  

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, dir_(getParamFromGroup<Scalar>(paramGroup, "Problem.Direction"))
    , boundaryRightPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryRightPressure"))
    , Swr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"))
    {        
		// Infer the type of left boundary conditions from input data
		bool bcSet = false;
		boundaryLeftRate_ = 1e100;
		try {	
			// If BoundaryLeftRate is present then left boundary is Neumann with the corresponding rate
			boundaryLeftRate_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftRate");		
			boundaryLeftDirichlet = false;
			bcSet = true;
			std::cout << "\nUsing Neumann conditions at the left boundary..\n" << std::endl;
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided for the left boundary
			//std::cout << e.what(); 
			try {	
				boundaryLeftPressure_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftPressure");
				boundaryLeftSaturation_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftSaturation");
				boundaryLeftDirichlet = true;		
				bcSet = true;
				std::cout << "\nUsing Dirichlet conditions at the left boundary..\n" << std::endl;
			} catch (const std::exception& e) {	}			
		}
		
		if (!bcSet) 
		{				
			std::cout << "\nNeither Dirichlet nor Neumann conditions are provided for the left boundary..\n" << std::endl;
			throw Dune::NotImplemented();
		}
			
		
		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();
        
        // Warn that p1s0 formulation is needed to set up constant pressure BC
        if (formulation != p1s0) {
            std::cout << "\nWARNING: Please set up the p1s0 formulation in porousmediumflow/2p/model.hh\n" << std::endl;
            //assert(formulation == p1s0);
        }
    }


    // NA: get the OIP and WIP
    // analogously to postTimeStep from richardsnc
    void getVolumesFluxes(std::vector<Scalar>& Volume, std::vector<Scalar>& InFlux, std::vector<Scalar>& OutFlux, 
    			  const SolutionVector& curSol,
                          const GridVariables& gridVariables)
    {    	
    	std::fill(Volume.begin(), Volume.end(), 0.);
    	std::fill(InFlux.begin(), InFlux.end(), 0.);
    	std::fill(OutFlux.begin(), OutFlux.end(), 0.);
        
        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            //fvGeometry.bindElement(element);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            //int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = x[eIdx];

            VolumeVariables volVars;

            for (auto&& scv : scvs(fvGeometry))
            {
                //const auto& volVars = elemVolVars[scv];
            	volVars = elemVolVars[scv];

                Scalar ef = volVars.extrusionFactor();

                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }

                Volume[nPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(nPhaseIdx);
                Volume[wPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(wPhaseIdx);

            }

            // Loop through the faces 
            // see porousmediumflow/velocityoutput.hh and porousmediumflow/nonequilibrium/gridvariables.hh
            // bind the element flux variables cache

            // the upwind term to be used for the volume flux evaluation
            auto upwindTermN = [](const auto& volVars) { return volVars.mobility(nPhaseIdx); };
            auto upwindTermW = [](const auto& volVars) { return volVars.mobility(wPhaseIdx); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {

            	if (scvf.boundary()) {

                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();
                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar fn = fluxVars.advectiveFlux(nPhaseIdx, upwindTermN);
                        Scalar fw = fluxVars.advectiveFlux(wPhaseIdx, upwindTermW);
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);                        
                    }
                    else if (bcTypes.hasOnlyNeumann()) {
                        NumEqVector flux = this->neumannAtPos(scvf.center());     
                        Scalar fn = flux[nPhaseIdx];
                        Scalar fw = flux[wPhaseIdx];
                        
                        fn *= scvf.area() * extrusionFactor / volVars.density(nPhaseIdx);   // Convert to volumetric flux
                        fw *= scvf.area() * extrusionFactor / volVars.density(wPhaseIdx);   // Convert to volumetric flux
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);                          
                    }
                    else {
                        DUNE_THROW(Dune::NotImplemented, "matrixproblem.hh: boundary condition not defined..");
                    }
                    
                }
                
            }
                        
        }

    }  
    
    Scalar poreVolume(const SolutionVector& curSol, const GridVariables& gridVariables)
    {
        Scalar PV = 0.0;

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                Scalar ef = volVars.extrusionFactor();
                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }
                PV += scv.volume() * volVars.porosity() * volVars.extrusionFactor();
            }
        }
        
        return PV;
    }
    

    /*!
     * \brief Restarting the matrix solution using the VTU file restartFilename
     */
    void readRestartSolution(SolutionVector& sol, const std::string& restartFilename)
    {
		// Identify the fields to be initialized
    	std::string pressure, saturation;
    	std::vector<Scalar> P;
    	std::vector<Scalar> S;
        if (formulation == p0s1) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as wetting phase pressure
            //    values[saturationIdx] is interpreted as non-wetting phase saturation
        	pressure = "p_w";
        	saturation = "S_n";
        }
        else if (formulation == p1s0) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as non-wetting phase pressure
            //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
        	pressure = "p_w";
        	saturation = "S_n";
        }
        else {
            std::cout << "readRestartSolution: formulation not implemented.." << std::endl;

        }

    	// Read the restart file
    	std::ifstream file( restartFilename.c_str() );
		if (!file)
		{
			std::cerr << "ERROR: couldn't open file `" << restartFilename << "'" << std::endl;
		}
		file.close();

		/*
		std::cout << "Restarting matrix solution using " << restartFilename.c_str() << std::endl;
		vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();

		reader->SetFileName(restartFilename.c_str());
		reader->Update();

		vtkUnstructuredGrid* ugrid = reader->GetOutput();
		unsigned int cellNumber = ugrid->GetNumberOfCells();
		//std::cout << "There are " << cellNumber << " input cells." << std::endl;

		vtkCellData *cellData = ugrid->GetCellData();
		for (int i = 0; i < cellData->GetNumberOfArrays(); i++)
		{
			vtkDataArray* data = cellData->GetArray(i);
			//cout << "name: " << data->GetName() << endl;

			// Reading pressures
			if (strcmp(pressure.c_str(), data->GetName()) == 0) {
				for (int j = 0; j < data->GetNumberOfTuples(); j++)
				{
					P.push_back(data->GetTuple1(j));
				}
			}

			// Reading saturations
			if (strcmp(saturation.c_str(), data->GetName()) == 0) {
				for (int j = 0; j < data->GetNumberOfTuples(); j++)
				{
					S.push_back(data->GetTuple1(j));
				}
			}
		}
		*/

        // Set the solution with corresponding values
		int i = 0;
		for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto dofIdxGlobal = this->fvGridGeometry().elementMapper().index(element);

            PrimaryVariables values;
            values[pressureIdx]   = P[i];
            values[saturationIdx] = S[i];
            i++;

            sol[dofIdxGlobal] = values;
        }
    }


    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {

    	BoundaryTypes values;

        // we consider buoancy-driven upwards migration of nitrogen and set
        // Dirichlet BCs on the top and bottom boundary
        // TODO Task A: Change boundary conditions and Dirichlet values!

		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];
		
		if (boundaryLeftDirichlet) 
		{
			// Dirichlet at the left and at the right
			values.setAllNeumann();
			if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}
		else 
		{
			// Neumann at the left, Dirichlet at the right
			values.setAllNeumann();        
			if (globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}
		
		return values;
		
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated. If this
        // permeability is lower than that of the matrix, this approach is able to
        // represent the resulting pressure jump across the fracture. If Dirichlet is set,
        // the pressure jump across the fracture is neglected and the pressure inside
        // the fracture is directly applied at the interface between fracture and matrix.
        // This assumption is justified for highly-permeable fractures, but lead to erroneous
        // results for low-permeable fractures.
        // Here, we consider "open" fractures for which we cannot define a normal permeability
        // and for which the pressure jump across the fracture is neglectable. Thus, we set
        // the interior boundary conditions to Dirichlet.
        // IMPORTANT: Note that you will never be asked to set any values at the interior boundaries!
        //            This simply chooses a different interface condition!
        // TODO Task C: Change coupling conditions!
        values.setAllDirichlet();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;

		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];	

        if (globalPos[dir_] < xmin + 1e-6) {
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryLeftPressure_;            // assign the oil pressure boundaryLeftPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - boundaryLeftSaturation_;   	// Oil saturation                
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryLeftPressure_ ;
                values[saturationIdx] = boundaryLeftSaturation_; 
            }
        }

        
        // Fixed right pressure 
        if (globalPos[dir_] > xmax - 1e-6) {
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryRightPressure_;   // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - Swr_; 
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryRightPressure_ ;
                values[saturationIdx] = Swr_; 
            }
            else {
                std::cout << "dirichletAtPos: formulation not implemented.." << std::endl;
                
            }

        }
        
        return values;
        
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
        
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];

        if (globalPos[dir_] < xmin + 1e-6) {
        	values[nPhaseIdx] =  -1e-100; // kg/(m*s) - Apparently need to set a non-zero value
            values[wPhaseIdx] = boundaryLeftRate_; // kg/(m*s)
        }

        return values;
    }
    
    
    

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {

        PrimaryVariables values;

        /*
        // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
        //    values[pressureIdx] is interpreted as non-wetting phase pressure
        //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
        values[pressureIdx] = boundaryRightPressure_ ;
        values[saturationIdx] = Swr_; //0.5;    // Swr_; 
        
        return values;
        */
        
        if (formulation == p0s1) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as wetting phase pressure
            //    values[saturationIdx] is interpreted as non-wetting phase saturation  
            values[pressureIdx] = boundaryRightPressure_ ;  // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
            values[saturationIdx] = 1 - Swr_;	
        }
    	else if (formulation == p1s0) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as non-wetting phase pressure
            //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
            values[pressureIdx] = boundaryRightPressure_ ;
            values[saturationIdx] = Swr_; 
        }
    	else {
            std::cout << "initialAtPos: formulation not implemented.." << std::endl;
    	}

        return values;    
        
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    
    

    void evalRelPerms() const
    {	    
		
		std::string filename( "matrix_relperms.csv");
		std::ofstream file( filename.c_str() );
		if (file) {	
			const char *header = "sw,krw,krn,pc";
			file << header << std::endl;        			
		}
		else
		{
			std::cerr << "ERROR: couldn't open file `" << filename << "'" << std::endl;
		}		
		
		GlobalPosition globalPos({0.0, 0.0});
		const auto& materialParams = this->spatialParams().materialLawParamsAtPos(globalPos);
		using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
		
		int N = 100;
		Scalar ds = 1. / (N+1);
		Scalar sw = 0;
		Scalar krw, krn, pc;
		for (int n = 0; n < N; n++) {
			krw = MaterialLaw::krw(materialParams, sw);
			krn = MaterialLaw::krn(materialParams, sw);		
			pc = MaterialLaw::pc(materialParams, sw);	
			sw += ds;
			
			file << sw  << "," <<              
                    krw << "," <<  
                    krn << "," <<  
                    pc << 
                    std::endl;	
		}
		file.close();		
	}
	    
    
    

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	bool boundaryLeftDirichlet;
	
	int dir_;
    Scalar boundaryLeftPressure_;
    Scalar boundaryLeftSaturation_;    
    Scalar boundaryRightPressure_;
	Scalar boundaryLeftRate_;		
	
    Scalar Swr_;
    
};

} // end namespace Dumux

#endif
