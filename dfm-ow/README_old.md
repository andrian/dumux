# Discrete Fracture-Matrix oil-water model

This project contains the DuMuX module for the Discrete Fracture-Matrix (DFM) two-phase (oil and water) model[^fn1]. The code is based on the DFM module[^fn2] with a modified fluid model, boundary conditions, and I/O.


## Prerequisites

Install the self-contained version of [DuMuX](0) as described [here](../README.md).


## Installation

Compile the DFM oil-water model
```bash
cd dfm-ow/build-cmake-O3/src/
make dfm-ow
```
(for other models, replace `dfm-ow` with either `dfm-ow3c` or `dfm-ownc`).


1. Create a new DuMuX module 


* Execute the following command (bash environment) in the top-folder, i.e. above the dumux folder

```bash
./dune-common/bin/duneproject
```

* Follow the introductions and specify
    * as name of the new module: `dumux-example`
    * as module dependencies: `dumux`
    * a version at your choice (the version of your project, not of dumux.)
    * your email address

<hr><br><br>
### Task 2: Rerun dunecontrol to configure your new project
<hr>

The following command will configure your new module

```bash
./dune-common/bin/dunecontrol --opts=<opts file> --only=dumux-example all
```


2. Download the source files

```bash
git pull https://git.iws.uni-stuttgart.de/andrian/low-salinity/low-salinity.git
``` 
 or download the archive.


3. Configure the code

```bash
./dune-common/bin/dunecontrol --opts=debug.opts --builddir=build-cmake-debug --only=rate-sens-nofrac all
```
  
4. Compile the code

```bash
cd ./dfm-ow/src
make dfm-ow
```


## References
[^fn1]: N. Andrianov and H. M. Nick, Modeling of waterflood efficiency using outcrop-based fractured models, submitted, 2019.
[^fn2]: D. Gläser, R. Helmig, B. Flemisch, and H Class, A discrete fracture model for two-phase flow in fractured porous media, Advances in Water Resources 110 (2017) 335–348.


