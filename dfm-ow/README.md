# Discrete Fracture-Matrix oil-water model

This project contains the DuMuX module for the Discrete Fracture-Matrix (DFM) two-phase (oil and water) model[^fn1]. The code is based on the DFM module[^fn2] with a modified fluid model, boundary conditions, and I/O.


## Prerequisites

Install the self-contained version of [DuMuX](0) as described [here](../README.md).


## Installation

Compile the DFM oil-water model
```bash
cd dfm-ow/build-cmake-O3/src/
make dfm-ow
```


## References
[^fn1]: N. Andrianov and H. M. Nick, Modeling of waterflood efficiency using outcrop-based fractured models, Journal of Petroleum Science and Engineering,
Volume 183, https://doi.org/10.1016/j.petrol.2019.106350.
[^fn2]: D. Gläser, R. Helmig, B. Flemisch, and H. Class, A discrete fracture model for two-phase flow in fractured porous media, Advances in Water Resources 110 (2017) 335–348.
[0]: https://dumux.org
