set(subgridincludedir  ${CMAKE_INSTALL_INCLUDEDIR}/dune/subgrid/subgrid)
set(subgridinclude_HEADERS  subgridentitypointer.hh
                  subgridentityiterator.hh
                  subgridhierarchiciterator.hh
                  subgridindexstorage.hh
                  subgridleafiterator.hh
                  subgridtools.hh
                  subgridentity.hh
                  subgridentityseed.hh
                  subgridgeometry.hh
                  subgridindexsets.hh
                  subgridintersection.hh
                  subgridleafintersectioniterator.hh
                  subgridlevelintersectioniterator.hh
                  subgridleveliterator.hh
                  subgridfaceiterator.hh)
# include not needed for CMake
# include $(top_srcdir)/am/global-rules
install(FILES ${subgridinclude_HEADERS} DESTINATION ${subgridincludedir})