#ifndef DUNE_SUBGRID_ENTITY_POINTER_HH
#define DUNE_SUBGRID_ENTITY_POINTER_HH

/** \file
* \brief The SubGridEntityPointer class
*/

#define DUNE_ENTITYPOINTER_DEPRECATED_MSG  DUNE_DEPRECATED_MSG("EntityPointer is deprecated and will be removed after the release of dune-grid-2.4. Instead, you can copy and store entities directly now. Note, this might lead to a decreased performance until all grid implementations properly addressed this interface change.")
namespace Dune {


/** Acts as a pointer to an  entities of a given codimension.
*/
template<int codim, class GridImp>
class SubGridEntityPointer :
    public Dune::SubGridEntityIterator<codim, GridImp, typename GridImp::HostGridType::Traits::template Codim<codim>::EntityPointer>
{
    private:

        typedef typename GridImp::HostGridType::Traits::template Codim<codim>::EntityPointer HostEntityPointer;
        typedef SubGridEntityIterator<codim, GridImp, HostEntityPointer> Base;

        using Base::hostIterator_;
        using Base::subGridContainsHostIterator;

        typedef typename Base::EntityImp EntityImp;

    public:

        DUNE_ENTITYPOINTER_DEPRECATED_MSG
        SubGridEntityPointer()
        {}

        /**
         * \brief Constructor from subgrid and hostgrid EntityPointer/Iterator
         */
        DUNE_ENTITYPOINTER_DEPRECATED_MSG
        SubGridEntityPointer(const GridImp* subGrid, const HostEntityPointer& hostIt) :
            Base(subGrid, hostIt)
        {}


        /**
         * \brief Constructor from arbitrary EntityPointer/Iterator
         *
         * This is needed to construct an EntityPointer from an Iterator
         */
        template<class OtherHostEntityIterator>
        DUNE_ENTITYPOINTER_DEPRECATED_MSG
        SubGridEntityPointer(const SubGridEntityIterator<codim, GridImp, OtherHostEntityIterator>& other) :
            Base(&other.grid(), HostEntityPointer(other.hostIterator()))
        {}


        /**
         * \brief Constructor from entity
         */
        DUNE_ENTITYPOINTER_DEPRECATED_MSG
        SubGridEntityPointer(const EntityImp& entity) :
            Base(&entity.grid(), entity.hostEntity())
        {}

        /** \brief Return the level of the entity. */
        DUNE_ENTITYPOINTER_DEPRECATED_MSG
        int level() const
        {
            return this->hostIterator_->level();
        }

};


} // end namespace Dune

#endif
