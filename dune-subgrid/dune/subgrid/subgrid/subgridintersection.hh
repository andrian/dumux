#ifndef DUNE_SUBGRIDINTERSECTION
#define DUNE_SUBGRIDINTERSECTION

#include <dune/subgrid/subgrid/subgridfaceiterator.hh>
#include <dune/subgrid/subgrid/subgridgeometry.hh>

#include <dune/grid/common/exceptions.hh>
#include <dune/geometry/multilineargeometry.hh>

namespace Dune {

/** /brief Base class for subgrid intersections. */
template<class GridImp>
class SubGridIntersection
{
    enum {dim=GridImp::dimension};
    enum {dimworld=GridImp::dimensionworld};

    typedef typename GridImp::ctype ctype;
    typedef typename GridImp::HostGridType::LevelIntersection HostLevelIntersection;

public:
    //! Default constructor
    SubGridIntersection()
    {}

    //! Construct from subgrid and the host intersection from which it is created
    SubGridIntersection(const GridImp* g, HostLevelIntersection insideIntersect) :
        subGrid_(g),
        insideIntersect_(std::move(insideIntersect))
    {}

    //! return outer normal, this should be dependent on local
    //! coordinates for higher order boundary
    const FieldVector<ctype, dimworld> outerNormal (const FieldVector<ctype, dim-1>& local) const {
        return insideIntersect_.outerNormal(local);
    }

    //! return outer normal, this should be dependent on local
    //! coordinates for higher order boundary
    const FieldVector<ctype, dimworld> unitOuterNormal (const FieldVector<ctype, dim-1>& local) const {
        return insideIntersect_.unitOuterNormal(local);
    }

    //! return information about the boundary
    size_t boundarySegmentIndex () const {
        DUNE_THROW(Dune::NotImplemented, "boundarySegmentIndex");
    }

   //! local number of codim 1 entity in self where intersection is contained in
    int indexInInside () const {
        return insideIntersect_.indexInInside();
    }

    //! local number of codim 1 entity in neighbor where intersection is contained
    int indexInOutside () const {
        if (!insideIntersect_.neighbor())
            DUNE_THROW(GridError, "There is no neighbor!");

        return insideIntersect_.indexInOutside();
    }

protected:
    //! The subgrid
    const GridImp* subGrid_;

    //! The intersection of the element from which the intersection was created.
    HostLevelIntersection insideIntersect_;

};


/** \brief Subgrid Leaf intersections. */
template<class GridImp>
class SubGridLeafIntersection :
    public SubGridIntersection<GridImp>
{
    enum {dim=GridImp::dimension};
    enum {dimworld=GridImp::dimensionworld};

    typedef SubGridIntersection<GridImp> Base;

    typedef typename GridImp::HostGridType::LevelIntersection HostLevelIntersection;
    typedef typename HostLevelIntersection::Entity HostEntity;
    typedef SubGridEntity<0,dim,GridImp> SubEntity;

    // The type of the actual geometry implementation of this intersection
    typedef SubGridGeometry<dim-1, dimworld, GridImp> GeometryImpl;

    // The type of the actual local geometry implementation
    typedef SubGridLocalGeometry<dim-1, dimworld, GridImp> LocalGeometryImpl;
    using ctype = typename GridImp::ctype;

    using Base::insideIntersect_;

public:
    typedef typename GridImp::template Codim<0>::Entity Entity;
    typedef typename GridImp::template Codim<1>::LocalGeometry LocalGeometry;
    typedef typename GridImp::template Codim<1>::Geometry Geometry;

    //! Default constructor.
    SubGridLeafIntersection()
    {}

    //! Inside Intersection
    SubGridLeafIntersection(const GridImp* g, HostLevelIntersection insideIntersect,
        HostEntity a, HostEntity b,
        HostLevelIntersection outsideIntersect) :
            Base(g,std::move(insideIntersect)),
        inside_(SubEntity(g,std::move(a))), outside_(SubEntity(g,std::move(b))),
        outsideIntersect_(std::move(outsideIntersect))
    {}

    //! Boundary Intersection
    SubGridLeafIntersection(const GridImp* g, HostLevelIntersection insideIntersect,
        HostEntity a, HostLevelIntersection outsideIntersect) :
            Base(g,std::move(insideIntersect)),
            inside_(SubEntity(g,a)), outside_(SubEntity(g,std::move(a))),
            outsideIntersect_(std::move(outsideIntersect))
    {}

    //! Copy constructor
    SubGridLeafIntersection(const SubGridLeafIntersection& other) :
        Base(other.subGrid_,other.insideIntersect_),
        inside_(other.inside_),
        outside_(other.outside_),
        outsideIntersect_(other.outsideIntersect_)
    {}

    //! Move constructor
    SubGridLeafIntersection(SubGridLeafIntersection&& other) :
        Base(other.subGrid_,other.insideIntersect_),
        inside_(std::move(other.inside_)),
        outside_(std::move(other.outside_)),
        outsideIntersect_(std::move(other.outsideIntersect_))
    {}

    //! Assignment Operator
    SubGridLeafIntersection& operator=(const SubGridLeafIntersection& other)
    {
        this->subGrid_ = other.subGrid_;
        insideIntersect_ = other.insideIntersect_;
        outsideIntersect_ = other.outsideIntersect_;
        inside_ = other.inside_;
        outside_ = other.outside_;

        return *this;
    }

    //! Move assignment Operator
    SubGridLeafIntersection& operator=(SubGridLeafIntersection&& other)
    {
        this->subGrid_ = other.subGrid_;
        insideIntersect_ = std::move(other.insideIntersect_);
        outsideIntersect_ = std::move(other.outsideIntersect_);
        inside_ = std::move(other.inside_);
        outside_ = std::move(other.outside_);

        return *this;
    }

    //! Return inside element
    Entity inside() const {
        return inside_;
    }

    //! Return outside element, throws exception for boundary elements
    Entity outside() const {
        if (!neighbor())
            DUNE_THROW(GridError, "There is no neighbor!");
        return outside_;
    }

    //! return true if across the edge a neighbor exists
    bool neighbor () const {
        return inside_ != outside_;
    }

    //! return true if intersection is with boundary.
    bool boundary () const {
        return !neighbor();
    }

    //! local number of codim 1 entity in neighbor where intersection is contained
    int indexInOutside () const {
        if (!neighbor())
            DUNE_THROW(GridError, "There is no neighbor!");
        // TODO It might be cheaper to check if the levels of the intersections are equal
        return (insideIntersect_ == outsideIntersect_) ? insideIntersect_.indexInOutside()
                                                : outsideIntersect_.indexInInside();
    }

    //! Return GeometryType of the intersection
    GeometryType type () const {
        if (outside_.level() > inside_.level())
            return outsideIntersect_.type();
        else
            return insideIntersect_.type();
    }

    /** \brief Return outer normal scaled with the integration element. */
    const FieldVector<ctype, dimworld> integrationOuterNormal (const FieldVector<ctype, dim-1>& local) const {
        if (neighbor() and (outside_.level() > inside_.level())) {
            auto normal = outsideIntersect_.integrationOuterNormal(outsideIntersect_.geometry().local(insideIntersect_.geometry().global(local)));
            normal *= -1;
            return normal;
        } else
          return  insideIntersect_.integrationOuterNormal(local);
    }

    /*! @brief Return unit outer normal at the center of the intersection geometry */
    FieldVector<ctype, dimworld> centerUnitOuterNormal () const
    {
        if (neighbor() and (outside_.level() > inside_.level())) {
            auto normal = outsideIntersect_.centerUnitOuterNormal();
            normal *= -1;
            return normal;
        } else
            return insideIntersect_.centerUnitOuterNormal();
    }

    //! Return the global geometry of the intersection
    Geometry geometry () const {
        if (outsideIntersect_.inside().level() > insideIntersect_.inside().level())
            return Geometry(GeometryImpl(outsideIntersect_.geometry()));
        else
            return Geometry(GeometryImpl(insideIntersect_.geometry()));
    }

    /** \brief Return true if this intersection is conforming */
    bool conforming () const {
        return boundary() or ((outside_.level() == inside_.level()) and insideIntersect_.conforming());
    }

    /** \brief Return the local geometry, mapping local coordinates of the intersection to
     *         local coordinates of the inside element.
     *  If the intersection is not part of the host grid, construct one using MultiLinearGeometry.
     */
    LocalGeometry geometryInInside () const {
        if (boundary())
            return LocalGeometry(LocalGeometryImpl(insideIntersect_.geometryInInside()));

        // Case 0: inside_'s the smaller element, or both elements are on the same level.
        if (inside_.level() >= outside_.level())
            return LocalGeometry(LocalGeometryImpl(insideIntersect_.geometryInInside()));

        // Case 1: outsideIntersect_ is the intersection we want, embedded in inside_'s geometry

        auto outsideIsGeo = outsideIntersect_.geometry();
        auto insideGeo = inside_.geometry();

        using Coords = std::decay_t<decltype(insideGeo.local(outsideIsGeo.corner(0)))>;
        auto corners = std::vector<Coords>(outsideIsGeo.corners());

        // Map the coordinates of the corners from the outside's local coordinates into inside's
        // local coordinates
        for (size_t i = 0; i < corners.size(); i++)
            corners[i] = insideGeo.local(outsideIsGeo.corner(i));

        // create a new geometry with the coordinates we just computed
        auto geo = Dune::MultiLinearGeometry<ctype, dim-1, dim>(outsideIsGeo.type(), corners);
        return LocalGeometry(LocalGeometryImpl(std::move(geo)));
    }

    /** \brief Return the local geometry, mapping local coordinates of the intersection to
     *         local coordinates of the outside element.
     */
    LocalGeometry geometryInOutside () const {
        if(boundary())
            DUNE_THROW(Dune::Exception, "No outside geometry available for boundary intersections");

        // Case 0: outside_'s the smaller element, or both elements are on the same level.
        if (inside_.level() < outside_.level()) {

            auto outsideIsGeo = outsideIntersect_.geometry();
            auto outsideGeo = outside_.geometry();

            using Coords = std::decay_t<decltype(outsideGeo.local(outsideIsGeo.corner(0)))>;
            auto corners = std::vector<Coords>(outsideIsGeo.corners());
            for (size_t i = 0; i < corners.size(); i++)
                corners[i] = outsideGeo.local(outsideIsGeo.corner(i));

            // create a new geometry with the coordinates we just computed
            auto geo = Dune::MultiLinearGeometry<ctype, dim-1, dim>(outsideIsGeo.type(), corners);
            return LocalGeometry(LocalGeometryImpl(geo));
        }

        // Case 1: outsideIntersect_ is the intersection we want, embedded in inside_'s geometry
        auto insideIsGeo = insideIntersect_.geometry();
        auto outsideGeo = outside_.geometry();

        using Coords = std::decay_t<decltype(outsideGeo.local(insideIsGeo.corner(0)))>;
        auto corners = std::vector<Coords>(insideIsGeo.corners());

        // Map the coordinates of the corners from the insides's local coordinates into outside's
        // local coordinates
        for (size_t i = 0; i < corners.size(); i++)
            corners[i] = outsideGeo.local(insideIsGeo.corner(i));

        // create a new geometry with the coordinates we just computed
        auto geo = Dune::MultiLinearGeometry<ctype, dim-1, dim>(insideIsGeo.type(), corners);
        return LocalGeometry(LocalGeometryImpl(std::move(geo)));
    }

    bool equals(const SubGridLeafIntersection<GridImp> & other) const {
        return inside_ == other.inside_ && (!neighbor() || outside_ == other.outside_);
    }

private:
    //! The inside element
    Entity inside_;

    //! The outside element
    Entity outside_;

    //! This intersection differs from the hostIntersect_ of the base class whenever the intersection does not
    //! exist in the host grid. In this case the outsideIntersect_ is the intersection of the 'outside' element
    HostLevelIntersection outsideIntersect_;
};

/** \brief Class that represent a LevelIntersection in the subgrid.
    All functionalities of a LevelIntersection can be implemented using one HostLevelIntersection.*/
template <class GridImp>
class SubGridLevelIntersection :
    public SubGridIntersection<GridImp>
{

    enum {dim=GridImp::dimension};
    enum {dimworld=GridImp::dimensionworld};

    typedef SubGridIntersection<GridImp> Base;

    // The type used to store coordinates
    typedef typename GridImp::ctype ctype;
    typedef typename GridImp::HostGridType::LevelIntersection HostLevelIntersection;
    typedef typename GridImp::HostGridType::template Codim<0>::Entity HostEntity;
    typedef SubGridEntity<0,dim, GridImp> SubEntity;

    typedef SubGridFatherFaceIterator<GridImp> FatherFaceIterator;

    // The type of the actual geometry implementation of this intersection
    typedef SubGridGeometry<dim-1, dimworld, GridImp> GeometryImpl;

    // The type of the actual local geometry implementation
    typedef SubGridLocalGeometry<dim-1, dimworld, GridImp> LocalGeometryImpl;

    using Base::insideIntersect_;
    using Base::subGrid_;
public:

    typedef typename GridImp::template Codim<1>::Geometry Geometry;
    typedef typename GridImp::template Codim<1>::LocalGeometry LocalGeometry;
    typedef typename GridImp::template Codim<0>::Entity Entity;

    SubGridLevelIntersection()
    {}

    SubGridLevelIntersection(const GridImp* g, HostLevelIntersection hostIntersect) :
        Base(g,std::move(hostIntersect)),
        boundaryCache_(-1)
    {}

    //! Copy constructor
    SubGridLevelIntersection(const SubGridLevelIntersection& other) :
        Base(other.subGrid_,other.insideIntersect_),
        boundaryCache_(other.boundaryCache_)
    {}

    //! Move constructor
    SubGridLevelIntersection(SubGridLevelIntersection&& other) :
        Base(other.subGrid_,std::move(other.insideIntersect_)),
        boundaryCache_(other.boundaryCache_)
    {}

    //! Assignment Operator
    SubGridLevelIntersection& operator=(const SubGridLevelIntersection& other)
    {
        this->subGrid_ = other.subGrid_;
        insideIntersect_ = other.insideIntersect_;
        return *this;
    }

    //! Move Assignment Operator
    SubGridLevelIntersection& operator=(SubGridLevelIntersection&& other)
    {
        this->subGrid_ = other.subGrid_;
        insideIntersect_ = std::move(other.insideIntersect_);
        return *this;
    }

    //! returns the inside element of the intersection
    Entity inside() const {
        return SubEntity(subGrid_, insideIntersect_.inside());
    }

    //! returns the outside element of the intersection or an error if intersection is with boundary
    Entity outside() const {

        if (!insideIntersect_.neighbor())
            DUNE_THROW(GridError,"no in neighbor found in outside()");

        HostEntity outside = insideIntersect_.outside();
        if (!subGrid_->template contains<0>(outside))
            DUNE_THROW(GridError,"no neighbor found in outside()");

        return SubEntity(subGrid_, std::move(outside));
    }

    /** \brief return true if intersection is with boundary.

        Warning: This method uses geometric information and will not work properly in all conceivable
        cases!!  If you find a better way to implement this please let me know.
     */
    bool boundary () const {

        if (boundaryCache_>=0)
            return (bool) boundaryCache_;

        // A boundary intersection of the host grid is also a boundary intersection of the subgrid
        if (insideIntersect_.boundary()) {
            boundaryCache_ = 1;
            return true;
        }

        // An intersection is boundary if its last ancestor does not have a neighbor,
        FatherFaceIterator fatherFaceIt(subGrid_, insideIntersect_, FatherFaceIterator::begin);
        FatherFaceIterator fatherFaceEndIt(subGrid_, insideIntersect_, FatherFaceIterator::end);

        // get coarsest father face, hack: when iterating until it==end, then it contains the last iterator
        do {
            ++fatherFaceIt;
        }
        while (fatherFaceIt != fatherFaceEndIt);

        // We have found the ancestor face on the coarsest level.  If this is boundary we are boundary
        const HostLevelIntersection& fatherFace = *fatherFaceIt;
        boundaryCache_ = !fatherFace.neighbor() || !subGrid_->template contains<0>(fatherFace.outside());
        return (bool) boundaryCache_;
    }

    //! return true if across the edge an neighbor on this level exists
    bool neighbor () const {
        return (insideIntersect_.neighbor()
            && subGrid_->template contains<0>(insideIntersect_.outside()));
    }

    /** \brief Get the geometry type */
    GeometryType type () const {
        return insideIntersect_.type();
    }

    /** \brief Return true if this intersection is conforming */
    bool conforming () const {
        return !neighbor() || insideIntersect_.conforming();
    }

    /*! @brief Return unit outer normal at the center of the intersection geometry */
    FieldVector<ctype, dimworld> centerUnitOuterNormal () const
    {
        return insideIntersect_.centerUnitOuterNormal();
    }

    /** \brief Return outer normal scaled with the integration element. */
    const FieldVector<ctype, dimworld> integrationOuterNormal (const FieldVector<ctype, dim-1>& local) const {
       return insideIntersect_.integrationOuterNormal(local);
    }

    //! intersection of codimension 1 of this neighbor with element where
    //! iteration started.
    //! Here returned element is in LOCAL coordinates of the element
    //! where iteration started.
    LocalGeometry geometryInInside () const {
        return LocalGeometry(LocalGeometryImpl(insideIntersect_.geometryInInside()));
    }

    //! intersection of codimension 1 of this neighbor with element where iteration started.
    //! Here returned element is in LOCAL coordinates of neighbor
    LocalGeometry geometryInOutside () const {
        if (neighbor())
            return LocalGeometry(LocalGeometryImpl(insideIntersect_.geometryInOutside()));
        else
            DUNE_THROW(Dune::Exception, "Intersection has no neighbouring element!");
    }

    //! intersection of codimension 1 of this neighbor with element where iteration started.
    //! Here returned element is in GLOBAL coordinates of the element where iteration started.
    Geometry geometry () const {
        return Geometry(GeometryImpl(insideIntersect_.geometry()));
    }

    bool equals(const SubGridLevelIntersection& other) const {
        return insideIntersect_ == other.insideIntersect_;
    }

private:
    //! cache if intersection is boundary, since it is expensive to compute
    mutable int boundaryCache_;
};

} // namespace Dune

#endif
