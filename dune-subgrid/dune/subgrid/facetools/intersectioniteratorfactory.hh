#ifndef DUNE_SUBGRID_FACETOOLS_INTERSECTION_ITERATOR_FACTORY_HH
#define DUNE_SUBGRID_FACETOOLS_INTERSECTION_ITERATOR_FACTORY_HH

namespace Dune {
namespace Facetools {

/**
 * \brief Simple factory for IntersectionIterators on a grid
 *
 * \tparam GridType type of underlying grid
 *
 * If you need to access intersection iterators for various entities
 * without you have to either create/copy the corresponding GridView
 * for each entity anew or to cache all potentially needed GridViews
 * beforehand. IntersectionIteratorFactory does exactly this and
 * provides simple access to Leaf- and LevelIntersectionIterators
 * without having to do this manually.
 */
template<class GridType>
class IntersectionIteratorFactory
{
    typedef typename GridType::LeafGridView LeafGridView;
    typedef typename GridType::LevelGridView LevelGridView;
public:
    typedef typename GridType::Traits::template Codim<0>::Entity Element;
    typedef typename GridType::LeafIntersectionIterator LeafIntersectionIterator;
    typedef typename GridType::LevelIntersectionIterator LevelIntersectionIterator;

    /**
     * \brief Construct IntersectionIteratorFactory for grid
     */
    IntersectionIteratorFactory(const GridType& grid) :
        grid_(&grid),
        leafView_(grid_->leafGridView())
    {
        update();
    }

    /**
     * \brief Update the chached grid views.
     *
     * To be called if the grid has changed.
     */
    void update()
    {
        leafView_ = grid_->leafGridView();

        // Use clear/reserve/push_back to avoid the
        // default constructor needed with resize/[]
        levelViews_.clear();
        levelViews_.reserve(grid_->maxLevel()+1);
        for (int i=0; i <= grid_->maxLevel(); ++i)
            levelViews_.push_back(grid_->levelGridView(i));
    }

    //! Create begin LeafIntersection
    const LeafIntersectionIterator iLeafBegin(const Element& e) const
    {
        return leafView_.ibegin(e);
    }

    //! Create end LeafIntersection
    const LeafIntersectionIterator iLeafEnd(const Element& e) const
    {
        return leafView_.iend(e);
    }

    //! Create begin LevelIntersection
    const LevelIntersectionIterator iLevelBegin(const Element& e) const
    {
        return levelViews_[e.level()].ibegin(e);
    }

    //! Create end LevelIntersection
    const LevelIntersectionIterator iLevelEnd(const Element& e) const
    {
        return levelViews_[e.level()].iend(e);
    }

private:
    //! Store the grid
    const GridType* grid_;

    //! Cache a LeafGridView to avoid creating it on and on
    LeafGridView leafView_;

    //! Cache all LevelGridViews to avoid creating them on and on
    typename std::vector<LevelGridView> levelViews_;
};



} // namespace Facetools
} // namespace Dune


#endif // DUNE_SUBGRID_FACETOOLS_INTERSECTION_ITERATOR_FACTORY_HH
