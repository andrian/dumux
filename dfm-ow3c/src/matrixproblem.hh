// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_MATRIXEXERCISE_MATRIX_PROBLEM_HH
#define DUMUX_COURSE_MATRIXEXERCISE_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// Need this for reading restarts from vtk (not properly tested)
// #include <vtkXMLUnstructuredGridReader.h>
// #include <vtkSmartPointer.h>
// #include <vtkUnstructuredGrid.h>
// #include <vtkCellData.h>

// Use Darcy's law which does not include the contributions from capillary forces for boundary fluxes.
// The implementation explicitly calls for pressure component depending on the current formulation.
#include "discretization/properties.hh"
// The original include file which does include contributions from capillary forces for boundary fluxes.
//#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include "model/model.hh"

// Incompressible water-oil system with parameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "fluidsystems/myliquidphase2c.hh"
#include "fluidsystems/myoilwater.hh" 
#include "components/mybrine.hh"
#include "components/mylowsalwater.hh"
#include "components/myoil.hh"

// The solid phase consists of an inert component and potentially adsorbed low salinity water component
#include "solidsystems/adsorptionsolidphase.hh" 

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the matrix sub-problem
// We need to put the facet-coupling type tag after the physics-related type tag
// because it overwrites some of the properties inherited from "TwoP". This is
// necessary because we need e.g. a modified implementation of darcys law that
// evaluates the coupling conditions on faces that coincide with the fractures.
NEW_TYPE_TAG(MatrixProblemTypeTag, INHERITS_FROM(TwoPNC, CCTpfaFacetCouplingModel));
// Set the grid type
SET_TYPE_PROP(MatrixProblemTypeTag, Grid, Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>);
// Set the problem type
SET_TYPE_PROP(MatrixProblemTypeTag, Problem, MatrixSubProblem<TypeTag>);
// set the spatial params
SET_TYPE_PROP(MatrixProblemTypeTag,
              SpatialParams,
              MatrixSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);

// Set the fluid system
SET_PROP(MatrixProblemTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using WaterPhase = typename FluidSystems::MyLiquidPhaseTwoC<Scalar, 
                                                              Components::MyIncompressibleBrineComponent<Scalar>,       // Main component of the water phase
                                                              Components::MyIncompressibleLowSalWaterComponent<Scalar>  // Secondary component of the water phase
                                                              >;
                                                              
    //using WaterPhase = typename FluidSystems::OnePLiquid<Scalar, Components::MyIncompressibleBrineComponent<Scalar> >;
    
    // Define the oil phase as single phase liquid system (OnePLiquid), consisting of a single component MyIncompressibleOilComponent, specidified in myOil.hh
    using OilPhase = typename FluidSystems::OnePLiquid<Scalar, Components::MyIncompressibleOilComponent<Scalar> >;
    
    // Using fluid system, consisting of 2 immiscible phases
    using type = typename FluidSystems::MyOilWater<Scalar, WaterPhase, OilPhase>;

}; 

// Set the solid system
SET_PROP(MatrixProblemTypeTag, SolidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = SolidSystems::AdsorptionSolidPhase<Scalar>;
};

} // end namespace Properties

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The sub-problem for the matrix domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
	using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
	using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

    enum
    {
    	wPhaseIdx = FluidSystem::phase0Idx,	// wetting phase index
        nPhaseIdx = FluidSystem::phase1Idx
    };    
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::switchIdx,     //Saturation (apparently set to 1)
        molarfracIdx = 2,                       // hardcoding
		
        //Indices of the components / equations / fluxes
        OilIdx  = FluidSystem::OilIdx,
        LSWIdx = FluidSystem::LSWIdx,
		BrineIdx = FluidSystem::BrineIdx,

        // Phase State
		firstPhaseOnly = Indices::firstPhaseOnly,	//!< Only the first phase (in fluid system) is present
        secondPhaseOnly = Indices::secondPhaseOnly, //!< Only the second phase (in fluid system) is present
        bothPhases = Indices::bothPhases,      		//!< Both phases are present
		
    };
    
    // formulations
    static constexpr auto p0s1 = TwoPFormulation::p0s1;
    static constexpr auto p1s0 = TwoPFormulation::p1s0;

    // the formulation that is actually used
    static constexpr auto formulation = ModelTraits::priVarFormulation();  

public:

    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    //, boundaryOverPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryOverPressure"))
    //, boundarySaturation_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundarySaturation"))
	, dir_(getParamFromGroup<Scalar>(paramGroup, "Problem.Direction"))	
    , boundaryRightPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryRightPressure"))	
	, injLowSalTime_(getParamFromGroup<Scalar>(paramGroup, "Problem.injLowSalTime"))			
    , Swr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"))
	, Snr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"))
    {		

		// Infer the type of left boundary conditions from input data
		bool bcSet = false;
		boundaryLeftRate_ = 1e100;
		try {	
			// If BoundaryLeftRate is present then left boundary is Neumann with the corresponding rate
			boundaryLeftRate_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftRate");		
			boundaryLeftDirichlet = false;
			bcSet = true;
			std::cout << "Using Neumann conditions at the left boundary.." << std::endl;
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided for the left boundary
			//std::cout << e.what(); 
			try {	
				boundaryLeftPressure_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftPressure");
				boundaryLeftSaturation_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftSaturation");
				boundaryLeftDirichlet = true;		
				bcSet = true;
				std::cout << "Using Dirichlet conditions at the left boundary.." << std::endl;
			} catch (const std::exception& e) {	}			
		}
		
		if (!bcSet) 
		{				
			std::cout << "\nNeither Dirichlet nor Neumann conditions are provided for the left boundary..\n" << std::endl;
			throw Dune::NotImplemented();
		}
				
		
		// initialize the fluid system
		FluidSystem::init();

		// initialize the solid system
//		SolidSystem::adsorptionIsotherm = false;
//		SolidSystem::density_ = 0;
//		SolidSystem::adsorptionGamma1 = 0;
//		SolidSystem::adsorptionGamma2 = 0;
//		SolidSystem::adsorptionN = 0;
		SolidSystem::init();

        
        // Warn that p1s0 formulation is needed to set up constant pressure BC
        if (formulation != p1s0) {
            std::cout << "\nWARNING: Please set up the p1s0 formulation in porousmediumflow/2p/model.hh\n" << std::endl;
            //assert(formulation == p1s0);
        }
        
        //const bool restartFromVTU = getParamFromGroup<bool>(paramGroup, "TimeLoop.RestartFromVTU");
        //if (restartFromVTU) {

        //	int restartVTUNumber= getParamFromGroup<int>(paramGroup, "TimeLoop.RestartVTUNumber");

		//  std::string matrixRestartFile = "matrix-" + std::to_string(restartVTUNumber) + ".vtu";
        //	readVTK(matrixRestartFile, matrixSw, matrixPn, matrixXBrine);
            /*std::ifstream matrixRestart( matrixRestartFile.c_str() );
            if( matrixRestart )
            {
            	readVTK(matrixRestart, matrixSw, matrixPn, matrixXBrine);
            }
            else
            {
              std::cerr << "ERROR: couldn't open file `" << matrixRestartFile << "'" << std::endl;
            }
            matrixRestart.close();
            */

        	//std::string fractureRestartFile = "fracture-" + std::to_string(restartVTUNumber) + ".vtp";
        	//readVTK(fractureRestartFile, fractureSw, fracturePn, fractureXBrine);
            /*std::ifstream fractureRestart( fractureRestartFile.c_str() );
            if( fractureRestart )
            {
            	readVTK(fractureRestart, fractureSw, fracturePn, fractureXBrine);
            }
            else
            {
              std::cerr << "ERROR: couldn't open file `" << fractureRestartFile << "'" << std::endl;
            }
            fractureRestart.close();
            */

        //}
    }

	// Set current time
    void setTime(Scalar time)
    {
        time_ = time;
    }


    // NA: get the OIP and WIP
    // analogously to postTimeStep from richardsnc
    void getVolumesFluxes(std::vector<Scalar>& Volume, std::vector<Scalar>& InFlux, std::vector<Scalar>& OutFlux, 
    			  const SolutionVector& curSol,
                          const GridVariables& gridVariables)
    {    	
    	std::fill(Volume.begin(), Volume.end(), 0.);
    	std::fill(InFlux.begin(), InFlux.end(), 0.);
    	std::fill(OutFlux.begin(), OutFlux.end(), 0.);
        
        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            //fvGeometry.bindElement(element);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            //int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = x[eIdx];

            VolumeVariables volVars;

            for (auto&& scv : scvs(fvGeometry))
            {
                //const auto& volVars = elemVolVars[scv];
            	volVars = elemVolVars[scv];

                Scalar ef = volVars.extrusionFactor();

                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }

                Volume[nPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(nPhaseIdx);
                Volume[wPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(wPhaseIdx);

            }

            // Loop through the faces 
            // see porousmediumflow/velocityoutput.hh and porousmediumflow/nonequilibrium/gridvariables.hh
            // bind the element flux variables cache

            // the upwind term to be used for the volume flux evaluation
            auto upwindTermN = [](const auto& volVars) { return volVars.mobility(nPhaseIdx); };
            auto upwindTermW = [](const auto& volVars) { return volVars.mobility(wPhaseIdx); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {

            	if (scvf.boundary()) {

                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();
                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar fn = fluxVars.advectiveFlux(nPhaseIdx, upwindTermN);
                        Scalar fw = fluxVars.advectiveFlux(wPhaseIdx, upwindTermW);
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);                        
                    }
                    else if (bcTypes.hasOnlyNeumann()) {
						// Read the mole fluxes [mol/s]
						NumEqVector flux = this->neumannAtPos(scvf.center());   
                        Scalar fOil = flux[OilIdx];
                        Scalar fLSW = flux[LSWIdx];
						Scalar fBrine = flux[BrineIdx];
						
						// Convert molar fluxes to mass fluxes [kg/s]
						fOil *= FluidSystem::molarMass(OilIdx);
						fLSW *= FluidSystem::molarMass(LSWIdx);
						fBrine *= FluidSystem::molarMass(BrineIdx);
						
						// Convert mass fluxes to volumetric fluxes
						fOil /= FluidSystem::Oil::liquidDensity(0., 0.);	// with dummy p, T
						fLSW /= FluidSystem::LowSalWater::liquidDensity(0., 0.);
						fBrine /= FluidSystem::Brine::liquidDensity(0., 0.);
                        
                        fOil *= scvf.area() * extrusionFactor;
						fLSW *= scvf.area() * extrusionFactor;
						fBrine *= scvf.area() * extrusionFactor;
                        
                        InFlux[nPhaseIdx]  += std::min(fOil, 0.);
                        OutFlux[nPhaseIdx] += std::max(fOil, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fLSW + fBrine, 0.);
                        OutFlux[wPhaseIdx] += std::max(fLSW + fBrine, 0.);                           
                    }
                    else {
                        DUNE_THROW(Dune::NotImplemented, "matrixproblem.hh: boundary condition not defined..");
                    }
                    
                }
                
            }
                        
        }

    }  
    
    Scalar poreVolume(const SolutionVector& curSol, const GridVariables& gridVariables)
    {
        Scalar PV = 0.0;

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                Scalar ef = volVars.extrusionFactor();
                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }
                PV += scv.volume() * volVars.porosity() * volVars.extrusionFactor();
            }
        }
        
        return PV;
    }
    
    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // we consider buoancy-driven upwards migration of nitrogen and set
        // Dirichlet BCs on the top and bottom boundary
        // TODO Task A: Change boundary conditions and Dirichlet values!
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];

		if (boundaryLeftDirichlet) 
		{
			// Dirichlet at the left and at the right
			values.setAllNeumann();
			if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}
		else 
		{
			// Neumann at the left, Dirichlet at the right
			values.setAllNeumann();        
			if (globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}		
		
        return values;        
        
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated. If this
        // permeability is lower than that of the matrix, this approach is able to
        // represent the resulting pressure jump across the fracture. If Dirichlet is set,
        // the pressure jump across the fracture is neglected and the pressure inside
        // the fracture is directly applied at the interface between fracture and matrix.
        // This assumption is justified for highly-permeable fractures, but lead to erroneous
        // results for low-permeable fractures.
        // Here, we consider "open" fractures for which we cannot define a normal permeability
        // and for which the pressure jump across the fracture is neglectable. Thus, we set
        // the interior boundary conditions to Dirichlet.
        // IMPORTANT: Note that you will never be asked to set any values at the interior boundaries!
        //            This simply chooses a different interface condition!
        // TODO Task C: Change coupling conditions!
        values.setAllDirichlet();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
    	values.setState(bothPhases);	// For compositional runs, need to specify properties of which phases will be calculated			
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];	

        if (globalPos[dir_] < xmin + 1e-6) {

        	Scalar molarFracLSW;
        	if (time_ < injLowSalTime_) {
        		molarFracLSW = 0;	// Inject brine
        	}
        	else {
        		molarFracLSW = 1;	// Inject LSW
        	}			
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryLeftPressure_;            // assign the oil pressure boundaryLeftPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - boundaryLeftSaturation_;   	// Oil saturation     
                values[molarfracIdx] = 1 - molarFracLSW;				// Brine molar fraction
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryLeftPressure_ ;
                values[saturationIdx] = boundaryLeftSaturation_; 
				values[molarfracIdx] = 1. - molarFracLSW;	
            }
        }

        
        // Fixed right pressure 
        if (globalPos[dir_] > xmax - 1e-6) {
            
            if (formulation == p0s1) {	
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:	    	
                //    values[pressureIdx] is interpreted as wetting phase pressure	    	
                //    values[saturationIdx] is interpreted as non-wetting phase saturation
                values[pressureIdx] = boundaryRightPressure_;   // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
                values[saturationIdx] = 1 - Swr_; 
				values[molarfracIdx] = 1.;						// Brine molar fraction
            }
            else if (formulation == p1s0) {
                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryRightPressure_ ;
                values[saturationIdx] = Swr_; 
				values[molarfracIdx] = 1.;						// Brine molar fraction
            }
            else {
                std::cout << "dirichletAtPos: formulation not implemented.." << std::endl;
                
            }

        }
        
        return values;
        
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
        
		Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];
		
		FluidState fluidState;
		
        fluidState.setMoleFraction(wPhaseIdx, OilIdx,   0.0);	// From myoilweater.hh: OilIdx = 0 
        fluidState.setMoleFraction(wPhaseIdx, LSWIdx,   1.0);	// LSWIdx = 1
		fluidState.setMoleFraction(wPhaseIdx, BrineIdx, 0.0);	// BrineIdx = 2

		const Scalar molarFlux = boundaryLeftRate_ / fluidState.averageMolarMass(wPhaseIdx);

        if (globalPos[dir_] < xmin + 1e-6) {
        	values[OilIdx] =   0.; 			  // kg/(m*s)
            values[LSWIdx] =   molarFlux * fluidState.moleFraction(wPhaseIdx, LSWIdx);
            values[BrineIdx] = molarFlux * fluidState.moleFraction(wPhaseIdx, BrineIdx);          
        }

        return values;
    }
    
    
    

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {

        PrimaryVariables values;
		
		// Currently, the phase equilibrium calculations in volumevariables.hh are tested for the case when both phases are present
        Scalar threshSwr = 1e-3;
        assert((Swr_ > threshSwr) && (Snr_ > threshSwr));
        values.setState(bothPhases);
        	
		Scalar molarFracLSW = 0.;	// Initially the water phase in the reservoir consists of brine only (Xw1 = 1)
		
		// NA: To debug the adsorption
		//molarFracLSW = 0.4;

        if (formulation == p0s1) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as wetting phase pressure
            //    values[saturationIdx] is interpreted as non-wetting phase saturation  
            values[pressureIdx] = boundaryRightPressure_ ;  // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
            values[saturationIdx] = 1 - Swr_;	
			values[molarfracIdx] = 1. - molarFracLSW;
        }
    	else if (formulation == p1s0) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as non-wetting phase pressure
            //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
            values[pressureIdx] = boundaryRightPressure_ ;
            values[saturationIdx] = Swr_; 
			values[molarfracIdx] = 1. - molarFracLSW;
        }
    	else {
            std::cout << "initialAtPos: formulation not implemented.." << std::endl;
    	}

        return values;    
        
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    

    // Save relative permeabilities and capillary pressure curves for a fixed value of X (=molarFracLSW)
    void evalRelPerms() const
    {	
		double X = 0.3;	// For instance..
		
		std::string filename( "matrix_relperms.csv");
		std::ofstream file( filename.c_str() );
		if (file) {	
			const char *header = "sw,krw_brine,krw_X,krw_LSW,krn_brine,krn_X,krn_LSW,pc_brine,pc_X,pc_LSW";
			file << header << std::endl;        			
		}
		else
		{
			std::cerr << "ERROR: couldn't open file `" << filename << "'" << std::endl;
			
		}		
		
		GlobalPosition globalPos({0.0, 0.0});
		const auto& materialParams = this->spatialParams().materialLawParamsAtPos(globalPos);
		using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
		
		int N = 100;
		Scalar ds = 1. / (N+1);
		Scalar sw = 0;
		Scalar krw_brine, krw_X, krw_LSW;
		Scalar krn_brine, krn_X, krn_LSW;
		Scalar pc_brine, pc_X, pc_LSW;
		for (int n = 0; n < N; n++) {
			krw_brine = MaterialLaw::krw(materialParams, sw, 0.);
			krn_brine = MaterialLaw::krn(materialParams, sw, 0.);		
			pc_brine = MaterialLaw::pc(materialParams, sw, 0.);	
			
			krw_X = MaterialLaw::krw(materialParams, sw, X);
			krn_X = MaterialLaw::krn(materialParams, sw, X);		
			pc_X = MaterialLaw::pc(materialParams, sw, X);	

			krw_LSW = MaterialLaw::krw(materialParams, sw, 1.);
			krn_LSW = MaterialLaw::krn(materialParams, sw, 1.);		
			pc_LSW = MaterialLaw::pc(materialParams, sw, 1.);	
			
			sw += ds;
			
			file << sw  << "," <<   
			krw_brine << "," <<  krw_X << "," <<  krw_LSW << "," <<  
			krn_brine << "," <<  krn_X << "," <<  krn_LSW << "," <<                    
			pc_brine  << "," <<  pc_X  << "," <<  pc_LSW  << "," <<
			std::endl;	
		}
		file.close();		
	}
    

    // Read fields Sw, Pn, XBrine from the restart filename
  //  void readVTK(std::string& filename, std::vector<Scalar>& Sw, std::vector<Scalar>& Pn, std::vector<Scalar>& XBrine) {

/*
    	//read all the data from the file
    	vtkSmartPointer<vtkXMLUnstructuredGridReader> reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();

    	reader->SetFileName(filename.c_str());
    	reader->Update();

    	

    	vtkUnstructuredGrid* ugrid = reader->GetOutput();
    	unsigned int cellNumber = ugrid->GetNumberOfCells();
    	std::cout << "There are " << cellNumber << " input cells." << std::endl;

    	vtkCellData *cellData = ugrid->GetCellData();
    	for (int i = 0; i < cellData->GetNumberOfArrays(); i++)
    	{
    		vtkDataArray* data = cellData->GetArray(i);	// j
    		cout << "name " << data->GetName() << endl;
    		for (int j = 0; j < data->GetNumberOfTuples(); j++)
    		{
    			double value = data->GetTuple1(j);
    			cout << "  value " << j << "th is " << value << endl;
    		}
    	}
    	*/

    //}


private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	bool boundaryLeftDirichlet;	
    
	int dir_;
    Scalar boundaryLeftPressure_;
    Scalar boundaryLeftSaturation_;    
    Scalar boundaryRightPressure_;
	Scalar boundaryLeftRate_;		
	Scalar injLowSalTime_;	

	Scalar Swr_;
    Scalar Snr_;

	Scalar time_; 
	
	// Restart variables
	std::vector<Scalar> matrixSw, matrixPn, matrixXBrine;
	std::vector<Scalar> fractureSw, fracturePn, fractureXBrine;

};

} // end namespace Dumux

#endif
