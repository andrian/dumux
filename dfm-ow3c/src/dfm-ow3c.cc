// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the exercise on two-phase flow in fractured porous media.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

// include the headers of the two sub-problems
// i.e. the problems for fractures and matrix
#include "matrixproblem.hh"
#include "fractureproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include <dumux/io/vtkoutputmodule.hh>

// Define some types for this test  so that we can set them as properties below and
// reuse them again in the main function with only one single definition of them here
using MatrixTypeTag = TTAG(MatrixProblemTypeTag);
using FractureTypeTag = TTAG(FractureProblemTypeTag);
using MatrixFVGridGeometry = typename GET_PROP_TYPE(MatrixTypeTag, FVGridGeometry);
using FractureFVGridGeometry = typename GET_PROP_TYPE(FractureTypeTag, FVGridGeometry);
using TheMultiDomainTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
using TheCouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
using TheCouplingManager = Dumux::FacetCouplingManager<TheMultiDomainTraits, TheCouplingMapper>;

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {
SET_TYPE_PROP(MatrixProblemTypeTag, CouplingManager, TheCouplingManager);
SET_TYPE_PROP(FractureProblemTypeTag, CouplingManager, TheCouplingManager);
} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::FakeMPIHelper::instance(argc, argv);	
    //const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);
	
    if (mpiHelper.isFake) {
        std::cout << "\nSerial run (no MPI)" << std::endl;
    }
    else {
       std::cout << "\nMPI run" << std::endl;
    }	

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);


    // initialize parameter tree
    Parameters::init(argc, argv);

    // We use the grid manager from the facet coupling framework (see alias above main)
    // This requires the grids used to be passed as template arguments, where
    // they are assumed to be ordered in descending grid dimension. Thus,
    // we pass the matrix grid as first and the fracture grid as second argument.
    using MatrixGrid = typename GET_PROP_TYPE(MatrixTypeTag, Grid);
    using FractureGrid = typename GET_PROP_TYPE(FractureTypeTag, Grid);
    using GridManager = Dumux::FacetCouplingGridManager<MatrixGrid, FractureGrid>;
    GridManager gridManager;

    // try to create a grid (from the grid file in the input file)
    // Init() creates the grid from the grid file specified in the input file.
    // This works with a single grid file in which in addition to the matrix
    // (d-dimensional) elements also the (d-1)-dimensional elements are specified
    // which are interpreted as the fracture elements. See the .geo file in the
    // grids folder on how to create such grids using gmsh. Note that currently
    // only the Gmsh mesh format (.msh) is supported!
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views (get them from grid manager)
    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);
    matrixFvGridGeometry->update();
    fractureFvGridGeometry->update();

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = typename GET_PROP_TYPE(MatrixTypeTag, Problem);
    using FractureProblem = typename GET_PROP_TYPE(FractureTypeTag, Problem);

    // pass the model parameter group to the spatial params so that they obtain the right
    // values from the input file since we use groups for matrix and fracture
    // We also want to use domain markers in this exercise. For this reason, we also pass
    // the grid data object from the grid manager to them, so that they have access to the
    // domain markers that were specified in the grid file.
    auto matrixGridData = gridManager.getGridData()->template getSubDomainGridData<matrixGridId>();
    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, matrixGridData, "Matrix");
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix");

    // extract domain height from the matrix problem and pass to fracture problem (needed for right initial pressure distribution)
    auto fractureGridData = gridManager.getGridData()->template getSubDomainGridData<fractureGridId>();
    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, fractureGridData, "Fracture");
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture");

    // the solution vector
    using SolutionVector = typename TheMultiDomainTraits::SolutionVector;
    SolutionVector x, xOld;

    // The domain ids within the multi-domain framework.
    // They do not necessarily have to be the same as the grid ids
    // in case you have more subdomains involved. We domain ids
    // correspond to the order of the type tags passed to the multidomain
    // traits (see definition of the traits class at the beginning of this file)
    static const auto matrixDomainId = typename TheMultiDomainTraits::template DomainIdx<0>();
    static const auto fractureDomainId = typename TheMultiDomainTraits::template DomainIdx<1>();

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());
    matrixProblem->applyInitialSolution(x[matrixDomainId]);
    fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // instantiate the class holding the coupling maps between the domains
    // this needs the information on embeddings (connectivity between matrix
    // and fracture domain). This information is extracted directly from the
    // grid during file read and can therefore be obtained from the grid manager.
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // the grid variables
    using MatrixGridVariables = typename GET_PROP_TYPE(MatrixTypeTag, GridVariables);
    using FractureGridVariables = typename GET_PROP_TYPE(FractureTypeTag, GridVariables);
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId], xOld[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId], xOld[fractureDomainId]);

    // intialize the vtk output modules
    VtkOutputModule<MatrixTypeTag> matrixVtkWriter(*matrixProblem, *matrixFvGridGeometry, *matrixGridVariables, x[matrixDomainId], matrixProblem->name());
    VtkOutputModule<FractureTypeTag> fractureVtkWriter(*fractureProblem, *fractureFvGridGeometry, *fractureGridVariables, x[fractureDomainId], fractureProblem->name());

    // Add model specific output fields
    using MatrixVtkOutputFields = typename GET_PROP_TYPE(MatrixTypeTag, VtkOutputFields);
    using FractureVtkOutputFields = typename GET_PROP_TYPE(FractureTypeTag, VtkOutputFields);
    MatrixVtkOutputFields::init(matrixVtkWriter);
    FractureVtkOutputFields::init(fractureVtkWriter);

    // add domain markers to output
    std::vector<int> matrixDomainMarkers(matrixFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(matrixFvGridGeometry->gridView()))
        matrixDomainMarkers[matrixFvGridGeometry->elementMapper().index(element)] = matrixProblem->spatialParams().getElementDomainMarker(element);
    matrixVtkWriter.addField(matrixDomainMarkers, "domainMarker");

    std::vector<int> fractureDomainMarkers(fractureFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(fractureFvGridGeometry->gridView()))
        fractureDomainMarkers[fractureFvGridGeometry->elementMapper().index(element)] = fractureProblem->spatialParams().getElementDomainMarker(element);
    fractureVtkWriter.addField(fractureDomainMarkers, "domainMarker");

    // write out initial solution
    matrixVtkWriter.write(0.0);
    fractureVtkWriter.write(0.0);

    // get some time loop parameters
    auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<double>("TimeLoop.DtInitial");
	auto dtInitial = dt;
	//auto injLowSalTime = getParamFromGroup<double>("", "Problem.injLowSalTime");
	auto injLowSalTime = getParam<double>("Matrix.Problem.injLowSalTime");
	auto injLowSalTimeFrac = getParam<double>("Fracture.Problem.injLowSalTime");
	if (injLowSalTime != injLowSalTimeFrac) {
		std::cout << "\nWarning: injLowSalTime is different in matrix and in fractures!! " << std::endl;
		std::cout << "     In matrix:    " <<  injLowSalTime << std::endl;
		std::cout << "     In fractures: " <<  injLowSalTimeFrac << std::endl;
	}
	
    // instantiate time loop
    auto timeLoop = std::make_shared< TimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop);

    // the linear solver
	//using LinearSolver = ILU0BiCGSTABBackend;
    //using LinearSolver = BlockDiagILU0BiCGSTABSolver;   // test to avoid linear solver crash (no success...)
    //using LinearSolver = BlockDiagAMGBiCGSTABSolver;   // test to avoid linear solver crash (really slow, and crashes even when the others pass well..)
    using LinearSolver = UMFPackBackend;  // UMFPackBackend not available
    //using LinearSolver = EigenBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    
    using Scalar = typename GET_PROP_TYPE(MatrixTypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(MatrixTypeTag, FluidSystem); // NA: assume the same fluid in matrix and in fractures
	
	// Save rel perm data to an external file
	fractureProblem->evalRelPerms();	
	matrixProblem->evalRelPerms();	

    // Inform matrix and fracture problems on current time
	matrixProblem->setTime(timeLoop->time());    	
	fractureProblem->setTime(timeLoop->time());  
	
    std::vector<Scalar> matrixVolume, fractureVolume;
    std::vector<Scalar> matrixInFlux, matrixOutFlux;    
    std::vector<Scalar> fractureInFlux, fractureOutFlux;    
    
    matrixVolume.resize(FluidSystem::numPhases);
    matrixInFlux.resize(FluidSystem::numPhases);
    matrixOutFlux.resize(FluidSystem::numPhases);

    fractureVolume.resize(FluidSystem::numPhases);
    fractureInFlux.resize(FluidSystem::numPhases);
    fractureOutFlux.resize(FluidSystem::numPhases);


    Scalar injOil = 0.;
    Scalar injWater = 0.;
    Scalar prodOil = 0.;
    Scalar prodWater = 0.;
    Scalar WCT = 0;
    
    // matrixPV is the pore volume, calculated from the .msh file geometry taking the width of fracture segments equal to zero
    // (domain height = matrix extrusionFactor() assumed to be equal 1)
    Scalar matrixPV = matrixProblem->poreVolume(x[matrixDomainId], *matrixGridVariables);
    // fracturePV is the pore volume of the set of fracture blocks, where each block volume has dimensions
    // fracture aperture (=fracture extrusionFactor())
    // * segment length  (=fracture volume())
    // * domain height = matrix extrusionFactor())
    // Fracture segments are stacked on top of each other (i.e. they are not overlapping),
    // therefore their total length may be greater than matrix domain length
    Scalar fracturePV = fractureProblem->poreVolume(x[fractureDomainId], *fractureGridVariables);
    Scalar PV = matrixPV + fracturePV;
    
    matrixProblem->getVolumesFluxes(matrixVolume, matrixInFlux, matrixOutFlux, x[matrixDomainId], *matrixGridVariables);
    fractureProblem->getVolumesFluxes(fractureVolume, fractureInFlux, fractureOutFlux, x[fractureDomainId], *fractureGridVariables); 
        
    Scalar OIP = matrixVolume[FluidSystem::phase1Idx] + fractureVolume[FluidSystem::phase1Idx]; //    	nPhaseIdx = FluidSystem::phase1Idx is the non-wetting phase index as defined in matrixproblem.hh and fractureproblem.hh
    Scalar WIP = matrixVolume[FluidSystem::phase0Idx] + fractureVolume[FluidSystem::phase0Idx]; //    	wPhaseIdx = FluidSystem::phase0Idx is the wetting phase index as defined in matrixproblem.hh and fractureproblem.hh
    Scalar OIIP = OIP;    
    Scalar WIIP = WIP;   
	Scalar RF = 0.;
	Scalar RF_prev, dRF;
        
    
    /*
    matrixVolume.resize(FluidSystem::numPhases);
    fractureVolume.resize(FluidSystem::numPhases);
    
    matrixInFlux.resize(FluidSystem::numPhases);
    matrixOutFlux.resize(FluidSystem::numPhases);
    
    fractureInFlux.resize(FluidSystem::numPhases);
    fractureOutFlux.resize(FluidSystem::numPhases)
    */
    
   // matrixProblem->getVolumesFluxes(matrixVolume, matrixInFlux, matrixOutFlux, x[matrixDomainId], *matrixGridVariables);
   // fractureProblem->getVolumesFluxes(fractureVolume, fractureInFlux, fractureOutFlux, x[matrixDomainId], *matrixGridVariables);
    
    //Scalar fOIP, fWIP, fFO, fFW;
    //fractureProblem->getVolumesFluxes(fOIP, fWIP, fFO, fFW, x[fractureDomainId], *fractureGridVariables);
    
    // External file for output of volumes
    std::string filename( "volumes.csv");
    std::ofstream file( filename.c_str() );
    if( file )
    {
    	//file << "Time (sec),Time (days),OIP (m3),WIP (m3),Recovery" << std::endl;
    	//file << timeLoop->time() << "," << timeLoop->time()/24./3600. << "," << OIP << "," << WIP << "," << RF << std::endl;
        
        const char *header = "Time (sec),"
                             "Time (days),"
                             "Injected oil volume (m3),"
                             "Injected water volume (m3),"
                             "Water PV injected,"
                             "Produced oil volume (m3),"
                             "Produced water volume (m3),"
                             "WCT,"
                             "OIP (m3),"
                             "WIP (m3),"
                             "Total fluid volume conservation error (m3),"
        					 "Total fluid volume conservation error (%%),"
                             "Recovery factor";
        file << header << std::endl;        
    }
    else
    {
      std::cerr << "ERROR: couldn't open file `" << filename << "'" << std::endl;
    }

    // Used to cut timestep when simulation time approaches a change in injection rate
    int haveCutTimes = 0;
	
	int saveVTKFrequency = getParam<int>("TimeLoop.SaveVTKFrequency");
	
    // time loop
	int loopNr = 0;
    timeLoop->start(); do
    {
		// Inform matrix and fracture problems on current time
		matrixProblem->setTime(timeLoop->time());    	
		fractureProblem->setTime(timeLoop->time());  
		
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        newtonSolver->solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        matrixGridVariables->advanceTimeStep();
        fractureGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();
        
        // Compute the volumes in place
        //matrixProblem->getVolumes(matrixVolume, mOIP, mWIP, x[matrixDomainId], *matrixGridVariables);
        //fractureProblem->getVolumes(fOIP, fWIP, x[fractureDomainId], *fractureGridVariables);
        //matrixProblem->getVolumesFluxes(mOIP, mWIP, mFO, mFW, x[matrixDomainId], *matrixGridVariables);
        //fractureProblem->getVolumesFluxes(fOIP, fWIP, fFO, fFW, x[fractureDomainId], *fractureGridVariables);
        
        matrixProblem->getVolumesFluxes(matrixVolume, matrixInFlux, matrixOutFlux, x[matrixDomainId], *matrixGridVariables);
        fractureProblem->getVolumesFluxes(fractureVolume, fractureInFlux, fractureOutFlux, x[fractureDomainId], *fractureGridVariables);     

        OIP = matrixVolume[FluidSystem::phase1Idx] + fractureVolume[FluidSystem::phase1Idx]; //    	nPhaseIdx = FluidSystem::phase1Idx is the non-wetting phase index as defined in matrixproblem.hh and fractureproblem.hh
        WIP = matrixVolume[FluidSystem::phase0Idx] + fractureVolume[FluidSystem::phase0Idx]; //    	wPhaseIdx = FluidSystem::phase0Idx is the wetting phase index as defined in matrixproblem.hh and fractureproblem.hh
        RF_prev = RF;
        RF = (OIIP - OIP) / OIIP;    
		if (RF_prev > 1e-8)
			dRF = (RF - RF_prev) / RF_prev;
		else
			dRF = 1.;
		
        
        Scalar oilInFlux = matrixInFlux[FluidSystem::phase1Idx] + fractureInFlux[FluidSystem::phase1Idx];
        Scalar waterInFlux = matrixInFlux[FluidSystem::phase0Idx] + fractureInFlux[FluidSystem::phase0Idx];     
        
        Scalar oilOutFlux = matrixOutFlux[FluidSystem::phase1Idx] + fractureOutFlux[FluidSystem::phase1Idx];
        Scalar waterOutFlux = matrixOutFlux[FluidSystem::phase0Idx] + fractureOutFlux[FluidSystem::phase0Idx];   
            
        dt = timeLoop->timeStepSize();
        injOil   += dt * std::abs(oilInFlux);
        injWater += dt * std::abs(waterInFlux);
        prodOil   += dt * std::abs(oilOutFlux);
        prodWater += dt * std::abs(waterOutFlux);
        
        WCT = waterOutFlux / (waterOutFlux + oilOutFlux);
        
        Scalar volError = OIP + WIP - OIIP - WIIP + injOil + injWater - prodOil - prodWater;

        
        file << timeLoop->time() << "," <<              // Time (sec)
                timeLoop->time()/24./3600. << "," <<    // Time (days)
                injOil   << "," <<                      // Injected oil volume (m3)
                injWater << "," <<                      // Injected water volume (m3)
                injWater / PV << "," <<                 // Water PV injected
                prodOil << "," <<                       // Produced oil volume (m3)
                prodWater << "," <<                     // Produced water volume (m3)
                WCT << "," <<                           // Water cut
                OIP << "," <<                           // OIP (m3)
                WIP << "," <<                           // WIP (m3)
                volError << "," <<                      // Total fluid volume conservation error (m3)
                volError / PV * 100 << "," <<           // Total fluid volume conservation error (%)
                RF <<                                   // Recovery factor
                std::endl;       

        
		// Eventually write vtk output
		if (loopNr % saveVTKFrequency == 0) 
		{
			matrixVtkWriter.write(timeLoop->time());
			fractureVtkWriter.write(timeLoop->time());
		}				
                
        
        // report statistics of this time step
        timeLoop->reportTimeStep();
	
        // Constantly shift the end time so that we never hit it
        if (timeLoop->time() + 10. * dt >= tEnd) {
        	tEnd = std::max(maxDt, tEnd + 10. * maxDt);	// Eventually increase the end time since we want to stop the simulation when WCT < 0.95
        	timeLoop->setEndTime(tEnd);					// Inform timeLoop about new tEnd so that the time step will not be cut
        }	
        
		// Eventually change timestep in problem due to change in injection rates
		Scalar nextDt = newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize());		
		if ((timeLoop->time() < injLowSalTime) && (timeLoop->time() + nextDt >= injLowSalTime)) {
			if (haveCutTimes == 1){
				nextDt = dtInitial;	
				haveCutTimes = 2;
			}
			else if (haveCutTimes == 0) {
				//nextDt = injLowSalTime - timeLoop->time() - dtInitial;
				nextDt = (injLowSalTime - timeLoop->time()) / 2.;
				haveCutTimes = 1;
			}			
		}
		timeLoop->setTimeStepSize(nextDt);	

		loopNr++;	
		
	} while (1);
    
    // Close the external file for output of volumes
    file.close();
        
    // output some Newton statistics
    newtonSolver->report();

    // report time loop statistics
    timeLoop->finalize();

    // print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
