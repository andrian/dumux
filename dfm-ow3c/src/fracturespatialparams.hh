// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial parameters for the fracture sub-domain in the exercise
 *        on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH

#include <dumux/io/grid/griddata.hh>

#include <dumux/material/spatialparams/fv.hh>

#include "fluidmatrixinteractions/regularizedbrookscorey.hh"
#include "fluidmatrixinteractions/efftoabslaw.hh"

namespace Dumux {

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial params the two-phase facet coupling test
 */
template< class FVGridGeometry, class Scalar >
class FractureSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, FractureSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = FractureSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParams< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
	using FVElementGeometry = typename FVGridGeometry::LocalView;	
	using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    // use a regularized van-genuchten material law
    //using EffectiveLaw = RegularizedVanGenuchten<Scalar>;
    // use a Brooks-Corey rel perms and polynomially fitted capillary pressure curve
   // using EffectiveLaw = MyBrooksCorey<Scalar>;

    // we identify those fractures as barriers, that have a domain marker
    // of 2. This is what is set in the grid file (see grids/complex.geo)
    static constexpr int barriersDomainMarker = 2;

public:
	//using SolidSystem = typename GET_PROP_TYPE(TypeTag, SolidSystem);
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw<RegularizedBrooksCorey<Scalar>>;
    using MaterialLawParams = typename MaterialLaw::Params;

    //! the constructor
    FractureSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          std::shared_ptr<const Dumux::GridData<Grid>> gridData,
                          const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    {
		/*
		porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");

        // set the material law parameters
        materialLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
        materialLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));        
        materialLawParams_.setPe(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.BCPe"));
        materialLawParams_.setLambda(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.BCLambda"));    
		materialLawParams_.setLSWSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Snr"));
        materialLawParams_.setLSWSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Swr"));

		permeabilityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityBarrier");
		porosityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PorosityBarrier");

        materialLawParamsBarrier_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.SnrBarrier"));
        materialLawParamsBarrier_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.SwrBarrier"));
        materialLawParamsBarrier_.setPe(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.BCPeBarrier"));
        materialLawParamsBarrier_.setLambda(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.BCLambdaBarrier"));
		materialLawParamsBarrier_.setLSWSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Snr"));
        materialLawParamsBarrier_.setLSWSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Swr"));
		
		*/
		
		        
		// set the fractures parameters	
		porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
		
		materialLawParams_.setKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.krw"));
		materialLawParams_.setKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.krn"));
		materialLawParams_.setNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.nw"));		
		materialLawParams_.setNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.nn"));
		materialLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));
		materialLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
		
		materialLawParams_.setLSWKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_krw"));
		materialLawParams_.setLSWKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_krn"));
		materialLawParams_.setLSWNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_nw"));		
		materialLawParams_.setLSWNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_nn"));
		materialLawParams_.setLSWSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Swr"));
		materialLawParams_.setLSWSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.LSW_Snr"));		
		
		
		// set the barrier parameters	
		permeabilityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Permeability");
		porosityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Porosity");
		        	
		materialLawParamsBarrier_.setKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_krw"));
		materialLawParamsBarrier_.setKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_krn"));
		materialLawParamsBarrier_.setNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_nw"));		
		materialLawParamsBarrier_.setNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_nn"));
		materialLawParamsBarrier_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_Swr"));
		materialLawParamsBarrier_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
		
		materialLawParamsBarrier_.setLSWKrw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_krw"));
		materialLawParamsBarrier_.setLSWKrn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_krn"));
		materialLawParamsBarrier_.setLSWNw(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_nw"));		
		materialLawParamsBarrier_.setLSWNn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_nn"));
		materialLawParamsBarrier_.setLSWSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_Swr"));
		materialLawParamsBarrier_.setLSWSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Barrier_LSW_Snr"));			
		
    }
    
	//! Define the constant inert volume inertVolumeFraction (for a dynamic inert volume fraction, see biominspatialparams.hh
    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        return 1-porosity_;
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template< class ElementSolution >
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        // TODO Task B: change fracture properties
        return permeability_;
    }

    //! Return the porosity
    template< class ElementSolution >
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        // TODO Task B: change fracture properties
        return porosity_;
    }

    //! Return the material law parameters
    template< class ElementSolution >
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        // TODO Task B: change fracture properties
        return materialLawParams_;
    }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the H2oN2 fluid system
        return FluidSystem::phase0Idx;
    }
    
    // NA
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        return materialLawParams_;
    }
    

    //! returns the domain marker for an element
    int getElementDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    Scalar porosity_;
    Scalar porosityBarrier_;
    PermeabilityType permeability_;
    PermeabilityType permeabilityBarrier_;
    MaterialLawParams materialLawParams_;
    MaterialLawParams materialLawParamsBarrier_;
};

} // end namespace Dumux

#endif
