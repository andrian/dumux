// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SolidStates
 * \brief update the solid volume fractions (inert and reacitve) and set them in the solidstate
 */
#ifndef DUMUX_UPDATE_SOLID_VOLUME_FRACTION_HH
#define DUMUX_UPDATE_SOLID_VOLUME_FRACTION_HH

namespace Dumux {

/*!
 * \ingroup SolidStates
 * \brief update the solid volume fractions (inert and reacitve) and set them in the solidstate
 * \note updates the inert components (TODO: these are assumed to come last right now in the solid system!)
 * \note gets the non-inert components from the primary variables
 */
template<class ElemSol, class Problem, class Element, class Scv, class FluidState, class SolidState>
void updateSolidVolumeFractions(const ElemSol& elemSol,
                                const Problem& problem,
                                const Element& element,
                                const Scv& scv,
								FluidState& fluidState,
                                SolidState& solidState,
                                const int solidVolFracOffset)
{
	using Scalar = typename FluidState::FluidSystem::Scalar;
    using FluidSystem = typename FluidState::FluidSystem;
    using SolidSystem = typename SolidState::SolidSystem;
    const auto& sp = problem.spatialParams();

	for (int sCompIdx = solidState.numComponents-solidState.numInertComponents; sCompIdx < solidState.numComponents; ++sCompIdx)
    {
//        const auto& sp = problem.spatialParams();
        const auto inertVolumeFraction = sp.template inertVolumeFraction<SolidSystem>(element, scv, elemSol, sCompIdx);
        solidState.setVolumeFraction(sCompIdx, inertVolumeFraction);
    }

    if (!(solidState.isInert()))
    {
//        auto&& priVars = elemSol[scv.localDofIndex()];
//        auto fvGeometry = localView(problem.fvGridGeometry());
//        //fvGeometry.bindElement(element);
//        fvGeometry.bind(element);
//        solidState.setVolumeFraction(0, 0);


        auto waterPhaseIdx = FluidSystem::waterPhaseIdx;
        auto LSWIdx = FluidSystem::LSWIdx;
        auto BrineIdx = FluidSystem::BrineIdx;

        // All mole fractions in the fluidState are set in completeFluidState()
        Scalar xwL = fluidState.moleFraction(waterPhaseIdx, LSWIdx);	// Mole fraction of LSW in water
        Scalar xwS = fluidState.moleFraction(waterPhaseIdx, BrineIdx);	// Mole fraction of SW in water

        // Get the cell-specific properties from the corresponding spatialParams and problem
        Scalar phi = sp.porosity(element, scv, elemSol);
        Scalar extrusionFactor = problem.extrusionFactor(element, scv, elemSol);
        Scalar Mr = SolidSystem::molarMass(0);		// Molar mass of rock
        Scalar V = scv.volume()* extrusionFactor;	// Total volume
        Scalar Vp = phi * V;						// Pore volume
        Scalar Sw = fluidState.saturation(waterPhaseIdx);
        Scalar rhow = fluidState.density(waterPhaseIdx);
        Scalar Mw = fluidState.averageMolarMass(waterPhaseIdx);

        // Parameter values before adsorption
        Scalar rhorI = getParam<Scalar>("Solid.Density");
        Scalar VrI = V * (1 - phi );					// Cell rock volume
        Scalar mrI = rhorI * VrI;						// Cell rock mass
        Scalar nrI = mrI / Mr;							// Number of moles of rock in the cell
        Scalar nwL = xwL * Sw * Vp * rhow / Mw;			// Number of moles of LSW in water phase
        Scalar nwS = (1. - xwL) * Sw * Vp * rhow / Mw;	// Number of moles of SW in water phase

        // Parameters of adsorbed LSW
        Scalar xrL = SolidSystem::adsorptionIsotherm( xwL);	// Mole fraction of adsorbed LSW on the rock
        Scalar nrL = nrI * xrL / (1 - xrL);					// Number of moles of adsorbed LSW in the cell
        Scalar rhorL = rhorI;								// ASSUME that the density of adsorbed LSW is equal to initial rock density
        Scalar VrL = nrL * Mw / rhorL;						// Volume of adsorbed LSW

        // Parameters after adsorption
        Vp = V - (VrI + VrL);
        phi = Vp / V;
        nwL = nwL - nrL;
        xwL = nwL / (nwL + nwS);	// nwS does not change due to adsorption
        xwS = 1 - xwL;
        Scalar VwL = Mw * nwL / rhow;
        Scalar VwS = Mw * nwS / rhow;
        Sw = (VwL + VwS) / Vp;

        // Modify the fluidState
        fluidState.setMoleFraction(waterPhaseIdx, LSWIdx, xwL);
        fluidState.setMoleFraction(waterPhaseIdx, BrineIdx, xwS);

    }
}

//template<class FluidState, class SolidState>
//double adsorptionIsotherm(FluidState& fluidState,
//						  SolidState& solidState,
//						  int fluidCompIdx,
//						  int solidCompIdx)
//{
//	using SolidSystem = typename SolidState::SolidSystem;
//
//	return SolidSystem::molarMass(solidCompIdx);
//
//}

} // end namespace Dumux

#endif
