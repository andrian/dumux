// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SolidSystems
 * \brief @copybrief Dumux::SolidSystems::InertSolidPhase
 */
#ifndef DUMUX_SOLIDSYSTEMS_ADSORPTION_SOLID_PHASE_HH
#define DUMUX_SOLIDSYSTEMS_ADSORPTION_SOLID_PHASE_HH

#include <string>
#include <dune/common/exceptions.hh>

// #include "../components/biofilm.hh"
// #include <dumux/material/components/calcite.hh>
// #include <dumux/material/components/granite.hh>

namespace Dumux {
namespace SolidSystems {

/*!
 * \ingroup SolidSystems
 *
 * \note Based on ./dumux-course/exercises/exercise-biomineralization/solidsystems/biominsolidphase.hh
 *
 */
template <class Scalar>
class AdsorptionSolidPhase
{
public:
	
//     using Biofilm = Components::Biofilm<Scalar>;
//     using Calcite = Components::Calcite<Scalar>;
//     using Granite = Components::Granite<Scalar>;

    /****************************************
     * Solid phase related static parameters
     ****************************************/
     static constexpr int numComponents = 2;
     static constexpr int numInertComponents = 1;

//     static constexpr int noAdsorption = 0;
//     static constexpr int linearAdsorption = 1;
//     static constexpr int freundlichAdsorption = 2;
//     static constexpr int langmuirAdsorption = 3;


//     static constexpr int BiofilmIdx = 0;
//     static constexpr int CalciteIdx = 1;
//     static constexpr int GraniteIdx = 2;

    /*!
     * \brief Return the human readable name of a solid phase
     *
     * \param compIdx The index of the solid phase to consider
     */
//     static std::string componentName(int compIdx)
//     {
//         switch (compIdx)
//         {
//             case BiofilmIdx: return Biofilm::name();
//             case CalciteIdx: return Calcite::name();
//             case GraniteIdx: return Granite::name();
//             default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
//         }
//     }


    /*!
     * \brief Initialize the solid system's static parameters
     */
    static void init()
    {

//        static bool adsorptionIsotherm;
//        static Scalar density_;
//        static Scalar adsorptionGamma1;
//        static Scalar adsorptionGamma2;
//        static Scalar adsorptionN;
//
//    	try {
//    		// If Solid.Density is present in the .input file, use the adsorption isotherm
//    		// as specified by AdsorptionGamma1, AdsorptionGamma2, and AdsorptionN
//    		density_ = getParam<Scalar>("Solid.Density");
//    		adsorptionGamma1 = getParam<Scalar>("Solid.AdsorptionGamma1");
//    		adsorptionGamma2 = getParam<Scalar>("Solid.AdsorptionGamma2");
//    		adsorptionN = getParam<Scalar>("Solid.AdsorptionN");
//    		adsorptionIsotherm = true;
//
//    	} catch (const std::exception& e) {
//    		adsorptionIsotherm = false;
//    	}

//        switch (adsorptionIsotherm)
//        {
//            case noAdsorption: std::cout << "\nNo adsorption..\n" << std::endl;
//            case linearAdsorption: std::cout << "\nUsing linear adsorption isotherm..\n" << std::endl;
//            case freundlichAdsorption: std::cout << "\nUsing Freundlich adsorption isotherm..\n" << std::endl;
//            case langmuirAdsorption: std::cout << "\nUsing Langmuir adsorption isotherm..\n" << std::endl;
//            default: DUNE_THROW(Dune::NotImplemented, "Invalid adsorption model..");
//        }

	}
    
    
    
    /*!
     * \brief A human readable name for the solid system.
     */
    static std::string name()
    { return "AdsorptionSolidPhase"; }

    /*!
     * \brief Returns whether the phase is incompressible
     */
    static constexpr bool isCompressible(int compIdx)
    { return false; }

    /*!
     * \brief Returns whether all components are inert (don't react)
     */
    static bool isInert()
	{
    	// Do not consider a separate reactive component in the solid phase
    	return true;
    	//return false;

//    	// Cannot use any protected members/flags because isInert() is called from compositionalsolidstate.hh as static
//    	try {
//    		// If Solid.Density is present in the .input file, use the adsorption isotherm
//    		// as specified by AdsorptionGamma1, AdsorptionGamma2, and AdsorptionN
//    		Scalar density_ = getParam<Scalar>("Solid.Density");
//    		return (numComponents == numInertComponents);
//
//    	} catch (const std::exception& e) {
//    		return false;
//    	}

//    	if (!(adsorptionIsotherm))
//    		return true;
//    	else
//    		return (numComponents == numInertComponents);
	}

    /*!
     * \brief Assume that the molar masses of all solid components are the same
     */
    static Scalar molarMass(int compIdx)
    {
    	return 60.08e-3;	// The value for granite
    }

    /*!
     * \brief Assume that the solid density does not change with adsorption and equals value of Solid.Density from .input file
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState)
    {
		return getParam<Scalar>("Solid.Density");
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the solid phase at a given pressure and temperature.
     * \brief Assume that the solid density does not change with adsorption and equals value of Solid.Density from .input file
     */
    template <class SolidState>
    static Scalar density(const SolidState& solidState, const int compIdx)
    {
    	return getParam<Scalar>("Solid.Density");
    }

    /*!
     * \brief The molar density of the solid phase at a given pressure and temperature.
     */
    template <class SolidState>
    static Scalar molarDensity(const SolidState& solidState, const int compIdx)
    {
    	return getParam<Scalar>("Solid.Density") / molarMass(compIdx);
    }


    /*!
     * \brief Calculate the mole fraction of the adsorbed component xr using a generalized adsorption isotherm equation
     *
     * 			xr = gamma1 * molarFracLSW^n / (1 + gamma2 * molarFracLSW)
     */
    static Scalar adsorptionIsotherm(Scalar molarFracLSW)
    {
    	// Calculate the adsorption isotherm if Solid.Density is present in the .input file
        static bool adsorptionIsotherm;
        static Scalar density_;
        static Scalar adsorptionGamma1;
        static Scalar adsorptionGamma2;
        static Scalar adsorptionN;
    	try {
    		// If Solid.Density is present in the .input file, use the adsorption isotherm
    		// as specified by AdsorptionGamma1, AdsorptionGamma2, and AdsorptionN
    		density_ = getParam<Scalar>("Solid.Density");
    		adsorptionGamma1 = getParam<Scalar>("Solid.AdsorptionGamma1");
    		adsorptionGamma2 = getParam<Scalar>("Solid.AdsorptionGamma2");
    		adsorptionN = getParam<Scalar>("Solid.AdsorptionN");
    		adsorptionIsotherm = true;
    	} catch (const std::exception& e) {
    		adsorptionIsotherm = false;
    	}

		if (adsorptionIsotherm) {
    		return adsorptionGamma1 * std::pow(molarFracLSW, adsorptionN) / (1 + adsorptionGamma2 * molarFracLSW);
		}
    	else
    		return 0;

    }




    /*!
     * \brief Thermal conductivity of the solid \f$\mathrm{[W/(m K)]}\f$.
     */
    template <class SolidState>
    static Scalar thermalConductivity(const SolidState &solidState)
    {
//         Scalar lambda1 = Biofilm::solidThermalConductivity(solidState.temperature());
//         Scalar lambda2 = Calcite::solidThermalConductivity(solidState.temperature());
//         Scalar lambda3 = Granite::solidThermalConductivity(solidState.temperature());
//         Scalar volFrac1 = solidState.volumeFraction(BiofilmIdx);
//         Scalar volFrac2 = solidState.volumeFraction(CalciteIdx);
//         Scalar volFrac3 = solidState.volumeFraction(GraniteIdx);
// 
//         return (lambda1*volFrac1+
//                 lambda2*volFrac2+
//                 lambda3*volFrac3)
//                /(volFrac1+volFrac2+volFrac3);
		return 0;
    }

    /*!
     * \brief Specific isobaric heat capacity of the pure solids \f$\mathrm{[J/(kg K)]}\f$.
     */
    template <class SolidState>
    static Scalar heatCapacity(const SolidState &solidState)
    {
//         Scalar c1 = Biofilm::solidHeatCapacity(solidState.temperature());
//         Scalar c2 = Calcite::solidHeatCapacity(solidState.temperature());
//         Scalar c3 = Granite::solidHeatCapacity(solidState.temperature());
//         Scalar volFrac1 = solidState.volumeFraction(BiofilmIdx);
//         Scalar volFrac2 = solidState.volumeFraction(CalciteIdx);
//         Scalar volFrac3 = solidState.volumeFraction(GraniteIdx);
// 
//         return (c1*volFrac1+
//                 c2*volFrac2+
//                 c3*volFrac3)
//                /(volFrac1+volFrac2+volFrac3);
		return 0;
    }


};

} // end namespace SolidSystems
} // end namespace Dumux

#endif
