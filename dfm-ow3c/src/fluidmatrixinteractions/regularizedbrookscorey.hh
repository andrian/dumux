// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 * \brief Implementation of a regularized version of the Brooks-Corey
 *        capillary pressure / relative permeability  <-> saturation relation.
 */
#ifndef REGULARIZED_BROOKS_COREY_HH
#define REGULARIZED_BROOKS_COREY_HH

#include "brookscorey.hh"
#include "regularizedbrookscoreyparams.hh"



#include <dumux/common/spline.hh>

namespace Dumux
{
/*!
 * \ingroup Fluidmatrixinteractions
 * \brief Implementation of the regularized  Brooks-Corey
 *        capillary pressure / relative permeability  <-> saturation relation.
 *        This class bundles the "raw" curves as
 *        static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 *        In order to avoid very steep gradients the marginal values are "regularized".
 *        This means that in stead of following the curve of the material law in these regions, some linear approximation is used.
 *        Doing this is not worse than following the material law. E.g. for very low wetting phase values the material
 *        laws predict infinite values for \f$\mathrm{p_c}\f$ which is completely unphysical. In case of very high wetting phase
 *        saturations the difference between regularized and "pure" material law is not big.
 *
 *        Regularizing has the additional benefit of being numerically friendly: Newton's method does not like infinite gradients.
 *
 *        The implementation is accomplished as follows:
 *        - check whether we are in the range of regularization
 *         - yes: use the regularization
 *         - no: forward to the standard material law.
 *
 *         For an example figure of the regularization: RegularizedVanGenuchten
 *
 * \see BrooksCorey
 */
template <class ScalarT, class ParamsT = RegularizedBrooksCoreyParams<ScalarT> >
class RegularizedBrooksCorey
{
    using BrooksCorey = Dumux::BrooksCorey<ScalarT, ParamsT>;

public:
    using Params = ParamsT;
    using Scalar = typename Params::Scalar;

    /*!
     * \brief A regularized Brooks-Corey capillary pressure-saturation
     *        curve.
     *
     * Since the Brooks-Corey pc is replaced with a polynomial approximation,
	 * there is no singularity at Swe = 0 and Swe = 1.
     */
    static Scalar pc(const Params &params, Scalar swe)
    {

        if (swe <= 0.0) {
            return BrooksCorey::pc(params, 0.0);
        }
        else if (swe >= 1.0) {
            return BrooksCorey::pc(params, 1.0);
        }

        // if the effective saturation is in an 'reasonable'
        // range, we use the real Brooks-Corey law...
        return BrooksCorey::pc(params, swe);
    }


    /*!
     * \brief   Regularized version of the Brooks-Corey relative permeability
     *          for the wetting phase
     */
    static Scalar krwBrine(const Params &params, Scalar swe)
    {
        if (swe <= 0.0)
            return 0.0;
        else if (swe >= 1.0)
            return params.Krw();

        return BrooksCorey::krwBrine(params, swe);
    }
    
    

    /*!
     * \brief   Regularized version of the Brooks-Corey relative permeability
     *          for the non-wetting phase
     */
   static Scalar krnBrine(const Params &params, Scalar swe)
   {
       if (swe >= 1.0)
           return 0.0;
       else if (swe <= 0.0)
           return params.Krn();

       return BrooksCorey::krnBrine(params, swe);
   }

    /*!
     * \brief   Regularized version of the Brooks-Corey relative permeability
     *          for the wetting phase
     */
    static Scalar krwLSW(const Params &params, Scalar swe)
    {
        if (swe <= 0.0)
            return 0.0;
        else if (swe >= 1.0)
            return params.LSWKrw();

        return BrooksCorey::krwLSW(params, swe);
    }
    
    

    /*!
     * \brief   Regularized version of the Brooks-Corey relative permeability
     *          for the non-wetting phase
     */
   static Scalar krnLSW(const Params &params, Scalar swe)
   {
       if (swe >= 1.0)
           return 0.0;
       else if (swe <= 0.0)
           return params.LSWKrn();

       return BrooksCorey::krnLSW(params, swe);
   }

};
}

#endif
