# Discrete Fracture-Matrix (DFM) oil-water 3-component model

This project contains the DuMuX module for the Discrete Fracture-Matrix (DFM) two-phase (oil and water) model[^fn1], where oil is represented by a single oil component, and water - by two components. The code is based on the DFM module[^fn2] with a modified fluid model, boundary conditions, and I/O.


## Prerequisites

Install the self-contained version of [DuMuX](https://dumux.org) as described [here](../README.md).


## Installation

Compile the DFM oil-water model
```bash
cd dfm-ow3c/build-cmake-O3/src/
make dfm-ow3c
```

## References
[^fn1]: N. Andrianov and H. M. Nick, Modeling of waterflood efficiency using outcrop-based fractured models, submitted, 2019.
[^fn2]: D. Gläser, R. Helmig, B. Flemisch, and H Class, A discrete fracture model for two-phase flow in fractured porous media, Advances in Water Resources 110 (2017) 335–348.
