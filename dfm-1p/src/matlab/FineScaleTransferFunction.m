classdef FineScaleTransferFunction < TransferFunction
    %% The transfer function based on fine-scale simulations
    properties
        smin
        smax
        qmax
        C
        E
    end

    methods

        function transferfunction = FineScaleTransferFunction(smin, a, b)

            transferfunction = transferfunction@TransferFunction();
            transferfunction.nphases = 2;   % For compatibility with the parent class
           
            transferfunction.smin = smin;
            %transferfunction.smax = smax;
            %transferfunction.qmax = qmax;
            
            % Change the parameters of the transfer function to handle the 
            % case when there are cells with no fractures 
            transferfunction.C = a;
            transferfunction.E = b;
            indf = find(transferfunction.C);
            indm = setdiff([1:length(smin)]', indf);
            transferfunction.E(indm) = 0;

        end


        function [Talpha] = calculate_transfer(ktf,model,fracture_fields,matrix_fields)

            %% All calculate_transfer method should have this call. This is a "sanity check" that
            % ensures that the correct structures are being sent to calculate the transfer
            ktf.validate_fracture_matrix_structures(fracture_fields,matrix_fields);

            % Get the varibles
            swm = matrix_fields.swm;
            swf = fracture_fields.swf;
  
% NA 25.04.2020                       
%             try
%                 aa = ~isempty(find(swf.val ~= 1)) || ~isempty(find(swm.val ~= 1)) ;
%             catch
%                 disp('error');
%             end
%                     
%             
%             if ~isempty(find(swf.val ~= 1)) || ~isempty(find(swm.val ~= 1)) 
%                 msg = 'Non single phase fluid in FineScaleTransferFunction.m... Exiting..';
%                 error(msg);
%             end

            % Only one phase is present
            pwm = matrix_fields.pom;
            pwf = fracture_fields.pof;

            % Dimensional parameters
            Km = model.rock_matrix.perm(:,1);
            mu = model.fluid_matrix.muW(pwm);
            bw = model.fluid.bW(pwm);            
            P = model.fluid.P;
            
            % Undimensionalizing the pressure difference
            dp = 1/P * (pwm-pwf);
            sgn = 2 * (dp >= 0) - 1;
%             ip = find(dp < -1e-8);
%             if ~isempty(ip)
%                 aa = 1;
%             end            
            adp = abs(dp);            
            
            % Zero the transfer function for matrix cells
            %matind = setdiff([1:model.G.cells.num]', model.G.fracind);
            
            % Get the absolute value of the dimensionaless transfer function
            %qDL = ktf.smin .* dp + (ktf.qmax-ktf.smin).*dp.^((ktf.smax-ktf.smin)./(ktf.qmax-ktf.smin)); 
             %%qDL = ktf.smin .* dp + ktf.C .* dp .^ ktf.E; 
             qDL = ktf.smin .* adp + ktf.C .* adp .^ ktf.E; 
             
             % Linear approximation
            qDL = ktf.smin .* adp;
             
             % The signed dimensionaless transfer function
             qDL = sgn .* qDL;

            % Calculate  the dimensional transfer function 
            q = - qDL .* Km ./ mu .* P;              
            %q(matind)=0;     
            
%             figure(8)            
%             qDL(matind)=0;
%             plot(dp, qDL, 'o')
             
            tw = bw .* q;
            to = 0 .* bw .* q;

            % Note that we return a 2x1 Transfer since our model is 2ph
            Talpha{1} = tw;
            Talpha{2} = to;
        end

        function [] = validate_fracture_matrix_structures(ktf,fracture_fields,matrix_fields)
            %% We use the superclass to validate the structures of matrix/fracture variables
            validate_fracture_matrix_structures@TransferFunction(ktf,fracture_fields,matrix_fields);
        end

    end


end
%{
Copyright 2009-2016 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
 