%% Set up the Laegerdorf test case 
%
% OUTPUT:
% G - the MRST grid structure
% rock_matrix, rock_fracture - MRST rock structures for matrix and fractures
% state - initial conditions
% t - simulation time instants
function [state, model, schedule, t] = setupLaegerdorf(varargin)

Nargs = 0;
opts = [];
if (nargin > Nargs) % if there are other arguments than (fracPos, Nx, Ny) then...
   n = [1:2:nargin-1-Nargs]; 
   opts = varargin(n); 
   n = [2:2:nargin-Nargs]; 
   vals = varargin(n);
end

% Default options
plotfracs = false;
plots = false;
plotrock = false;
for k = 1:length(opts)       
    if strcmpi(opts{k},'plotfracs')
        plotfracs = vals{k};
    end
    if strcmpi(opts{k},'plots')
        plots = vals{k};
    end
    if strcmpi(opts{k},'plotrock')
        plotrock = vals{k};
    end
end


%% Matrix and fracture properties 

% Matrix properties chosen to ensure the diffusivity Km/(mu*phi*Cm) = 1
phi = 0.3;           % Porosity
Km = 9.8692e-14;     % Matrix permeability, [m2] (= 100 mD)
Cm = 1e-9;           % Fluid compressibility, [1/Pa]
mu = 3.2897e-04;     % Fluid viscosity, [Pa*sec]

% Fracture properties chosen to ensure the diffusivity Km/(mu*phi*Cm)=1000
apert = 0.01; %0.1*milli*meter;
phi = 0.3;           % Porosity
Kf = 10^4*9.8692e-14; % Matrix permeability, [m2] (= 100 D)
Cm = 1e-9;           % Fluid compressibility, [1/Pa]
mu = 3.2897e-04;     % Fluid viscosity, [Pa*sec]

% Initial conditions
pm0 = 21e6;         % Matrix pressure, [Pa]

% Boundary conditions
pf = 20e6;          % Fracture pressure, [Pa]

pf = 20;    % To match the erroneous data in geiger.input

% Scaling parameter
P = pm0 - pf;

% The characteristic time
%tD = phi * L^2 * mu / ((pm0-pf)* Km);


% Dimensional constants
P0 = 20e6;
L = 2;
phim = 0.3;           % Porosity
Km = 9.8692e-14;     % Matrix permeability, [m2] (= 100 mD)
mu = 3.2897e-04;     % Fluid viscosity, [Pa*sec]
T = phim * L^2 * mu / (P * Km);


%% Get the fine scale solutions for Km <> 0 and Km = 0

% Reading the fine scale results for Km <> 0
fname = 'laegerdorf_results.csv';    
%fname = 'block_results.csv';    

fid = fopen(fname);
if (fid == -1)
    disp(['Cannot open ' fname]);
    return;
end   

disp(['Reading ' fname]);
header = strsplit(fgetl(fid), ',');
fclose(fid);
data = readtable(fname);

t = data{:, 1};   
pmDL = data{:, 3};      
pfDL = data{:, 4};
qtDL = data{:, 5};
qbDL = data{:, 6};

% % Reading the fine scale results for Km = 0
% fname = 'laegerdorf_results_Km0.csv';    
% fid = fopen(fname);
% if (fid == -1)
%     disp(['Cannot open ' fname]);
%     return;
% end   
% 
% disp(['Reading ' fname]);
% header = strsplit(fgetl(fid), ',');
% fclose(fid);
% data = readtable(fname);
% 
% t0 = data{:, 1};   
% pmDL0 = data{:, 3};      
% pfDL0 = data{:, 4};
% qtDL0 = data{:, 5};
% qbDL0 = data{:, 6};

% Dimensional output
qb = qbDL * Km/mu * P;
%qb0 = qbDL0 * 9.8692e-20/mu * P;    % Handcoding ...

if (plots)
    % Comparing the solutions with different Km
    figure(3)
    semilogx(t, qb, '-r')
    hold on
    %semilogx(t0, qb0, 'x-')
    xlabel('Time (sec)');
    ylabel('Boundary flux (m^2/sec)')
    %legend('Km <> 0', 'Km = 0')
    title('Boundary flux')
    
    figure(7)
    loglog(t, qb, '-r')
    hold on
    %semilogx(t0, qb0, 'x-')
    xlabel('Time (sec)');
    ylabel('Boundary flux (m^2/sec)')
    %legend('Km <> 0', 'Km = 0')
    title('Boundary flux')
end

%% Reading the upscaled permeabilities, porosities, and transfer rate parameters

% The grid and rock data are created with getUpscaledPoroPerm.m 
load('laegerdorf_grid.mat')
load('laegerdorf_rock.mat')
load('laegerdorf_transfer.mat')

% Create the upscaled geometry
G = cartGrid([Nx Ny],[xmax-xmin ymax-ymin]);
G.nodes.coords(:,1) = G.nodes.coords(:,1) + xmin;
G.nodes.coords(:,2) = G.nodes.coords(:,2) + ymin;
G = computeGeometry(G);


% % debug - use the values from setupNoetinger.m
% %fracCells = zeros(G.cells.num, 1);
% fracind = find(Kf(:, 3));
% %fracCells(fracind) = 1;
% 
% Nc = length(Kf);
% Kf = zeros(Nc, 2);
% Kf(fracind, 2) = 5.428060000000000e-11;
% 
% Km = 9.326394000000000e-14;
% 
% phim = 0.2835;
% phif = 0.0165;
% 
% smin(fracind) = 13.0206; 
% smax(fracind) = 2089.8167; 
% qmax(fracind)= 246.68;  

% % try to increase the uniform perm
% Kf = Kf(1, 3);
% Km = Km(1, 1);
% phif = phif(1);
% phim = phim(1);

% Set zero fracture porosities to a small value
izp = find(phif == 0);
if ~isempty(izp)
    poro0 = mean(phif) * 1e-8;
    disp(['Avoid MRST complaints by setting zero fracture porosities to ' num2str(poro0)])
    phif(izp) = poro0;
end

% Create the rock structures for fractures and the matrix
rock_fracture = makeRock(G, Kf, phif);
rock_matrix = makeRock(G, Km, phim);

if (plotfracs)
    % Plot the fractures on Figure 1
    plotMM('plot', 'data');
   
    % Overlay the dual porosity grid
    figure(1)
    hold on
    x = [xmin : (xmax-xmin)/Nx :xmax];
    y = [ymin : (ymax-ymin)/Ny :ymax];
    for i = 1:Nx+1
        line([x(i) x(i)], [ymin ymax], 'Color', 'r')
    end
    for j = 1:Ny+1
        line([xmin xmax], [y(j) y(j)], 'Color', 'r')
    end
    el = 1:G.cells.num;
    text(G.cells.centroids(:,1), G.cells.centroids(:,2), num2str(el(:)) )
end

if (plotrock)    
    
    md = milli*darcy;
    figure(60);
    plotCellData(G, rock_fracture.perm(:,1)/md)
    axis equal
    title('Kf_{xx} (mD)')
    colorbar
    
    figure(61);
    plotCellData(G, rock_fracture.perm(:,2)/md)
    axis equal
    title('Kf_{xy} (mD)')
    colorbar
    
    figure(62);
    plotCellData(G, rock_fracture.perm(:,3)/md)
    axis equal
    title('Kf_{yy} (mD)')
    colorbar
    
    
    
%     el = 1:G.cells.num;
%     hold on; text(G.cells.centroids(:,1), G.cells.centroids(:,2), num2str(el(:)) )
%     ef = 1:G.faces.num;
%     text(G.faces.centroids(:,1), G.faces.centroids(:,2), num2str(ef(:)), 'Color', 'r' )
end

% Use a two-phase fluid model with dummy oil properties to be able to use the TwoPhaseOilWaterDPModel below
fluid = initSimpleADIFluid('phases','WO',          ... % Fluid phases: water and oil
                           'mu',  [mu, mu],        ... % Viscosities
                           'rho', [730,730],       ... % Surface densities [kg/m^3]
                           'c',   [Cm, Cm],        ... % Fluid compressibility[Cm, Cm],              
                           'pRef', pm0,            ... % Reference pressure 
                           'cR',  0                ... % Rock compressibility
                           );

% Keep the scaling pressure in the fluid structure for use in FineScaleTransferFunction
fluid.P = P;                       
                       
% Use the 2 phase dual porosity - dual permeability model
model = TwoPhaseOilWaterDPDPModel(G, ...
                                rock_fracture, fluid, ...   % Fracture properties
                                rock_matrix, fluid, ...     % Matrix properties
                                [], ...                     % dp_info
                                'gravity', [0 0] ...
                                );
                            
% Initialize the transfer function from values from fine-scale simulations

% % debug
% fracCells = ones(G.cells.num, 1);
% smin = 13.0206; smax = 2089.8167; qmax = 246.68;   % For aperture = 0.01, Kf/Km=1e4,  and L = 2 (modified Noetinger)
% smin = smin * fracCells;
% smax = smax * fracCells;
% qmax = qmax * fracCells;


model.transfer_model_object = FineScaleTransferFunction(smin, smax, qmax);



% Initial conditions: 
state.pressure = ones(G.cells.num,1) * pm0;  % Fracture pressure
state.pom = ones(G.cells.num,1) * pm0;      % Matrix pressure
state.s = repmat([1 0],G.cells.num,1);
state.swm = ones(G.cells.num,1);

% In the current DPDP implementation, boundary conditions are only set for the fractures
% No-flow conditions implied for the matrix
% Set fixed pressure BC for the fractures
bc = pside([],G,'North',pf,'sat',[1 0]);
bc = pside(bc,G,'South',pf,'sat',[1 0]);
bc = pside(bc,G,'East',pf,'sat',[1 0]);
bc = pside(bc,G,'West',pf,'sat',[1 0]);

% For schedule, use the same time instants as specified in the fine scale results
dt = diff([0 t']);   % Time steps
schedule = struct(...
    'control', struct('W',{[]}), ...    % Apparently need a fictious well
    'step',    struct('control', ones(numel(dt),1), 'val', dt));
schedule.control.bc = bc;



end
