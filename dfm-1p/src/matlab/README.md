# MRST dual porosity/dual permeability model

The files in this folder add the dual permeability functionality in the [dual porosity module of MRST](https://www.sintef.no/contentassets/2551f5f85547478590ceca14bc13ad51/dual_porosity.html) for the single phase case[^fn1]. 

Currently only the no-flow boundary conditions for the matrix are implemented.


## Prerequisites

The dual permeability option has only been tested with [MRST 2018b](https://www.sintef.no/projectweb/mrst/download/previous-releases/).

## References
[^fn1]: N. Andrianov and H. M. Nick, Towards a nonlinear transfer function in dual porosity models, submitted, 2020.
 