%% Runs the dual porosity/dual permeability on a specified problem

mrstModule add ad-props ad-core ad-blackoil dual-porosity


% Read the geometry
%[state, model, schedule, t] = setupNoetinger;
[state, model, schedule, t] = setupLaegerdorf('plots', true, 'plotfracs', false, 'plotrock', false); 

plots = true;

G = model.G;

% Run simulations
[~, states] = simulateScheduleAD(state, model, schedule);


% Get the mean pressure
pfmean = zeros(numel(states), 1);
pmmean = zeros(numel(states), 1);
for i=1:numel(states)
    pfmean(i) = mean(states{i}.pressure);
    pmmean(i) = mean(states{i}.pom);
end

% Get the fluxes across the boundary of the fracture domain
boundaryFace = [ find(G.faces.neighbors(:,1)==0); find(G.faces.neighbors(:,2)==0) ];   % Indices of boundary faces
qb = zeros(length(states), 1);
for i=1:length(states)
    qb(i) = sum(abs(states{i}.flux(boundaryFace(:), 1)));
end

if (plots)
    figure(10)
    semilogx(t, qb);
    xlabel('t');
    ylabel('Boundary flux')
    title('Fracture boundary flux');
end

% Plot the evolution of the averaged pressure in time
if (plots)
    figure(1);
    semilogx(t, pmmean);
    xlabel('t');
    ylabel('Mean pressure')
end

if (plots)
    % Animate solutions
    hp1 = [];
    hp2 = [];
    for i=1:numel(states)

        figure(2);        
        delete(hp1);
        hp1 = plotCellData(G,states{i}.pressure/barsa,'EdgeColor','none');   
        title(['Fracture pressure at t=' num2str(t(i))])
        colorbar
        drawnow;
                
        figure(3);        
        delete(hp2);
        hp2 = plotCellData(G,states{i}.pom/barsa,'EdgeColor','none');   
        title(['Matrix pressure at t=' num2str(t(i))])
        colorbar
        drawnow;        
        
    end
end

% Get the fluxes across boundary edges
[bface, ~, ~] = find(G.faces.neighbors == 0);   % For boundary faces, one of the neighbor indices is zero
qtot = zeros(length(states), 1);
for i=1:length(states)
    qtot(i) = sum(abs(states{i}.flux(bface)));
end

% 
% % Calculate the dimensionless shape factor
% aa2 = mu * qtot ./ (Km .* (pmean - pf));
% 
% if (plots)
%     figure
%     semilogx(t/tD, aa2);
%     xlabel('t/tD');
%     ylabel('\alpha a^2')
%     title('Dimensionless shape factor');
% end
