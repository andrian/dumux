// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the exercise on two-phase flow in fractured porous media.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

// include the headers of the two sub-problems
// i.e. the problems for fractures and matrix
#include "matrixproblem.hh"
#include "fractureproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/traits.hh>

// use the local cell-centered assembler
//#include <dumux/multidomain/fvassembler.hh>
#include "model/fvassembler.hh"

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include <dumux/io/vtkoutputmodule.hh>

// Define some types for this test  so that we can set them as properties below and
// reuse them again in the main function with only one single definition of them here
using MatrixTypeTag = TTAG(MatrixProblemTypeTag);
using FractureTypeTag = TTAG(FractureProblemTypeTag);
using MatrixFVGridGeometry = typename GET_PROP_TYPE(MatrixTypeTag, FVGridGeometry);
using FractureFVGridGeometry = typename GET_PROP_TYPE(FractureTypeTag, FVGridGeometry);
using TheMultiDomainTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
using TheCouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
using TheCouplingManager = Dumux::FacetCouplingManager<TheMultiDomainTraits, TheCouplingMapper>;

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {
SET_TYPE_PROP(MatrixProblemTypeTag, CouplingManager, TheCouplingManager);
SET_TYPE_PROP(FractureProblemTypeTag, CouplingManager, TheCouplingManager);
} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::FakeMPIHelper::instance(argc, argv);
    //const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    if (mpiHelper.isFake) {
        std::cout << "\nSerial run (no MPI)" << std::endl;
    }
    else {
       std::cout << "\nMPI run" << std::endl;
    }

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);


    // initialize parameter tree
    Parameters::init(argc, argv);

    // We use the grid manager from the facet coupling framework (see alias above main)
    // This requires the grids used to be passed as template arguments, where
    // they are assumed to be ordered in descending grid dimension. Thus,
    // we pass the matrix grid as first and the fracture grid as second argument.
    using MatrixGrid = typename GET_PROP_TYPE(MatrixTypeTag, Grid);
    using FractureGrid = typename GET_PROP_TYPE(FractureTypeTag, Grid);
    using GridManager = Dumux::FacetCouplingGridManager<MatrixGrid, FractureGrid>;
    GridManager gridManager;

    // try to create a grid (from the grid file in the input file)
    // Init() creates the grid from the grid file specified in the input file.
    // This works with a single grid file in which in addition to the matrix
    // (d-dimensional) elements also the (d-1)-dimensional elements are specified
    // which are interpreted as the fracture elements. See the .geo file in the
    // grids folder on how to create such grids using gmsh. Note that currently
    // only the Gmsh mesh format (.msh) is supported!
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views (get them from grid manager)
    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);
    matrixFvGridGeometry->update();
    fractureFvGridGeometry->update();

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = typename GET_PROP_TYPE(MatrixTypeTag, Problem);
    using FractureProblem = typename GET_PROP_TYPE(FractureTypeTag, Problem);

    // pass the model parameter group to the spatial params so that they obtain the right
    // values from the input file since we use groups for matrix and fracture
    // We also want to use domain markers in this exercise. For this reason, we also pass
    // the grid data object from the grid manager to them, so that they have access to the
    // domain markers that were specified in the grid file.
    auto matrixGridData = gridManager.getGridData()->template getSubDomainGridData<matrixGridId>();
    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, matrixGridData, "Matrix");
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix");

    // extract domain height from the matrix problem and pass to fracture problem (needed for right initial pressure distribution)
    auto fractureGridData = gridManager.getGridData()->template getSubDomainGridData<fractureGridId>();
    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, fractureGridData, "Fracture");
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture");

    // the solution vector
    using SolutionVector = typename TheMultiDomainTraits::SolutionVector;
    SolutionVector x, xOld;
	
    using Scalar = typename GET_PROP_TYPE(MatrixTypeTag, Scalar);
    using FluidSystem = typename GET_PROP_TYPE(MatrixTypeTag, FluidSystem); // NA: assume the same fluid in matrix and in fractures		

    // The domain ids within the multi-domain framework.
    // They do not necessarily have to be the same as the grid ids
    // in case you have more subdomains involved. We domain ids
    // correspond to the order of the type tags passed to the multidomain
    // traits (see definition of the traits class at the beginning of this file)
    static const auto matrixDomainId = typename TheMultiDomainTraits::template DomainIdx<0>();
    static const auto fractureDomainId = typename TheMultiDomainTraits::template DomainIdx<1>();

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());

	matrixProblem->applyInitialSolution(x[matrixDomainId]);
	fractureProblem->applyInitialSolution(x[fractureDomainId]);
	

    // instantiate the class holding the coupling maps between the domains
    // this needs the information on embeddings (connectivity between matrix
    // and fracture domain). This information is extracted directly from the
    // grid during file read and can therefore be obtained from the grid manager.
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // the grid variables
    using MatrixGridVariables = typename GET_PROP_TYPE(MatrixTypeTag, GridVariables);
    using FractureGridVariables = typename GET_PROP_TYPE(FractureTypeTag, GridVariables);
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId], xOld[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId], xOld[fractureDomainId]);

    // intialize the vtk output modules
    VtkOutputModule<MatrixTypeTag> matrixVtkWriter(*matrixProblem, *matrixFvGridGeometry, *matrixGridVariables, x[matrixDomainId], matrixProblem->name());
    VtkOutputModule<FractureTypeTag> fractureVtkWriter(*fractureProblem, *fractureFvGridGeometry, *fractureGridVariables, x[fractureDomainId], fractureProblem->name());

    // Add model specific output fields
    using MatrixVtkOutputFields = typename GET_PROP_TYPE(MatrixTypeTag, VtkOutputFields);
    using FractureVtkOutputFields = typename GET_PROP_TYPE(FractureTypeTag, VtkOutputFields);
    MatrixVtkOutputFields::init(matrixVtkWriter);
    FractureVtkOutputFields::init(fractureVtkWriter);

    // add domain markers to output
    std::vector<int> matrixDomainMarkers(matrixFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(matrixFvGridGeometry->gridView()))
        matrixDomainMarkers[matrixFvGridGeometry->elementMapper().index(element)] = matrixProblem->spatialParams().getElementDomainMarker(element);
    matrixVtkWriter.addField(matrixDomainMarkers, "domainMarker");

    std::vector<int> fractureDomainMarkers(fractureFvGridGeometry->gridView().size(0));
    for (const auto& element : elements(fractureFvGridGeometry->gridView()))
        fractureDomainMarkers[fractureFvGridGeometry->elementMapper().index(element)] = fractureProblem->spatialParams().getElementDomainMarker(element);
    fractureVtkWriter.addField(fractureDomainMarkers, "domainMarker");

    // write out initial solution
    matrixVtkWriter.write(0.0);
    fractureVtkWriter.write(0.0);

    // get some time loop parameters
    auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<double>("TimeLoop.DtInitial");
	
    // instantiate time loop
    auto timeLoop = std::make_shared< TimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    //using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::numeric, /*implicit?*/true>; // DiffMethod::numeric, /*implicit?*/true>;  DiffMethod::analytic
    using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::analytic, /*implicit?*/true>; // DiffMethod::numeric, /*implicit?*/true>;  DiffMethod::analytic
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop);

    // the linear solver
    //using LinearSolver = ILU0BiCGSTABBackend;
    //using LinearSolver = BlockDiagILU0BiCGSTABSolver;   // test to avoid linear solver crash (no success...)
    //using LinearSolver = BlockDiagAMGBiCGSTABSolver;   // test to avoid linear solver crash (really slow, and crashes even when the others pass well..)
    using LinearSolver = UMFPackBackend;  
    //using LinearSolver = EigenBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    
    // External file for output of results
    std::string filename = getParam<std::string>("Grid.File");
    //std::string filename( "results.csv");
    filename = filename.substr(0, filename.find("."));
    std::string fname_results =  filename + "_results.csv";
    std::ofstream file( fname_results.c_str() );
    if( file )
    {
//         const char *header = "Time (sec),"
// 							 "Dimensionless time,"
//                              "Dimensionless pressure,"
//                              "Dimensionless shape factor,"
// 							 "Matrix-fracture volumetric flux,"
// 							 "Matrix-fracture mass flux,"
// 							 "Fluid mass in the matrix (kg),"
// 							 "Matrix average pressure";

        const char *header = "Time (sec),"
							 "Dimensionless time,"
                             "Dimensionless pressure,"
                             "Dimensionless shape factor," 
							 "Matrix-fracture mass flux,"
							 "Matrix-fracture produced fluid (m3),"							
							 "Fluid mass in the matrix (kg),"
							 "Total fluid mass conservation error (kg),"
							 "Total fluid mass conservation error (%),"
							 "Matrix-fracture average pressure,"
							 "Matrix average pressure";
							 							 
							 
							 
        file << header << std::endl;
    }
    else
    {
      std::cerr << "ERROR: couldn't open file `" << fname_results << "'" << std::endl;
    }

    int saveVTKFrequency = getParam<int>("TimeLoop.SaveVTKFrequency");
	
	Scalar matrixAveragePressure, fractureAveragePressure, mfPressure, matrixMass, matrixMass0, matrixBoundaryFlux;
	Scalar volFMFlux, massFMFlux;
	Scalar dimlessTime, dimlessPressure, shapeFactor;
	Scalar mfProdFluid = 0.;
	
	// set the bounding box for the fractures using the dimensions of the matrix domain
	fractureProblem->setBoundingBox(matrixDomainId);

	// get the total domain area
	Scalar area = matrixProblem->getArea(); // fractureProblem->getArea() yields fracture length since it is a lower-dimensional object
	
	// calculate the time scaling factor
	Scalar phi = getParam<Scalar>("Matrix.SpatialParams.Porosity");
	Scalar mu = FluidSystem::viscosity(0., 0.);	// For pressure = 0 and temperature = 0
	Scalar C =  getParam<Scalar>("Fluid.Compressibility");
	Scalar Km = getParam<Scalar>("Matrix.SpatialParams.Permeability");
	Scalar tD = phi * mu * C * area / Km;
	
	// get the fracture pressure from the BC for the fracture domain and the matrix initial pressure
	Scalar fracturePressure = getParam<Scalar>("Fracture.Problem.BoundaryPressure");
	Scalar initialPressure = getParam<Scalar>("Matrix.Problem.InitialPressure");
	
	// array of shape factors for last iterations
	std::vector<Scalar> lastSF;
	Scalar averageSF;

	// array of dimensionless pressure for last iterations
	std::vector<Scalar> lastDP;
	bool repeat = true;
	Scalar averageDP;
	size_t Nlast = 20;
	int err_code = 0;
	
	// get the initial fluid mass in the matrix
	matrixProblem->getResults(matrixAveragePressure, mfPressure, matrixMass0, matrixBoundaryFlux, volFMFlux, massFMFlux,
							  x[matrixDomainId], *matrixGridVariables, *assembler,
							  matrixDomainId);	

    // time loop
	int loopNr = 0;
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        newtonSolver->solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        matrixGridVariables->advanceTimeStep();
        fractureGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

		// calculate the matrix average pressure and the boundary flux
		matrixProblem->getResults(matrixAveragePressure, mfPressure, matrixMass, matrixBoundaryFlux, volFMFlux, massFMFlux,
								  x[matrixDomainId], *matrixGridVariables, *assembler,
								  matrixDomainId);
		
		// calculate the fracture average pressure
		fractureProblem->getResults(fractureAveragePressure,
								  x[fractureDomainId], *fractureGridVariables);
		
		// Calculate the dimensionless time and pressure
        dimlessTime = timeLoop->time() / tD;
		dimlessPressure = (matrixAveragePressure - initialPressure) / (fracturePressure - initialPressure);
		
		// calculate the dimensionless shape 
		// here we distinguish between the Bourbiaux and Noetinger cases:
		// For Bourbiaux, fractures lie at the domain boundary, so we use matrixBoundaryFlux
		// to calculate the shape factor.
		// For Noetinger, we make sure that the matrix boundary flux is zero, and use the fracture-matrix
		// flux to calculate the shape factor.
// 		if (matrixBoundaryFlux != 0)
// 			shapeFactor = mu * matrixBoundaryFlux / (Km * (matrixAveragePressure - fracturePressure) );	// Bourbiaux
// 		else
// 			shapeFactor = mu * fmFlux / (Km * (matrixAveragePressure - fracturePressure) );	// Noetinger
			
		Scalar flux;
		if (matrixBoundaryFlux != 0)
			flux = matrixBoundaryFlux;	// Bourbiaux
		else
			flux = volFMFlux;	// Noetinger
			
		shapeFactor = mu * flux / (Km * (matrixAveragePressure - fracturePressure) );	
		
		Scalar dt = timeLoop->timeStepSize();
		mfProdFluid += dt * massFMFlux;
		Scalar massError = matrixMass - matrixMass0 - mfProdFluid;		
		
			
		// Save the results
//         file << timeLoop->time() << "," <<         // Time (sec)
// 				dimlessTime << "," <<              // Dimensionless time
//         		dimlessPressure  << "," <<         // Dimensionless pressure
// 				shapeFactor << "," <<              // Dimensionless shape factor
// 				volFMFlux << "," << 			   // Matrix-fracture volumetric flux
// 				massFMFlux << "," << 			   // Matrix-fracture mass flux
// 				matrixMass << "," << 			   // Fluid mass in the matrix (kg)
// 				matrixAveragePressure <<		   // Matrix average pressure
//                 std::endl;
// 
		
		        
		file << timeLoop->time() << "," <<         // Time (sec)
		dimlessTime << "," <<              // Dimensionless time
        dimlessPressure  << "," <<         // Dimensionless pressure
		shapeFactor << "," <<              // Dimensionless shape factor
		massFMFlux << "," << 			   // Matrix-fracture mass flux
		mfProdFluid << "," << 			   // Matrix-fracture produced fluid (kg)
		matrixMass << "," << 			   // Fluid mass in the matrix (kg)
		massError << "," <<				   // Total fluid mass conservation error (kg)
		massError/matrixMass0*100 << "," << // Total fluid mass conservation error (%)
		mfPressure << "," <<			   // Matrix-fracture average pressure
		matrixAveragePressure <<		   // Matrix average pressure
        std::endl;
				
		
				
/*        const char *header = "Time (sec),"
							 "Dimensionless time,"
                             "Dimensionless pressure,"
                             "Dimensionless shape factor,"
							 "Matrix-fracture produced fluid (m3),"							
							 "Fluid mass in the matrix (kg),"
							 "Total fluid volume conservation error (m3),"
							 "Total fluid volume conservation error (%),"
							 "Matrix average pressure";		*/		
				
				

        // Eventually write vtk output
		if (loopNr % saveVTKFrequency == 0) 
		{
			matrixVtkWriter.write(timeLoop->time());
			fractureVtkWriter.write(timeLoop->time());
		}

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        if (timeLoop->time() + 10. * dt >= tEnd) {
        	tEnd = std::max(maxDt, tEnd + 10. * maxDt);	// Eventually increase the end time since we want to stop the simulation when WCT < 0.95
        	timeLoop->setEndTime(tEnd);					// Inform timeLoop about new tEnd so that the time step will not be cut
        }
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));
		
		loopNr++;

//		// iterate until the shape factor over the last iterations does not change significantly
//		if (dimlessTime > 1)
//		{
//			if (lastSF.size() <= Nlast)
//				lastSF.push_back(shapeFactor);
//			else
//			{
//				repeat = false;
//				lastSF.erase(lastSF.begin());	// Remove the first element so that the size() == Nlast
//				averageSF = std::accumulate(lastSF.begin(), lastSF.end(), 0.) / Nlast;
//				// If any of the last shape factors is too far from the current one, continue time steps
//				for (auto& sf : lastSF)
//					if (fabs(averageSF - sf) > 1e-3)
//					{
//						repeat = repeat | true;
//						break;
//					}
//			}
//		}


		// iterate until the dimensionless pressure over the last iterations does not change significantly
		if (lastDP.size() <= Nlast)
			lastDP.push_back(dimlessPressure);
		else
		{
			repeat = false;
			lastDP.erase(lastDP.begin());	// Remove the first element so that the size() == Nlast
			averageDP = std::accumulate(lastDP.begin(), lastDP.end(), 0.) / Nlast;
			// If any of the last pressures is too far from the current one, continue time steps
			for (auto& dp : lastDP)
				if (fabs(averageDP - dp) > 1e-3)
				{
					repeat = repeat | true;
					break;
				}
		}

		/*
		// break if the dimensionless shape factor starts to increase over last iterations
		if (!repeat)
		{
			std::cout << "\nStopping iterations since the dimensionless pressure does not change over the last iterations...\n" << std::endl;
		}
		else
		{
			if (lastSF.size() <= Nlast)
				lastSF.push_back(shapeFactor);
			else
			{
				lastSF.erase(lastSF.begin());	// Remove the first element so that the size() == Nlast
				averageSF = std::accumulate(lastSF.begin(), lastSF.end(), 0.) / Nlast;
				// break if the last shape factor is greater than the average of the last ones
				if (shapeFactor > averageSF)
				{
					err_code = -1;
					repeat = false;
					std::cout << "\nStopping iterations since the shape factor is increasing...\n" << std::endl;
				}
			}
		}
		*/


    } while (repeat);
    //} while (!timeLoop->finished());
    //} while (WCT < 0.95);		
	//} while (true);
    //} while (dimlessTime < 1);
		
    // Close the external file for output of volumes
    file.close();

    // save the error code and the last shape factor in a separate file for an ease of processing
    std::string fname_sf =  filename + "_sf.dat";
    file.open( fname_sf.c_str() );
    if( file )
    {
    	file << err_code << "," << shapeFactor << std::endl;
    }
    else
    {
      std::cerr << "ERROR: couldn't open file `" << fname_sf << "'" << std::endl;
    }
    file.close();

    // output some Newton statistics
    newtonSolver->report();

    // report time loop statistics
    timeLoop->finalize();

    // print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
	
