""" The convolutional neural network used in N. Andrianov and H. M. Nick, 
    "Learning the matrix-fractures transfer rate using a convolutional 
    neural network", submitted, 2020.
"""

#import cv2
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.utils import np_utils

import numpy as np
import csv
import matplotlib.pyplot as plt


def load_data(fname):
    """Loads the dataset with the parameters of the matrix-fracture transfer function 
       and the corresponding pixelized geometry

    # Arguments
        fname: a csv file with with the entries:
            N: number of pixels in the square box geometry
            smin, smax, qmax: parameters of the transfer function
            pixels: array of N x N, representing the pixelized fracture geometry

    # Returns
        (x, y): a tuple of numpy arrays with
            x = pixels
            y = smin, smax, qmax
    """

    faug = fname.split('.')
    if (faug[-1] == 'csv'):
        # Load from text CSV - slow I/O, and get floats
        data = np.genfromtxt(fname, delimiter=',')
    elif (faug[-1] == 'npz'):
        # Load from internal numpy format
        data = np.load(fname)
        with np.load(fname) as f:
            data = f['data']
    else:
        print('Unknown format, exiting..')
        return 1


    Npixels = int(data[0][0])
    Nsamples = data.shape[0]
    y = data[:, 1:4]
    x = data[:, 4:]
    x = np.array(x, dtype='uint8')

    if (x.shape[1] != Npixels * Npixels):
        print('ERROR: pixelized geometry is not ' + str(Npixels) + 'x' + str(Npixels) )

    # Temporarily keep just 1 component of y
    y = y[:, 0]

    # Reshape (x, y) to match Keras input
    y = y.transpose()
    x = x.reshape(Nsamples, Npixels, Npixels, 1)

    return (x, y)


# Main

# Load the datasets with pixelated fractures
fname = 'dataset_linear_laegerdorf_4_train.npz'
(X_train, Y_train) = load_data(fname)
 
fname = 'dataset_linear_laegerdorf_4_test.npz'
(X_test, Y_test) = load_data(fname)

batch_size, img_rows, img_cols = 64, X_test.shape[1], X_test.shape[2]
input_shape = (img_rows, img_cols, 1)

# Normalize 
X_train = X_train.astype("float32")
X_test = X_test.astype("float32")
X_train /= 255
X_test /= 255

# Define a convolutional neural network
model = Sequential()

model.add(Convolution2D(32, 5, 5, border_mode="same", input_shape=input_shape))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode="same"))
model.add(Convolution2D(64, 5, 5, border_mode="same", input_shape=input_shape))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), border_mode="same"))

model.add(Flatten())
model.add(Dense(1024))
model.add(Activation("relu"))
model.add(Dropout(0.5))

model.add(Dense(1))
model.add(Activation("linear"))
model.compile(loss="mean_squared_error", optimizer="adam")

history = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=10,
		  verbose=1, validation_data=(X_test, Y_test))


# Plot the convergence history
plt.figure(10)
plt.semilogy(history.history['loss'])
plt.title('model loss')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show(block=False)

# Visualize the results
yp = model.predict(X_test)
plt.figure(11)
plt.plot(Y_test, yp, "o")
plt.xlabel('Testing data')
plt.ylabel('Prediction data')
plt.title('Prediction on the testing dataset')
plt.plot([0,40], [0,40], 'r-')
plt.savefig('results.png')
plt.show()

# Save the model
fname = "cnn_laegerdorf_regression.h5"
model.save(fname)
print("Model saved in " + fname)



 