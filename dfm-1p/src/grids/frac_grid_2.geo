// Gmsh geometry specification for the fracture grid in a box
// 2 vertical fractures


// Density of mesh near Points
lc = 0.1; // initial: 0.06
lcf = 0.01;	// Near fracture


// Size of the square box 
Lm = 2;

// Vertical position of the fractures
Lf1 = 0.66;
Lf2 = 1.33;

// Geometrical entities = 1d and 2D regions indices
// #2D_REGIONS
MATRIX = 666;

// #1D_REGIONS
OPEN_FRACTURES = 777;


// Outer box
Point(1) = {0,  0, 0, lc};
Point(2) = {Lm, 0, 0, lc};
Point(3) = {Lm, Lm, 0, lc};
Point(4) = {0,  Lm, 0, lc};

// Fractures' endpoints
Point(5) = {Lf1, Lm, 0, lcf};
Point(6) = {Lf1, 0, 0, lcf};
Point(7) = {Lf2, Lm, 0, lcf};
Point(8) = {Lf2, 0, 0, lcf};

// Additional points along the fracture to get the refineme
Point(10) = {Lf1, Lm/4, lcf};
Point(11) = {Lf1, 2*Lm/4, lcf};
Point(12) = {Lf1, 3*Lm/4, lcf};
Point(13) = {Lf2, Lm/4, lcf};
Point(14) = {Lf2, 2*Lm/4, lcf};
Point(15) = {Lf2, 3*Lm/4, lcf};



// Define the bounded domains
// NOTE: If a Line contains > 2 Points, then the triangulation nodes can NOT include the interior Points!
Line(500) = {1, 6};
Line(501) = {6, 8};
Line(502) = {8, 2};
Line(503) = {2, 3};
Line(504) = {3, 7};
Line(505) = {7, 5};
Line(506) = {5, 4};
Line(507) = {4, 1};

Line(508) = {5, 6};
Line(509) = {7, 8};

Line Loop(600) = {500, 501, 502, 503, 504, 505, 506, 507}; 
Plane Surface(600) = {600}; 

// Including fractures in triangulation
Line{508, 509} In Surface {600};


// Physical entities
// Fracture appears in the msh file as a set of line elements 
// (the 2nd parameter in the $Elements block elm-type is =1)
// with the 4th parameter = number of Physical Line
// and the 5th = number of corresponding Line
// Physical Line(LEFT) = {503};	
// Physical Line(RIGHT) = {501};	
// Physical Line(TOP) = {502};
// Physical Line(BOTTOM) = {500};


// Physical entities
Physical Line(OPEN_FRACTURES) = {506};

// Triangulated domain
// The triangular $Elements (the 2nd parameter in the $Elements block elm-type is =2)
// have the the 4th parameter = number of Physical Surface
// and the 5th = number of corresponding Plane Surface
Physical Surface(MATRIX) = {600}; 
