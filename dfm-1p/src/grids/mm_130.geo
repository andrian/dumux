// Gmsh 2D geometry specification

// Density of mesh near Points
lcf = 2.918145e+00;

// Geometrical entities = 1d and 2D regions indices
// #2D_REGIONS
MATRIX = 666;

// #1D_REGIONS
OPEN_FRACTURES = 777;

// Points
Point(1) = {1.444389e+02, 1.487585e+02, 0, lcf};
Point(2) = {1.449650e+02, 1.482324e+02, 0, lcf};
Point(3) = {1.454911e+02, 1.477062e+02, 0, lcf};
Point(4) = {1.459070e+02, 1.472903e+02, 0, lcf};
Point(5) = {1.463229e+02, 1.468745e+02, 0, lcf};
Point(6) = {1.490432e+02, 1.441541e+02, 0, lcf};
Point(7) = {1.517636e+02, 1.414338e+02, 0, lcf};
Point(8) = {1.534629e+02, 1.431332e+02, 0, lcf};
Point(9) = {1.551623e+02, 1.448325e+02, 0, lcf};
Point(10) = {1.329971e+02, 1.671153e+02, 0, lcf};
Point(11) = {1.321730e+02, 1.679394e+02, 0, lcf};
Point(12) = {1.313489e+02, 1.687635e+02, 0, lcf};
Point(13) = {1.502805e+02, 1.612180e+02, 0, lcf};
Point(14) = {1.524611e+02, 1.590374e+02, 0, lcf};
Point(15) = {1.546417e+02, 1.568568e+02, 0, lcf};
Point(16) = {1.500664e+02, 1.522815e+02, 0, lcf};
Point(17) = {1.327853e+02, 1.371050e+02, 0, lcf};
Point(18) = {1.340556e+02, 1.358346e+02, 0, lcf};
Point(19) = {1.353259e+02, 1.345643e+02, 0, lcf};
Point(20) = {1.526199e+02, 1.405775e+02, 0, lcf};
Point(21) = {1.573904e+02, 1.453479e+02, 0, lcf};
Point(22) = {1.621608e+02, 1.501184e+02, 0, lcf};
Point(23) = {1.395033e+02, 1.488236e+02, 0, lcf};
Point(24) = {1.452962e+02, 1.546165e+02, 0, lcf};
Point(25) = {1.510891e+02, 1.604094e+02, 0, lcf};
Point(26) = {1.386302e+02, 1.479505e+02, 0, lcf};
Point(27) = {1.390668e+02, 1.483870e+02, 0, lcf};
Point(28) = {1.361885e+02, 1.418546e+02, 0, lcf};
Point(29) = {1.385276e+02, 1.441937e+02, 0, lcf};
Point(30) = {1.408667e+02, 1.465328e+02, 0, lcf};
Point(31) = {1.423162e+02, 1.479823e+02, 0, lcf};
Point(32) = {1.437656e+02, 1.494317e+02, 0, lcf};
Point(33) = {1.321121e+02, 1.377782e+02, 0, lcf};
Point(34) = {1.341503e+02, 1.398164e+02, 0, lcf};
Point(35) = {1.470205e+02, 1.595947e+02, 0, lcf};
Point(36) = {1.488999e+02, 1.614740e+02, 0, lcf};
Point(37) = {1.507793e+02, 1.633534e+02, 0, lcf};
Point(38) = {1.482414e+02, 1.608155e+02, 0, lcf};
Point(39) = {1.494622e+02, 1.620363e+02, 0, lcf};
Point(40) = {1.532084e+02, 1.373287e+02, 0, lcf};
Point(41) = {1.555728e+02, 1.396932e+02, 0, lcf};
Point(42) = {1.579372e+02, 1.420576e+02, 0, lcf};
Point(43) = {1.376187e+02, 1.436209e+02, 0, lcf};
Point(44) = {1.380393e+02, 1.432002e+02, 0, lcf};
Point(45) = {1.384600e+02, 1.427796e+02, 0, lcf};
Point(46) = {1.414494e+02, 1.457690e+02, 0, lcf};
Point(47) = {1.351510e+02, 1.460885e+02, 0, lcf};
Point(48) = {1.363849e+02, 1.448547e+02, 0, lcf};
Point(49) = {1.405307e+02, 1.514682e+02, 0, lcf};
Point(50) = {1.459104e+02, 1.568478e+02, 0, lcf};
Point(51) = {1.363098e+02, 1.502709e+02, 0, lcf};
Point(52) = {1.331549e+02, 1.534258e+02, 0, lcf};
Point(54) = {1.372194e+02, 1.415391e+02, 0, lcf};
Point(56) = {1.302040e+02, 1.563767e+02, 0, lcf};
Point(57) = {1.301020e+02, 1.562747e+02, 0, lcf};
Point(60) = {1.331549e+02, 1.471160e+02, 0, lcf};
Point(61) = {1.313927e+02, 1.357123e+02, 0, lcf};
Point(62) = {1.679078e+02, 1.320870e+02, 0, lcf};
Point(63) = {1.689513e+02, 1.310435e+02, 0, lcf};
Point(65) = {1.376081e+02, 1.322822e+02, 0, lcf};
Point(67) = {1.373584e+02, 1.314250e+02, 0, lcf};
Point(68) = {1.366459e+02, 1.307125e+02, 0, lcf};
Point(70) = {1.625786e+02, 1.374163e+02, 0, lcf};
Point(72) = {1.495440e+02, 1.336644e+02, 0, lcf};
Point(73) = {1.660804e+02, 1.540380e+02, 0, lcf};
Point(75) = {1.687299e+02, 1.632662e+02, 0, lcf};
Point(76) = {1.693650e+02, 1.639012e+02, 0, lcf};
Point(78) = {1.693650e+02, 1.626312e+02, 0, lcf};
Point(80) = {1.307306e+02, 1.693818e+02, 0, lcf};
Point(82) = {1.344394e+02, 1.685577e+02, 0, lcf};
Point(84) = {1.541026e+02, 1.666767e+02, 0, lcf};
Point(87) = {1.458895e+02, 1.656090e+02, 0, lcf};
Point(89) = {1.654513e+02, 1.696018e+02, 0, lcf};
Point(90) = {1.650532e+02, 1.692037e+02, 0, lcf};
Point(92) = {1.671284e+02, 1.671284e+02, 0, lcf};
Point(94) = {1.560186e+02, 1.439762e+02, 0, lcf};
Point(95) = {1.377867e+02, 1.434528e+02, 0, lcf};
Point(96) = {1.489747e+02, 1.619293e+02, 0, lcf};
Point(97) = {1.314783e+02, 1.364174e+02, 0, lcf};
Point(98) = {1.625793e+02, 1.374165e+02, 0, lcf};
Point(99) = {1.698603e+02, 1.643965e+02, 0, lcf};
// Boundary points
Point(53) = {1.300000e+02, 1.565807e+02, 0, lcf};
Point(55) = {1.300000e+02, 1.343196e+02, 0, lcf};
Point(58) = {1.300000e+02, 1.561727e+02, 0, lcf};
Point(59) = {1.300000e+02, 1.439611e+02, 0, lcf};
Point(64) = {1.699949e+02, 1.300000e+02, 0, lcf};
Point(66) = {1.398903e+02, 1.300000e+02, 0, lcf};
Point(69) = {1.359334e+02, 1.300000e+02, 0, lcf};
Point(71) = {1.458796e+02, 1.300000e+02, 0, lcf};
Point(74) = {1.700000e+02, 1.579576e+02, 0, lcf};
Point(77) = {1.700000e+02, 1.645363e+02, 0, lcf};
Point(79) = {1.700000e+02, 1.619961e+02, 0, lcf};
Point(81) = {1.301124e+02, 1.700000e+02, 0, lcf};
Point(83) = {1.358817e+02, 1.700000e+02, 0, lcf};
Point(85) = {1.574259e+02, 1.700000e+02, 0, lcf};
Point(86) = {1.414985e+02, 1.700000e+02, 0, lcf};
Point(88) = {1.658495e+02, 1.700000e+02, 0, lcf};
Point(91) = {1.700000e+02, 1.642568e+02, 0, lcf};
Point(93) = {1.642568e+02, 1.700000e+02, 0, lcf};
// Corner points
Point(100) = {1.300000e+02, 1.300000e+02, 0, lcf};
Point(101) = {1.700000e+02, 1.300000e+02, 0, lcf};
Point(102) = {1.700000e+02, 1.700000e+02, 0, lcf};
Point(103) = {1.300000e+02, 1.700000e+02, 0, lcf};

// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 9};
Line(9) = {10, 11};
Line(10) = {11, 12};
Line(11) = {13, 14};
Line(12) = {14, 15};
Line(13) = {3, 16};
Line(14) = {16, 15};
Line(15) = {17, 18};
Line(16) = {18, 19};
Line(17) = {20, 94};
Line(18) = {94, 21};
Line(19) = {21, 22};
Line(20) = {23, 24};
Line(21) = {24, 25};
Line(22) = {26, 27};
Line(23) = {27, 23};
Line(24) = {28, 95};
Line(25) = {95, 29};
Line(26) = {29, 30};
Line(27) = {30, 31};
Line(28) = {31, 32};
Line(29) = {33, 34};
Line(30) = {34, 28};
Line(31) = {35, 36};
Line(32) = {36, 96};
Line(33) = {96, 39};
Line(34) = {39, 37};
Line(35) = {35, 38};
Line(36) = {38, 96};
Line(37) = {40, 41};
Line(38) = {41, 42};
Line(39) = {43, 95};
Line(40) = {95, 44};
Line(41) = {44, 45};
Line(42) = {45, 46};
Line(43) = {46, 1};
Line(44) = {47, 48};
Line(45) = {48, 43};
Line(46) = {47, 49};
Line(47) = {49, 50};
Line(48) = {51, 52};
Line(49) = {52, 53};
Line(50) = {1, 54};
Line(51) = {54, 97};
Line(52) = {97, 55};
Line(53) = {56, 57};
Line(54) = {57, 58};
Line(55) = {59, 60};
Line(56) = {60, 51};
Line(57) = {55, 61};
Line(58) = {61, 97};
Line(59) = {97, 17};
Line(60) = {62, 63};
Line(61) = {63, 64};
Line(62) = {19, 65};
Line(63) = {65, 66};
Line(64) = {67, 68};
Line(65) = {68, 69};
Line(66) = {64, 98};
Line(67) = {98, 70};
Line(68) = {70, 94};
Line(69) = {94, 9};
Line(70) = {71, 72};
Line(71) = {72, 40};
Line(72) = {22, 73};
Line(73) = {73, 74};
Line(74) = {75, 76};
Line(75) = {76, 99};
Line(76) = {99, 77};
Line(77) = {75, 78};
Line(78) = {78, 79};
Line(79) = {12, 80};
Line(80) = {80, 81};
Line(81) = {10, 82};
Line(82) = {82, 83};
Line(83) = {37, 84};
Line(84) = {84, 85};
Line(85) = {86, 87};
Line(86) = {87, 39};
Line(87) = {39, 13};
Line(88) = {88, 89};
Line(89) = {89, 90};
Line(90) = {91, 99};
Line(91) = {99, 92};
Line(92) = {92, 93};
// Boundary lines
Line(93) = {100, 69};
Line(94) = {69, 66};
Line(95) = {66, 71};
Line(96) = {71, 64};
Line(97) = {64, 101};
Line(98) = {101, 74};
Line(99) = {74, 79};
Line(100) = {79, 91};
Line(101) = {91, 77};
Line(102) = {77, 102};
Line(103) = {102, 88};
Line(104) = {88, 93};
Line(105) = {93, 85};
Line(106) = {85, 86};
Line(107) = {86, 83};
Line(108) = {83, 81};
Line(109) = {81, 103};
Line(110) = {103, 53};
Line(111) = {53, 58};
Line(112) = {58, 59};
Line(113) = {59, 55};
Line(114) = {55, 100};

Line Loop(1) = {93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114};
Plane Surface(1) = {1};

// Including fractures in triangulation
Line{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92} In Surface {1};

// Physical entities

Physical Line(OPEN_FRACTURES) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92};

Physical Surface(MATRIX) = {1};
