// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the matrix domain for the single-phase compressible flow in fractured porous media.
 */
#ifndef DUMUX_FRACTURES_MATRIX_PROBLEM_HH
#define DUMUX_FRACTURES_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// We are using the framework for models that consider coupling
// across the element facets of the bulk domain using a cell-centered 
// finite volume scheme with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

// Slightly compressible single phase liquid with parameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "components/slightlycompressibleliquid.hh"

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the matrix sub-problem
NEW_TYPE_TAG(MatrixProblemTypeTag, INHERITS_FROM(OneP, CCTpfaFacetCouplingModel));

// Set the grid type
SET_TYPE_PROP(MatrixProblemTypeTag, Grid, Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>);

// Set the problem type
SET_TYPE_PROP(MatrixProblemTypeTag, Problem, MatrixSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(MatrixProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(MatrixProblemTypeTag,
              SpatialParams,
              MatrixSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);


// Set the fluid system
SET_PROP(MatrixProblemTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::SlightlyCompressibleComponent<Scalar> >;
};

} // end namespace Properties


/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the matrix domain 
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;	
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, initialPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialPressure"))
	{

		// Infer the type of left boundary conditions from input data
		bool bcSet = false;
		boundaryRate_ = 1e100;
		boundaryPressure_ = 1e100;
		try {	
			// If BoundaryRate is present all matrix boundaries is Neumann with the corresponding rate
			boundaryRate_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryRate");		
			boundaryDirichlet = false;
			bcSet = true;
			std::cout << "Using Neumann conditions at matrix boundaries..\n" << std::endl;
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided 
			//std::cout << e.what(); 
			try {	
				boundaryPressure_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryPressure");
				boundaryDirichlet = true;		
				bcSet = true;
				std::cout << "\nUsing Dirichlet conditions at matrix boundaries..\n" << std::endl;
			} catch (const std::exception& e) {	}			
		}
		
		if (!bcSet) 
		{				
			std::cout << "\nNeither Dirichlet nor Neumann conditions are provided..\n" << std::endl;
			throw Dune::NotImplemented();
		}
		
		
		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();		
	}
	
	//! Calculate the matrix average pressure and the dimensionless shape factor 
	void getResults(Scalar time, Scalar& dimlessTime, Scalar& dimlessPressure, Scalar& shapeFactor,
					const SolutionVector& curSol, const GridVariables& gridVariables)
    {        
			
		Scalar averagePressure = 0.;
		Scalar boundaryFlux = 0;
		Scalar area = 0.;
		
		// loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	

            VolumeVariables volVars;


            for (auto&& scv : scvs(fvGeometry))
            {
            	volVars = elemVolVars[scv];

                Scalar ef = volVars.extrusionFactor();

                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }
                
                averagePressure += volVars.pressure();

                area += scv.volume();

            }

            // Loop through the faces 
            
            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {								            	
				
				if (scvf.boundary()) {
					
                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar flux = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
                        boundaryFlux += flux;	// > 0 for outflow
					}
					else if (bcTypes.hasOnlyNeumann()) {
						Scalar flux = this->neumannAtPos(scvf.center());  
						boundaryFlux *= scvf.area() * extrusionFactor / volVars.density(FluidSystem::phase0Idx);   // Convert to volumetric flux
					}
					else {
                        DUNE_THROW(Dune::NotImplemented, "matrixproblem.hh: boundary condition not defined..");
                    }
                    
					
				}
				
			}
			
			
		}	
		
        // Calculate the results
		Scalar phi = getParam<Scalar>("Matrix.SpatialParams.Porosity");
        Scalar mu = FluidSystem::viscosity(0., 0.);	// For pressure = 0 and temperature = 0
        Scalar C =  getParam<Scalar>("Fluid.Compressibility");
		Scalar Km = getParam<Scalar>("Matrix.SpatialParams.Permeability");
        Scalar tD = phi * mu * C * area / Km;
        dimlessTime = time / tD;

		// Set fracture pressure from the BC for the fracture domain
		Scalar fracturePressure = getParam<Scalar>("Fracture.Problem.BoundaryPressure");
		Scalar initialPressure = getParam<Scalar>("Matrix.Problem.InitialPressure");

		auto meshSize = this->fvGridGeometry().gridView().size(0);
		averagePressure /= meshSize;
		dimlessPressure = (averagePressure - initialPressure) / (fracturePressure - initialPressure);
		
		// calculate the dimensionless shape factor  - mu * qtot * L * L ./ (Km .* (pmean - pf));

		shapeFactor = mu * boundaryFlux / (Km * (averagePressure - fracturePressure) );

	}



    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
		BoundaryTypes values;
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[0];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
		Scalar ymin = this->fvGridGeometry().bBoxMin()[1];
        Scalar ymax = this->fvGridGeometry().bBoxMax()[1];		
		
		if (globalPos[0] < xmin + 1e-6 || globalPos[0] > xmax - 1e-6 ||
			globalPos[1] < ymin + 1e-6 || globalPos[1] > ymax - 1e-6)
		{
			if (boundaryDirichlet) 
				values.setAllDirichlet();
			else
				values.setAllNeumann();    
		}
		
		return values;		
		
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated. If this
        // permeability is lower than that of the matrix, this approach is able to
        // represent the resulting pressure jump across the fracture. If Dirichlet is set,
        // the pressure jump across the fracture is neglected and the pressure inside
        // the fracture is directly applied at the interface between fracture and matrix.
        // This assumption is justified for highly-permeable fractures, but lead to erroneous
        // results for low-permeable fractures.
        // Here, we consider "open" fractures for which we cannot define a normal permeability
        // and for which the pressure jump across the fracture is neglectable. Thus, we set
        // the interior boundary conditions to Dirichlet.
        // IMPORTANT: Note that you will never be asked to set any values at the interior boundaries!
        //            This simply chooses a different interface condition!
        values.setAllDirichlet();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;   
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[0];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
		Scalar ymin = this->fvGridGeometry().bBoxMin()[1];
        Scalar ymax = this->fvGridGeometry().bBoxMax()[1];			
		
		if (globalPos[0] < xmin + 1e-6 || globalPos[0] > xmax - 1e-6 ||
			globalPos[1] < ymin + 1e-6 || globalPos[1] > ymax - 1e-6)	
		{
			values = boundaryPressure_;
		}
		
        return values;        
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);        
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[0];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
		Scalar ymin = this->fvGridGeometry().bBoxMin()[1];
        Scalar ymax = this->fvGridGeometry().bBoxMax()[1];			
		
		if (globalPos[0] < xmin + 1e-6 || globalPos[0] > xmax - 1e-6 ||
			globalPos[1] < ymin + 1e-6 || globalPos[1] > ymax - 1e-6)	
		{
			values = boundaryRate_;	// kg/(m*s)
		}		
		
        return values;
    }
    
    

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
		values = initialPressure_;
        return values;            
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    


private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
	
	bool boundaryDirichlet;
	Scalar boundaryPressure_;
	Scalar boundaryRate_;
	Scalar initialPressure_;
};

} // end namespace Dumux

#endif
    
    	










