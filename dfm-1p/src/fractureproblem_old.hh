// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the fracture domain for the single-phase compressible flow in fractured porous media.
 */
#ifndef DUMUX_FRACTURES_FRACTURE_PROBLEM_HH
#define DUMUX_FRACTURES_FRACTURE_PROBLEM_HH

// we use foam grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

// Slightly compressible single phase liquid with parameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "components/slightlycompressibleliquid.hh"

// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class FractureSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the fracture sub-problem
NEW_TYPE_TAG(FractureProblemTypeTag, INHERITS_FROM(OneP, CCTpfaModel));

// Set the grid type
SET_TYPE_PROP(FractureProblemTypeTag, Grid, Dune::FoamGrid<1, 2>);

// Set the problem type
SET_TYPE_PROP(FractureProblemTypeTag, Problem, FractureSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(FractureProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(FractureProblemTypeTag,
              SpatialParams,
              FractureSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);


// Set the fluid system
SET_PROP(FractureProblemTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::SlightlyCompressibleComponent<Scalar> >;
};

} // end namespace Properties


/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the fracture domain 
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
	using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;	
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, boundaryPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryPressure"))	
	, initialPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialPressure"))	
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))	
	{

		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();
		
	}


    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
    	BoundaryTypes values;		
		
		Scalar xmin = this->fvGridGeometry().bBoxMin()[0];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
		Scalar ymin = this->fvGridGeometry().bBoxMin()[1];
        Scalar ymax = this->fvGridGeometry().bBoxMax()[1];		
		
		if (globalPos[0] < xmin + 1e-6 || globalPos[0] > xmax - 1e-6 ||
			globalPos[1] < ymin + 1e-6 || globalPos[1] > ymax - 1e-6)
		{
			values.setAllDirichlet();
		}
		
		return values;		
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }    
    
     //! Set the aperture as extrusion factor.
	template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {        
        return aperture_;
    }    
    


    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;        

		Scalar xmin = this->fvGridGeometry().bBoxMin()[0];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[0];
		Scalar ymin = this->fvGridGeometry().bBoxMin()[1];
        Scalar ymax = this->fvGridGeometry().bBoxMax()[1];	
		
		if (globalPos[0] < xmin + 1e-6 || globalPos[0] > xmax - 1e-6 ||
			globalPos[1] < ymin + 1e-6 || globalPos[1] > ymax - 1e-6)	
		{
			values = boundaryPressure_;
		}
		
        return values;        
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);        
        return values;
    }
    
    

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
		values = initialPressure_;
        return values;            
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    


private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	Scalar boundaryPressure_;	
	Scalar initialPressure_;
    Scalar aperture_;
};

} // end namespace Dumux

#endif
    
    	










