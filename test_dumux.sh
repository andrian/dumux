# Compile and runs a simple
# one-phase ground water flow example
# and visualizes the result using paraview.
cd dumux/build-cmake/test/porousmediumflow/1p/implicit
echo "Compiling a one-phase test using a cell-centered TPFA discretization (might take a while)..."
make -B test_1pcctpfa
echo "Running simulation..."
./test_1pcctpfa test_1pfv.input -Grid.Cells "100 100"
paraview *pvd
