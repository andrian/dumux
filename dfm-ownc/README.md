# Discrete Fracture-Matrix (DFM) oil-water $`\displaystyle N`$-component model

This project contains the DuMuX module for the Discrete Fracture-Matrix (DFM) two-phase (oil and water) model[^fn1], where oil is represented by a single oil component, and water - by $`\displaystyle N-1`$ components. The code is based on the DFM module[^fn2] with a modified fluid model, boundary conditions, and I/O. The interaction of $`\displaystyle N-1`$ aqueous components with each other and with the rock is handled via a sequential non-iterative coupling with [PhreeqcRM](https://wwwbrr.cr.usgs.gov/projects/GWC_coupled/phreeqc)[^fn3].


## Prerequisites

Install the self-contained version of [DuMuX](https://dumux.org) as described [here](../README.md).


## Installation

Compile the DFM oil-water model
```bash
cd dfm-ownc/build-cmake-O3/src/
make dfm-ownc
```

## References
[^fn1]: N. Andrianov and H. M. Nick, Numerical simulation of low salinity waterflood on fractured chalk outcrop-based models, in 20th European Symposium on Improved Oil Recovery, 8-11 April 2019, Pau, France.
[^fn2]: D. Gläser, R. Helmig, B. Flemisch, and H Class, A discrete fracture model for two-phase flow in fractured porous media, Advances in Water Resources 110, 335–348, 2017.
[^fn3]: D. L. Parkhurst, and L. Wissmeier, PhreeqcRM: A reaction module for transport simulators based on the geochemical model PHREEQC, Advances in Water Resources 83, 176-189, 2015.
 