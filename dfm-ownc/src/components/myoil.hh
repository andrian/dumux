// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise-fluidsystem.
 */
#ifndef DUMUX_MYINCOMPRESSIBLEOILCOMPONENT_HH
#define DUMUX_MYINCOMPRESSIBLEOILCOMPONENT_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {    
/*!
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise-fluidsystem.
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class MyIncompressibleOilComponent
: public Components::Base<Scalar, MyIncompressibleOilComponent<Scalar> >
, public Components::Liquid<Scalar, MyIncompressibleOilComponent<Scalar> >
{
public:
    /*!
     * \brief A human readable name for MyIncompressibleComponent.
     */
    static std::string name()
    { return "OilComp"; }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible
     */
    static constexpr bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief Estimated molar mass in \f$\mathrm{[kg/mol]}\f$ of oil
     * 		  using the apparent average molar weight from
     * 		  Osjord et al (1985), Distribution of ...
     * 		  J. High Resol. Chromatogr., 8: 683-690.
     */
    static Scalar molarMass()
    {
        return 254e-3;
    }


    /*!
     * \brief The density 
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        Scalar liquidDensity = getParam<Scalar>("Fluids.OilDensity");
        return liquidDensity;
    }

    /*!
     * \brief The molar density of MyIncompressibleComponent in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     */
    static Scalar liquidMolarDensity(Scalar temperature, Scalar pressure)
    { return liquidDensity(temperature, pressure)/molarMass(); }

    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[Pa*s]}\f$ of the pure component.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        Scalar liquidViscosity = getParam<Scalar>("Fluids.OilViscosity");
        return liquidViscosity;
    }
    
     /*!
     * \brief Specific enthalpy of liquid water \f$\mathrm{[J/kg]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static const Scalar liquidEnthalpy(Scalar temperature,
                                       Scalar pressure)
    {
        return 4180*(temperature - 293.15);
    }
    
};

} // end namespace Components
} // end namespace Dume

#endif
