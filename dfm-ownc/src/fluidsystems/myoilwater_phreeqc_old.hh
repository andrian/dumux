// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief A fluid system with water and oil as phases
 */
#ifndef DUMUX_MYWATEROIL_FLUID_SYSTEM_HH
#define DUMUX_MYWATEROIL_FLUID_SYSTEM_HH

#include <unistd.h>
#include <dumux/common/parameters.hh>
#include <dumux/material/idealgas.hh>
#include <dumux/material/fluidsystems/base.hh>

#include "../components/myoil.hh"
//#include "../components/mybrine.hh"
//#include "../components/mylowsalwater.hh"

#include <PhreeqcRM.h>


namespace Dumux {    
namespace FluidSystems {

/*!
 * \ingroup Fluidsystems
 * \brief A compositional fluid with brine and carbon dioxide as
 *        components in both, the liquid and the gas (supercritical) phase,
 *        additional biomineralisation components (Ca and Urea) in the liquid phase
 *
 * This class provides acess to the Bio fluid system when no property system is used.
 * For Dumux users, using BioMinFluid<TypeTag> and the documentation therein is
 * recommended.
 *
 *  The user can provide their own material table for co2 properties.
 *  This fluidsystem is initialized as default with the tabulated version of
 *  water of the IAPWS-formulation, and the tabularized adapter to transfer
 *  this into brine.
 *  In the non-TypeTagged version, salinity information has to be provided with
 *  the init() methods.
 */
template <class Scalar, class Fluid0, class Fluid1>
class MyOilWater
: public BaseFluidSystem<Scalar, MyOilWater<Scalar, Fluid0, Fluid1> >
{
    static_assert((Fluid0::numPhases == 1), "Fluid0 has more than one phase");
    static_assert((Fluid1::numPhases == 1), "Fluid1 has more than one phase");    

    using ThisType = MyOilWater<Scalar, Fluid0, Fluid1>;
    using Base = BaseFluidSystem<Scalar, ThisType>;

public:
    

    /*!
     * \brief Return the temperature of the fluid system in \f$\mathrm{[K]}\f$ as set in PHREEQC input file
     */
    static Scalar temperature()
    {
    	return _temperature;
    }

    /****************************************
     * Fluid phase related static parameters
     ****************************************/
        
    static constexpr int numPhases = 2; //!< Number of phases in the fluid system
    static constexpr int phase0Idx = 0; //!< index of the first phase
    static constexpr int phase1Idx = 1; //!< index of the second phase
    
    static constexpr int waterPhaseIdx = phase0Idx;
    static constexpr int oilPhaseIdx = phase1Idx;

    /*!
     * \brief Return the human readable name of a fluid phase
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        static std::string name[] = { "Water", "Oil" };
        return name[phaseIdx];        
    }

    /*!
     * \brief Returns whether the fluids are miscible
     */
    static constexpr bool isMiscible()
    { return false; }

    /*!
     * \brief Return whether a phase is liquid
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     * \param phaseIdx The index of the fluid phase to consider
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if immiscibility is assumed. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        // we assume immisibility
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isIdealGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return false;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means. that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return false;
    }

    /*!
     * \brief Returns true if the liquid phase viscostiy is constant
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool viscosityIsConstant(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
//    static bool isIdealFluid1(int phaseIdx)
//    {
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//
//        // let the fluids decide
//        if (phaseIdx == phase0Idx)
//            return Fluid0::isIdealFluid1();
//        return Fluid1::isIdealFluid1();
//    }

    /****************************************
     * Component related static parameters
     ****************************************/

    /* Brine mass fraction is the 3rd component of the primary variables vector
     * This choice of numbering is dictated by completeFluidState() in 2pnc/volumevariables.hh,
     * where the numMajorComponents=NumPhases=2 components' mole fraction of phase knownPhaseIdx=0,
     * known at the beginning of compositional flash is set to priVars[numMajorComponents].
     */

    //using Brine = Components::MyIncompressibleBrineComponent<Scalar>;
    //using LowSalWater = Components::MyIncompressibleLowSalWaterComponent<Scalar>;
    using Oil = Components::MyIncompressibleOilComponent<Scalar>;

    static constexpr int numComponents = 15;

    static constexpr int OilIdx = 0;
    static constexpr int LSWIdx = 1;
//    static constexpr int BrineIdx = 2;
    
    // NA: used in 2pnc/volumevariables.hh update() - not clear...
    static constexpr int comp0Idx = OilIdx;
    static constexpr int comp1Idx = LSWIdx;

    //using FieldVector = Dune::FieldVector<Scalar, numComponents>;

    
    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx index of the component
     */
    static std::string componentName(int compIdx)
    { 
        assert (compIdx < numComponents);
        if (compIdx == OilIdx)
        	return Oil::name();
        else
        	return compName[compIdx - 1];
    }

    /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     * \param compIdx index of the component
     */
    static Scalar molarMass(int compIdx)
    {
        assert (compIdx < numComponents);
        if (compIdx == OilIdx)
        	return Oil::molarMass();
        else
        	return elemMolarMass[compIdx - 1];
    }

    /*!
     * \brief Return numAqueousComponents entries of molar fractions of components
     *		  in the [1..NumComponents] entries of the vector moleFraction,
     *		  and the vector of components' molar densities.
     *		  If FieldVector is substituted by PrimaryVariables, then the first 2 entries
     *		  are overwritten by (pn, Sw)
     * \param elemIdx index of the element
     */
    template <class FieldVector>
    static void getElementMoleFractionDensity(FieldVector& moleFraction, int elemIdx)    {

// Use the constant value set in in init() instead
//    	Scalar  waterMolarDensity = 0;
//    	for (int i = 0; i < numAqueousElements; i++)
//    		waterMolarDensity += compMolarDensity[ idComp[i] * Ncells + elemIdx];

    	moleFraction[0] = 0;	// The oil component molar fraction in water phase
    	for (int i = 0; i < numAqueousElements; i++)
    		moleFraction[1 + i] = compMolarDensity[ idComp[i] * Ncells + elemIdx] / waterMolarDensity;

    }


    /*!
     * \brief Return numAqueousComponents entries of molar fractions of components
     *		  in the [1..NumComponents] entries of the vector bcMoleFraction.
     *		  The 0-th entry is set to zero which corresponds to no oil influx.
     * \param elemIdx index of the element
     */
    static std::vector<double> getBoundaryMoleFraction()
    {
    	return bcMoleFraction;
    }

    /*!
     * \brief Return the average molar mass of the water phase
     *        using the mole fractions at the inflow boundary
     */
    static Scalar getBoundaryWaterAverageMolarMass()
    {
        return averageMolarMass;
    }


    /****************************************
     * thermodynamic relations
     ****************************************/

    /*!
     * \brief Initialize the fluid system's static parameters
     */
    static void init(const std::string& domain,
    		 	     std::vector<Scalar>& poro,
    				 std::vector<Scalar>& rv,
					 std::vector<Scalar>& sat,
					 std::vector<Scalar>& pressure)
    {


	    std::cout << "     Initializing PHREEQC instance for " << domain << ".." << std::endl;


		Ncells = poro.size();	// Number of cells in the domain
		int nthreads = 1;
		phreeqc_rm = new PhreeqcRM(Ncells, nthreads);
	
		// Set properties
		IRM_RESULT status;
		status = phreeqc_rm->SetErrorHandlerMode(1);		// 0: return with an error return code; 1: throw an exception; 2: attempt to exit gracefully
		status = phreeqc_rm->SetComponentH2O(false);
		status = phreeqc_rm->SetRebalanceFraction(0.5);	// Set the fraction of cells that are transferred among threads or processes when rebalancing
		status = phreeqc_rm->SetRebalanceByCell(true);	// True: individual cell times used in rebalancing; False: average times used in rebalancing
		phreeqc_rm->UseSolutionDensityVolume(false);		// True: solution density & volume as calculated by PHREEQC will be used to calculate concentrations;
														// False: solution density set by SetDensity and the volume determined by the product of SetSaturation, SetPorosity, and SetRepresentativeVolume, will be used to calculate concentrations retrieved by GetConcentrations.
		phreeqc_rm->SetPartitionUZSolids(false);			// True: fraction of solids and gases available for reaction is equal to the saturation; False: all solids and gases are reactive regardless of saturation
		
		// Set concentration units
		status = phreeqc_rm->SetUnitsSolution(2);           // 1, mg/L; 2, mol/L; 3, kg/kgs
		status = phreeqc_rm->SetUnitsPPassemblage(1);       // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsExchange(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsSurface(1);            // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsGasPhase(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock
		status = phreeqc_rm->SetUnitsSSassemblage(1);       // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsKinetics(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????

		// Set conversion from seconds to user units (days)
		double time_conversion = 1.0 / 86400;
		status = phreeqc_rm->SetTimeConversion(time_conversion);

		// Set representative volume
		rv.assign(Ncells, 1.0);		// Try that...
		status = phreeqc_rm->SetRepresentativeVolume(rv);

		// Set initial porosity
		//poro.assign(Ncells, 0.2);
		status = phreeqc_rm->SetPorosity(poro);

		// Set initial saturation
		//sat.assign(Ncells, 1);
		status = phreeqc_rm->SetSaturation(sat);

		// Set initial pressure (in bar)
		phreeqc_rm->SetPressure(pressure);
		
		// Map the transport grids to the chemistry solver
		// Solve chemistry in all the grids.
		std::vector<int> grid2chem;
		grid2chem.resize(Ncells, -1);
		for (int i = 0; i < Ncells; i++)
		{
			grid2chem[i] = i;
		}
		status = phreeqc_rm->CreateMapping(grid2chem);
		if (status < 0) phreeqc_rm->DecodeError(status);
		int nchem = phreeqc_rm->GetChemistryCellCount();	
		
		// Load database
		char cCurrentPath[FILENAME_MAX];
		getcwd(cCurrentPath, sizeof(cCurrentPath));
		std::string path(cCurrentPath);
		std::string fname = path + "/" + "phreeqc.dat";
		status = phreeqc_rm->LoadDatabase(fname.c_str());
		
		// Load the PQI file with the adjusted number of cells in the PQI file for the current domain (Matrix or Fracture)
		bool workers = true;             // Worker instances do the reaction calculations for transport
		bool initial_phreeqc = true;     // InitialPhreeqc instance accumulates initial and boundary conditions
		bool utility = true;             // Utility instance is available for processing
//		std::string oldPQI = path + "/" + "advect.pqi";
//		std::string newPQI = path + "/" + domain + "_" + "advect.pqi";
		std::string oldPQI = path + "/" + "debruin_composition.pqi";
		std::string newPQI = path + "/" + domain + "_" + "debruin_composition.pqi";

	    std::cout << "     Adjusting " <<  newPQI << " for " << Ncells << " cells.." << std::endl;

		adjustPQI(oldPQI, newPQI, Ncells);
		status = phreeqc_rm->RunFile(workers, initial_phreeqc, utility, newPQI.c_str());
		
		// Read the temperatures as set up in advect.pqi
		std::vector<double> temp = phreeqc_rm->GetTemperature();

		// Ensure that FluidState returns the temperature from advect.pqi
		// (all temp values are the same from SOLUTION 1)
		_temperature = temp[0] + 273.15;	// Convert from °C to °K

		// According to PHREEQC terminology, the aqueous components are the chemical elements and the charge imbalance
		numAqueousComponents = phreeqc_rm->FindComponents();

		// Get names of components
		compName = phreeqc_rm->GetComponents();

		// The number of chemical elements in the aqueous phase
		numAqueousElements = numAqueousComponents - 1;

		// Extract the indices of chemical elements
		idComp.resize(numAqueousElements);
		int i = 0;
		bool foundCharge = false;
		bool foundH = false;
		bool foundO = false;
		for (int j = 0; j < numAqueousComponents; j++)
		{
			if ((compName[j] == "H") && (i == 0)) foundH = true;
			if ((compName[j] == "O") && (i == 1)) foundO = true;

			if (compName[j] != "Charge")
			{
				idComp[i] = j;
				i++;
			}
			else
				foundCharge = true;

		}

		if (!foundCharge)
			DUNE_THROW(Dune::NotImplemented, "Charge component not found in PHREEQC input file...");

		if (!foundH)
			DUNE_THROW(Dune::NotImplemented, "H component not found as a 0th component as read from PHREEQC input file...");

		if (!foundO)
			DUNE_THROW(Dune::NotImplemented, "O component not found as a 1st component as read from PHREEQC input filee...");

		// The index to start salinity calculations
		startSalinityIdx = 2;


//		if (numAqueousElements != numAqueousComponents - 1)
//		{
//    		std::string errorMsg = "PHREEQC components are not elements + charge";
//    		DUNE_THROW(ParameterException, errorMsg.c_str());
//		}

		// Get the components' molar masses and set the elements molar masses
		static std::vector<double> compMolarMass;
		compMolarMass = phreeqc_rm->GetGfw();
		elemMolarMass.resize(numAqueousElements);
		for (int i = 0; i < numAqueousElements; i++)
			elemMolarMass[i] = compMolarMass[ idComp[i] ] / 1000;			// Convert PHREEQC's gram-formula weight in g/mol to kg/mol


		// Check if the number of input elements matches the static constexpr int numComponents + 1
		// ( + 1 for the single oil component)
		// (apparently cannot work with dynamic concentrations arrays in DuMuX...)
		if (numAqueousElements + 1 != numComponents)
		{
			std::cout << "     Please set constexpr int numComponents equal to " << numAqueousElements + 1
					  << " in myoilwater.hh and re-compile the code..\n" << std::endl;
			throw Dune::NotImplemented();
		}
		else
		{
			std::cout << "     Setting the PrimaryVariables dimension to " << numComponents << " to include the single oil component.." << std::endl;
		}

		// Set array of initial conditions
		std::vector<int> ic1, ic2;
		ic1.resize(Ncells*7, -1);
		for (int i = 0; i < Ncells; i++)
		{
			ic1[i] = 1;              // Solution 1
			ic1[Ncells + i] = -1;      // Equilibrium phases none
			ic1[2*Ncells + i] = 1;     // Exchange 1
			ic1[3*Ncells + i] = -1;    // Surface none
			ic1[4*Ncells + i] = -1;    // Gas phase none
			ic1[5*Ncells + i] = -1;    // Solid solutions none
			ic1[6*Ncells + i] = -1;    // Kinetics none
		}

		// Set the initial solution ic1
		// Mixing is set if ic2>0 using fractions f1
		//status = phreeqc_rm->InitialPhreeqc2Module(ic1, ic2, f1);
		// No mixing is defined, so the following is equivalent
		status = phreeqc_rm->InitialPhreeqc2Module(ic1);
		if (status < 0) phreeqc_rm->DecodeError(status);

		// Initial equilibration of cells
		double time = 0.0;
		double time_step = 0.0;
		concentration.resize(Ncells * numAqueousComponents);
		status = phreeqc_rm->SetTime(time);				// Simulation time, in seconds
		status = phreeqc_rm->SetTimeStep(time_step);	// Time step over which kinetic reactions are integrated

		// Concentrations from the PQI input file are apparently interpreted as the amount of moles per volume
		// of the corresponding representative volume. In order to get back the concentrations from the PQI input file,
		// the "concentrations" vector need to be multiplied by the corresponding representative volume.
		status = phreeqc_rm->GetConcentrations(concentration);

//		// Get the initial components' concentrations SetUnitsSolution() units (before equilibration)
//		std::cout << "     Before equilibration: " << std::endl;
//		int outwidth = 15;
//		std::cout << "     " << std::setw(outwidth) <<  "Component"
//				  << std::setw(outwidth) << "Weight, kg/mol"
//				  << std::setw(outwidth) << "Concentration"
//				  << std::endl;
//		for (int i = 0; i < numAqueousComponents; i++)
//		{
//			std::cout << "     " << std::setw(outwidth) << compName[i]
//					  << std::setw(outwidth) << compMolarMass[i]
//					  << std::setw(outwidth) << concentration[i*Ncells + 1]		// Print the values of concentrations in the 1st cell in the domain
//					  << std::endl;
//		}

		status = phreeqc_rm->RunCells();						// Runs a reaction step for all reaction cells
		status = phreeqc_rm->GetConcentrations(concentration);	// Get the updated concentrations in SetUnitsSolution() units

		// Convert concentrations from [mol/L] to [mol/m3]
		compMolarDensity.resize(Ncells * numAqueousComponents);
		for (int n = 0; n < Ncells; n++)
			for (int i = 0; i < numAqueousComponents; i++)
				compMolarDensity[i*Ncells + n] = concentration[i*Ncells + n] * 1000;


		// Calculate the constant components' mass densities [kg/m3]
		compMassDensity.resize(numAqueousElements);
    	for (int i = 0; i < numAqueousElements; i++)
    		compMassDensity[i] = compMolarDensity[idComp[i] * Ncells + 1] * elemMolarMass[i];	// Take the compMolarDensity from the 1st cell in the domain


		// Set boundary conditions bc1 and bc2 using the concentrations of SOLUTION 0 from Initial IPhreeqc instance
		std::vector<double> bcConcentration, bc_f1;
		std::vector<int> bc1, bc2;
		int nbound = 1;
		bc1.resize(nbound, 0);
		bc2.resize(nbound, -1);                     // no bc2 solution for mixing
		bc_f1.resize(nbound, 1.0);                  // mixing fraction for bc1
		status = phreeqc_rm->InitialPhreeqc2Concentrations(bcConcentration, bc1, bc2, bc_f1);

		// Mole density of formation water [mol/L]
    	Scalar brineWaterMolarDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		brineWaterMolarDensity += compMolarDensity[ idComp[i] * Ncells + 1] / 1000;

		// Mass density of formation water [kg/m3]
    	Scalar brineWaterMassDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		brineWaterMassDensity += compMolarDensity[ idComp[i] * Ncells + 1] * elemMolarMass[i];

		// Mole density of injection water [mol/L]
    	Scalar bcWaterMolarDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		bcWaterMolarDensity += bcConcentration[ idComp[i] ];

		// Mass density of injection water [kg/m3]
    	Scalar bcWaterMassDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		bcWaterMassDensity += bcConcentration[ idComp[i] ] * 1000 * elemMolarMass[i];

		// Get the molar fractions of boundary conditions
    	bcMoleFraction.resize(numComponents);
    	bcMoleFraction[0] = 0;	// The 0-th entry is set to zero which corresponds to no oil influx
    	for (int i = 0; i < numAqueousElements; i++)
    		bcMoleFraction[1 + i] = bcConcentration[ idComp[i] ] * 1000 / bcWaterMolarDensity;

    	// Compute the average molar mass of the water phase for the inflow boundary [kg/mol]
    	// (compMolarMass are specified for aqueous elements only)
    	averageMolarMass = 0.;
    	for (int i = 0; i < numAqueousElements; i++)
    		averageMolarMass += bcMoleFraction[1 + i] * elemMolarMass[i];


    	// Calculate the salinity for the formation water (high salinity)... [kg/m3]
    	highSal = 0;
    	for (int i = startSalinityIdx; i < numAqueousElements; i++)
    		highSal += elemMolarMass[i] * compMolarDensity[idComp[i]*Ncells + 1];	// Use e.g. the values of concentrations in the 1st cell in the domain
    	highSal *= 1e3;	// Convert to mg/L

    	// ... and for the injection water (low salinity) [kg/m3]
    	lowSal = 0;
    	for (int i = startSalinityIdx; i < numAqueousElements; i++)
    		lowSal += elemMolarMass[i] * bcConcentration[ idComp[i] ] * 1000;
    	lowSal *= 1e3;	// Convert to mg/L

    	// Output the properties of the formation water
		std::cout << "     Formation water @ " << temp[0] << "C" << std::endl;
		int outwidth = 15;
		std::cout << "     " << std::setw(outwidth) <<  "Component"
				  << std::setw(outwidth) << "Weight [g/mol]"
				  << std::setw(outwidth) << "#moles per 1 L"
				  << std::endl;
		for (int i = 0; i < numAqueousComponents; i++)
		{
			std::cout << "     " << std::setw(outwidth) << compName[i]
					  << std::setw(outwidth) << compMolarMass[i]
					  << std::setw(outwidth) << concentration[i*Ncells + 1]	* rv[1]	// Print the values of concentrations in the 1st cell in the domain
					  << std::endl;
		}
		std::cout << "         Molar density: " << brineWaterMolarDensity << " [mol/L]" << std::endl;
		std::cout << "         Mass density: " << brineWaterMassDensity << " [kg/m3]" << std::endl;
		std::cout << "         Salinity: " << highSal << " [ppm]" << std::endl;

		// Output the properties of the injection water
		std::cout << "     Injection water @ " << temp[0] << "C" << std::endl;
		std::cout << "     " << std::setw(outwidth) <<  "Component"
				  << std::setw(outwidth) << "Weight [g/mol]"
				  << std::setw(outwidth) << "#moles per 1 L"
				  << std::endl;
		for (int i = 0; i < numAqueousComponents; i++)
		{
			std::cout << "     " << std::setw(outwidth) << compName[i]
					  << std::setw(outwidth) << compMolarMass[i]
					  << std::setw(outwidth) << bcConcentration[i]		// Print the values of concentrations in the 1st cell in the domain
					  << std::endl;
		}
		std::cout << "         Molar density: " << bcWaterMolarDensity << " [mol/L]" << std::endl;
		std::cout << "         Mass density: " << bcWaterMassDensity << " [kg/m3]" << std::endl;
		std::cout << "         Salinity: " << lowSal << " [ppm]" << std::endl;

    	// Setting the constant molar density
		waterMolarDensity = 0.5 *(brineWaterMolarDensity + bcWaterMolarDensity);
		Scalar err = std::abs( (brineWaterMolarDensity - waterMolarDensity)/brineWaterMolarDensity * 100);
		waterMolarDensity *= 1000;	// Convert molar density to [mol/m3]
		std::cout << "     Setting the constant molar density: " << waterMolarDensity << " [mol/m3] (error = " << err << "%)" << std::endl;



		// Vector of components concentrations at a one spatial location (oil component + aqueous components)
		//compConcentration.resize(numComponents, 0.);
			
		/*	
		// Get the number of components in the input file
		int numInputComponents = 3;
		
		// Need to keep the number of components as static constexpr
		assert(numInputComponents == numComponents);
		compName.resize(numComponents, "");
		compConcentration.resize(numComponents, 0.);
		
		std::vector<std::string> waterType = {"FormationWater", "LowSalinityWater"};
		
		try {	
			std::stringstream comp;
			for (size_t n = 0; n < waterType.size(); n++) 
			{
				for (int i = 0; i < numComponents; i++)
				{					
					comp.str("");
					comp << waterType[n] << ".Component" << i << ".Name";
					compName[i] = getParam<std::string>(comp.str().c_str());
					comp.str("");
					comp << waterType[n] << ".Component" << i << ".Concentration";
					compConcentration[i] = getParam<Scalar>(comp.str().c_str());	
				}				
			}
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided for the left boundary
			std::cout << e.what(); 
		}
		*/
		
		

		
    }

	// Adjust the number of cells in the PQI file for the current domain
    static void adjustPQI(std::string oldPQI, std::string newPQI, int Ncells)
    {
    	std::string oldDim = "1-9999";
    	std::string newDim = "1-" + std::to_string(Ncells);

    	std::ifstream infile;
    	infile.open(oldPQI.c_str());
    	if (!infile) {
    		std::string errorMsg = "Unable to open file " + oldPQI;
    		DUNE_THROW(ParameterException, errorMsg.c_str());
    	}

        std::ofstream outfile;
    	outfile.open(newPQI.c_str());
    	if (!outfile) {
    		std::string errorMsg = "Unable to open file " + newPQI;
    		DUNE_THROW(ParameterException, errorMsg.c_str());
    	}

        // Loop through the old PQI file, replace the dimension there, and write the updated lines to the newPQI
    	std::string str;
    	while (!infile.eof())
    	{
    		getline(infile, str);
    		size_t index = 0;
    		index = str.find(oldDim, index);
    		if (index != std::string::npos)
    			str.replace(index, oldDim.length(), newDim);
    		outfile << str << std::endl;
    	}

    	infile.close();
    	outfile.close();
    }

    // Calculate salinity for the element elemIdx
    static Scalar getSalinityInterpolant(int elemIdx)
    {
    	Scalar salinity = 0;
    	for (int i = startSalinityIdx; i < numAqueousElements; i++)
    		salinity += elemMolarMass[ idComp[i] ] * concentration[ idComp[i] * Ncells + elemIdx];	// * rv[elemIdx];
    	salinity *= 1e6;	// Convert to mg/L

    	Scalar interpolant;
    	if (abs(highSal - lowSal) < 1e-8)
    		interpolant = 0;
    	else
    		interpolant = (salinity - lowSal) / (highSal - lowSal);

    	return interpolant;

    }

    using Base::density;
    /*!
     * \brief Calculate the density \f$\mathrm{[kg/m^3]}\f$ of a fluid phase
     *
     */
    /*
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        Scalar massDensity = 0;
        if (phaseIdx == phase0Idx)
        {
            for (int i = 0; i < numAqueousElements; i++)
            	massDensity += fluidState.moleFraction(phaseIdx, i + 1) * compMassDensity[i];
        }
        else
        	massDensity = Fluid1::density(temperature, pressure);

        return massDensity;
    }
    */

    /*!
     * \brief Calculate the mass density of a fluid phase
     *
     * The mass density for the water phase is calculated from the constant molar density.
     *
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        Scalar massDensity = 0;
        if (phaseIdx == phase0Idx)
        {
            for (int i = 0; i < numAqueousElements; i++)
            	massDensity += fluidState.moleFraction(phaseIdx, i + 1) * elemMolarMass[i];	// "+1" to account for the 0th oil component
            massDensity *= waterMolarDensity;
        }
        else
        	massDensity = Fluid1::density(temperature, pressure);

        return massDensity;
    }

    using Base::molarDensity;
    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density is defined by the
     * mass density \f$\rho_\alpha\f$ and the component molar mass \f$M_\alpha\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{M_\alpha} \;.\f]
     */
    /*
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
       Scalar temperature = fluidState.temperature(phaseIdx);
       Scalar pressure = fluidState.pressure(phaseIdx);
       Scalar _molarDensity;
       if (phaseIdx == phase0Idx)
       {
           Scalar massDensity = density(fluidState, phaseIdx);
           Scalar averageMolarMass = 0.;
           for (int i = 0; i < numAqueousElements; i++)
        	   averageMolarMass += fluidState.moleFraction(phaseIdx, i) * elemMolarMass[ idComp[i] ];
           _molarDensity = massDensity / averageMolarMass;
       }
       else
    	   _molarDensity = Fluid1::molarDensity(temperature, pressure);

       return _molarDensity;

    }
    */

    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density for the water phase is constant
     */
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
       Scalar temperature = fluidState.temperature(phaseIdx);
       Scalar pressure = fluidState.pressure(phaseIdx);
       Scalar _molarDensity;
       if (phaseIdx == phase0Idx)
           _molarDensity = waterMolarDensity;
       else
    	   _molarDensity = Fluid1::molarDensity(temperature, pressure);

       return _molarDensity;

    }

    using Base::viscosity;
    /*!
     * \brief Return the viscosity of a phase \f$\mathrm{[Pa*s]}\f$.
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        Scalar phaseViscosity;
        if (phaseIdx == phase0Idx)
        	phaseViscosity = getParam<Scalar>("Fluids.WaterViscosity");
        else
        	phaseViscosity = Fluid1::viscosity(temperature, pressure);
        return phaseViscosity;
    }

    using Base::fugacityCoefficient;
    /*!
     * \brief Calculate the fugacity coefficient \f$\mathrm{[-]}\f$ of an individual
     *        component in a fluid phase
     *
     * The fugacity coefficient \f$\mathrm{\phi^\kappa_\alpha}\f$ is connected to the
     * fugacity \f$\mathrm{f^\kappa_\alpha}\f$ and the component's mole
     * fraction \f$\mathrm{x^\kappa_\alpha}\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     * \f]
     *
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     * \param compIdx index of the component
     */
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        assert(0 <= compIdx  && compIdx < numComponents);

//		std::cout << "     Please set constexpr int numComponents equal to " << numAqueousElements + 1
//				  << "in myoilwater.hh and re-compile the code..\n" << std::endl;
		DUNE_THROW(Dune::NotImplemented, "Equilibrium calculations are done within PHREEQCRM!!");
//		throw Dune::NotImplemented();

		//        if (phaseIdx == waterPhaseIdx) {
//        	switch (compIdx) {
//            	case OilIdx:	return 1;	// arbitrary value
//                default: 		return 0;	// All aqueous components stay in water phase
//            }
//        }
//        else {
//        	switch (compIdx) {
//            	case OilIdx:	return 0;	// oil component stays in oil phase
//            	default: 		return 1;	// arbitrary value
//            }
//        }

    }

    using Base::diffusionCoefficient;
    /*!
     * \brief Calculate the binary molecular diffusion coefficient for
     *        a component in a fluid phase \f$\mathrm{[mol^2 * s / (kg*m^3)]}\f$
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     * \param compIdx index of the component
     *
     * Molecular diffusion of a compoent \f$\mathrm{\kappa}\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \mathbf{grad} \mu_\kappa \f]
     *
     * where \f$\mathrm{\mu_\kappa]}\f$ is the component's chemical potential,
     * \f$\mathrm{D}\f$ is the diffusion coefficient and \f$\mathrm{J}\f$ is the
     * diffusive flux. \f$\mathrm{\mu_\kappa}\f$ is connected to the component's
     * fugacity \f$\mathrm{f_\kappa}\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$\mathrm{p_\alpha}\f$ and \f$\mathrm{T_\alpha}\f$ are the fluid phase'
     * pressure and temperature.
     */
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "Diffusion coefficients of components not miplemented..");
    }

    using Base::binaryDiffusionCoefficient;
    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for components
     *        \f$\mathrm{i}\f$ and \f$\mathrm{j}\f$ in this phase.
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     * \param compIIdx index of the component i
     * \param compJIdx index of the component j
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)

    {
//         DUNE_THROW(Dune::InvalidStateException,
//                    "Binary diffusion coefficients of components are meaningless if"
//                    " immiscibility is assumed");
		
		return 0.;

        //assert(phaseIdx < numPhases);
        //return BinaryCoefficients::liquidDiffCoeff(fluidState.temperature(phaseIdx), fluidState.pressure(phaseIdx));
		
    }

    using Base::enthalpy;
    /*!
     * \brief Return the specific enthalpy of a fluid phase \f$\mathrm{[J/kg]}\f$.
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     */
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                                 int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        return 0.;	// Isothermal system
    }

    using Base::thermalConductivity;
    /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx Index of the fluid phase
     */
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        return 0.;	// Isothermal system
    }

    using Base::heatCapacity;
    /*!
     * @copybrief Base::thermalConductivity
     *
     * Additional comments:
     *
     * Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/(kg*K)]}\f$.
     *
     * \param fluidState The fluid state of the two-phase model
     * \param phaseIdx for which phase to give back the heat capacity
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        return 0.;	// Isothermal system
    }
    
private:

    static Scalar _temperature;

    static int Ncells;								// Number of cells in the domain
    static int numAqueousComponents;				// Number of aqueous components
    static int numAqueousElements;					// Number of aqueous elements
    static std::vector<int> idComp;					// Map from components to elements
    static int startSalinityIdx;			 		// The index to start salinity calculations
	static std::vector<std::string> compName;
	static std::vector<double> elemMolarMass;
	static std::vector<double> concentration;		// Vector of molar densities (concentrations in PHREEQC terminology)
	static std::vector<double> compMolarDensity;	// Vector of molar densities in mol/m3
	static std::vector<double> compMassDensity;		// Vector of components mass densities in m3/kg
	static std::vector<double> bcMoleFraction;		// Vector of molar densities at the inflow boundary
	static Scalar averageMolarMass;
	static Scalar waterMolarDensity;				// The constant molar density
	
	static Scalar lowSal;
	static Scalar highSal;

	static PhreeqcRM *phreeqc_rm;
		 		 
};

template <class Scalar, class Fluid0, class Fluid1>
Scalar MyOilWater<Scalar, Fluid0, Fluid1>::_temperature;
template <class Scalar, class Fluid0, class Fluid1>
int MyOilWater<Scalar, Fluid0, Fluid1>::Ncells;
template <class Scalar, class Fluid0, class Fluid1>
int MyOilWater<Scalar, Fluid0, Fluid1>::numAqueousComponents;
template <class Scalar, class Fluid0, class Fluid1>
int MyOilWater<Scalar, Fluid0, Fluid1>::numAqueousElements;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<int>  MyOilWater<Scalar, Fluid0, Fluid1>::idComp;
template <class Scalar, class Fluid0, class Fluid1>
int MyOilWater<Scalar, Fluid0, Fluid1>::startSalinityIdx;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<std::string> MyOilWater<Scalar, Fluid0, Fluid1>::compName;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<double> MyOilWater<Scalar, Fluid0, Fluid1>::elemMolarMass;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<double> MyOilWater<Scalar, Fluid0, Fluid1>::concentration;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<double> MyOilWater<Scalar, Fluid0, Fluid1>::compMolarDensity;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<double> MyOilWater<Scalar, Fluid0, Fluid1>::compMassDensity;
template <class Scalar, class Fluid0, class Fluid1>
Scalar MyOilWater<Scalar, Fluid0, Fluid1>::averageMolarMass;
template <class Scalar, class Fluid0, class Fluid1>
Scalar MyOilWater<Scalar, Fluid0, Fluid1>::waterMolarDensity;
template <class Scalar, class Fluid0, class Fluid1>
std::vector<double> MyOilWater<Scalar, Fluid0, Fluid1>::bcMoleFraction;
template <class Scalar, class Fluid0, class Fluid1>
Scalar MyOilWater<Scalar, Fluid0, Fluid1>::lowSal;
template <class Scalar, class Fluid0, class Fluid1>
Scalar MyOilWater<Scalar, Fluid0, Fluid1>::highSal;
template <class Scalar, class Fluid0, class Fluid1>
PhreeqcRM *MyOilWater<Scalar, Fluid0, Fluid1>::phreeqc_rm;



} // end namespace FluidSystems
} // end namespace Dumux

#endif
