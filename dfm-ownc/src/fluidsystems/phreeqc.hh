// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief A wrapper for PHREEQCRM methods
 */
#ifndef DUMUX_PHREEQC_FLUID_SYSTEM_HH
#define DUMUX_PHREEQC_FLUID_SYSTEM_HH

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <unistd.h>

#include <PhreeqcRM.h>
#include <IPhreeqc.hpp>

namespace Dumux {    
namespace FluidSystems {

class PHREEQC
{

public:

    // Constructor: Initializes the fluid system's parameters
	PHREEQC(std::string filePQI,
			int _numComponents,
			int _numMajorComponents,
			std::string& domain,
	 	    std::vector<double>& poro,
			std::vector<double>& rv,
			std::vector<double>& _sat,
			std::vector<double>& _pressure)
	{

		std::cout << "     Initializing PHREEQC instance for " << domain << ".." << std::endl;

	    numComponents = _numComponents;
	    numMajorComponents = _numMajorComponents;

		Ncells = poro.size();	// Number of cells in the domain

		// Apparently there are 100% errors in molar density for Ncells = 1 case..
		if (Ncells == 1)
			DUNE_THROW(Dune::NotImplemented, "Increase the mesh resolution to have >1 cells in the " + domain << " domain..");

		int nthreads = 1;
		phreeqc_rm = new PhreeqcRM(Ncells, nthreads);

		// Set properties
		IRM_RESULT status;
		status = phreeqc_rm->SetErrorHandlerMode(1);		// 0: return with an error return code; 1: throw an exception; 2: attempt to exit gracefully
		status = phreeqc_rm->SetComponentH2O(false);
		status = phreeqc_rm->SetRebalanceFraction(0.5);	// Set the fraction of cells that are transferred among threads or processes when rebalancing
		status = phreeqc_rm->SetRebalanceByCell(true);	// True: individual cell times used in rebalancing; False: average times used in rebalancing
		phreeqc_rm->UseSolutionDensityVolume(false);		// True: solution density & volume as calculated by PHREEQC will be used to calculate concentrations;
														// False: solution density set by SetDensity and the volume determined by the product of SetSaturation, SetPorosity, and SetRepresentativeVolume, will be used to calculate concentrations retrieved by GetConcentrations.
		phreeqc_rm->SetPartitionUZSolids(false);			// True: fraction of solids and gases available for reaction is equal to the saturation; False: all solids and gases are reactive regardless of saturation
		
		// Get the fields species_names, species_stoichiometry etc filled 
		phreeqc_rm->SetSpeciesSaveOn(true);
		
		// Set concentration units
		status = phreeqc_rm->SetUnitsSolution(2);           // 1, mg/L; 2, mol/L; 3, kg/kgs
		status = phreeqc_rm->SetUnitsPPassemblage(1);       // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsExchange(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsSurface(1);            // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsGasPhase(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock
		status = phreeqc_rm->SetUnitsSSassemblage(1);       // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????
		status = phreeqc_rm->SetUnitsKinetics(1);           // 0, mol/L cell; 1, mol/L water; 2 mol/L rock  ???????

		// Set conversion from seconds to user units (days)
		double time_conversion = 1.0 / 86400;
		status = phreeqc_rm->SetTimeConversion(time_conversion);

		// debug - error...
		//const std::vector<double> &sv = phreeqc_rm->GetSolutionVolume();

		// Set representative volume
		// By default the representative volume of each reaction cell is 1 liter.
		// The volume of water in a reaction cell is determined by the product of the representative volume,
		// the porosity (@ref SetPorosity), and the saturation (@ref SetSaturation).
		//rv.assign(Ncells, 1.0);		// Try that...
		status = phreeqc_rm->SetRepresentativeVolume(rv);

		// Set initial porosity
		status = phreeqc_rm->SetPorosity(poro);

		// Set initial saturation
		sat = _sat;
		status = phreeqc_rm->SetSaturation(sat);

		// Set initial pressure (in bar)
		pressure = _pressure;
		phreeqc_rm->SetPressure(pressure);

		// Map the transport grids to the chemistry solver
		// Solve chemistry in all the grids.
		std::vector<int> grid2chem;
		grid2chem.resize(Ncells, -1);
		for (int i = 0; i < Ncells; i++)
		{
			grid2chem[i] = i;
		}
		status = phreeqc_rm->CreateMapping(grid2chem);
		if (status < 0) phreeqc_rm->DecodeError(status);
		//int nchem = phreeqc_rm->GetChemistryCellCount();

		// Load the database from the original PQI
        char cCurrentPath[FILENAME_MAX];
		getcwd(cCurrentPath, sizeof(cCurrentPath));
		std::string path(cCurrentPath);
        filePQI = getParam<std::string>("PHREEQC.File");
        std::string dbasename = getDatabaseName(path + "/" + filePQI);
        if (!dbasename.empty())
            std::cout << "     Loading a local copy of database " <<  dbasename << ", specified in " << filePQI << std::endl;
        else 
        {
	    	std::cout << "     Loading the database phreeqc.dat" << std::endl;
            dbasename = "phreeqc.dat";
        }

		std::string fname = path + "/" + dbasename;
		status = phreeqc_rm->LoadDatabase(fname.c_str());

		// Load the PQI file with the adjusted number of cells in the PQI file for the current domain (Matrix or Fracture)
		std::string oldPQI = path + "/" + filePQI;
		std::string newPQI = path + "/" + domain + "_" + filePQI;

//		std::string oldPQI = path + "/" + "advect.pqi";
//		std::string newPQI = path + "/" + domain + "_" + "advect.pqi";
//		std::string oldPQI = path + "/" + "debruin_composition.pqi";
//		std::string newPQI = path + "/" + domain + "_" + "debruin_composition.pqi";

	    std::cout << "     Adjusting " <<  newPQI << " for " << Ncells << " cells.." << std::endl;
		adjustPQI(oldPQI, newPQI, Ncells);

		bool workers = true;             // Worker instances do the reaction calculations for transport
		bool initial_phreeqc = true;     // InitialPhreeqc instance accumulates initial and boundary conditions
		bool utility = true;             // Utility instance is available for processing
		status = phreeqc_rm->RunFile(workers, initial_phreeqc, utility, newPQI.c_str());

		// Read the temperatures as set up in advect.pqi
		std::vector<double> temp = phreeqc_rm->GetTemperature();

		// Ensure that FluidState returns the temperature from advect.pqi
		// (all temp values are the same from SOLUTION 1)
		_temperature = temp[0] + 273.15;	// Convert from °C to °K

		// According to PHREEQC terminology, the aqueous components are the chemical elements and the charge imbalance
		numAqueousComponents = phreeqc_rm->FindComponents();

        // Get the names of solid components (the one returned by GetKineticReactions() are in wrong - alphabetical - order)
		numMinerals = phreeqc_rm->GetKineticReactionsCount();	// Number of reacting minerals	
		mineralName.resize(numMinerals);
		status = phreeqc_rm->GetMineralNames(mineralName);	
		
		// Get the moles of solid components 
		mineralMolarVolume.resize(numMinerals);
		for (int i = 0; i < numMinerals; i++)
		{
			try {	
				mineralMolarVolume[i] = getParam<double>("PHREEQC.Vm_" + mineralName[i]);
				mineralMolarVolume[i] *= 1e-6;	// Convert to m3/mol
			}
			catch (const std::exception& e) {
				DUNE_THROW(Dune::InvalidStateException, "Please provide the molar volume for " + mineralName[i]);
			}
		}

		// Get the volume fractions of solid components 
		mineralVolumeFraction.resize(numMinerals);
		for (int i = 0; i < numMinerals; i++)
		{
			try {	
				mineralVolumeFraction[i] = getParam<double>("PHREEQC.x_" + mineralName[i]);
			}
			catch (const std::exception& e) {
				DUNE_THROW(Dune::InvalidStateException, "Please provide the volume fraction for " + mineralName[i]);
			}
		}		

		// Overwrite the moles of solid components in phreeqc_rm
		std::vector<double> mineralMoles;
		mineralMoles.resize(Ncells * numMinerals);
		for (int n = 0; n < Ncells; n++)
			for (int i = 0; i < numMinerals; i++)
				mineralMoles[i*Ncells + n] = rv[n] * (1 - poro[n]) * mineralVolumeFraction[i] / mineralMolarVolume[i];

		status = phreeqc_rm->SetMineralMoles(mineralMoles);	

		int nspecies = phreeqc_rm->GetSpeciesCount();
		const std::vector<std::string> &species = phreeqc_rm->GetSpeciesNames();
		std::vector<double> c;
		status = phreeqc_rm->GetSpeciesConcentrations(c);
		// std::cout << "Species" << std::endl;
		// for (int i = 0; i < nspecies; i++)
		// {
		// 	std::cout << i << "     " << species[i] << " " << c[i] << std::endl;
		// }

		// Get names of components
		compName = phreeqc_rm->GetComponents();

		// The number of chemical elements in the aqueous phase
		numAqueousElements = numAqueousComponents - 1;

		// Extract the indices of chemical elements
		idComp.resize(numAqueousElements);
		int i = 0;
		bool foundCharge = false;
		int indCharge;
		bool foundH = false;
		bool foundO = false;
		for (int j = 0; j < numAqueousComponents; j++)
		{
			if ((compName[j] == "H") && (i == 0)) foundH = true;
			if ((compName[j] == "O") && (i == 1)) foundO = true;

			if (compName[j] != "Charge")
			{
				idComp[i] = j;
				i++;
			}
			else
			{
				foundCharge = true;
				indCharge = j;
			}

		}

		if (!foundCharge)
			DUNE_THROW(Dune::NotImplemented, "Charge component not found in PHREEQC input file...");

		if (!foundH)
			DUNE_THROW(Dune::NotImplemented, "H component not found as a 0th component as read from PHREEQC input file...");

		if (!foundO)
			DUNE_THROW(Dune::NotImplemented, "O component not found as a 1st component as read from PHREEQC input filee...");

		// The index to start salinity calculations
		startSalinityIdx = 2;


//		if (numAqueousElements != numAqueousComponents - 1)
//		{
//    		std::string errorMsg = "PHREEQC components are not elements + charge";
//    		DUNE_THROW(ParameterException, errorMsg.c_str());
//		}

		// Get the components' molar masses and set the elements molar masses
		std::vector<double> compMolarMass;
		compMolarMass = phreeqc_rm->GetGfw();
		elemMolarMass.resize(numAqueousElements);
		for (int i = 0; i < numAqueousElements; i++)
			elemMolarMass[i] = compMolarMass[ idComp[i] ] / 1000;			// Convert PHREEQC's gram-formula weight in g/mol to kg/mol


		// Check if the number of input elements matches the constexpr int numComponents + 1
		// ( + 1 for the single oil component)
		// (apparently cannot work with dynamic concentrations arrays in DuMuX...)
		if (numAqueousElements + 1 != numComponents)
		{
			std::cout << "     Please set constexpr int numComponents equal to " << numAqueousElements + 1
					  << " in myoilwater.hh and re-compile the code..\n" << std::endl;
			throw Dune::NotImplemented();
		}
		else
		{
			std::cout << "     Setting the PrimaryVariables dimension to " << numComponents << " to include the single oil component.." << std::endl;
		}

		// Set array of initial conditions
		std::vector<int> ic1, ic2;
		ic1.resize(Ncells*7, -1);
		for (int i = 0; i < Ncells; i++)
		{
			ic1[i] = 1;              // Solution 1
			ic1[Ncells + i] = -1;      // Equilibrium phases none
			ic1[2*Ncells + i] = 1;     // Exchange 1
			ic1[3*Ncells + i] = -1;    // Surface none
			ic1[4*Ncells + i] = -1;    // Gas phase none
			ic1[5*Ncells + i] = -1;    // Solid solutions none
			ic1[6*Ncells + i] = +1;    // Kinetics 
		}

		// Set the initial solution ic1
		// Mixing is set if ic2>0 using fractions f1
		//status = phreeqc_rm->InitialPhreeqc2Module(ic1, ic2, f1);
		// No mixing is defined, so the following is equivalent
		status = phreeqc_rm->InitialPhreeqc2Module(ic1);
		if (status < 0) phreeqc_rm->DecodeError(status);

		// Resize the solution vectors
		concentration.resize(Ncells * numAqueousComponents);
		moleMinerals.resize(Ncells * numMinerals);

		// Set up the time 
		double time = 0.0;
		double time_step = 0.0;		
		status = phreeqc_rm->SetTime(time);				// Simulation time, in seconds
		status = phreeqc_rm->SetTimeStep(time_step);	// Time step over which kinetic reactions are integrated

		// Concentrations from the PQI input file are apparently interpreted as the amount of moles per volume
		// of the corresponding representative volume. In order to get back the concentrations from the PQI input file,
		// the "concentrations" vector need to be multiplied by the corresponding representative volume.
		status = phreeqc_rm->GetConcentrations(concentration);

		// Initial equilibration of cells
		status = phreeqc_rm->RunCells();						// Runs a reaction step for all reaction cells
		status = phreeqc_rm->GetConcentrations(concentration);	// Get the updated concentrations in SetUnitsSolution() units

		// Convert concentrations from [mol/L] to [mol/m3]
		compMolarDensity.resize(Ncells * numAqueousComponents);
		for (int n = 0; n < Ncells; n++)
			for (int i = 0; i < numAqueousComponents; i++)
				compMolarDensity[i*Ncells + n] = concentration[i*Ncells + n] * 1000;

		// Set boundary conditions bc1 and bc2 using the concentrations of SOLUTION 0 from Initial IPhreeqc instance
		std::vector<double> bcConcentration, bc_f1;
		std::vector<int> bc1, bc2;
		int nbound = 1;
		bc1.resize(nbound, 0);
		bc2.resize(nbound, -1);                     // no bc2 solution for mixing
		bc_f1.resize(nbound, 1.0);                  // mixing fraction for bc1
		status = phreeqc_rm->InitialPhreeqc2Concentrations(bcConcentration, bc1, bc2, bc_f1);

		// Mole density of formation water [mol/L]
    	double brineWaterMolarDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		brineWaterMolarDensity += compMolarDensity[ idComp[i] * Ncells + 1] / 1000;

		// Mass density of formation water [kg/m3]
    	double brineWaterMassDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		brineWaterMassDensity += compMolarDensity[ idComp[i] * Ncells + 1] * elemMolarMass[i];

		// Mole density of injection water [mol/L]
    	double bcWaterMolarDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		bcWaterMolarDensity += bcConcentration[ idComp[i] ];

		// Mass density of injection water [kg/m3]
    	double bcWaterMassDensity = 0;
    	for (int i = 0; i < numAqueousElements; i++)
    		bcWaterMassDensity += bcConcentration[ idComp[i] ] * 1000 * elemMolarMass[i];

		// Get the molar fractions of boundary conditions
    	bcMoleFraction.resize(numComponents);
    	bcMoleFraction[0] = 0;	// The 0-th entry is set to zero which corresponds to no oil influx
    	for (int i = 0; i < numAqueousElements; i++)
    		bcMoleFraction[1 + i] = bcConcentration[ idComp[i] ] / bcWaterMolarDensity;

    	// Compute the average molar mass of the water phase for the inflow boundary [kg/mol]
    	// (compMolarMass are specified for aqueous elements only)
    	averageMolarMass = 0.;
    	for (int i = 0; i < numAqueousElements; i++)
    		averageMolarMass += bcMoleFraction[1 + i] * elemMolarMass[i];


    	// Calculate the salinity for the formation water (high salinity)... [kg/m3]
    	highSal = 0;
    	for (int i = startSalinityIdx; i < numAqueousElements; i++)
    		highSal += elemMolarMass[i] * compMolarDensity[idComp[i]*Ncells + 1];	// Use e.g. the values of concentrations in the 1st cell in the domain
    	highSal *= 1e3;	// Convert to mg/L

    	// ... and for the injection water (low salinity) [kg/m3]
    	lowSal = 0;
    	for (int i = startSalinityIdx; i < numAqueousElements; i++)
    		lowSal += elemMolarMass[i] * bcConcentration[ idComp[i] ] * 1000;
    	lowSal *= 1e3;	// Convert to mg/L

    	// Setting the constant molar density
		waterMolarDensity = 0.5 *(brineWaterMolarDensity + bcWaterMolarDensity);
		double errMolarDensity = std::abs( (brineWaterMolarDensity - waterMolarDensity)/brineWaterMolarDensity * 100);
		waterMolarDensity *= 1000;	// Convert molar density to [mol/m3]

		// Determine the error due to constant molar density assumption
		std::vector<double> moleFraction;
		moleFraction.resize(numAqueousComponents, 0);
		int elemIdx = 1;
		double sumMolarFract = 0;
		for (int i = 0; i < numAqueousComponents; i++)
		{
			moleFraction[i] = compMolarDensity[ i * Ncells + elemIdx] / waterMolarDensity;
			if (i != indCharge) sumMolarFract += moleFraction[i];
		}
		double errMolarFrac = (1 - sumMolarFract) * 100;

		// Output the properties of the formation water
		std::cout << "     Formation water @ " << temp[0] << "C" << std::endl;
		int outwidth = 15;
		std::cout << "     " << std::setw(outwidth) <<  "Component"
				  << std::setw(outwidth) << "Weight [g/mol]"
				  << std::setw(outwidth) << "#moles per 1 L"
				  << std::setw(outwidth) << "Mole fraction"
				  << std::endl;

		for (int i = 0; i < numAqueousComponents; i++)
		{
			std::cout << "     " << std::setw(outwidth) << compName[i]
					  << std::setw(outwidth) << compMolarMass[i]
					  << std::setw(outwidth) << concentration[i*Ncells + 1]	* rv[1]	// Print the values of concentrations in the 1st cell in the domain
					  << std::setw(outwidth) << moleFraction[i]
					  << std::endl;
		}
		std::cout << "         Molar density: " << brineWaterMolarDensity << " [mol/L]" << std::endl;
		std::cout << "         Mass density: " << brineWaterMassDensity << " [kg/m3]" << std::endl;
		std::cout << "         Salinity: " << highSal << " [ppm]" << std::endl;

		// Output the properties of the injection water
		std::cout << "     Injection water @ " << temp[0] << "C" << std::endl;
		std::cout << "     " << std::setw(outwidth) <<  "Component"
				  << std::setw(outwidth) << "Weight [g/mol]"
				  << std::setw(outwidth) << "#moles per 1 L"
				  << std::endl;
		for (int i = 0; i < numAqueousComponents; i++)
		{
			std::cout << "     " << std::setw(outwidth) << compName[i]
					  << std::setw(outwidth) << compMolarMass[i]
					  << std::setw(outwidth) << bcConcentration[i]		// Print the values of concentrations in the 1st cell in the domain
					  << std::endl;
		}
		std::cout << "         Molar density: " << bcWaterMolarDensity << " [mol/L]" << std::endl;
		std::cout << "         Mass density: " << bcWaterMassDensity << " [kg/m3]" << std::endl;
		std::cout << "         Salinity: " << lowSal << " [ppm]" << std::endl;

		std::cout << "     Setting the constant molar density: " << waterMolarDensity << " [mol/m3] " << std::endl;
		std::cout << "         Error in molar density = " << errMolarDensity << "%" << std::endl;
		std::cout << "         Error in molar fractions = " << errMolarFrac << "%" << std::endl;

	}

	// Destructor
	~PHREEQC()
	{
		delete phreeqc_rm;
	}


   /*!
	 * \brief Return numAqueousComponents entries of molar fractions of components
	 *		  in the [1..NumComponents] entries of the vector moleFraction,
	 *		  and the vector of components' molar densities.
	 *		  If FieldVector is substituted by PrimaryVariables, then the first 2 entries
	 *		  are overwritten by (pn, Sw)
	 * \param elemIdx index of the element
	 */
	template <class FieldVector>
	void getElementMoleFraction(FieldVector& moleFraction, int elemIdx)
	{

		moleFraction[0] = 0;	// The oil component molar fraction in water phase
		for (int i = 0; i < numAqueousElements; i++)
			moleFraction[1 + i] = compMolarDensity[ idComp[i] * Ncells + elemIdx] / waterMolarDensity;

	}


    /*!
     * \brief Return numAqueousComponents entries of molar fractions of components
     *		  in the [1..NumComponents] entries of the vector bcMoleFraction.
     *		  The 0-th entry is set to zero which corresponds to no oil influx.
     * \param elemIdx index of the element
     */
    std::vector<double> getBoundaryMoleFraction()
    {
    	return bcMoleFraction;
    }

    /*!
     * \brief Return the average molar mass of the water phase
     *        using the mole fractions at the inflow boundary
     */
    double getBoundaryWaterAverageMolarMass()
    {
        return averageMolarMass;
    }


    /*!
     * \brief Set the saturation, pressure, and molar densities for the
     * 		  elemIdx-th entry in PHREEQC input vectors.
     */
    template <class FieldVector>
    void setValues(FieldVector priVars, int elemIdx)
	{
    	// Set pressure and saturation in PHREEQC vectors
    	pressure[elemIdx] = priVars[0] / 1e5;	// Convert to bar
		sat[elemIdx] = priVars[1];

		// Restore the molar fraction of the only minor component (H)
        double sumMoleFracMajorComponents = 0.;
        for (int compIdx = numMajorComponents; compIdx < numComponents; ++compIdx)
            sumMoleFracMajorComponents += priVars[compIdx];

        double moleFracH = 1 - sumMoleFracMajorComponents;

        // Re-arrange the entries of priVars to incorporate Charge
        priVars[0] = moleFracH;		// H
        priVars[1] = priVars[2];	// O
        priVars[2] = 0;				// Set Charge to 0 for simplicity..

        // Set the molar densities in the PHREEQC vector
        for (int i = 0; i < numAqueousElements; i++)
        	concentration[i*Ncells + elemIdx] = priVars[i] * waterMolarDensity / 1000;	// Convert from [mol/m3] to [mol/L]
	}


    /*!
     * \brief Get results from PHREEQC to the transport solver
     *
     *    - Sets the pressure, saturation, and concentration vectors
     *    - Runs PHREEQC reactions at provided time and timestep
     *    - Returns the updated components' molar densities.
     */
    template <class FieldVector>
    void getResults(std::vector<FieldVector>& priVars,
				    double time,
				    double timestep)
    {


		// debug
		// std::cout << "  -solid_solutions " << "\n";
		// // solid solutions
		// std::vector ss_comps = phreeqc_rm->GetSolidSolutionComponents();
		// std::vector ss_names = phreeqc_rm->GetSolidSolutionNames();
		// for (size_t i = 0; i < phreeqc_rm->GetSolidSolutionComponentsCount(); i++)
		// {
		// 	std::cout << "    ";
		// 	std::cout  << std::left << ss_comps[i];
		// 	std::cout << " # " << ss_names[i] << "\n";
		// }		

		// std::cout << "  -kinetics " << "\n";
		// // kinetic reactions
		// std::vector kin_reactions = phreeqc_rm->GetKineticReactions();
		// for (size_t i = 0; i < phreeqc_rm->GetKineticReactionsCount(); i++)
		// {
		// 	std::cout << "    " << kin_reactions[i] << "\n";
		// }

		// std::cout << "  -equilibrium_phases " << "\n";
		// // equilibrium phases
		// std::vector eq_phases = phreeqc_rm->GetEquilibriumPhases();
		// for (size_t i = 0; i < phreeqc_rm->GetEquilibriumPhasesCount(); i++)
		// {
		// std::cout << "    " << eq_phases[i] << "\n";
		// }		



	   IRM_RESULT status;

	   // Transfer data to PhreeqcRM for reactions
	   status = phreeqc_rm->SetSaturation(sat);
	   status = phreeqc_rm->SetPressure(pressure);
	   status = phreeqc_rm->SetConcentrations(concentration);

	   status = phreeqc_rm->SetTimeStep(timestep / 24. / 3600.);  // Time step for kinetic reactions [days]
	   status = phreeqc_rm->SetTime(time / 24./ 3600.);

	   // Runs a reaction step for all reaction cells
	   status = phreeqc_rm->RunCells();
 
	   // Get updated concentrations and minerals
	   status = phreeqc_rm->GetConcentrations(concentration); 
	   status = phreeqc_rm->GetMineralMoles(moleMinerals);

	   // Get together the primary variables' vector
	   for (int n = 0; n < Ncells; n++)
	   {
		   priVars[n][0] = pressure[n] * 1e5;		// Convert to Pa
		   priVars[n][1] = sat[n];
		   priVars[n][numMajorComponents] = concentration[ (numMajorComponents - 1) * Ncells + n] * 1000 / waterMolarDensity;
		   for (int i = numMajorComponents + 1; i < numAqueousComponents; i++)
			   priVars[n][i] = concentration[i*Ncells + n] * 1000 / waterMolarDensity;
	   }

   }

   PhreeqcRM* getPHREEQCRM()
   {
	   return phreeqc_rm;
   }


private:

	// Adjust the number of cells in the PQI file for the current domain
    void adjustPQI(std::string oldPQI, std::string newPQI, int Ncells)
    {
    	std::string oldDim = "1-9999";
    	std::string newDim = "1-" + std::to_string(Ncells);

    	std::ifstream infile;
    	infile.open(oldPQI.c_str());
    	if (!infile) {
    		std::string errorMsg = "Unable to open file " + oldPQI;
    		DUNE_THROW(ParameterException, errorMsg.c_str());
    	}

        std::ofstream outfile;
    	outfile.open(newPQI.c_str());
    	if (!outfile) {
    		std::string errorMsg = "Unable to open file " + newPQI;
    		DUNE_THROW(ParameterException, errorMsg.c_str());
    	}

        // Loop through the old PQI file, replace the dimension there, and write the updated lines to the newPQI
    	std::string str;
    	while (!infile.eof())
    	{
    		getline(infile, str);
    		size_t index = 0;
    		index = str.find(oldDim, index);
    		if (index != std::string::npos)
    			str.replace(index, oldDim.length(), newDim);
    		outfile << str << std::endl;
    	}

    	infile.close();
    	outfile.close();
    }

	// Get the database name from the provided PQI file
    std::string getDatabaseName(std::string PQI)
    {
        std::string dbasename = "";
    	std::ifstream infile;
    	infile.open(PQI.c_str());
    	if (!infile) {
    		std::string errorMsg = "Unable to open file " + PQI;
    		DUNE_THROW(ParameterException, errorMsg.c_str());
    	}

        // Loop through the PQI file, look for the DATABASE keyword, and get the database name
    	std::string str;
    	while (!infile.eof())
    	{
    		getline(infile, str);
    		size_t index = 0;
    		index = str.find("DATABASE", index);
    		if (index != std::string::npos)
            {                
                std::size_t found = str.find_last_of("/\\");
                dbasename = str.substr(found+1, str.length());
                // Eventually remove the carriage return \r
                dbasename.erase( std::remove(dbasename.begin(), dbasename.end(), '\r'), dbasename.end() );
                break;
            }
    	}
    	infile.close();
        return dbasename;
    }    	


    double _temperature;

    int Ncells;								// Number of cells in the domain

    int numComponents;
    int numMajorComponents;
    int numAqueousComponents;				// Number of aqueous components
    int numAqueousElements;					// Number of aqueous elements
	int numMinerals;						// Number of reacting minerals

    std::vector<int> idComp;					// Map from components to elements
    int startSalinityIdx;			 		// The index to start salinity calculations
	std::vector<std::string> compName;
	std::vector<double> elemMolarMass;
	std::vector<double> concentration;		// Vector of molar densities (concentrations in PHREEQC terminology)
	std::vector<double> bcMoleFraction;		// Vector of molar densities at the inflow boundary
	double averageMolarMass;
	double waterMolarDensity;				// The constant molar density
	std::vector<double> moleMinerals;		// Vector of moles of minerals
	std::vector<std::string> mineralName;	
	std::vector<double> mineralMolarVolume;
	std::vector<double> mineralVolumeFraction;

	double lowSal;
	double highSal;

	PhreeqcRM *phreeqc_rm;

	// The interface to the transport solver
	std::vector<double> compMolarDensity;	// Vector of molar densities in mol/m3
	std::vector<double> sat;				// Vector of saturations according to the current Dumux formulation
	std::vector<double> pressure;			// Vector of pressures according to the current Dumux formulation

};
} // end namespace FluidSystems
} // end namespace Dumux

#endif
    
 