// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \ingroup MultiDomain
  * \ingroup MultiDomainFacet
  * \ingroup TwoPTests
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
  */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_PROBLEM_HH

// we use alu grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>

// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cellcentered/tpfa/properties.hh>

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include "model/model.hh"

// Incompressible water-oil system with paraameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "fluidsystems/myliquidphasenc.hh"
#include "fluidsystems/myoilwater.hh" 
#include "components/mybrine.hh"
#include "components/mylowsalwater.hh"
#include "components/myoil.hh"

// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"

#include "fluidsystems/phreeqc.hh"


namespace Dumux {
// forward declarations
template<class TypeTag> class FractureSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag nodes
NEW_TYPE_TAG(FractureProblemTypeTag, INHERITS_FROM(CCTpfaModel, TwoPNC));
// Set the grid type
SET_TYPE_PROP(FractureProblemTypeTag, Grid, Dune::FoamGrid<1, 2>);
// Set the problem type
SET_TYPE_PROP(FractureProblemTypeTag, Problem, FractureSubProblem<TypeTag>);
// set the spatial params
SET_TYPE_PROP(FractureProblemTypeTag,
              SpatialParams,
              FractureSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                     typename GET_PROP_TYPE(TypeTag, Scalar) >);

// Set the fluid system
SET_PROP(FractureProblemTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using WaterPhase = typename FluidSystems::MyLiquidPhaseNC<Scalar, 
                                                              Components::MyIncompressibleBrineComponent<Scalar>,       // Main component of the water phase
                                                              Components::MyIncompressibleLowSalWaterComponent<Scalar>  // Secondary component of the water phase
                                                              >;
                                                              
    //using WaterPhase = typename FluidSystems::OnePLiquid<Scalar, Components::MyIncompressibleBrineComponent<Scalar> >;
    
    // Define the oil phase as single phase liquid system (OnePLiquid), consisting of a single component MyIncompressibleOilComponent, specidified in myOil.hh
    using OilPhase = typename FluidSystems::OnePLiquid<Scalar, Components::MyIncompressibleOilComponent<Scalar> >;
    
    // Using fluid system, consisting of 2 immiscible phases
    using type = typename FluidSystems::MyOilWater<Scalar, WaterPhase, OilPhase>;

}; 


} // end namespace Properties

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
  * \brief The sub-problem for the fracture domain in the exercise on two-phase flow in fractured porous media.
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);
	using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);	
	using GridIndexType = typename GridView::IndexSet::IndexType;

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem); 
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);
    
    enum
    {
    	wPhaseIdx = FluidSystem::phase0Idx,	// wetting phase index
        nPhaseIdx = FluidSystem::phase1Idx
    };
    
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::switchIdx,     //Saturation (apparently set to 1)

        // Phase State
		firstPhaseOnly = Indices::firstPhaseOnly,	//!< Only the first phase (in fluid system) is present
        secondPhaseOnly = Indices::secondPhaseOnly, //!< Only the second phase (in fluid system) is present
        bothPhases = Indices::bothPhases,      		//!< Both phases are present
		
    };
    
    // formulations
    static constexpr auto p0s1 = TwoPFormulation::p0s1;
    static constexpr auto p1s0 = TwoPFormulation::p1s0;

    // the formulation that is actually used
    static constexpr auto formulation = ModelTraits::priVarFormulation();    

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                       const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, dir_(getParamFromGroup<Scalar>(paramGroup, "Problem.Direction"))	  
    , boundaryRightPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryRightPressure"))
	
	, injLowSalTime_(getParamFromGroup<Scalar>(paramGroup, "Problem.injLowSalTime"))	
	
    , Swr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"))
	, Snr_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"))
	
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    {
    	std::cout << "Initializing the " << paramGroup << " domain" << std::endl;
    	// Infer the type of left boundary conditions from input data
		bool bcSet = false;
		boundaryLeftRate_ = 1e100;
		try {	
			// If BoundaryLeftRate is present then left boundary is Neumann with the corresponding rate
			boundaryLeftRate_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftRate");		
			boundaryLeftDirichlet = false;
			bcSet = true;
			std::cout << "     Using Neumann conditions at the left boundary.." << std::endl;
		} catch (const std::exception& e) {	
			// Check whether Dirichlet data are provided for the left boundary
			//std::cout << e.what(); 
			try {	
				boundaryLeftPressure_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftPressure");
				boundaryLeftSaturation_ = getParamFromGroup<Scalar>(paramGroup, "Problem.BoundaryLeftSaturation");
				boundaryLeftDirichlet = true;		
				bcSet = true;
				std::cout << "     Using Dirichlet conditions at the left boundary.." << std::endl;
			} catch (const std::exception& e) {	}			
		}
		
		if (!bcSet) 
		{				
			std::cout << "     Neither Dirichlet nor Neumann conditions are provided for the left boundary..\n" << std::endl;
			throw Dune::NotImplemented();
		}			
		        
        // Only allow for p1s0 formulation for simplicity
        if (formulation != p1s0) {
        	std::cout << "     WARNING: Please set up the p1s0 formulation in porousmediumflow/2p/model.hh\n" << std::endl;
            assert(formulation == p1s0);
        }

        // Set the initial conditions to the values from the .input file; if these are not present, use the 
        // right BC pressure and the irreducible water saturation
		try {	
			initialPressure = getParamFromGroup<Scalar>(paramGroup, "Problem.InitialPressure");		
			InitialSn = getParamFromGroup<Scalar>(paramGroup, "Problem.InitialSn");	
            std::cout << "     Using the provided initial data.." << std::endl;   
            } catch (const std::exception& e) 
        {
			initialPressure = boundaryRightPressure_;	
			InitialSn = 1 - Swr_;
			std::cout << "     Initializing the solution with the right BC pressure and the irreducible water saturation.." << std::endl;           
        }              

        // Number of cells
	    Ncells = this->fvGridGeometry().gridView().size(0);
	    
        // Get the array of elements' volumes and consecutive indices to be used in PHREEQC referencing
	    std::vector<Scalar> rv;
	    rv.resize(Ncells);
	    //int n = 0;
		GridIndexType n = 0;
		std::vector<int> indElem;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                // volVars.extrusionFactor() is the fracture aperture
                // scv.volume() is the length of the fracture segment
                // Assuming that extrusionFactor() == 1 for the matrix, get the correct volume of the fracture segment
            	rv[n] = scv.volume() * aperture_;

				// Ensure that the Dune indices are consecutive from 0
				if (scv.elementIndex() == n)
					indElem.push_back(n);
				else
				{
					std::cout << "ERROR in numbering: element #" << scv.elementIndex() << " is not consecutive (<>" << n << ")" << std::endl;
					DUNE_THROW(Dune::NotImplemented, "DUNE indices not consecutive");
				}
                n++;
            }
        }


        /****************************************
         * Initialize the interface to PHREEQC
         ****************************************/

    	// Array of elements' porosities
	    std::vector<Scalar> poro;
	    Scalar phi = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
	    poro.resize(Ncells, phi);

		if (formulation != p1s0) {
            std::cout << "Matrix constructor: formulation not implemented.." << std::endl;
    	}

	    // Initialize water saturations
	    sat.resize(Ncells, 1 - InitialSn);								// Declare globally for later use in solveReactions()

	    // Initialize PHREEQC water pressures with OIL pressure (to avoid negative pressures)
	    pressure.resize(Ncells, initialPressure / 1e5);	// Convert to bar as required by PHREEQC

		// initialize the fluid system
		FluidSystem::init(paramGroup, poro, rv, sat, pressure);

	    // Molar densities in PHREEQC format for concentrations
		compMolarDensity.resize(Ncells * FluidSystem::numAqueousComponents);

		// Initialize the PHREEQC object which keeps track of Matrix-specific spatially distributed properties
		std::string domain = paramGroup;
		phreeqc = new FluidSystems::PHREEQC(FluidSystem::filePQI,
											FluidSystem::numComponents,
											FluidSystem::numMajorComponents,
											domain,
											poro,
											rv,
											sat,
											pressure);

        // The primary variables to get PHREEQC-updated results
        priVars.resize(Ncells);

		std::cout << std::endl;
	
    }
    
    //! The destructor
    ~FractureSubProblem()
	{
		delete phreeqc;
	}


	// Set current time
    void setTime(Scalar time)
    {
        time_ = time;
    }    

    //! Initialize the bounding box from the matrix sub-problem
    // (otherwise this->fvGridGeometry().bBoxMin() gives the bounding box for the fractures)
    template<class MatrixIdType>
    void setBoundingBox(const MatrixIdType& matrixDomainId)
    {
    	auto& matrixProblem = couplingManagerPtr_->template problem<matrixDomainId>();
		xmin = matrixProblem.fvGridGeometry().bBoxMin()[0];
        xmax = matrixProblem.fvGridGeometry().bBoxMax()[0];
		ymin = matrixProblem.fvGridGeometry().bBoxMin()[1];
        ymax = matrixProblem.fvGridGeometry().bBoxMax()[1];
    }

    
    // NA: get the OIP and WIP
    // analogously to postTimeStep from richardsnc    
    void getVolumesFluxes(std::vector<Scalar>& Volume,
    					  std::vector<Scalar>& InFlux, std::vector<Scalar>& OutFlux, NumEqVector& waterMolarOutFlux,
						  const SolutionVector& curSol, const GridVariables& gridVariables)
    {    	
    	std::fill(Volume.begin(), Volume.end(), 0.);
    	std::fill(InFlux.begin(), InFlux.end(), 0.);
    	std::fill(OutFlux.begin(), OutFlux.end(), 0.);     
    	std::fill(waterMolarOutFlux.begin(), waterMolarOutFlux.end(), 0.);
        
        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            //fvGeometry.bindElement(element);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            //int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = x[eIdx];
            
            VolumeVariables volVars;

            for (auto&& scv : scvs(fvGeometry))
            {
                //const auto& volVars = elemVolVars[scv];
                volVars = elemVolVars[scv];
                
                // volVars.extrusionFactor() is the fracture aperture
                // scv.volume() is the length of the fracture segment
                // Assuming that extrusionFactor() == 1 for the matrix, get the correct volume of the fracture segment
                Volume[nPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(nPhaseIdx);
                Volume[wPhaseIdx] += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.saturation(wPhaseIdx);

            }

            // Loop through the faces 
            // see porousmediumflow/velocityoutput.hh and porousmediumflow/nonequilibrium/gridvariables.hh
            // bind the element flux variables cache

            // the upwind term to be used for the volume flux evaluation
            auto upwindTermN = [](const auto& volVars) { return volVars.mobility(nPhaseIdx); };
            auto upwindTermW = [](const auto& volVars) { return volVars.mobility(wPhaseIdx); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {

            	if (scvf.boundary()) {

                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();
                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar fn = fluxVars.advectiveFlux(nPhaseIdx, upwindTermN); 
                        Scalar fw = fluxVars.advectiveFlux(wPhaseIdx, upwindTermW); 
                        
                        InFlux[nPhaseIdx]  += std::min(fn, 0.);
                        OutFlux[nPhaseIdx] += std::max(fn, 0.);
                        
                        InFlux[wPhaseIdx]  += std::min(fw, 0.);
                        OutFlux[wPhaseIdx] += std::max(fw, 0.);

						// Accumulate molar fluxes at the outflux
						waterMolarOutFlux[0] = 0;		// Set oil component molar concentration to zero
						for (int i = 1; i < FluidSystem::numComponents; i++)
							waterMolarOutFlux[i] += std::max(fw, 0.) * volVars.density(wPhaseIdx) 		// Get total mass outflux
												 / volVars.fluidState().averageMolarMass(wPhaseIdx)		// Get total molar outflux
												 * volVars.fluidState().moleFraction(wPhaseIdx, i);		// Get components' molar outflux
                    }
                    else if (bcTypes.hasOnlyNeumann()) {
            			// Read the mole fluxes per unit length [mol/(m*s)]
						NumEqVector moleFlux = this->neumannAtPos(scvf.center());

						// Get the mass fluxes for components [kg/sec]
						NumEqVector massFlux;
						for (int i = 0; i < FluidSystem::numComponents; i++)
							massFlux[i] = moleFlux[i] * scvf.area() * extrusionFactor * FluidSystem::molarMass(i);

						// Get the total mass flux for the two phases [kg/sec]
						Scalar oilMassFluxTotal = massFlux[0];
						Scalar waterMassFluxTotal = 0.;
						for (int i = 1; i < FluidSystem::numComponents; i++)
							waterMassFluxTotal += massFlux[i];

						// Get the total volumetric flux for the two phases [m3/sec]
						Scalar oilVolumeFlux = oilMassFluxTotal / volVars.density(nPhaseIdx);
						Scalar waterVolumeFlux = waterMassFluxTotal / volVars.density(wPhaseIdx);

						InFlux[nPhaseIdx]  += std::min(oilVolumeFlux, 0.);
						OutFlux[nPhaseIdx] += std::max(oilVolumeFlux, 0.);

						InFlux[wPhaseIdx]  += std::min(waterVolumeFlux, 0.);
						OutFlux[wPhaseIdx] += std::max(waterVolumeFlux, 0.);

						// Accumulate molar fluxes at the outflux
						waterMolarOutFlux[0] = 0;		// Set oil component molar concentration to zero
						for (int i = 1; i < FluidSystem::numComponents; i++)
							waterMolarOutFlux[i] += std::max(moleFlux[i], 0.);
                    }
                    else {
                        DUNE_THROW(Dune::NotImplemented, "fractureproblem.hh: boundary condition not defined..");
                    }
                    
                }
                
            }
                        
        }

    }  
    
    
    // Compute the pore volume occupied by fractures
    Scalar poreVolume(const SolutionVector& curSol, const GridVariables& gridVariables)
    {
        Scalar PV = 0.0;

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                
                // volVars.extrusionFactor() is the fracture aperture
                // scv.volume() is the length of the fracture segment
                // Assuming that extrusionFactor() == 1 for the matrix, get the correct volume of the fracture segment
                PV += scv.volume() * volVars.porosity() * volVars.extrusionFactor();
            }
        }
        
        return PV;
    }
    
   
    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        // We only use no-flow boundary conditions for all immersed fractures
        // in the domain (fracture tips that do not touch the domain boundary)
        // Otherwise, we would lose mass leaving across the fracture tips.
        values.setAllNeumann();
		
		if (boundaryLeftDirichlet) 
		{
			// Dirichlet at the left and at the right
			values.setAllNeumann();
			if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}
		else 
		{
			// Neumann at the left, Dirichlet at the right
			values.setAllNeumann();        
			if (globalPos[dir_] > xmax - 1e-6)
				values.setAllDirichlet();
		}

        return values;
                
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        // We treat the fractures as lower-dimensional in the grid,
        // but we have to give it the aperture as extrusion factor
        // such that the dimensions are correct in the end.
        return aperture_;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    //PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {  
        PrimaryVariables values;
		values.setState(bothPhases);	// For compositional runs, need to specify properties of which phases will be calculated

        GridIndexType elemIdx = this->fvGridGeometry().elementMapper().index(element);
        auto globalPos = scvf.center();
 
        if (globalPos[dir_] < xmin + 1e-6) {
			
        	Scalar molarFracLSW;
        	if (time_ < injLowSalTime_) {
    			std::cout << "Injectin lSW at later times not implemennted.." << std::endl;
    			throw Dune::NotImplemented();
        	}

			std::cout << "Dirichlet BC at the inflow are not implemented.." << std::endl;
			throw Dune::NotImplemented();
            
//            if (formulation == p0s1) {
//                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
//                //    values[pressureIdx] is interpreted as wetting phase pressure
//                //    values[saturationIdx] is interpreted as non-wetting phase saturation
//                values[pressureIdx] = boundaryLeftPressure_;            // assign the oil pressure boundaryLeftPressure_ to the wetting phase pressure
//                values[saturationIdx] = 1 - boundaryLeftSaturation_;   	// Oil saturation
//                values[molarfracIdx] = 1 - molarFracLSW;				// Brine molar fraction
//            }
//            else if (formulation == p1s0) {
//                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
//                //    values[pressureIdx] is interpreted as non-wetting phase pressure
//                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
//                values[pressureIdx] = boundaryLeftPressure_ ;
//                values[saturationIdx] = boundaryLeftSaturation_;
//				values[molarfracIdx] = 1 - molarFracLSW;				// Brine molar fraction
//            }
        }

        
        // Fixed right pressure 
        if (globalPos[dir_] > xmax - 1e-6) {
            
            if (formulation == p0s1) {

            	std::cout << "p0s1 formulation is not implemented.." << std::endl;
    			throw Dune::NotImplemented();

//                // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
//                //    values[pressureIdx] is interpreted as wetting phase pressure
//                //    values[saturationIdx] is interpreted as non-wetting phase saturation
//                values[pressureIdx] = boundaryRightPressure_;   // assign the oil pressure boundaryRightPressure_ to the wetting phase pressure
//                values[saturationIdx] = 1 - Swr_;
//				values[molarfracIdx] = 1.;						// Brine molar fraction
            }
            else if (formulation == p1s0) {

                // Set the last numAqueousElements entries with the elements' mole fractions
                phreeqc->getElementMoleFraction(values, elemIdx);

                // Overwrite the first 2 entries with pressure and saturation
                // The mole fraction of the 1st element (H) gets overwritten

            	// In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
                //    values[pressureIdx] is interpreted as non-wetting phase pressure
                //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
                values[pressureIdx] = boundaryRightPressure_ ;
                values[saturationIdx] = Swr_;
            }
            else {
                std::cout << "dirichletAtPos: formulation not implemented.." << std::endl;
    			throw Dune::NotImplemented();
                
            }
            
        }
        
        return values;
        
    }
    

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
		
        // Calculate the water molar influx (assume no oil is injected)
		std::vector<double> bcMoleFraction = phreeqc->getBoundaryMoleFraction();
		Scalar averageMolarMass = phreeqc->getBoundaryWaterAverageMolarMass();

		const Scalar molarFlux = boundaryLeftRate_ / averageMolarMass;	// in mol/(m*s)

        if (globalPos[dir_] < xmin + 1e-6) {
        	for (int compIdx = 0; compIdx < FluidSystem::numComponents; compIdx++)
        		values[compIdx] = molarFlux * bcMoleFraction[compIdx];
        }

        // Set the boundary molar fractions also in fluidState (accessible from volVars.fluidstate)
		FluidState fluidState;
		for (int compIdx = 0; compIdx < FluidSystem::numComponents; compIdx++)
			fluidState.setMoleFraction(wPhaseIdx, compIdx, bcMoleFraction[compIdx]);

        return values;
    }
    
        
    //! evaluate the initial conditions
	PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables values;

		// Currently, the phase equilibrium calculations in volumevariables.hh are tested for the case when both phases are present
        Scalar threshSwr = 1e-3;
        assert((Swr_ > threshSwr) && (Snr_ > threshSwr));
        values.setState(bothPhases);

        GridIndexType elemIdx = this->fvGridGeometry().elementMapper().index(element);

        // Set the last numAqueousElements entries with the elements' mole fractions
        phreeqc->getElementMoleFraction(values, elemIdx);

        // Overwrite the first 2 entries with pressure and saturation
        // The mole fraction of the 1st element (H) gets overwritten
		if (formulation == p1s0) {
            // In porousmediumflow/2p/volumevariables.hh if wPhaseIdx == 0:
            //    values[pressureIdx] is interpreted as non-wetting phase pressure
            //    values[saturationIdx] is interpreted as wetting phase saturation(after correcting the bug in porousmediumflow/2p/volumevariables.hh)
            values[pressureIdx] = initialPressure;
            values[saturationIdx] = 1 - InitialSn;
        }
    	else {
            std::cout << "initial: formulation not implemented.." << std::endl;
    	}

		return values;
    }


    // Solve equilibrium and kinetic reactions as defined in the input PHREEQC file in a sequential non-iterative approach
    void solveReactions(SolutionVector& curSol,
                        GridVariables& gridVariables,
						Scalar time,
						Scalar timestep)
    {

    	// Pass the data to PHREEQC structures
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            // Get the primary variables' vector for the current element
            int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = curSol[eIdx];

            // Set the concentration, pressure, and saturations in PHREEQC structures
            phreeqc->setValues(curSol[eIdx], eIdx);
        }

        // Get results from PHREEQC at current time using the current timestep
        phreeqc->getResults(priVars, time, timestep);

    	// Set the updated primary variables
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            // Get the primary variables' vector for the current element
            int eIdx = this->fvGridGeometry().elementMapper().index(element);
            priVars[eIdx].setState(bothPhases);	// To get rid of the complaints in volumevariables.hh
            curSol[eIdx] = priVars[eIdx];
        }

    }

	void getSpeciesConcentrations(std::array<Scalar, FluidSystem::numSpecies>& speciesConcentration_,
                                  const Element& element, const SubControlVolume& scv) const
	{
		double time = 1;
	}  

	void getMolesMinerals(std::array<Scalar, FluidSystem::numMinerals>& molesMinerals_,
                                  const Element& element, const SubControlVolume& scv) const
	{
        int numMinerals_ = phreeqc->getPHREEQCRM()->GetKineticReactionsCount();
		//const std::vector<std::string> &species = phreeqc_rm->GetSpeciesNames();
		
        GridIndexType n = scv.elementIndex();
        std::vector<double> moles;
        moles.resize(Ncells * FluidSystem::numMinerals);
		int status = phreeqc->getPHREEQCRM()->GetMineralMoles(moles);

        for (int i = 0; i < FluidSystem::numMinerals; i++)
			molesMinerals_[i] = moles[i * Ncells + n];      

	}  



    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    {
    	return FluidSystem::temperature();
    }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    
    
    // Save relative permeabilities and capillary pressure curves for a fixed value of X
    void evalRelPerms() const
    {	
		double X = 0.3;	// For instance..
		
		std::string filename( "matrix_relperms.csv");
		std::ofstream file( filename.c_str() );
		if (file) {	
			const char *header = "sw,krw_X,krn_X,pc_X";
			file << header << std::endl;        			
		}
		else
		{
			std::cerr << "ERROR: couldn't open file `" << filename << "'" << std::endl;
			
		}		
		
		GlobalPosition globalPos({0.0, 0.0});
		const auto& materialParams = this->spatialParams().materialLawParamsAtPos(globalPos);
		using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
		
		int N = 100;
		Scalar ds = 1. / (N+1);
		Scalar sw = 0;
		Scalar krw, krn, pc;
		for (int n = 0; n < N; n++) {
			krw = MaterialLaw::krw(materialParams, sw, X);
			krn = MaterialLaw::krn(materialParams, sw, X);		
			pc = MaterialLaw::pc(materialParams, sw, X);	
			sw += ds;
			
			file << sw  << "," <<              
                    krw << "," <<  
                    krn << "," <<  
                    pc << 
                    std::endl;	
		}
		file.close();		
	}

    FluidSystems::PHREEQC* getPhreeqc()
    {
        return phreeqc;
    }    
	    

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	bool boundaryLeftDirichlet;
	
	int dir_;
    Scalar boundaryLeftPressure_;
    Scalar boundaryLeftSaturation_;    
    Scalar boundaryRightPressure_;
	Scalar boundaryLeftRate_;		
	Scalar injLowSalTime_;	
    Scalar initialPressure, InitialSn;

	Scalar Swr_;
    Scalar Snr_;
    Scalar aperture_;
    Scalar xmin, xmax, ymin, ymax;
	
	Scalar time_;    	

	// Interface to PHREEQCRM
	FluidSystems::PHREEQC *phreeqc;			// Instance of PHREEQC for Matrix domain
    int Ncells;    							// Number of cells in Matrix domain
	std::vector<Scalar> compMolarDensity;	// Vector of molar densities in mol/m3
    std::vector<Scalar> sat;				// Vector of saturations according to the current formulation
    std::vector<Scalar> pressure;			// Vector of pressures according to the current formulation
	std::vector<PrimaryVariables> priVars;	// The primary variables to get PHREEQC-updated results
    
};

} // end namespace Dumux

#endif
 