// The 100 m stripe with a horizontal fracture is located in the middle 
// The stripe is meshed with 200 right-angled triangles

// Density of mesh near Points
lc = 1; 

// Length and width of the stripe
Lx = 100;
Ly = 1;

// Geometrical entities = 1d and 2D regions indices
// #2D_REGIONS
MATRIX = 666;

// #1D_REGIONS
OPEN_FRACTURES = 777;


// Outer box
Point(1) = {0,  0, 0, lc};
Point(2) = {Lm, 0, 0, lc};
Point(3) = {Lm, Lm, 0, lc};
Point(4) = {0,  Lm, 0, lc};

// Fractures' endpoints
Point(5) = {Lm/3, Lm/2, 0, lc};
Point(6) = {2*Lm/3,  Lm/2, 0, lc};

// Define the bounded domains
// NOTE: If a Line contains > 2 Points, then the triangulation nodes can NOT include the interior Points!
Line(500) = {1, 2};
Line(501) = {2, 3};
Line(502) = {3, 4};
Line(503) = {4, 1};

Line(505) = {5, 6};

Line Loop(600) = {500, 501, 502, 503}; 
Plane Surface(600) = {600}; 

// Including fractures in triangulation
Line{505} In Surface {600};


// Physical entities
// Fracture appears in the msh file as a set of line elements 
// (the 2nd parameter in the $Elements block elm-type is =1)
// with the 4th parameter = number of Physical Line
// and the 5th = number of corresponding Line
// Physical Line(LEFT) = {503};	
// Physical Line(RIGHT) = {501};	
// Physical Line(TOP) = {502};
// Physical Line(BOTTOM) = {500};


// Physical entities
Physical Line(OPEN_FRACTURES) = {505};

// Triangulated domain
// The triangular $Elements (the 2nd parameter in the $Elements block elm-type is =2)
// have the the 4th parameter = number of Physical Surface
// and the 5th = number of corresponding Plane Surface
Physical Surface(MATRIX) = {600}; 
 