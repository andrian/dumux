// Gmsh 2D geometry specification

// Density of mesh near Points
lcf = 4.382109e-01;

// Geometrical entities = 1d and 2D regions indices
// #2D_REGIONS
MATRIX = 666;

// #1D_REGIONS
OPEN_FRACTURES = 777;

// Points
Point(1) = {4.064000e+01, 6.510000e+01, 0, lcf};
Point(2) = {4.028000e+01, 6.476000e+01, 0, lcf};
Point(3) = {3.992000e+01, 6.442000e+01, 0, lcf};
Point(4) = {3.961000e+01, 6.404000e+01, 0, lcf};
Point(5) = {3.930000e+01, 6.365000e+01, 0, lcf};
Point(6) = {3.900000e+01, 6.324000e+01, 0, lcf};
Point(7) = {3.871000e+01, 6.284000e+01, 0, lcf};
Point(8) = {3.841000e+01, 6.244000e+01, 0, lcf};
Point(9) = {3.811000e+01, 6.204000e+01, 0, lcf};
Point(10) = {3.782000e+01, 6.164000e+01, 0, lcf};
Point(11) = {3.752000e+01, 6.123000e+01, 0, lcf};
Point(12) = {3.724000e+01, 6.082000e+01, 0, lcf};
Point(13) = {3.697000e+01, 6.040000e+01, 0, lcf};
Point(14) = {3.671000e+01, 5.997000e+01, 0, lcf};
Point(15) = {3.646000e+01, 5.954000e+01, 0, lcf};
Point(16) = {3.619000e+01, 5.912000e+01, 0, lcf};
Point(17) = {3.593000e+01, 5.870000e+01, 0, lcf};
Point(18) = {3.571000e+01, 5.825000e+01, 0, lcf};
Point(19) = {3.549000e+01, 5.780000e+01, 0, lcf};
Point(20) = {3.527000e+01, 5.735000e+01, 0, lcf};
Point(21) = {4.475000e+01, 6.466000e+01, 0, lcf};
Point(22) = {4.475000e+01, 6.414000e+01, 0, lcf};
Point(23) = {4.476000e+01, 6.361000e+01, 0, lcf};
Point(24) = {4.473000e+01, 6.309000e+01, 0, lcf};
Point(25) = {4.471000e+01, 6.256000e+01, 0, lcf};
Point(26) = {4.467000e+01, 6.203000e+01, 0, lcf};
Point(27) = {4.462000e+01, 6.151000e+01, 0, lcf};
Point(28) = {4.457000e+01, 6.099000e+01, 0, lcf};
Point(29) = {4.453000e+01, 6.046000e+01, 0, lcf};
Point(30) = {4.449000e+01, 5.994000e+01, 0, lcf};
Point(32) = {3.834000e+01, 6.495000e+01, 0, lcf};
Point(33) = {3.650000e+01, 6.421000e+01, 0, lcf};
Point(34) = {3.686000e+01, 6.454000e+01, 0, lcf};
Point(35) = {3.508000e+01, 6.062000e+01, 0, lcf};
Point(36) = {3.534000e+01, 6.021000e+01, 0, lcf};
Point(37) = {3.560000e+01, 5.980000e+01, 0, lcf};
Point(38) = {3.582000e+01, 5.935000e+01, 0, lcf};
Point(39) = {3.515000e+01, 5.934000e+01, 0, lcf};
Point(40) = {3.556000e+01, 5.907000e+01, 0, lcf};
Point(42) = {3.510000e+01, 5.823000e+01, 0, lcf};
Point(43) = {4.153000e+01, 6.196000e+01, 0, lcf};
Point(44) = {4.202000e+01, 6.187000e+01, 0, lcf};
Point(45) = {4.252000e+01, 6.178000e+01, 0, lcf};
Point(46) = {4.300000e+01, 6.168000e+01, 0, lcf};
Point(47) = {4.349000e+01, 6.159000e+01, 0, lcf};
Point(48) = {4.392000e+01, 6.132000e+01, 0, lcf};
Point(49) = {4.434000e+01, 6.106000e+01, 0, lcf};
Point(50) = {4.473000e+01, 6.074000e+01, 0, lcf};
Point(51) = {4.467000e+01, 6.290000e+01, 0, lcf};
Point(52) = {4.427000e+01, 6.258000e+01, 0, lcf};
Point(53) = {4.388000e+01, 6.226000e+01, 0, lcf};
Point(54) = {4.349000e+01, 6.193000e+01, 0, lcf};
Point(55) = {4.310000e+01, 6.161000e+01, 0, lcf};
Point(56) = {4.266000e+01, 6.135000e+01, 0, lcf};
Point(57) = {4.223000e+01, 6.109000e+01, 0, lcf};
Point(58) = {4.183000e+01, 6.078000e+01, 0, lcf};
Point(59) = {4.144000e+01, 6.047000e+01, 0, lcf};
Point(60) = {4.095000e+01, 6.035000e+01, 0, lcf};
Point(61) = {4.400000e+01, 6.143000e+01, 0, lcf};
Point(62) = {4.380000e+01, 6.101000e+01, 0, lcf};
Point(63) = {4.359000e+01, 6.059000e+01, 0, lcf};
Point(64) = {4.344000e+01, 6.015000e+01, 0, lcf};
Point(65) = {4.329000e+01, 5.970000e+01, 0, lcf};
Point(66) = {4.318000e+01, 5.925000e+01, 0, lcf};
Point(67) = {4.308000e+01, 5.879000e+01, 0, lcf};
Point(68) = {4.309000e+01, 5.832000e+01, 0, lcf};
Point(69) = {4.311000e+01, 5.786000e+01, 0, lcf};
Point(70) = {4.304000e+01, 5.740000e+01, 0, lcf};
Point(71) = {4.297000e+01, 5.693000e+01, 0, lcf};
Point(72) = {4.289000e+01, 5.647000e+01, 0, lcf};
Point(73) = {4.281000e+01, 5.602000e+01, 0, lcf};
Point(74) = {4.282000e+01, 5.555000e+01, 0, lcf};
Point(75) = {4.017000e+01, 5.917000e+01, 0, lcf};
Point(76) = {4.032000e+01, 5.871000e+01, 0, lcf};
Point(77) = {4.047000e+01, 5.826000e+01, 0, lcf};
Point(78) = {4.074000e+01, 5.786000e+01, 0, lcf};
Point(79) = {4.101000e+01, 5.747000e+01, 0, lcf};
Point(80) = {4.130000e+01, 5.709000e+01, 0, lcf};
Point(81) = {4.159000e+01, 5.671000e+01, 0, lcf};
Point(82) = {4.189000e+01, 5.633000e+01, 0, lcf};
Point(83) = {4.218000e+01, 5.595000e+01, 0, lcf};
Point(84) = {4.251000e+01, 5.561000e+01, 0, lcf};
Point(85) = {4.346000e+01, 6.030000e+01, 0, lcf};
Point(86) = {4.306000e+01, 5.990000e+01, 0, lcf};
Point(87) = {4.266000e+01, 5.951000e+01, 0, lcf};
Point(88) = {4.233000e+01, 5.906000e+01, 0, lcf};
Point(89) = {4.199000e+01, 5.861000e+01, 0, lcf};
Point(90) = {4.181000e+01, 5.807000e+01, 0, lcf};
Point(91) = {3.959000e+01, 6.180000e+01, 0, lcf};
Point(92) = {3.986000e+01, 6.136000e+01, 0, lcf};
Point(93) = {4.012000e+01, 6.092000e+01, 0, lcf};
Point(94) = {4.040000e+01, 6.049000e+01, 0, lcf};
Point(95) = {4.068000e+01, 6.006000e+01, 0, lcf};
Point(96) = {4.090000e+01, 5.961000e+01, 0, lcf};
Point(97) = {4.113000e+01, 5.915000e+01, 0, lcf};
Point(98) = {4.130000e+01, 5.867000e+01, 0, lcf};
Point(99) = {4.148000e+01, 5.819000e+01, 0, lcf};
Point(100) = {4.162000e+01, 5.770000e+01, 0, lcf};
Point(101) = {4.177000e+01, 5.721000e+01, 0, lcf};
Point(102) = {4.187000e+01, 5.671000e+01, 0, lcf};
Point(103) = {4.197000e+01, 5.620000e+01, 0, lcf};
Point(104) = {4.207000e+01, 5.570000e+01, 0, lcf};
Point(105) = {4.016000e+01, 5.923000e+01, 0, lcf};
Point(106) = {4.007000e+01, 5.877000e+01, 0, lcf};
Point(107) = {3.997000e+01, 5.831000e+01, 0, lcf};
Point(108) = {3.986000e+01, 5.786000e+01, 0, lcf};
Point(109) = {3.974000e+01, 5.741000e+01, 0, lcf};
Point(110) = {3.964000e+01, 5.695000e+01, 0, lcf};
Point(111) = {3.954000e+01, 5.649000e+01, 0, lcf};
Point(112) = {3.951000e+01, 5.603000e+01, 0, lcf};
Point(113) = {3.948000e+01, 5.556000e+01, 0, lcf};
Point(114) = {3.956000e+01, 5.511000e+01, 0, lcf};
Point(115) = {3.977000e+01, 5.761000e+01, 0, lcf};
Point(116) = {3.986000e+01, 5.707000e+01, 0, lcf};
Point(117) = {3.994000e+01, 5.654000e+01, 0, lcf};
Point(118) = {3.997000e+01, 5.599000e+01, 0, lcf};
Point(119) = {4.043000e+01, 6.003000e+01, 0, lcf};
Point(120) = {4.018000e+01, 5.961000e+01, 0, lcf};
Point(121) = {3.993000e+01, 5.920000e+01, 0, lcf};
Point(122) = {3.969000e+01, 5.878000e+01, 0, lcf};
Point(123) = {3.979000e+01, 5.838000e+01, 0, lcf};
Point(124) = {3.940000e+01, 5.814000e+01, 0, lcf};
Point(125) = {3.901000e+01, 5.790000e+01, 0, lcf};
Point(126) = {3.884000e+01, 5.744000e+01, 0, lcf};
Point(127) = {3.866000e+01, 5.697000e+01, 0, lcf};
Point(128) = {3.848000e+01, 5.651000e+01, 0, lcf};
Point(129) = {3.830000e+01, 5.605000e+01, 0, lcf};
Point(130) = {3.814000e+01, 5.558000e+01, 0, lcf};
Point(131) = {3.752000e+01, 6.130000e+01, 0, lcf};
Point(132) = {3.743000e+01, 6.080000e+01, 0, lcf};
Point(133) = {3.735000e+01, 6.031000e+01, 0, lcf};
Point(134) = {3.732000e+01, 5.981000e+01, 0, lcf};
Point(135) = {3.728000e+01, 5.931000e+01, 0, lcf};
Point(136) = {3.733000e+01, 5.880000e+01, 0, lcf};
Point(137) = {3.738000e+01, 5.830000e+01, 0, lcf};
Point(138) = {3.738000e+01, 5.780000e+01, 0, lcf};
Point(139) = {3.737000e+01, 5.730000e+01, 0, lcf};
Point(140) = {3.736000e+01, 5.679000e+01, 0, lcf};
Point(141) = {3.734000e+01, 5.629000e+01, 0, lcf};
Point(142) = {3.737000e+01, 5.579000e+01, 0, lcf};
Point(143) = {3.760000e+01, 6.078000e+01, 0, lcf};
Point(144) = {3.767000e+01, 6.023000e+01, 0, lcf};
Point(145) = {3.782000e+01, 5.970000e+01, 0, lcf};
Point(146) = {3.660000e+01, 6.248000e+01, 0, lcf};
Point(147) = {3.669000e+01, 6.201000e+01, 0, lcf};
Point(148) = {3.679000e+01, 6.154000e+01, 0, lcf};
Point(149) = {3.692000e+01, 6.108000e+01, 0, lcf};
Point(150) = {3.705000e+01, 6.062000e+01, 0, lcf};
Point(151) = {3.724000e+01, 6.017000e+01, 0, lcf};
Point(152) = {3.742000e+01, 5.973000e+01, 0, lcf};
Point(153) = {3.775000e+01, 5.941000e+01, 0, lcf};
Point(154) = {3.808000e+01, 5.909000e+01, 0, lcf};
Point(155) = {3.844000e+01, 5.877000e+01, 0, lcf};
Point(156) = {3.881000e+01, 5.845000e+01, 0, lcf};
Point(157) = {3.919000e+01, 5.816000e+01, 0, lcf};
Point(158) = {3.633000e+01, 6.037000e+01, 0, lcf};
Point(159) = {3.625000e+01, 5.989000e+01, 0, lcf};
Point(160) = {3.616000e+01, 5.941000e+01, 0, lcf};
Point(161) = {3.616000e+01, 5.892000e+01, 0, lcf};
Point(162) = {3.617000e+01, 5.843000e+01, 0, lcf};
Point(163) = {3.616000e+01, 5.794000e+01, 0, lcf};
Point(164) = {3.615000e+01, 5.745000e+01, 0, lcf};
Point(165) = {3.622000e+01, 5.696000e+01, 0, lcf};
Point(166) = {3.630000e+01, 5.648000e+01, 0, lcf};
Point(167) = {3.633000e+01, 5.599000e+01, 0, lcf};
Point(168) = {3.637000e+01, 5.550000e+01, 0, lcf};
Point(169) = {3.640000e+01, 5.501000e+01, 0, lcf};
Point(170) = {4.272000e+01, 5.482000e+01, 0, lcf};
Point(171) = {4.292000e+01, 5.528000e+01, 0, lcf};
Point(172) = {4.312000e+01, 5.573000e+01, 0, lcf};
Point(173) = {4.330000e+01, 5.620000e+01, 0, lcf};
Point(174) = {4.348000e+01, 5.666000e+01, 0, lcf};
Point(175) = {4.365000e+01, 5.713000e+01, 0, lcf};
Point(176) = {4.381000e+01, 5.759000e+01, 0, lcf};
Point(177) = {4.400000e+01, 5.805000e+01, 0, lcf};
Point(178) = {4.418000e+01, 5.851000e+01, 0, lcf};
Point(179) = {4.430000e+01, 5.899000e+01, 0, lcf};
Point(180) = {4.441000e+01, 5.948000e+01, 0, lcf};
Point(181) = {4.459000e+01, 5.994000e+01, 0, lcf};
Point(182) = {4.300000e+01, 6.466000e+01, 0, lcf};
Point(183) = {4.274000e+01, 6.422000e+01, 0, lcf};
Point(184) = {4.249000e+01, 6.379000e+01, 0, lcf};
Point(185) = {4.223000e+01, 6.336000e+01, 0, lcf};
Point(186) = {4.196000e+01, 6.293000e+01, 0, lcf};
Point(187) = {4.175000e+01, 6.248000e+01, 0, lcf};
Point(188) = {4.153000e+01, 6.202000e+01, 0, lcf};
Point(189) = {4.126000e+01, 6.160000e+01, 0, lcf};
Point(190) = {4.099000e+01, 6.117000e+01, 0, lcf};
Point(191) = {4.075000e+01, 6.073000e+01, 0, lcf};
Point(192) = {4.051000e+01, 6.029000e+01, 0, lcf};
Point(193) = {4.036000e+01, 5.981000e+01, 0, lcf};
Point(194) = {4.020000e+01, 5.933000e+01, 0, lcf};
Point(195) = {4.001000e+01, 5.886000e+01, 0, lcf};
Point(196) = {3.961000e+01, 5.794000e+01, 0, lcf};
Point(197) = {3.941000e+01, 5.748000e+01, 0, lcf};
Point(198) = {3.919000e+01, 5.702000e+01, 0, lcf};
Point(199) = {3.898000e+01, 5.657000e+01, 0, lcf};
Point(200) = {3.879000e+01, 5.610000e+01, 0, lcf};
Point(201) = {3.860000e+01, 5.563000e+01, 0, lcf};
Point(202) = {3.839000e+01, 5.518000e+01, 0, lcf};
Point(203) = {3.505000e+01, 5.690000e+01, 0, lcf};
Point(204) = {4.446000e+01, 5.941000e+01, 0, lcf};
Point(205) = {3.828000e+01, 6.442000e+01, 0, lcf};
Point(206) = {3.723000e+01, 6.488000e+01, 0, lcf};
Point(207) = {3.603000e+01, 5.891000e+01, 0, lcf};
Point(208) = {3.598000e+01, 5.880000e+01, 0, lcf};
Point(209) = {3.552000e+01, 5.793000e+01, 0, lcf};
Point(211) = {4.046000e+01, 6.024000e+01, 0, lcf};
Point(212) = {4.284000e+01, 5.508000e+01, 0, lcf};
Point(213) = {4.285000e+01, 5.526000e+01, 0, lcf};
Point(214) = {4.163000e+01, 5.754000e+01, 0, lcf};
Point(215) = {4.217000e+01, 5.520000e+01, 0, lcf};
Point(216) = {3.964000e+01, 5.466000e+01, 0, lcf};
Point(217) = {4.000000e+01, 5.545000e+01, 0, lcf};
Point(218) = {3.944000e+01, 5.837000e+01, 0, lcf};
Point(219) = {3.797000e+01, 5.510000e+01, 0, lcf};
Point(220) = {3.740000e+01, 5.529000e+01, 0, lcf};
Point(221) = {3.796000e+01, 5.916000e+01, 0, lcf};
Point(222) = {3.957000e+01, 5.787000e+01, 0, lcf};
Point(224) = {4.476000e+01, 6.040000e+01, 0, lcf};
Point(225) = {3.818000e+01, 5.472000e+01, 0, lcf};
Point(226) = {4.456156e+01, 6.087821e+01, 0, lcf};
Point(227) = {3.977897e+01, 5.755615e+01, 0, lcf};
Point(228) = {3.732860e+01, 5.995341e+01, 0, lcf};
Point(229) = {3.707435e+01, 6.056232e+01, 0, lcf};
Point(230) = {3.616000e+01, 5.907154e+01, 0, lcf};
Point(231) = {4.447309e+01, 5.964123e+01, 0, lcf};
Point(232) = {4.315142e+01, 6.165219e+01, 0, lcf};
Point(233) = {4.165050e+01, 5.760036e+01, 0, lcf};
Point(234) = {3.929825e+01, 5.807739e+01, 0, lcf};
Point(235) = {3.794394e+01, 5.922193e+01, 0, lcf};
// Boundary points
Point(31) = {3.840000e+01, 6.547000e+01, 0, lcf};
Point(41) = {3.468000e+01, 5.853000e+01, 0, lcf};
Point(210) = {4.511000e+01, 6.042000e+01, 0, lcf};
Point(223) = {3.642000e+01, 5.453000e+01, 0, lcf};
Point(236) = {3.468000e+01, 5.453000e+01, 0, lcf};
Point(237) = {4.511000e+01, 5.453000e+01, 0, lcf};
Point(238) = {4.511000e+01, 6.547000e+01, 0, lcf};
Point(239) = {3.468000e+01, 6.547000e+01, 0, lcf};

// Lines
Line(1) = {1, 2};
Line(2) = {3, 4};
Line(3) = {5, 6};
Line(4) = {7, 8};
Line(5) = {9, 10};
Line(6) = {11, 12};
Line(7) = {13, 14};
Line(8) = {15, 16};
Line(9) = {17, 18};
Line(10) = {19, 20};
Line(11) = {21, 22};
Line(12) = {23, 24};
Line(13) = {25, 26};
Line(14) = {27, 28};
Line(15) = {29, 30};
Line(16) = {31, 32};
Line(17) = {33, 34};
Line(18) = {35, 36};
Line(19) = {37, 38};
Line(20) = {39, 40};
Line(21) = {41, 42};
Line(22) = {43, 44};
Line(23) = {45, 46};
Line(24) = {47, 48};
Line(25) = {49, 226};
Line(26) = {226, 50};
Line(27) = {51, 52};
Line(28) = {53, 54};
Line(29) = {55, 56};
Line(30) = {57, 58};
Line(31) = {59, 60};
Line(32) = {61, 48};
Line(33) = {48, 62};
Line(34) = {63, 64};
Line(35) = {65, 66};
Line(36) = {67, 68};
Line(37) = {69, 70};
Line(38) = {71, 72};
Line(39) = {73, 74};
Line(40) = {75, 76};
Line(41) = {77, 78};
Line(42) = {79, 80};
Line(43) = {81, 82};
Line(44) = {83, 84};
Line(45) = {85, 86};
Line(46) = {87, 88};
Line(47) = {89, 90};
Line(48) = {91, 92};
Line(49) = {93, 94};
Line(50) = {95, 96};
Line(51) = {97, 98};
Line(52) = {99, 100};
Line(53) = {101, 102};
Line(54) = {103, 104};
Line(55) = {105, 106};
Line(56) = {107, 108};
Line(57) = {109, 110};
Line(58) = {111, 112};
Line(59) = {113, 114};
Line(60) = {115, 227};
Line(61) = {227, 116};
Line(62) = {117, 118};
Line(63) = {119, 120};
Line(64) = {121, 122};
Line(65) = {123, 124};
Line(66) = {125, 126};
Line(67) = {127, 128};
Line(68) = {129, 130};
Line(69) = {131, 11};
Line(70) = {11, 132};
Line(71) = {133, 228};
Line(72) = {228, 134};
Line(73) = {135, 136};
Line(74) = {137, 138};
Line(75) = {139, 140};
Line(76) = {141, 142};
Line(77) = {11, 143};
Line(78) = {144, 145};
Line(79) = {146, 147};
Line(80) = {148, 149};
Line(81) = {150, 229};
Line(82) = {229, 151};
Line(83) = {152, 153};
Line(84) = {154, 155};
Line(85) = {156, 157};
Line(86) = {158, 159};
Line(87) = {160, 230};
Line(88) = {230, 161};
Line(89) = {162, 163};
Line(90) = {164, 165};
Line(91) = {166, 167};
Line(92) = {168, 169};
Line(93) = {170, 212};
Line(94) = {212, 171};
Line(95) = {172, 173};
Line(96) = {174, 175};
Line(97) = {176, 177};
Line(98) = {178, 179};
Line(99) = {180, 231};
Line(100) = {231, 181};
Line(101) = {182, 183};
Line(102) = {184, 185};
Line(103) = {186, 187};
Line(104) = {188, 189};
Line(105) = {190, 191};
Line(106) = {192, 119};
Line(107) = {119, 193};
Line(108) = {194, 195};
Line(109) = {123, 196};
Line(110) = {197, 198};
Line(111) = {199, 200};
Line(112) = {201, 202};
Line(113) = {2, 3};
Line(114) = {4, 5};
Line(115) = {6, 7};
Line(116) = {8, 9};
Line(117) = {10, 11};
Line(118) = {12, 229};
Line(119) = {229, 13};
Line(120) = {14, 15};
Line(121) = {16, 230};
Line(122) = {230, 17};
Line(123) = {18, 19};
Line(124) = {20, 203};
Line(125) = {22, 23};
Line(126) = {24, 25};
Line(127) = {26, 27};
Line(128) = {28, 226};
Line(129) = {226, 29};
Line(130) = {30, 231};
Line(131) = {231, 204};
Line(132) = {32, 205};
Line(133) = {34, 206};
Line(134) = {36, 37};
Line(135) = {38, 207};
Line(136) = {40, 208};
Line(137) = {42, 209};
Line(138) = {44, 45};
Line(139) = {46, 232};
Line(140) = {232, 47};
Line(141) = {48, 49};
Line(142) = {50, 210};
Line(143) = {52, 53};
Line(144) = {54, 232};
Line(145) = {232, 55};
Line(146) = {56, 57};
Line(147) = {58, 59};
Line(148) = {60, 192};
Line(149) = {192, 211};
Line(150) = {62, 63};
Line(151) = {64, 65};
Line(152) = {66, 67};
Line(153) = {68, 69};
Line(154) = {70, 71};
Line(155) = {72, 73};
Line(156) = {74, 213};
Line(157) = {213, 212};
Line(158) = {76, 77};
Line(159) = {78, 79};
Line(160) = {80, 81};
Line(161) = {82, 103};
Line(162) = {103, 83};
Line(163) = {84, 213};
Line(164) = {86, 87};
Line(165) = {88, 89};
Line(166) = {90, 233};
Line(167) = {233, 214};
Line(168) = {92, 93};
Line(169) = {94, 192};
Line(170) = {192, 95};
Line(171) = {96, 97};
Line(172) = {98, 99};
Line(173) = {100, 233};
Line(174) = {233, 101};
Line(175) = {102, 103};
Line(176) = {104, 215};
Line(177) = {106, 107};
Line(178) = {108, 227};
Line(179) = {227, 109};
Line(180) = {110, 111};
Line(181) = {112, 113};
Line(182) = {114, 216};
Line(183) = {116, 117};
Line(184) = {118, 217};
Line(185) = {120, 121};
Line(186) = {122, 218};
Line(187) = {124, 234};
Line(188) = {234, 125};
Line(189) = {126, 127};
Line(190) = {128, 129};
Line(191) = {130, 219};
Line(192) = {132, 133};
Line(193) = {134, 135};
Line(194) = {136, 137};
Line(195) = {138, 139};
Line(196) = {140, 141};
Line(197) = {142, 220};
Line(198) = {143, 144};
Line(199) = {145, 235};
Line(200) = {235, 221};
Line(201) = {147, 148};
Line(202) = {149, 150};
Line(203) = {151, 228};
Line(204) = {228, 152};
Line(205) = {153, 235};
Line(206) = {235, 154};
Line(207) = {155, 156};
Line(208) = {157, 234};
Line(209) = {234, 222};
Line(210) = {159, 160};
Line(211) = {161, 162};
Line(212) = {163, 164};
Line(213) = {165, 166};
Line(214) = {167, 168};
Line(215) = {169, 223};
Line(216) = {171, 172};
Line(217) = {173, 174};
Line(218) = {175, 176};
Line(219) = {177, 178};
Line(220) = {179, 180};
Line(221) = {181, 224};
Line(222) = {183, 184};
Line(223) = {185, 186};
Line(224) = {187, 188};
Line(225) = {189, 190};
Line(226) = {191, 192};
Line(227) = {193, 194};
Line(228) = {195, 123};
Line(229) = {196, 197};
Line(230) = {198, 199};
Line(231) = {200, 201};
Line(232) = {202, 225};
// Boundary lines
Line(233) = {236, 223};
Line(234) = {223, 237};
Line(235) = {237, 210};
Line(236) = {210, 238};
Line(237) = {238, 31};
Line(238) = {31, 239};
Line(239) = {239, 41};
Line(240) = {41, 236};

Line Loop(1) = {233, 234, 235, 236, 237, 238, 239, 240};
Plane Surface(1) = {1};

// Including fractures in triangulation
Line{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 
161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 
201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232} In Surface {1};

// Physical entities

Physical Line(OPEN_FRACTURES) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 
81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 
161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 
201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232};

Physical Surface(MATRIX) = {1};
