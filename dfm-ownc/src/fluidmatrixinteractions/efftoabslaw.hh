// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 * \brief This material law takes a material law defined for effective
 *        saturations and converts it to a material law defined on
 *        absolute saturations.
 */
#ifndef DUMUX_EFF_TO_ABS_LAW_HH
#define DUMUX_EFF_TO_ABS_LAW_HH

#include "efftoabslawparams.hh"

namespace Dumux
{
/*!
 * \ingroup Fluidmatrixinteractions
 *
 * \brief This material law takes a material law defined for effective
 *        saturations and converts it to a material law defined on absolute
 *        saturations.
 *
 *        The idea: "material laws" (like VanGenuchten or BrooksCorey) are defined for effective saturations.
 *        The numeric calculations however are performed with absolute saturations. The EffToAbsLaw class gets
 *        the "material laws" actually used as well as the corresponding parameter container as template arguments.
 *
 *        Subsequently, the desired function (pc, sw... ) of the actually used "material laws" are called but with the
 *        saturations already converted from absolute to effective.
 *
 *        This approach makes sure that in the "material laws" only effective saturations are considered, which makes sense,
 *        as these laws only deal with effective saturations. This also allows for changing the calculation of the effective
 *        saturations easily, as this is subject of discussion / may be problem specific.
 *
 *        Additionally, handing over effective saturations to the "material laws" in stead of them calculating effective
 *        saturations prevents accidently "converting twice".
 *
 *        This boils down to:
 *        - the actual material laws (linear, VanGenuchten...) do not need to deal with any kind of conversion
 *        - the definition of the material law in the spatial parameters is not really intuitive, but using it is:
 *          Hand in values, get back values, do not deal with conversion.
 */
template <class EffLawT, class AbsParamsT = EffToAbsLawParams<typename EffLawT::Params> >
class EffToAbsLaw
{
    using EffLaw = EffLawT;

public:
    using Params = AbsParamsT;
    using Scalar = typename EffLaw::Scalar;

    /*!
     * \brief The capillary pressure-saturation curve.
     * \param sw Absolute saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$. It is converted to effective saturation
     *                  and then handed over to the material law actually used for calculation.
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
	 * \param X  = molarFracLSW
     * \return          Capillary pressure calculated by specific constitutive relation
     *                  (EffLaw e.g. Brooks & Corey, van Genuchten, linear...)
     */
    static Scalar pc(const Params &params, Scalar sw, Scalar X)
    {
    	//Scalar X = 0;

		Scalar SweL = swToSweBrine(params, sw);		// Water effective saturation for the case of no LSW (pure brine)
		Scalar pcL = EffLaw::pc(params, SweL);		// The same capillar pressure curve for both brine and LSW

		Scalar SweR = swToSweLSW(params, sw);		// Water effective saturation for the case pure LSW (no brine)
		Scalar pcR = EffLaw::pc(params, SweR);		// Corresponding cap pressure value

		Scalar pc = (1 - X) * pcL + X * pcR;

		return pc;


    }

    /*!
     * \brief The relative permeability for the wetting phase.
     *
     * \param sw Absolute saturation of the wetting phase \f$\mathrm{[\overline{S}_w]}\f$. It is converted to effective saturation
     *                  and then handed over to the material law actually used for calculation.
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     * \return Relative permeability of the wetting phase calculated as implied by
     *                  EffLaw e.g. Brooks & Corey, van Genuchten, linear... .
     */
	static Scalar krw(const Params &params, Scalar sw, Scalar X)
    {
		Scalar SweL = swToSweBrine(params, sw);			// Water effective saturation for the case of no LSW (pure brine)
		Scalar krwL = EffLaw::krwBrine(params, SweL);	// Corresponding Brooks-Corey water rel perm

		Scalar SweR = swToSweLSW(params, sw);			// Water effective saturation for the case pure LSW (no brine)
		Scalar krwR = EffLaw::krwLSW(params, SweR);		// Corresponding Brooks-Corey water rel perm

		Scalar krw = (1 - X) * krwL + X * krwR;

		return krw;
	}


    /*!
     * \brief The relative permeability for the non-wetting phase.
     *
     * \param sw Absolute saturation of the wetting phase \f$\mathrm{[{S}_w]}\f$. It is converted to effective saturation
     *                  and then handed over to the material law actually used for calculation.
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     * \return          Relative permeability of the non-wetting phase calculated as implied by
     *                  EffLaw e.g. Brooks & Corey, van Genuchten, linear... .
     */
	static Scalar krn(const Params &params, Scalar sw, Scalar X)
    {
        //return EffLaw::krn(params, swToSwe(params, sw), X);

		Scalar SweL = swToSweBrine(params, sw);			// Water effective saturation for the case of no LSW (pure brine)
		Scalar krnL = EffLaw::krnBrine(params, SweL);	// Corresponding Brooks-Corey water rel perm

		Scalar SweR = swToSweLSW(params, sw);			// Water effective saturation for the case pure LSW (no brine)
		Scalar krnR = EffLaw::krnLSW(params, SweR);		// Corresponding Brooks-Corey water rel perm

		Scalar krn = (1 - X) * krnL + X * krnR;

		return krn;

	}


    /*!
     * \brief Convert an absolute wetting saturation to an effective one.
     *
     * \param sw Absolute saturation of the wetting phase \f$\mathrm{[{S}_w]}\f$.
     * \param params A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen,
     *                  and then the params container is constructed accordingly. Afterwards the values are set there, too.
     * \return Effective saturation of the wetting phase.
     */
    static Scalar swToSweBrine(const Params &params, Scalar sw)
    {
        return (sw - params.swr())/(1. - params.swr() - params.snr());
    }


    static Scalar swToSweLSW(const Params &params, Scalar sw)
    {
        return (sw - params.LSWswr())/(1. - params.LSWswr() - params.LSWsnr());
    }


};
}

#endif
