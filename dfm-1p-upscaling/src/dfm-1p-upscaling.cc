// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the exercise on two-phase flow in fractured porous media.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

// include the headers of the two sub-problems
// i.e. the problems for fractures and matrix
#include "matrixproblem.hh"
#include "fractureproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/traits.hh>

// use the local cell-centered assembler
//#include <dumux/multidomain/fvassembler.hh>
#include "model/fvassembler.hh"

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

#include<dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

// Define some types for this test  so that we can set them as properties below and
// reuse them again in the main function with only one single definition of them here
using MatrixTypeTag = TTAG(MatrixProblemTypeTag);
using FractureTypeTag = TTAG(FractureProblemTypeTag);
using MatrixFVGridGeometry = typename GET_PROP_TYPE(MatrixTypeTag, FVGridGeometry);
using FractureFVGridGeometry = typename GET_PROP_TYPE(FractureTypeTag, FVGridGeometry);
using TheMultiDomainTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
using TheCouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
using TheCouplingManager = Dumux::FacetCouplingManager<TheMultiDomainTraits, TheCouplingMapper>;

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {
SET_TYPE_PROP(MatrixProblemTypeTag, CouplingManager, TheCouplingManager);
SET_TYPE_PROP(FractureProblemTypeTag, CouplingManager, TheCouplingManager);
} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::FakeMPIHelper::instance(argc, argv);
    //const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    if (mpiHelper.isFake) {
        std::cout << "\nSerial run (no MPI)" << std::endl;
    }
    else {
       std::cout << "\nMPI run" << std::endl;
    }

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);


    // initialize parameter tree
    Parameters::init(argc, argv);

    // We use the grid manager from the facet coupling framework (see alias above main)
    // This requires the grids used to be passed as template arguments, where
    // they are assumed to be ordered in descending grid dimension. Thus,
    // we pass the matrix grid as first and the fracture grid as second argument.
    using MatrixGrid = typename GET_PROP_TYPE(MatrixTypeTag, Grid);
    using FractureGrid = typename GET_PROP_TYPE(FractureTypeTag, Grid);
    using GridManager = Dumux::FacetCouplingGridManager<MatrixGrid, FractureGrid>;
    GridManager gridManager;

    // try to create a grid (from the grid file in the input file)
    // Init() creates the grid from the grid file specified in the input file.
    // This works with a single grid file in which in addition to the matrix
    // (d-dimensional) elements also the (d-1)-dimensional elements are specified
    // which are interpreted as the fracture elements. See the .geo file in the
    // grids folder on how to create such grids using gmsh. Note that currently
    // only the Gmsh mesh format (.msh) is supported!
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views (get them from grid manager)
    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);
    matrixFvGridGeometry->update();
    fractureFvGridGeometry->update();

    // shorthands for some types
    using Scalar = typename GET_PROP_TYPE(MatrixTypeTag, Scalar);
    using MatrixGridView = typename MatrixFVGridGeometry::GridView;
    using Element = typename MatrixGridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using MatrixGridVariables = typename GET_PROP_TYPE(MatrixTypeTag, GridVariables);
    using FractureGridVariables = typename GET_PROP_TYPE(FractureTypeTag, GridVariables);
    using FluidSystem = typename GET_PROP_TYPE(MatrixTypeTag, FluidSystem); // NA: assume the same fluid in matrix and in fractures

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = typename GET_PROP_TYPE(MatrixTypeTag, Problem);
    using FractureProblem = typename GET_PROP_TYPE(FractureTypeTag, Problem);

    // pass the model parameter group to the spatial params so that they obtain the right
    // values from the input file since we use groups for matrix and fracture
    // We also want to use domain markers in this exercise. For this reason, we also pass
    // the grid data object from the grid manager to them, so that they have access to the
    // domain markers that were specified in the grid file.
    auto matrixGridData = gridManager.getGridData()->template getSubDomainGridData<matrixGridId>();
    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, matrixGridData, "Matrix");

    // extract domain height from the matrix problem and pass to fracture problem (needed for right initial pressure distribution)
    auto fractureGridData = gridManager.getGridData()->template getSubDomainGridData<fractureGridId>();
    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, fractureGridData, "Fracture");

    // the solution vector
    using SolutionVector = typename TheMultiDomainTraits::SolutionVector;
    SolutionVector x, xOld;

    // The domain ids within the multi-domain framework.
    // They do not necessarily have to be the same as the grid ids
    // in case you have more subdomains involved. We domain ids
    // correspond to the order of the type tags passed to the multidomain
    // traits (see definition of the traits class at the beginning of this file)
    static const auto matrixDomainId = typename TheMultiDomainTraits::template DomainIdx<0>();
    static const auto fractureDomainId = typename TheMultiDomainTraits::template DomainIdx<1>();

    // instantiate the class holding the coupling maps between the domains
    // this needs the information on embeddings (connectivity between matrix
    // and fracture domain). This information is extracted directly from the
    // grid during file read and can therefore be obtained from the grid manager.
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Solve the subproblems for the flow in horizontal direction
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "------------------------------------------------------------" << std::endl;
	std::cout << "Solving the flow problem in horizontal direction..\n" << std::endl;

    int flowDirection = 0;
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix", flowDirection);
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture", flowDirection);

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());

	matrixProblem->applyInitialSolution(x[matrixDomainId]);
	fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // the grid variables
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId], xOld[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId], xOld[fractureDomainId]);

    // get some time loop parameters
    auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<double>("TimeLoop.DtInitial");
	
    // instantiate time loop
    auto timeLoop = std::make_shared< TimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    //using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::numeric, /*implicit?*/true>; // DiffMethod::numeric, /*implicit?*/true>;  DiffMethod::analytic
    using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::analytic, /*implicit?*/true>; // DiffMethod::numeric, /*implicit?*/true>;  DiffMethod::analytic
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop);

    // the linear solver
    //using LinearSolver = ILU0BiCGSTABBackend;
    //using LinearSolver = BlockDiagILU0BiCGSTABSolver;   // test to avoid linear solver crash (no success...)
    //using LinearSolver = BlockDiagAMGBiCGSTABSolver;   // test to avoid linear solver crash (really slow, and crashes even when the others pass well..)
    using LinearSolver = UMFPackBackend;  
    //using LinearSolver = EigenBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

	// set the bounding box for the fractures using the dimensions of the matrix domain
	fractureProblem->setBoundingBox(matrixDomainId);
	// array of shape factors for last iterations
	std::vector<Scalar> lastSF;

    // Keep the original matrix permeability from the position (0,0), and change the used permeability
	// to a small value in order to get correct fracture upscaled permeabilities
	GlobalPosition pos(0);
    Scalar matrixPermeability =  matrixProblem->spatialParams().permeabilityAtPos(pos);
    Scalar fac = 1e-6;
    std::cout << "\nSetting matrix permeability to " << matrixPermeability * fac << "\n" << std::endl;
    matrixProblem->spatialParams().setPermeability(matrixPermeability * fac);


    // solve one time step
    timeLoop->start();

    // set previous solution for storage evaluations
    assembler->setPreviousSolution(xOld);

    // solve the non-linear system with time step control
    newtonSolver->solve(x, *timeLoop);

	//constexpr int mdimWorld = MatrixFVGridGeometry::GridView::dimensionworld;
    GlobalPosition averVel0, averPGrad0;
	fractureProblem->getAveragedSolution(averVel0, averPGrad0,
									 x[fractureDomainId], *fractureGridVariables);

    GlobalPosition avermPGrad0;
	matrixProblem->getAveragedSolution(avermPGrad0,
									 x[matrixDomainId], *matrixGridVariables, *assembler,
									  matrixDomainId);


	averPGrad0 += avermPGrad0;

	Scalar fracArea = fractureProblem->getArea();
	Scalar matrixArea = matrixProblem->getArea();

    // Divide by matrixArea only because of the low-dim representation of fractures (matrixArea is not affected by the area of fractures)
    //averPGrad0 /= matrixArea;  
    averPGrad0 /= (fracArea + matrixArea);      
	averVel0 /= matrixArea;

	//averPGrad0 /= (fractureProblem->getArea() + matrixProblem->getArea());
	//averVel0 /= (fractureProblem->getArea() + matrixProblem->getArea());


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Solve the subproblems for the flow in vertical direction
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "------------------------------------------------------------" << std::endl;
	std::cout << "Solving the flow problem in vertical direction..\n" << std::endl;


    flowDirection = 1;
    matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix", flowDirection);
    fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture", flowDirection);

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());

	matrixProblem->applyInitialSolution(x[matrixDomainId]);
	fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // the coupling manager (needs the coupling mapper)
    couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // the grid variables
    matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId], xOld[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId], xOld[fractureDomainId]);

    // instantiate time loop
    timeLoop = std::make_shared< TimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop);

    // the non-linear solver
    newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

	// set the bounding box for the fractures using the dimensions of the matrix domain
	fractureProblem->setBoundingBox(matrixDomainId);

    // solve one time step
    timeLoop->start();

    // set previous solution for storage evaluations
    assembler->setPreviousSolution(xOld);

    // solve the non-linear system with time step control
    newtonSolver->solve(x, *timeLoop);

    // get the velocities and pressure gradients, averaged over fracture segments
	//constexpr int mdimWorld = MatrixFVGridGeometry::GridView::dimensionworld;
    GlobalPosition averVel1, averPGrad1;
	fractureProblem->getAveragedSolution(averVel1, averPGrad1,
									 x[fractureDomainId], *fractureGridVariables);

    GlobalPosition avermPGrad1;
	matrixProblem->getAveragedSolution(avermPGrad1,
									 x[matrixDomainId], *matrixGridVariables, *assembler,
									  matrixDomainId);

	averPGrad1 += avermPGrad1;

    // Divide by matrixArea only because of the low-dim representation of fractures (matrixArea is not affected by the area of fractures)
    averPGrad1 /= (fracArea + matrixArea);  
	averVel1 /= matrixArea;

	//averPGrad1 /= (fracArea + matrixArea);
	//averVel1 /= (fracArea + matrixArea);


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculating the upscaled permeabilities
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "------------------------------------------------------------" << std::endl;
	std::cout << "Calculating the upscaled permeabilities..\n" << std::endl;

    std::cout << "Restoring matrix permeability to " << matrixPermeability << "\n" << std::endl;
    matrixProblem->spatialParams().setPermeability(matrixPermeability);

	Scalar mu = FluidSystem::viscosity(0., 0.);

	Scalar kxx, kxy, kyx, kyy;

	// Solving the linear system for the upscaled permeabilities
	bool enforceSymmetry = true;
	if (enforceSymmetry)
	{
	    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > Matrix;
	    Matrix m(4, 4, 4, 0.1, Matrix::implicit);	// Create a 4x4 matrix with expected 4 entries per row

	    Scalar pscale = (averPGrad0[0] + averPGrad1[1]) * (averPGrad0[0] + averPGrad1[1]);

	    m.entry(0,0) = averPGrad0[0] * averPGrad0[0] + averPGrad1[0] * averPGrad1[0];
	    m.entry(0,1) = averPGrad0[1] * averPGrad0[0] + averPGrad1[1] * averPGrad1[0];

	    m.entry(1,0) = averPGrad0[1] * averPGrad0[0] + averPGrad1[1] * averPGrad1[0];
	    m.entry(1,1) = averPGrad0[1] * averPGrad0[1] + averPGrad1[1] * averPGrad1[1] + pscale;
	    m.entry(1,2) = -pscale;

	    m.entry(2,1) = -pscale;
	    m.entry(2,2) = averPGrad0[0] * averPGrad0[0] + averPGrad1[0] * averPGrad1[0] + pscale;
	    m.entry(2,3) = averPGrad0[1] * averPGrad0[0] + averPGrad1[1] * averPGrad1[0];

	    m.entry(3,2) = averPGrad0[1] * averPGrad0[0] + averPGrad1[1] * averPGrad1[0];
	    m.entry(3,3) = averPGrad0[1] * averPGrad0[1] + averPGrad1[1] * averPGrad1[1];

	    Dune::BlockVector<Dune::FieldVector<Scalar, 1> > k, rhs;
	    k.resize(4);
	    rhs.resize(4);

	    rhs[0] = -mu*(averVel0[0] * averPGrad0[0] + averVel1[0] * averPGrad1[0]);
	    rhs[1] = -mu*(averVel0[0] * averPGrad0[1] + averVel1[0] * averPGrad1[1]);
	    rhs[2] = -mu*(averVel0[1] * averPGrad0[0] + averVel1[1] * averPGrad1[0]);
	    rhs[3] = -mu*(averVel0[1] * averPGrad0[1] + averVel1[1] * averPGrad1[1]);

	    linearSolver->solve(m, k, rhs);

	    kxx = k[0]; kxy = k[1]; kyx = k[2]; kyy = k[3];

	}
	else
	{
		Scalar det = averPGrad0[0] * averPGrad1[1] - averPGrad0[1] * averPGrad1[0];
		if (fabs(det) < 1e-6)
		{
			std::cout << "Badly conditioned pressure gradient matrix, det = " << det << std::endl;
		}

		kxx = -mu/det * (  averPGrad1[1] * averVel0[0] - averPGrad0[1] * averVel1[0]);
		kxy = -mu/det * (- averPGrad1[0] * averVel0[0] + averPGrad0[0] * averVel1[0]);

		kyx = -mu/det * (  averPGrad1[1] * averVel0[1] - averPGrad0[1] * averVel1[1]);
		kyy = -mu/det * (- averPGrad1[0] * averVel0[1] + averPGrad0[0] * averVel1[1]);
	}

	Scalar Kf = getParam<Scalar>("Fracture.SpatialParams.Permeability");
	Scalar mD = 9.869232667160130e-16;
	std::cout << "Fracture permeability (in mD):" << Kf/mD << std::endl;
	std::cout << "Upscaled fracture permeability (in mD):" << std::endl;
	std::cout << kxx/mD << "  " << kxy/mD << std::endl;
	std::cout << kyx/mD << "  " << kyy/mD << std::endl;

    // Save the permeabilities to a csv file
    std::string filename = getParam<std::string>("Grid.File");
    //std::string filename( "results.csv");
    filename = filename.substr(0, filename.find("."));
    std::string fname_results =  filename + "_perm.csv";
    std::ofstream file( fname_results.c_str() );
    if( file )
    {
        const char *header = "Kxx (m2),"
							 "Kxy (m2),"
                             "Kyx (m2),"
        					 "Kyy (m2)";
        file << header << std::endl;
    }
    else
    {
      std::cerr << "ERROR: couldn't open file `" << fname_results << "'" << std::endl;
    }

    // Upscaled fracture permeabilities
	file <<
	kxx << "," <<
	kxy << "," <<
    kyx << "," <<
	kyy <<
    std::endl;

	// Upscaled matrix permeabilities
	Scalar Km = getParam<Scalar>("Matrix.SpatialParams.Permeability");
	//kxx = matrixArea / (fracArea + matrixArea) * Km;
    kxx = (matrixArea - fracArea) / matrixArea * Km;    // matrixArea represents the total element area because of the low-dim representation of fractures (matrixArea is not affected by the area of fractures)
	kxy = 0;
	kyx = 0;
	kyy = kxx;

	file <<
	kxx << "," <<
	kxy << "," <<
    kyx << "," <<
	kyy <<
    std::endl;

    file.close();

    // Save the porosities to a csv file
    filename = getParam<std::string>("Grid.File");
    filename = filename.substr(0, filename.find("."));
    fname_results =  filename + "_poro.csv";
    file.open( fname_results.c_str() );
    if( file )
    {
        const char *header = "phif_eff,"
							 "phim_eff";
        file << header << std::endl;
    }
    else
    {
      std::cerr << "ERROR: couldn't open file `" << fname_results << "'" << std::endl;
    }

    // Calculate the upscaled porosities
    Scalar phif = getParam<Scalar>("Fracture.SpatialParams.Porosity");
    Scalar phim = getParam<Scalar>("Matrix.SpatialParams.Porosity");
    //phif = fracArea / (fracArea + matrixArea) * phif;   
    //phim = matrixArea / (fracArea + matrixArea) * phim;    
    phif = fracArea / matrixArea * phif;    // matrixArea represents the total element area because of the low-dim representation of fractures (matrixArea is not affected by the area of fractures)	
    phim = (matrixArea - fracArea) / matrixArea * phim;

	file <<
	phif << "," <<
	phim <<
    std::endl;

    file.close();

    // output some Newton statistics
    newtonSolver->report();

    // report time loop statistics
    timeLoop->finalize();

    // print dumux message to say goodbye
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
	
 