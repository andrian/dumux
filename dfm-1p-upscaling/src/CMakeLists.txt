dune_symlink_to_source_files(FILES "grids" "flowproblems.input")

dune_add_test(NAME dfm-1p-upscaling
	          CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )"
              SOURCES dfm-1p-upscaling.cc
              COMMAND ./dfm-1p-upscaling
              CMD_ARGS flowproblems.input)

