// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the fracture domain for the single-phase compressible flow in fractured porous media.
 */
#ifndef DUMUX_FRACTURES_FRACTURE_PROBLEM_HH
#define DUMUX_FRACTURES_FRACTURE_PROBLEM_HH

// we use foam grid for the discretization of the fracture domain
// as this grid manager is able to represent network/surface grids
#include <dune/foamgrid/foamgrid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// we use a cell-centered finite volume scheme with tpfa here
#include <dumux/discretization/cellcentered/tpfa/properties.hh>

// replace the standard immisible localresidual for one phase with the one with an analytic jacobian.
#include "model/compressiblelocalresidual.hh"

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

// Slightly compressible single phase liquid with parameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "components/slightlycompressibleliquid.hh"

// the spatial parameters (permeabilities, material parameters etc.)
#include "fracturespatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class FractureSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the fracture sub-problem
NEW_TYPE_TAG(FractureProblemTypeTag, INHERITS_FROM(OneP, CCTpfaModel));

// Set the grid type
SET_TYPE_PROP(FractureProblemTypeTag, Grid, Dune::FoamGrid<1, 2>);

// Set the problem type
SET_TYPE_PROP(FractureProblemTypeTag, Problem, FractureSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(FractureProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);
// set the OneP Incompressible local residual for the OnePIncompressible type tag. This provides an analytic jacobian to be used for the analytic solution. Change that by setting:
SET_TYPE_PROP(FractureProblemTypeTag, LocalResidual, OnePCompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(FractureProblemTypeTag,
              SpatialParams,
              FractureSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);


// Set the fluid system
SET_PROP(FractureProblemTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::SlightlyCompressibleComponent<Scalar> >;
};

} // end namespace Properties


/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the fracture domain 
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
	using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;	
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "",
					 const int flowDirection = 0)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, initialPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialPressure"))
	, inletBoundaryPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InletBoundaryPressure"))
	, outletBoundaryPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.OutletBoundaryPressure"))
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))	
	{

    	dir_ = flowDirection;

		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();
	}

    //! Initialize the bounding box from the matrix sub-problem
    // (otherwise this->fvGridGeometry().bBoxMin() gives the bounding box for the fractures)
    template<class MatrixIdType>
    void setBoundingBox(const MatrixIdType& matrixDomainId)
    {
    	auto& matrixProblem = couplingManagerPtr_->template problem<matrixDomainId>();
		xmin = matrixProblem.fvGridGeometry().bBoxMin()[dir_];
        xmax = matrixProblem.fvGridGeometry().bBoxMax()[dir_];
    }


    //! Calculate the fracture average pressure
    void getResults(Scalar& averagePressure, Scalar& boundaryFlux,
    			  const SolutionVector& curSol, const GridVariables& gridVariables)
    {

    	averagePressure = 0.;
    	boundaryFlux = 0.;

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            //fvGeometry.bindElement(element);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	// Following the explanation by Dennis

            //int eIdx = this->fvGridGeometry().elementMapper().index(element);
            //PrimaryVariables values = x[eIdx];

            VolumeVariables volVars;

            for (auto&& scv : scvs(fvGeometry))
            {
                volVars = elemVolVars[scv];
                averagePressure += volVars.pressure();
            }

            // Loop through the faces

            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {

            	// Calculate the boundary flux
				if (scvf.boundary()) {
                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar flux = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
                        boundaryFlux += flux;	// > 0 for outflow
					}
					else if (bcTypes.hasOnlyNeumann()) {
						Scalar flux = this->neumannAtPos(scvf.center());
						flux *= scvf.area() * extrusionFactor / volVars.density(FluidSystem::phase0Idx);   // Convert to volumetric flux
						boundaryFlux += flux;
					}
					else {
                        DUNE_THROW(Dune::NotImplemented, "fractureproblem.hh: boundary condition not defined..");
                    }
				}
			}

        }

        // Calculate the average pressure in the fracture domain
		auto meshSize = this->fvGridGeometry().gridView().size(0);
		averagePressure /= meshSize;

    }


    //! Calculate the velocities and pressure gradients, averaged over fracture segments (a la Hamid)
    void getAveragedSolution(GlobalPosition& averVel, GlobalPosition& averPGrad,
    			  const SolutionVector& curSol, const GridVariables& gridVariables)
    {
    	averVel = 0;
    	averPGrad = 0;
    	//std::fill(averPGrad.begin(), averPGrad.end(), 0.);

    	// Get the number of vertices of the 0th fracture element (should be the same for all fracture elements)
        auto fvGeometry = localView(this->fvGridGeometry());
        auto e = fvGeometry.fvGridGeometry().element(0);
        int Nv = e.geometry().corners();
        assert(Nv == 2);

    	Scalar p;							// Segment pressure
    	std::vector<Scalar> Pi;				// Segment's endpoints pressures
    	std::vector<Scalar> Q;				// Segment's endpoints volumetric fluxes
    	std::vector<GlobalPosition> vcoord;	// Coordinates of the endpoints
    	Pi.resize(Nv);
    	Q.resize(Nv);
    	vcoord.resize(Nv);

    	Scalar mu = FluidSystem::viscosity(0., 0.);
    	GlobalPosition uv;		// Unity vector in the segment's direction

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            fvGeometry.bind(element);
            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);
            VolumeVariables volVars;

            GlobalPosition c;
            Scalar length;
            Scalar Kf, aperture;

            for (auto&& scv : scvs(fvGeometry))
            {
                volVars = elemVolVars[scv];
                p = volVars.pressure();
                c = scv.center();
                length = scv.volume();
                Kf = this->spatialParams().permeabilityAtPos(c);
                aperture = volVars.extrusionFactor();
            }

            // Loop through the faces

            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            int n = 0;
            for (auto&& scvf : scvfs(fvGeometry)) {

                FluxVariables fluxVars;
                fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                if (scvf.boundary())
                {
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                    	Q[n] = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
					}
					else if (bcTypes.hasOnlyNeumann()) {
						Q[n] = this->neumannAtPos(scvf.center());
						Q[n] *= scvf.area() * aperture / volVars.density(FluidSystem::phase0Idx);   // Convert to volumetric flux
	                	if (Q[n] != 0)
	                		DUNE_THROW(Dune::NotImplemented, "fractureproblem.hh: Non-zero Neumann boundary condition not implemented..");
					}
					else {
                        DUNE_THROW(Dune::NotImplemented, "fractureproblem.hh: boundary condition not defined..");
                    }
                }
                else
                {
//                    // Get the inside and outside volume variables
//                    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
//                    const auto& insideVolVars = elemVolVars[insideScv];
//                    const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];
//
//                    Scalar p0 = insideVolVars.pressure();
//                    Scalar p1 = outsideVolVars.pressure();

                    Q[n] = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
                }

                vcoord[n] = scvf.center();
                Pi[n] = p - 0.5 * length * Q[n] * mu / (aperture * Kf);

                n++;
			}

            // The unity vector along the fracture segment
            // (Split operations because vector-scalar operators are not defined in densevector.hh)
            uv = vcoord[0] - vcoord[1];
            uv /= length;

            // Calculate the mass conservation error
            Scalar q = 0.5*(Q[0] - Q[1]);
            if (fabs(q) > 1e-6)
            {
				Scalar err = fabs((Q[0] + Q[1])/q);
				if (err > 1e-3)
				{
					std::cout << "fractureproblem.hh: mass conservation error is " << err << std::endl;
				}
            }

            // Calculate the directional derivative along the fracture segment
            Scalar dPidl = (Pi[0] - Pi[1]) / length;
            if (fabs(q + aperture*Kf/mu * dPidl) > 1e-6)
            {
            	std::cout << "fractureproblem.hh: error in flux calculation is " << fabs(q + aperture*Kf/mu * dPidl) << std::endl;
            }

            // The reconstructed pressure gradient
            auto DP = uv;
            DP *= dPidl;

            // The velocity vector along the fracture segment
            auto U = uv;
            U *= q;
            U /= aperture;

            // The integrals of the velocity and the pressure gradient over the fracture segment area
            U *= aperture;
            U *= length;
            DP *= aperture;
            DP *= length;

            // Accumulating the integrals of the pressure gradient and the velocity
            averPGrad += DP;
            averVel += U;
        }

//        // Calculate the average values
//		Scalar area = getArea();
//		averPGrad /= area;
//		averVel /= area;

    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
		BoundaryTypes values;

		// Dirichlet at the left and at the right
		values.setAllNeumann();
		if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
			values.setAllDirichlet();
		
		return values;		
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }    
    
     //! Set the aperture as extrusion factor.
	template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {        
        return aperture_;
    }    
    


    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;

		if (globalPos[dir_] < xmin + 1e-6)
			values = inletBoundaryPressure_;
		
		if (globalPos[dir_] > xmax - 1e-6)
			values = outletBoundaryPressure_;
		
        return values;
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);        
        return values;
    }
    
    

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
		values = initialPressure_;
        return values;            
    }
    
    
	//! Calculate the area of the fracture domain
	Scalar getArea()
    {        
		Scalar area = 0.;		
		// loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                area += scv.volume() * aperture_;
            }
		}
		return area;
	}
    
    

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }
    


private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;

	int dir_;
	Scalar initialPressure_;
	Scalar inletBoundaryPressure_;
	Scalar outletBoundaryPressure_;
    Scalar aperture_;

	Scalar xmin, xmax;
};

} // end namespace Dumux

#endif
    
    	










