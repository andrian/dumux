// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the matrix domain for the single-phase compressible flow in fractured porous media.
 */
#ifndef DUMUX_FRACTURES_MATRIX_PROBLEM_HH
#define DUMUX_FRACTURES_MATRIX_PROBLEM_HH

// we use alu grid for the discretization of the matrix domain
#include <dune/alugrid/grid.hh>

// we need this in this test in order to define the domain
// id of the fracture problem (see function interiorBoundaryTypes())
#include <dune/common/indices.hh>

// We are using the framework for models that consider coupling
// across the element facets of the bulk domain using a cell-centered 
// finite volume scheme with tpfa.
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>

// replace the standard immisible localresidual for one phase with the one with an analytic jacobian.
#include "model/compressiblelocalresidual.hh"

// include the base problem and the model we inherit from
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

// Slightly compressible single phase liquid with parameters initialized from .input file
#include <dumux/material/fluidsystems/1pliquid.hh>
#include "components/slightlycompressibleliquid.hh"

// the spatial parameters (permeabilities, material parameters etc.)
#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
NEW_PROP_TAG(GridData); // forward declaration of property

// create the type tag node for the matrix sub-problem
NEW_TYPE_TAG(MatrixProblemTypeTag, INHERITS_FROM(OneP, CCTpfaFacetCouplingModel));

// Set the grid type
SET_TYPE_PROP(MatrixProblemTypeTag, Grid, Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>);

// Set the problem type
SET_TYPE_PROP(MatrixProblemTypeTag, Problem, MatrixSubProblem<TypeTag>);

// the local residual containing the analytic derivative methods
//SET_TYPE_PROP(MatrixProblemTypeTag, LocalResidual, TwoPIncompressibleLocalResidual<TypeTag>);
// set the OneP Incompressible local residual for the OnePIncompressible type tag. This provides an analytic jacobian to be used for the analytic solution. Change that by setting:
SET_TYPE_PROP(MatrixProblemTypeTag, LocalResidual, OnePCompressibleLocalResidual<TypeTag>);

// set the spatial params
SET_TYPE_PROP(MatrixProblemTypeTag,
              SpatialParams,
              MatrixSpatialParams< typename GET_PROP_TYPE(TypeTag, FVGridGeometry),
                                   typename GET_PROP_TYPE(TypeTag, Scalar) >);


// Set the fluid system
SET_PROP(MatrixProblemTypeTag, FluidSystem)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
public:
    using type = FluidSystems::OnePLiquid<Scalar, Components::SlightlyCompressibleComponent<Scalar> >;
};

} // end namespace Properties


/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup OnePTests
 * \brief The sub-problem for the matrix domain 
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;
    using VolumeVariables = typename GET_PROP_TYPE(TypeTag, VolumeVariables);

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;	
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluxVariables = typename GET_PROP_TYPE(TypeTag, FluxVariables);

    // some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);    
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);    
    using ModelTraits = typename GET_PROP_TYPE(TypeTag, ModelTraits);

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "",
					 const int flowDirection = 0)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
	, initialPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InitialPressure"))
	, inletBoundaryPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.InletBoundaryPressure"))
	, outletBoundaryPressure_(getParamFromGroup<Scalar>(paramGroup, "Problem.OutletBoundaryPressure"))
	{

    	dir_ = flowDirection;
		
		// initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        //using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
        FluidSystem::init();		
	}
	
	//! Calculate the matrix average pressure and the boundary flux
    template<class Assembler, class MatrixIdType>
	void getResults(Scalar& averagePressure, Scalar& mfPressure, Scalar& Mass, Scalar& boundaryFlux, Scalar& volFMFlux, Scalar& massFMFlux, 
					const SolutionVector& curSol, const GridVariables& gridVariables, const Assembler& assembler,
					const MatrixIdType& matrixDomainId)
    {   

    	averagePressure = 0.;
		mfPressure = 0.;
		Mass = 0.;
		boundaryFlux = 0;
		Scalar area = 0.;
		volFMFlux = 0.;
		massFMFlux = 0.;
		int nmfElem = 0;	// Number of elements at matrix-fracture boundaries

		
		// loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);	

            VolumeVariables volVars;


            for (auto&& scv : scvs(fvGeometry))
            {
            	volVars = elemVolVars[scv];

                Scalar ef = volVars.extrusionFactor();

                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }
                
                averagePressure += volVars.pressure();
				Mass += scv.volume() * volVars.porosity() * volVars.extrusionFactor() * volVars.density(FluidSystem::phase0Idx);
                area += scv.volume();

            }

            // Loop through the faces 
            
            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(); };
            //auto upwindTerm = [](const auto& volVars) { return 1; };	// as in multidomain/facet/cellcentered/upwindscheme.hh apply() line 150

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto&& scvf : scvfs(fvGeometry)) {
				
            	// Calculate the boundary flux
				if (scvf.boundary()) {
                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    Scalar extrusionFactor = volVars.extrusionFactor();                    
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                        Scalar flux = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
                        boundaryFlux += flux;	// > 0 for outflow
					}
					else if (bcTypes.hasOnlyNeumann()) {
						Scalar flux = this->neumannAtPos(scvf.center());  
						flux *= scvf.area() * extrusionFactor / volVars.density(FluidSystem::phase0Idx);   // Convert to volumetric flux
						boundaryFlux += flux;
					}
					else {
                        DUNE_THROW(Dune::NotImplemented, "matrixproblem.hh: boundary condition not defined..");
                    }
				}
				
				// Calculate the matrix-fracture flux
				if (couplingManager().isOnInteriorBoundary(element, scvf))
				{

//					auto matrixDomainId = CouplingManager::bulkGridId;
//					auto fractureDomainId = CouplingManager::lowDimGridId;
					//couplingManager().bindCouplingContext(matrixDomainId, element, assembler);	// matrixDomainId

					// Use the non const getter for the coupling manager (compilation error otherwise)
					mycouplingManager().bindCouplingContext(matrixDomainId, element, assembler);	// matrixDomainId

					//mycouplingManager().mybindCouplingContext(element, assembler);	// matrixDomainId

					FluxVariables fluxVars;
					fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
					Scalar flux;

//					std::cout << "Face @ y=" << scvf.center()[1]
//											 << ", normal = [" << scvf.unitOuterNormal()[0] << ", " << scvf.unitOuterNormal()[1] << "]"
//											 << ", area=" << scvf.area();
					flux = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);

					//std::cout << ", flux=" << flux << std::endl;
					if (flux < 0) {
//						flux = 0.;
//						std::cout << "\nNegative flux = " << flux << std::endl;
//						std::cout << "Face @ y=" << scvf.center()[1]
//								  << ", normal = [" << scvf.unitOuterNormal()[0] << ", " << scvf.unitOuterNormal()[1] << "]"
//								  << ", area=" << scvf.area() << std::endl;
					}
					volFMFlux += flux;
					
					// Since the flow is from matrix to fractures, volVars are upwind
					massFMFlux += flux * volVars.density(FluidSystem::phase0Idx);
					
					mfPressure += volVars.pressure();
					nmfElem++;

				}


			}
			
			
		}	
		
        // Calculate the average pressure in the domain and among the fracture-adjacent elements
		auto meshSize = this->fvGridGeometry().gridView().size(0);
		averagePressure /= meshSize;
		mfPressure /= nmfElem;

	}


    //! Calculate the pressure gradients, averaged over matrix elements (a la Hamid)
    template<class Assembler, class MatrixIdType>
    void getAveragedSolution(GlobalPosition& averPGrad,
    			    const SolutionVector& curSol, const GridVariables& gridVariables, const Assembler& assembler,
					const MatrixIdType& matrixDomainId)
    {
    	averPGrad = 0;

    	// Get the number of vertices of the 0th matrix element (should be the same for all fracture elements)
        auto fvGeometry = localView(this->fvGridGeometry());
        auto e = fvGeometry.fvGridGeometry().element(0);
        int Nv = e.geometry().corners();
        assert(Nv == 3);

    	Scalar p;							// Cell-centered pressure
    	std::vector<Scalar> Pi;				// Face pressures
    	std::vector<Scalar> Q;				// Segment's endpoints volumetric fluxes
    	std::vector<GlobalPosition> vcoord;	// Coordinates of the endpoints
    	Pi.resize(Nv);
    	Q.resize(Nv);
    	vcoord.resize(Nv);

    	Scalar mu = FluidSystem::viscosity(0., 0.);

        // loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
        	fvGeometry.bind(element);
            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, curSol);
            VolumeVariables volVars;

            GlobalPosition cElem, cFace;
            Scalar ef;
            Scalar Km;
            for (auto&& scv : scvs(fvGeometry))
            {
                volVars = elemVolVars[scv];
                p = volVars.pressure();
                cElem = scv.center();
                ef = volVars.extrusionFactor();
                Km = this->spatialParams().permeabilityAtPos(cElem);
                // Assume that extrusionFactor() == 1 for the matrix in order to get the correct volume of the fracture segment
                if (ef != 1) {
                	std::cout << "matrixproblem.hh: Extrusion factor = " << ef << std::endl;
                }
            }

            // Loop through the faces

            // the upwind term to be used for the volume flux evaluation
            auto upwindTerm = [](const auto& volVars) { return volVars.mobility(); };

            auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            //GlobalPosition intDP(0);
            int n = 0;
            for (auto&& scvf : scvfs(fvGeometry)) {

                FluxVariables fluxVars;
                fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                if (scvf.boundary())
                {
                    auto bcTypes = this->boundaryTypesAtPos(scvf.ipGlobal());
                    if (bcTypes.hasOnlyDirichlet()) {
                    	Q[n] = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
					}
					else if (bcTypes.hasOnlyNeumann()) {
						Q[n] = this->neumannAtPos(scvf.center());
						Q[n] *= scvf.area() * ef / volVars.density(FluidSystem::phase0Idx);   // Convert to volumetric flux
					}
					else {
                        DUNE_THROW(Dune::NotImplemented, "matrixproblem.hh: boundary condition not defined..");
                    }
                }
                else
                {
    				if (couplingManager().isOnInteriorBoundary(element, scvf))
    				{
    					mycouplingManager().bindCouplingContext(matrixDomainId, element, assembler);
    					fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
    				}

                	Q[n] = fluxVars.advectiveFlux(FluidSystem::phase0Idx, upwindTerm);
                }

                cFace = scvf.center();
                Scalar cn = (cFace - cElem) * scvf.unitOuterNormal();
                Scalar T = scvf.area() * Km / mu * cn / ((cFace - cElem) *(cFace - cElem));
                Pi[n] = p -  Q[n] / T;

                GlobalPosition intDP = scvf.unitOuterNormal();
                intDP *= scvf.area();
                intDP *= Pi[n];

                // Accumulating the integrals of the pressure gradient
                averPGrad += intDP;

                n++;
			}

            // Accumulating the integrals of the pressure gradient
            //averPGrad += intDP;

        }

//        // Calculate the average values
//		Scalar area = getArea();
//		averPGrad /= area;
//		averVel /= area;

    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {

    	// debug
    	Scalar Kf = this->spatialParams().permeabilityAtPos(globalPos);



    	BoundaryTypes values;

        Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];

		// Dirichlet at the left and at the right
		values.setAllNeumann();
		if (globalPos[dir_] < xmin + 1e-6 || globalPos[dir_] > xmax - 1e-6)
			values.setAllDirichlet();
		
		return values;		
		
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // Here we set the type of condition to be used on faces that coincide
        // with a fracture. If Neumann is specified, a flux continuity condition
        // on the basis of the normal fracture permeability is evaluated. If this
        // permeability is lower than that of the matrix, this approach is able to
        // represent the resulting pressure jump across the fracture. If Dirichlet is set,
        // the pressure jump across the fracture is neglected and the pressure inside
        // the fracture is directly applied at the interface between fracture and matrix.
        // This assumption is justified for highly-permeable fractures, but lead to erroneous
        // results for low-permeable fractures.
        // Here, we consider "open" fractures for which we cannot define a normal permeability
        // and for which the pressure jump across the fracture is neglectable. Thus, we set
        // the interior boundary conditions to Dirichlet.
        // IMPORTANT: Note that you will never be asked to set any values at the interior boundaries!
        //            This simply chooses a different interface condition!
        values.setAllDirichlet();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;   

        Scalar xmin = this->fvGridGeometry().bBoxMin()[dir_];
        Scalar xmax = this->fvGridGeometry().bBoxMax()[dir_];
		
		if (globalPos[dir_] < xmin + 1e-6)
			values = inletBoundaryPressure_;
		
		if (globalPos[dir_] > xmax - 1e-6)
			values = outletBoundaryPressure_;
		
        return values;        
    }
    
    
    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);        

        return values;
    }
       
    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
		values = initialPressure_;
        return values;            
    }
    
	//! Calculate the area of the matrix domain
	Scalar getArea()
    {        
		Scalar area = 0.;		
		// loop through the elements
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bind(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                area += scv.volume();
            }
		}
		return area;
	}

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }


    //! returns reference to the coupling manager.
    CouplingManager& mycouplingManager()
    { return *couplingManagerPtr_; }



private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
	
	int dir_;
	Scalar initialPressure_;
	Scalar inletBoundaryPressure_;
	Scalar outletBoundaryPressure_;

};

} // end namespace Dumux

#endif
    
    	










