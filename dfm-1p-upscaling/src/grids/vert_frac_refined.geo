// Gmsh geometry specification for the vertical fracture in a box example (Noetinger et al. (TPM 2001))

// Density of mesh near Points
lc = 0.1; 
lcf = 0.01;	// Near fracture


// Size of the square box 
Lm = 2;

// Vertical position of the fracture
Lf = 1;

// Geometrical entities = 1d and 2D regions indices
// #2D_REGIONS
MATRIX = 666;

// #1D_REGIONS
OPEN_FRACTURES = 777;


// Outer box
Point(1) = {0,  0, 0, lc};
Point(2) = {Lm, 0, 0, lc};
Point(3) = {Lm, Lm, 0, lc};
Point(4) = {0,  Lm, 0, lc};

// Fractures' endpoints
Point(5) = {Lf, Lm, 0, lcf};
Point(6) = {Lf, 0, 0, lcf};


// Define the bounded domains
// NOTE: If a Line contains > 2 Points, then the triangulation nodes can NOT include the interior Points!
Line(500) = {1, 6};
Line(501) = {6, 2};
Line(502) = {2, 3};
Line(503) = {3, 5};
Line(504) = {5, 4};
Line(505) = {4, 1};

// Additional points along the fracture to get the refinement
Line(506) = {5, 6};
Point(10) = {Lf, Lm/4, lcf};
Point(11) = {Lf, 2*Lm/4, lcf};
Point(12) = {Lf, 3*Lm/4, lcf};



Line Loop(600) = {500, 501, 502, 503, 504, 505}; 
Plane Surface(600) = {600}; 

// Including fractures in triangulation
Line{506} In Surface {600};


// Physical entities
// Fracture appears in the msh file as a set of line elements 
// (the 2nd parameter in the $Elements block elm-type is =1)
// with the 4th parameter = number of Physical Line
// and the 5th = number of corresponding Line
// Physical Line(LEFT) = {503};	
// Physical Line(RIGHT) = {501};	
// Physical Line(TOP) = {502};
// Physical Line(BOTTOM) = {500};


// Physical entities
Physical Line(OPEN_FRACTURES) = {506};

// Triangulated domain
// The triangular $Elements (the 2nd parameter in the $Elements block elm-type is =2)
// have the the 4th parameter = number of Physical Surface
// and the 5th = number of corresponding Plane Surface
Physical Surface(MATRIX) = {600}; 
