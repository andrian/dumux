# Discrete Fracture-Matrix single phase model

This project contains the DuMuX module for the Discrete Fracture-Matrix (DFM) single-phase model[^fn1], used to calculate the tensor of upscaled permeabilities. The code is based on the DFM module[^fn2] with a modified fluid model, boundary conditions, and I/O.


## Prerequisites

Install the self-contained version of [DuMuX](0) as described [here](../README.md).


## Installation

Compile the model
```bash
cd dfm-1p-upscaling/build-cmake-O3/src/
make dfm-1p-upscaling
```


## References
[^fn1]: N. Andrianov and H. M. Nick, Towards a nonlinear transfer function in dual porosity models, submitted, 2020.
[^fn2]: D. Gläser, R. Helmig, B. Flemisch, and H. Class, A discrete fracture model for two-phase flow in fractured porous media, Advances in Water Resources 110 (2017) 335–348.
[0]: https://dumux.org
